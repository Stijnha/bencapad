USE OneSumX; 
 
SET NOCOUNT ON;
  
EXEC p_dec_create_configuration_ps
	@fda_table = N't_deal_type',
	@data_fields = N'description, origin_code,char_cust_element1,char_cust_element2',
	@type_name = N'Deal Type',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
	@recreate_validation = 0;
	

EXEC p_dec_create_configuration_ps
	@fda_table = N't_deal_subtype',
	@data_fields = N'description, origin_code,char_cust_element1,char_cust_element2,char_cust_element3,char_cust_element4,char_cust_element5',
	@type_name = N'Deal Subtype',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
	@recreate_validation = 0;

EXEC p_dec_create_configuration_ps
	@fda_table = N't_deal_type_subtype_link',
	@data_fields = NULL,--N'description,source_system_type,source_system_subtype,origin_code,char_cust_element1',
	@type_name = N'Deal Type Subtype Link',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
	@recreate_validation = 0;

EXEC p_dec_create_configuration_ps
	@fda_table = N't_amount_type',
	@data_fields = N'amount_type_name,amount_type_desc,char_cust_element1',
	@type_name = N'Cashflow types',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_amount_class',
	@data_fields = N'description,allow_imbalanced_journals_ccy,allow_imbalanced_journals_lcl,is_allowed_for_bulk_upload',
	@type_name = N'Amount Class',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_account_code',
	@data_fields = N'end_validity_date,description,account_section,title_only_ind,division,allow_manual_entry,char_cust_element1,char_cust_element2,char_cust_element3,char_cust_element4,char_cust_element5,num_cust_element1,num_cust_element2,num_cust_element3,num_cust_element4,num_cust_element5',
	@type_name = N'Account Code',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_account_section',
	@data_fields = N'account_section_name,is_profit_and_loss',
	@type_name = N'Account Section',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_account_category',
	@data_fields = N'description',
	@type_name = N'Account Category',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_layers',
	@data_fields = N'layer,description,active',
	@type_name = N'Financial Layer Setup',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_account_groups',
	@data_fields = N'group_name,description,active',
	@type_name = N'Account Group',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_account_pools',
	@data_fields = N'pool,description,layer_id,criterion,active',
	@type_name = N'Account Pool',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_account_pools_link',
	@data_fields = N'pool_id,group_id',
	@type_name = N'Account Pool Link',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_domain',
	@data_fields = N'domain_name,domain_desc,is_rolled_to_next_period,period_roll_to_domain_id,is_rolled_to_next_year,year_roll_to_domain_id',
	@type_name = N'Domains Set Up',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_calendar',
	@data_fields = N'description,calendar_start_date,calendar_end_date,is_valid,is_monday_workday,is_tuesday_workday,is_wednesday_workday,is_thursday_workday,is_friday_workday,is_saturday_workday,is_sunday_workday',
	@type_name = N'Calendar',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_calendar_date',
	@data_fields = N'is_working_day',
	@type_name = N'Calendar Date',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_calendar_period',
	@data_fields = N'period_start_date,period_end_date,description,last_locked_date',
	@type_name = N'Calendar Periods',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_profit_centre',
	@data_fields = N'description,division,capitalized_ind,active_ind,type',
	@type_name = N'Profit Centre',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_currency_code',
	@data_fields = N'end_validity_date,description,number_of_dec_places,emu_member_ind,year_basis,active_until_date',
	@type_name = N'Currency Codes',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_currency_rate_type',
	@data_fields = N'rate_desc,rate_life,rate_active',
	@type_name = N'Currency Rate Type',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_book_code',
	@data_fields = N'description,is_trading_book,num_cust_element1, division, char_cust_element1, char_cust_element2',
	@type_name = N'Book Codes',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 
 
	EXEC p_dec_create_configuration_ps
	@fda_table = N't_risk_rating',
	@data_fields = N'description,is_rating_unrated,quality_ascending_sequence,rating_rank',
	@type_name = N'Risk Rating',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

	EXEC p_dec_create_configuration_ps
	@fda_table = N't_risk_rating_type',
	@data_fields = N'description,short_term_rating_ind,investment_grade_min_rating',
	@type_name = N'Risk Rating Type',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

	EXEC p_dec_create_configuration_ps
	@fda_table = N't_trader',
	@data_fields = N'trader_name, division',
	@type_name = N'Trader',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

    EXEC p_dec_create_configuration_ps
	@fda_table = N't_property_name',
	@data_fields = N'property_desc',
	@type_name = N'Properties',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
    @recreate_validation=0; 

EXEC p_dec_create_configuration_ps
	@fda_table = N't_valuation_type',
	@data_fields = NULL,
	@type_name = N'Valuation Types',
	@debug = 0,
	@execute_immediately = 1,
	@database_name = NULL,
	@recreate_validation = 0;

