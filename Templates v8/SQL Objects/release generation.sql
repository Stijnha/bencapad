USE OneSumX_SHA;
GO

SET NOCOUNT ON;

DECLARE @destination_server NVARCHAR(MAX) = N'STVSQLN02.sit.qtc.com.au',
		@destination_portal NVARCHAR(MAX) = N'C:\inetpub\wwwroot\qtc_portal_sit',
		@backup_path NVARCHAR(MAX) = N'H:\Backups_for_release_do_not_delete\',
		@ssrs_serverpath NVARCHAR(MAX) = N'http://stvsqlrs01/Reportserver',
		@deploy_path NVARCHAR(MAX) = N'C:\WKFS\Deploy\', --N'C:\WKREL\',
		@release_number NVARCHAR(200) = N'wki01c01m02s33', --N'I01C01M01S00',
		@environment NVARCHAR(200) = N'sit',
		@datasource_filename NVARCHAR(200) = N'onesumxdb_qtc.rds',
		@app_server NVARCHAR(200) = N'10.50.5.19';



DECLARE @bat_file_name NVARCHAR(500),
        @backup_file NVARCHAR(500),
		@backup_path2 NVARCHAR(2000),
		@out_file_name NVARCHAR(500),
		@solution SYSNAME,
		@destination_folder NVARCHAR(2000),
		@source_folder NVARCHAR(2000),
		@source_path NVARCHAR(MAX),
		@bat_file_folder NVARCHAR(2000),
		@datasource_folder NVARCHAR(MAX),
		@source_folder2 NVARCHAR(256),
		@deploy_path2 NVARCHAR(2000);

SELECT @source_path = @deploy_path + @release_number + N'\source code\',
       @bat_file_folder = @deploy_path + @release_number + CASE @environment WHEN N'dev' THEN N'\0 DEV\bat\'
	                                                                         WHEN N'sit' THEN N'\1 SIT\bat\' 
																			 WHEN N'uat' THEN N'\2 UAT\bat\'	   
	   END,
	   @datasource_folder = @deploy_path + @release_number + N'\source code\08 SSRS reports\SSRS\';
--SELECT @deploy_path;
SET @deploy_path2 = @deploy_path + CASE WHEN @release_number <> N'' THEN @release_number + N'\' ELSE N'' END;
--SELECT @deploy_path2

IF (SELECT OBJECT_ID(N't_temp_commands_to_be_executed')) IS NOT NULL
BEGIN
    DROP TABLE t_temp_commands_to_be_executed
END

CREATE TABLE t_temp_commands_to_be_executed
(
    lvl INT NULL,
	cmd NVARCHAR(MAX) NULL
);

CREATE TABLE #files2
(
    id              INT IDENTITY(1, 1),
    subdirectory    sysname,
    parent_id       INT NULL,
    depth           INT,
    is_file         BIT
);

-- 0. Stop all services
SELECT @out_file_name = @deploy_path2 + N'bat\0_kill_processes_' + @release_number + '.log';

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 0,
       cmd = ''
UNION     
SELECT lvl = 1,
       cmd = 'echo %date% %time% > ' + @out_file_name;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 2,
       N'sc \\' + @app_server + N' STOP qtc_FinStudio.TaskRunner'

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 3,
       N'echo start script stop_agent.ps1>>' + @out_file_name

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 4,
       N'Powershell.exe -executionpolicy remotesigned -File "' + @deploy_path2 + N'bat\stop_agent.ps1" >>' + @out_file_name 

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 5,
       N'echo end script stop_agent.ps1>>' + @out_file_name

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 100,
       N'sqlcmd -S "' + @destination_server + N'" -i "' + @deploy_path2 + N'bat\kill_processes.sql" -E -b -r1 2>>' + @out_file_name

SELECT @bat_file_name = @bat_file_folder + N'0_kill_processes_' + @release_number + N'.bat';

EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0

DELETE FROM t_temp_commands_to_be_executed;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 1,
       N'
USE master;
GO

SET NOCOUNT ON;

DECLARE @db nvarchar(128)
DECLARE @mode nvarchar(128)
DECLARE @sql nvarchar(200)

SET @db = ''onesumxdb_qtc''

exec master..dp_killdbusers @db

SET @sql = ''ALTER DATABASE ['' + @db + ''] SET RESTRICTED_USER WITH NO_WAIT''
EXECUTE( @sql )

SELECT @mode = user_access_desc FROM sys.databases WHERE name = @db
IF ( @mode = ''RESTRICTED_USER'' )
	BEGIN
	PRINT ''Database mode set to RESTRICTED_USER''
	END
ELSE
	BEGIN
	PRINT ''ERROR - database mode = '' + @mode + '' not restricted''
	END';

SELECT @bat_file_name = @bat_file_folder + N'kill_processes.sql';
EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0

DELETE FROM t_temp_commands_to_be_executed;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 1,
N'$agentsrvc = get-service -name sqlserveragent -ComputerName STVSQLN02
if ( $agentsrvc.status -eq "Stopped" )
    { "SQL Server Agent already stopped" }
    else
    {
    $agentsrvc.Stop()
    $agentsrvc.WaitForStatus( ''Stopped'', ''00:00:20'' )     
    if ( $agentsrvc.status -ne ''Stopped'' )
        {
        "SQL Agent not stopped in time. Status is $agentsrvc.Status"
        exit( 1 )
        }
        else
        { "SQL Agent successfully stopped" }
    }
';

SELECT @bat_file_name = @bat_file_folder + N'stop_agent.ps1';
EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0

DELETE FROM t_temp_commands_to_be_executed;

-- 1. Backup databases
SELECT @out_file_name = @deploy_path2 + N'bat\1_backup_databases_' + @release_number + '.log';

-- 1.1. onesumxdb_qtc
SELECT @backup_file = N'onesumxdb_qtc_' + @release_number + '.bak';
SELECT @backup_path2 = @backup_path + @backup_file;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 0,
       cmd = ''
UNION     
SELECT lvl = 1,
       cmd = 'echo %date% %time% > ' + @out_file_name;


INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 100,
       N'sqlcmd -S "' + @destination_server + N'" -Q "BACKUP DATABASE [onesumxdb_qtc] TO  DISK = ''' + @backup_path2 + ''' WITH NOFORMAT, INIT, COPY_ONLY, NAME = N''onesumxdb_qtc-Full Database Backup'', SKIP, NOREWIND, NOUNLOAD,  STATS = 10;" -E -b -r1 2>>' + @out_file_name

-- 1.2. staging
SELECT @backup_file = N'staging_' + @release_number + '.bak';
SELECT @backup_path2 = @backup_path + @backup_file;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 200,
       N'sqlcmd -S "' + @destination_server + N'" -Q "IF DB_ID(''staging'') IS NOT NULL BEGIN BACKUP DATABASE [staging] TO  DISK = ''' + @backup_path2 + ''' WITH NOFORMAT, INIT, COPY_ONLY, NAME = N''staging-Full Database Backup'', SKIP, NOREWIND, NOUNLOAD,  STATS = 10 END;" -E -b -r1 2>>' + @out_file_name

-- 1.3. tng
SELECT @backup_file = N'tng_' + @release_number + '.bak';
SELECT @backup_path2 = @backup_path + @backup_file;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 300,
       N'sqlcmd -S "' + @destination_server + N'" -Q "BACKUP DATABASE [tng] TO  DISK = ''' + @backup_path2 + ''' WITH NOFORMAT, INIT, COPY_ONLY, NAME = N''tng-Full Database Backup'', SKIP, NOREWIND, NOUNLOAD,  STATS = 10;" -E -b -r1 2>>' + @out_file_name

-- 1.4. SSISDB
SELECT @backup_file = N'SSISDB_' + @release_number + '.bak';
SELECT @backup_path2 = @backup_path + @backup_file;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 400,
       N'sqlcmd -S "' + @destination_server + N'" -Q "IF DB_ID(''SSISDB'') IS NOT NULL BEGIN BACKUP DATABASE [SSISDB] TO  DISK = ''' + @backup_path2 + ''' WITH NOFORMAT, INIT, COPY_ONLY, NAME = N''SSISDB-Full Database Backup'', SKIP, NOREWIND, NOUNLOAD,  STATS = 10 END;" -E -b -r1 2>>' + @out_file_name

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 1999 AS lvl, 'goto :EOF' AS cmd
UNION
SELECT 2000, ':error'
UNION ALL
SELECT 2001, 'echo ERROR'
UNION ALL
SELECT 2002, 'exit /b';

SELECT @bat_file_name = @bat_file_folder + N'1_backup_databases_' + @release_number + N'.bat';
EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0

DELETE FROM t_temp_commands_to_be_executed;
---------------------------------------------------------------
-- 2. SSIS Package for ETL
INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 0,
       cmd = '$ServerName ="' + @destination_server + '"'
UNION ALL
SELECT lvl = 1,
       cmd = '$SSISCatalog = "SSISDB"'
UNION ALL
SELECT lvl = 2,
       cmd = '$CatalogPwd = "P@ssw0rd1"'
UNION ALL
SELECT lvl = 3,
       cmd = '$ProjectFilePath = "' + @deploy_path2 + N'07 SSIS packages\QTC ETL SSIS\QTC_ETL_SSIS\bin\Development\QTC_ETL_SSIS.ispac"' 
UNION ALL
SELECT lvl = 4,
       cmd = '$ProjectName = "QTC_ETL_SSIS"'
UNION ALL
SELECT lvl = 5,
       cmd = '$FolderName = "ETL"'
UNION ALL
SELECT lvl = 6,
       cmd = '$EnvironmentName = "ETL"'
UNION ALL
SELECT lvl = 7,
       cmd = ''
UNION ALL
SELECT lvl = 8,
       cmd = '# Load the IntegrationServices Assembly'
UNION ALL
SELECT lvl = 9,
       cmd = '[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Management.IntegrationServices")'
UNION ALL
SELECT lvl = 10,
       cmd = ''
UNION ALL
SELECT lvl = 11,
       cmd = '# Store the IntegrationServices Assembly namespace to avoid typing it every time'
UNION ALL
SELECT lvl = 12,
       cmd = '$ISNamespace = "Microsoft.SqlServer.Management.IntegrationServices"'
UNION ALL
SELECT lvl = 13,
       cmd = ''
UNION ALL
SELECT lvl = 14,
       cmd = 'Write-Host "Connecting to server ..."'
UNION ALL
SELECT lvl = 15,
       cmd = ''
UNION ALL
SELECT lvl = 16,
       cmd = '# Create a connection to the server'
UNION ALL
SELECT lvl = 17,
       cmd = '$sqlConnectionString = "Data Source=$ServerName;Initial Catalog=master;Integrated Security=SSPI;"'
UNION ALL
SELECT lvl = 18,
       cmd = '$sqlConnection = New-Object System.Data.SqlClient.SqlConnection $sqlConnectionString'
UNION ALL
SELECT lvl = 19,
       cmd = '$connection = "Data Source=$ServerName;Initial Catalog=onesumxdb_qtc;Integrated Security=True;Application Name=SSIS-QTC_ETL_SSIS-{2E1D69F9-1D9A-4557-9A32-0D0CB2DC524C}$ServerName.onesumxdb_qtc;"'
UNION ALL
SELECT lvl = 20,
       cmd = 'Write-Host $connection'
UNION ALL
SELECT lvl = 21,
       cmd = '$integrationServices = New-Object "$ISNamespace.IntegrationServices" $sqlConnection'
UNION ALL
SELECT lvl = 22,
       cmd = ''
UNION ALL
SELECT lvl = 23,
       cmd = '$catalog = $integrationServices.Catalogs[$SSISCatalog]'
UNION ALL
SELECT lvl = 24,
       cmd = ''
UNION ALL
SELECT lvl = 25,
       cmd = '# Create the Integration Services object if it does not exist'
UNION ALL
SELECT lvl = 26,
       cmd = 'if (!$catalog) {'
UNION ALL
SELECT lvl = 27,
       cmd = '    # Provision a new SSIS Catalog'
UNION ALL
SELECT lvl = 28,
       cmd = '    Write-Host "Creating SSIS Catalog ..."'
UNION ALL
SELECT lvl = 29,
       cmd = '    $catalog = New-Object "$ISNamespace.Catalog" ($integrationServices, $SSISCatalog, $CatalogPwd)'
UNION ALL
SELECT lvl = 30,
       cmd = '    $catalog.Create()'
UNION ALL
SELECT lvl = 31,
       cmd = '}'
UNION ALL
SELECT lvl = 32,
       cmd = ''
UNION ALL
SELECT lvl = 33,
       cmd = '$folder = $catalog.Folders[$FolderName]'
UNION ALL
SELECT lvl = 34,
       cmd = ''
UNION ALL
SELECT lvl = 35,
       cmd = 'if (!$folder) '
UNION ALL
SELECT lvl = 36,
       cmd = '{'
UNION ALL
SELECT lvl = 37,
       cmd = '    #Create a folder in SSISDB'
UNION ALL
SELECT lvl = 38,
       cmd = '    Write-Host "Creating Folder ..."'
UNION ALL
SELECT lvl = 39,
       cmd = '    $folder = New-Object "$ISNamespace.CatalogFolder" ($catalog, $FolderName, $FolderName)        '
UNION ALL
SELECT lvl = 40,
       cmd = '    $folder.Create()  '
UNION ALL
SELECT lvl = 41,
       cmd = '}'
UNION ALL
SELECT lvl = 42,
       cmd = ''
UNION ALL
SELECT lvl = 43,
       cmd = '# Read the project file, and deploy it to the folder'
UNION ALL
SELECT lvl = 44,
       cmd = 'Write-Host "Deploying Project ..."'
UNION ALL
SELECT lvl = 45,
       cmd = '[byte[]] $projectFile = [System.IO.File]::ReadAllBytes($ProjectFilePath)'
UNION ALL
SELECT lvl = 46,
       cmd = '$folder.DeployProject($ProjectName, $projectFile)'
UNION ALL
SELECT lvl = 47,
       cmd = ''
UNION ALL
SELECT lvl = 48,
       cmd = '$environment = $folder.Environments[$EnvironmentName]'
UNION ALL
SELECT lvl = 49,
       cmd = ''
UNION ALL
SELECT lvl = 50,
       cmd = 'if (!$environment)'
UNION ALL
SELECT lvl = 51,
       cmd = '{'
UNION ALL
SELECT lvl = 52,
       cmd = '    Write-Host "Creating environment ..." '
UNION ALL
SELECT lvl = 53,
       cmd = '    $environment = New-Object "$ISNamespace.EnvironmentInfo" ($folder, $EnvironmentName, $EnvironmentName)'
UNION ALL
SELECT lvl = 54,
       cmd = '    $environment.Create()     '
UNION ALL
SELECT lvl = 55,
       cmd = '}'
UNION ALL
SELECT lvl = 56,
       cmd = ''
UNION ALL
SELECT lvl = 57,
       cmd = '$project = $folder.Projects[$ProjectName]'
UNION ALL
SELECT lvl = 58,
       cmd = '$ref = $project.References[$EnvironmentName, $folder.Name]'
UNION ALL
SELECT lvl = 60,
       cmd = ''
UNION ALL
SELECT lvl = 61,
       cmd = 'if (!$ref)'
UNION ALL
SELECT lvl = 62,
       cmd = '{'
UNION ALL
SELECT lvl = 63,
       cmd = '    # making project refer to this environment'
UNION ALL
SELECT lvl = 64,
       cmd = '    Write-Host "Adding environment reference to project ..."'
UNION ALL
SELECT lvl = 65,
       cmd = '    $project.References.Add($EnvironmentName, $folder.Name)'
UNION ALL
SELECT lvl = 66,
       cmd = '    $project.Alter() '
UNION ALL
SELECT lvl = 67,
       cmd = '}'
UNION ALL
SELECT lvl = 68,
       cmd = ''
UNION ALL
SELECT lvl = 69,
       cmd = '# Adding variable to our environment'
UNION ALL
SELECT lvl = 70,
       cmd = '# Constructor args: variable name, type, default value, sensitivity, description'
UNION ALL
SELECT lvl = 71,
       cmd = '$connection_string = $environment.Variables["connection_string"];'
UNION ALL
SELECT lvl = 72,
       cmd = ''
UNION ALL
SELECT lvl = 73,
       cmd = 'if (!$connection_string)'
UNION ALL
SELECT lvl = 74,
       cmd = '{'
UNION ALL
SELECT lvl = 75,
       cmd = '    Write-Host "Adding environment variables ..."'
UNION ALL
SELECT lvl = 76,
       cmd = '    $environment.Variables.Add('
UNION ALL
SELECT lvl = 77,
       cmd = '        �connection_string�, '
UNION ALL
SELECT lvl = 78,
       cmd = '        [System.TypeCode]::String, $connection, $false, �connection_string�)'
UNION ALL
SELECT lvl = 79,
       cmd = '    $environment.Alter()'
UNION ALL
SELECT lvl = 80,
       cmd = '    $connection_string = $environment.Variables["connection_string"];'
UNION ALL
SELECT lvl = 81,
       cmd = '}'
UNION ALL
SELECT lvl = 82,
       cmd = ''
UNION ALL
SELECT lvl = 83,
       cmd = '$project.Parameters["connection_string"].Set('
UNION ALL
SELECT lvl = 84,
       cmd = '    [Microsoft.SqlServer.Management.IntegrationServices.ParameterInfo+ParameterValueType]::Referenced, '
UNION ALL
SELECT lvl = 85,
       cmd = '    $connection_string.Name)'
UNION ALL
SELECT lvl = 86,
       cmd = '$project.Alter()  ';

DELETE FROM #files2;
SELECT @source_folder2 = @source_path + N'07 SSIS packages\QTC ETL SSIS\';
INSERT INTO #files2 (subdirectory, depth, is_file)
EXEC xp_dirtree @source_folder2, 0, 1;

IF EXISTS (SELECT 1 FROM #files2)
BEGIN
  SELECT @bat_file_name = @bat_file_folder + N'create_ssisdb.ps1';
  EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0
END;

DELETE FROM t_temp_commands_to_be_executed;

SELECT @out_file_name = @deploy_path2 + N'bat\2_1_SSIS_' + @release_number + '.log';

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 0,
       cmd = ''
UNION ALL
SELECT lvl = 1,
       cmd = 'echo %date% %time% > ' + @out_file_name
UNION ALL
SELECT lvl = 2,
       cmd = 'echo start script 001 create_ssisdb.ps1>> ' + @out_file_name
UNION ALL
SELECT lvl = 3,
       cmd = 'Powershell.exe -executionpolicy remotesigned -File "' + @deploy_path2 + N'bat\create_ssisdb.ps1" >> ' + @out_file_name
UNION ALL
SELECT lvl = 4,
       cmd = 'echo end script 001 create_ssisdb.ps1>> ' + @out_file_name
UNION ALL
SELECT lvl = 1999,
       cmd = 'goto :EOF'
UNION ALL
SELECT lvl = 2000,
       cmd = ':error'
UNION ALL
SELECT lvl = 2001,
       cmd = 'echo ERROR'
UNION ALL
SELECT lvl = 2002,
       cmd = 'exit /b';

--SELECT *
--  FROM t_temp_commands_to_be_executed
IF EXISTS (SELECT 1 FROM #files2)
BEGIN

	SELECT @bat_file_name = @bat_file_folder + N'2_1_SSIS_' + @release_number + '.bat';
	EXEC p_write_resultset
						 @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
						 @filename               = @bat_file_name,
						 @write_mode             = N'W',
						 @null_value             = N'<NULL>',
						 @add_record_count_ind   = 0,
						 @verbosity_lvl          = 0
END;

DELETE FROM t_temp_commands_to_be_executed;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 0,
       cmd = '$ServerName ="' + @destination_server + '"'
UNION ALL
SELECT lvl = 1,
       cmd = '$SSISCatalog = "SSISDB"'
UNION ALL
SELECT lvl = 2,
       cmd = '$CatalogPwd = "P@ssw0rd1"'
UNION ALL
SELECT lvl = 3,
       cmd = '$ProjectFilePath = "' + @deploy_path2 + N'07 SSIS packages\QTC ELO REPORTS\Extract Reports\bin\Development\Extract Reports.ispac"' 
UNION ALL
SELECT lvl = 4,
       cmd = '$ProjectName = "Extract Reports"'
UNION ALL
SELECT lvl = 5,
       cmd = '$FolderName = "ELO_REPORTS"'
UNION ALL
SELECT lvl = 6,
       cmd = '$EnvironmentName = "ETL"'
UNION ALL
SELECT lvl = 7,
       cmd = ''
UNION ALL
SELECT lvl = 8,
       cmd = '# Load the IntegrationServices Assembly'
UNION ALL
SELECT lvl = 9,
       cmd = '[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Management.IntegrationServices")'
UNION ALL
SELECT lvl = 10,
       cmd = ''
UNION ALL
SELECT lvl = 11,
       cmd = '# Store the IntegrationServices Assembly namespace to avoid typing it every time'
UNION ALL
SELECT lvl = 12,
       cmd = '$ISNamespace = "Microsoft.SqlServer.Management.IntegrationServices"'
UNION ALL
SELECT lvl = 13,
       cmd = ''
UNION ALL
SELECT lvl = 14,
       cmd = 'Write-Host "Connecting to server ..."'
UNION ALL
SELECT lvl = 15,
       cmd = ''
UNION ALL
SELECT lvl = 16,
       cmd = '# Create a connection to the server'
UNION ALL
SELECT lvl = 17,
       cmd = '$sqlConnectionString = "Data Source=$ServerName;Initial Catalog=master;Integrated Security=SSPI;"'
UNION ALL
SELECT lvl = 18,
       cmd = '$sqlConnection = New-Object System.Data.SqlClient.SqlConnection $sqlConnectionString'
UNION ALL
SELECT lvl = 19,
       cmd = '$connection = "Data Source=$ServerName;Initial Catalog=onesumxdb_qtc;Integrated Security=True;Application Name=SSIS-Extract Reports-{A8EC343C-DA35-4899-9047-7601A61EE175}$ServerName.onesumxdb_qtc;"'
UNION ALL
SELECT lvl = 20,
       cmd = 'Write-Host $connection'
UNION ALL
SELECT lvl = 21,
       cmd = '$integrationServices = New-Object "$ISNamespace.IntegrationServices" $sqlConnection'
UNION ALL
SELECT lvl = 22,
       cmd = ''
UNION ALL
SELECT lvl = 23,
       cmd = '$catalog = $integrationServices.Catalogs[$SSISCatalog]'
UNION ALL
SELECT lvl = 24,
       cmd = ''
UNION ALL
SELECT lvl = 25,
       cmd = '# Create the Integration Services object if it does not exist'
UNION ALL
SELECT lvl = 26,
       cmd = 'if (!$catalog) {'
UNION ALL
SELECT lvl = 27,
       cmd = '    # Provision a new SSIS Catalog'
UNION ALL
SELECT lvl = 28,
       cmd = '    Write-Host "Creating SSIS Catalog ..."'
UNION ALL
SELECT lvl = 29,
       cmd = '    $catalog = New-Object "$ISNamespace.Catalog" ($integrationServices, $SSISCatalog, $CatalogPwd)'
UNION ALL
SELECT lvl = 30,
       cmd = '    $catalog.Create()'
UNION ALL
SELECT lvl = 31,
       cmd = '}'
UNION ALL
SELECT lvl = 32,
       cmd = ''
UNION ALL
SELECT lvl = 33,
       cmd = '$folder = $catalog.Folders[$FolderName]'
UNION ALL
SELECT lvl = 34,
       cmd = ''
UNION ALL
SELECT lvl = 35,
       cmd = 'if (!$folder) '
UNION ALL
SELECT lvl = 36,
       cmd = '{'
UNION ALL
SELECT lvl = 37,
       cmd = '    #Create a folder in SSISDB'
UNION ALL
SELECT lvl = 38,
       cmd = '    Write-Host "Creating Folder ..."'
UNION ALL
SELECT lvl = 39,
       cmd = '    $folder = New-Object "$ISNamespace.CatalogFolder" ($catalog, $FolderName, $FolderName)        '
UNION ALL
SELECT lvl = 40,
       cmd = '    $folder.Create()  '
UNION ALL
SELECT lvl = 41,
       cmd = '}'
UNION ALL
SELECT lvl = 42,
       cmd = ''
UNION ALL
SELECT lvl = 43,
       cmd = '# Read the project file, and deploy it to the folder'
UNION ALL
SELECT lvl = 44,
       cmd = 'Write-Host "Deploying Project ..."'
UNION ALL
SELECT lvl = 45,
       cmd = '[byte[]] $projectFile = [System.IO.File]::ReadAllBytes($ProjectFilePath)'
UNION ALL
SELECT lvl = 46,
       cmd = '$folder.DeployProject($ProjectName, $projectFile)'
UNION ALL
SELECT lvl = 47,
       cmd = ''
UNION ALL
SELECT lvl = 48,
       cmd = '$environment = $folder.Environments[$EnvironmentName]'
UNION ALL
SELECT lvl = 49,
       cmd = ''
UNION ALL
SELECT lvl = 50,
       cmd = 'if (!$environment)'
UNION ALL
SELECT lvl = 51,
       cmd = '{'
UNION ALL
SELECT lvl = 52,
       cmd = '    Write-Host "Creating environment ..." '
UNION ALL
SELECT lvl = 53,
       cmd = '    $environment = New-Object "$ISNamespace.EnvironmentInfo" ($folder, $EnvironmentName, $EnvironmentName)'
UNION ALL
SELECT lvl = 54,
       cmd = '    $environment.Create()     '
UNION ALL
SELECT lvl = 55,
       cmd = '}'
UNION ALL
SELECT lvl = 56,
       cmd = ''
UNION ALL
SELECT lvl = 57,
       cmd = '$project = $folder.Projects[$ProjectName]'
UNION ALL
SELECT lvl = 58,
       cmd = '$ref = $project.References[$EnvironmentName, $folder.Name]'
UNION ALL
SELECT lvl = 60,
       cmd = ''
UNION ALL
SELECT lvl = 61,
       cmd = 'if (!$ref)'
UNION ALL
SELECT lvl = 62,
       cmd = '{'
UNION ALL
SELECT lvl = 63,
       cmd = '    # making project refer to this environment'
UNION ALL
SELECT lvl = 64,
       cmd = '    Write-Host "Adding environment reference to project ..."'
UNION ALL
SELECT lvl = 65,
       cmd = '    $project.References.Add($EnvironmentName, $folder.Name)'
UNION ALL
SELECT lvl = 66,
       cmd = '    $project.Alter() '
UNION ALL
SELECT lvl = 67,
       cmd = '}'
UNION ALL
SELECT lvl = 68,
       cmd = ''
UNION ALL
SELECT lvl = 69,
       cmd = '# Adding variable to our environment'
UNION ALL
SELECT lvl = 70,
       cmd = '# Constructor args: variable name, type, default value, sensitivity, description'
UNION ALL
SELECT lvl = 71,
       cmd = '$connection_string = $environment.Variables["connection_string"];'
UNION ALL
SELECT lvl = 72,
       cmd = ''
UNION ALL
SELECT lvl = 73,
       cmd = 'if (!$connection_string)'
UNION ALL
SELECT lvl = 74,
       cmd = '{'
UNION ALL
SELECT lvl = 75,
       cmd = '    Write-Host "Adding environment variables ..."'
UNION ALL
SELECT lvl = 76,
       cmd = '    $environment.Variables.Add('
UNION ALL
SELECT lvl = 77,
       cmd = '        �connection_string�, '
UNION ALL
SELECT lvl = 78,
       cmd = '        [System.TypeCode]::String, $connection, $false, �connection_string�)'
UNION ALL
SELECT lvl = 79,
       cmd = '    $environment.Alter()'
UNION ALL
SELECT lvl = 80,
       cmd = '    $connection_string = $environment.Variables["connection_string"];'
UNION ALL
SELECT lvl = 81,
       cmd = '}'
UNION ALL
SELECT lvl = 82,
       cmd = ''
UNION ALL
SELECT lvl = 83,
       cmd = '$project.Parameters["connection_string"].Set('
UNION ALL
SELECT lvl = 84,
       cmd = '    [Microsoft.SqlServer.Management.IntegrationServices.ParameterInfo+ParameterValueType]::Referenced, '
UNION ALL
SELECT lvl = 85,
       cmd = '    $connection_string.Name)'
UNION ALL
SELECT lvl = 86,
       cmd = '$project.Alter()  ';

DELETE FROM #files2;
SELECT @source_folder2 = @source_path + N'07 SSIS packages\QTC ELO REPORTS\';
INSERT INTO #files2 (subdirectory, depth, is_file)
EXEC xp_dirtree @source_folder2, 0, 1;

IF EXISTS (SELECT 1 FROM #files2)
BEGIN
  SELECT @bat_file_name = @bat_file_folder + N'deploy_elo.ps1';
  EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0
END;

DELETE FROM t_temp_commands_to_be_executed;

SELECT @out_file_name = @deploy_path2 + N'bat\2_2_SSIS_' + @release_number + '.log';

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 0,
       cmd = ''
UNION ALL
SELECT lvl = 1,
       cmd = 'echo %date% %time% > ' + @out_file_name
UNION ALL
SELECT lvl = 2,
       cmd = 'echo start script 001 bat\deploy_elo.ps1>> ' + @out_file_name
UNION ALL
SELECT lvl = 3,
       cmd = 'Powershell.exe -executionpolicy remotesigned -File "' + @deploy_path2 + N'bat\deploy_elo.ps1" >> ' + @out_file_name
UNION ALL
SELECT lvl = 4,
       cmd = 'echo end script 001 bat\deploy_elo.ps1>> ' + @out_file_name
UNION ALL
SELECT lvl = 1999,
       cmd = 'goto :EOF'
UNION ALL
SELECT lvl = 2000,
       cmd = ':error'
UNION ALL
SELECT lvl = 2001,
       cmd = 'echo ERROR'
UNION ALL
SELECT lvl = 2002,
       cmd = 'exit /b';

--SELECT *
--  FROM t_temp_commands_to_be_executed
IF EXISTS (SELECT 1 FROM #files2)
BEGIN

	SELECT @bat_file_name = @bat_file_folder + N'2_2_SSIS_' + @release_number + '.bat';
	EXEC p_write_resultset
						 @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
						 @filename               = @bat_file_name,
						 @write_mode             = N'W',
						 @null_value             = N'<NULL>',
						 @add_record_count_ind   = 0,
						 @verbosity_lvl          = 0
END;

DELETE FROM t_temp_commands_to_be_executed;

--Create datasource file
INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 1,
       cmd = N'<?xml version="1.0" encoding="utf-8"?>'
UNION ALL
SELECT lvl = 2,
       cmd = N'<RptDataSource xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" Name="onesumxdb_qtc">'
UNION ALL
SELECT lvl = 3,
       cmd = N'  <ConnectionProperties>'
UNION ALL
SELECT lvl = 4,
       cmd = N'    <Extension>SQL</Extension>'
UNION ALL
SELECT lvl = 5,
       cmd = N'    <ConnectString>Data Source=' + @destination_server + ';Initial Catalog=onesumxdb_qtc</ConnectString>'
UNION ALL
SELECT lvl = 6,
       cmd = N'    <IntegratedSecurity>true</IntegratedSecurity>'
UNION ALL
SELECT lvl = 7,
       cmd = N'  </ConnectionProperties>'
UNION ALL
SELECT lvl = 8,
       cmd = N'  <DataSourceID>5daffb67-916b-40ec-97af-c744a193a66e</DataSourceID>'
UNION ALL
SELECT lvl = 9,
       cmd = N'</RptDataSource>' 

IF EXISTS (SELECT 1 FROM #files2)
BEGIN
       
	SELECT @datasource_filename = @datasource_folder + N'onesumxdb_qtc.rds';
	EXEC p_write_resultset
						 @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
						 @filename               = @datasource_filename,
						 @write_mode             = N'W',
						 @null_value             = N'<NULL>',
						 @add_record_count_ind   = 0,
						 @verbosity_lvl          = 0
END;

DELETE FROM t_temp_commands_to_be_executed;
DELETE FROM #files2;
---------------------------------------------------------------

-- 3. Scripts
-- 3.1. Server
SELECT @bat_file_name = N'3_1_Server_' + @release_number + '.bat',
       @out_file_name = @deploy_path2 + N'bat\3_1_Server_' + @release_number + '.log',
	   @source_folder = @source_path + N'01 Server\',
	   @destination_folder = @deploy_path2 + N'01 Server\';

EXEC p_generate_migration_commands_qtc
  @source_folder = @source_folder,
  @destination_folder = @destination_folder,
  @destination_server = @destination_server,
  @destination_db = N'onesumxdb_qtc',
  @bat_file_folder = @bat_file_folder,
  @bat_file_name = @bat_file_name,
  @out_file_name = @out_file_name;

-- 3.2. staging
SELECT @bat_file_name = N'3_2_staging_' + @release_number + '.bat',
       @out_file_name = @deploy_path2 + N'bat\3_2_staging_' + @release_number + '.log',
	   @source_folder = @source_path + N'02 staging\',
	   @destination_folder = @deploy_path2 + N'02 staging\';

EXEC p_generate_migration_commands_qtc
  @source_folder = @source_folder,
  @destination_folder = @destination_folder,
  @destination_server = @destination_server,
  @destination_db = N'staging',
  @bat_file_folder = @bat_file_folder,
  @bat_file_name = @bat_file_name,
  @out_file_name = @out_file_name;

-- 3.3. tng
SELECT @bat_file_name = N'3_3_tng_' + @release_number + '.bat',
       @out_file_name = @deploy_path2 + N'bat\3_3_tng_' + @release_number + '.log',
	   @source_folder = @source_path + N'03 tng\',
	   @destination_folder = @deploy_path2 + N'03 tng\';

EXEC p_generate_migration_commands_qtc
  @source_folder = @source_folder,
  @destination_folder = @destination_folder,
  @destination_server = @destination_server,
  @destination_db = N'tng',
  @bat_file_folder = @bat_file_folder,
  @bat_file_name = @bat_file_name,
  @out_file_name = @out_file_name;

IF @release_number = N'wki01c01m01s03'
BEGIN
	-- 3.4. onesumxdb_qtc special case
	SELECT @bat_file_name = N'3_4_a_onesumxdb_qtc_' + @release_number + '.bat',
		   @out_file_name = @deploy_path2 + N'bat\3_4_a_onesumxdb_qtc_' + @release_number + '.log',
		   @source_folder = @source_path + N'04 onesumxdb_qtc\04 Programmability\01 Functions\',
		   @destination_folder = @deploy_path2 + N'04 onesumxdb_qtc\04 Programmability\01 Functions\';
	SELECT @destination_folder
	EXEC p_generate_migration_commands_qtc
	  @source_folder = @source_folder,
	  @destination_folder = @destination_folder,
	  @destination_server = @destination_server,
	  @destination_db = N'onesumxdb_qtc',
	  @bat_file_folder = @bat_file_folder,
	  @bat_file_name = @bat_file_name,
	  @out_file_name = @out_file_name;
END;

-- 3.4. onesumxdb_qtc
SELECT @bat_file_name = N'3_4_onesumxdb_qtc_' + @release_number + '.bat',
       @out_file_name = @deploy_path2 + N'bat\3_4_onesumxdb_qtc_' + @release_number + '.log',
	   @source_folder = @source_path + N'04 onesumxdb_qtc\',
	   @destination_folder = @deploy_path2 + N'04 onesumxdb_qtc\';

EXEC p_generate_migration_commands_qtc
  @source_folder = @source_folder,
  @destination_folder = @destination_folder,
  @destination_server = @destination_server,
  @destination_db = N'onesumxdb_qtc',
  @bat_file_folder = @bat_file_folder,
  @bat_file_name = @bat_file_name,
  @out_file_name = @out_file_name;

-- 3.5 Tools
SELECT @bat_file_name = N'3_5_Tools_' + @release_number + '.bat',
       @out_file_name = @deploy_path2 + N'bat\3_5_Tools_' + @release_number + '.log',
	   @source_folder = @source_path + N'05 Tools\',
	   @destination_folder = @deploy_path2 + N'05 Tools\';

EXEC p_generate_migration_commands_qtc
  @source_folder = @source_folder,
  @destination_folder = @destination_folder,
  @destination_server = @destination_server,
  @destination_db = N'onesumxdb_qtc',
  @bat_file_folder = @bat_file_folder,
  @bat_file_name = @bat_file_name,
  @out_file_name = @out_file_name;

-- 3.6 L1
SELECT @bat_file_name = N'3_6_L1_' + @release_number + '.bat',
       @out_file_name = @deploy_path2 + N'bat\3_6_L1_' + @release_number + '.log',
	   @source_folder = @source_path + N'12 L1\',
	   @destination_folder = @deploy_path2 + N'12 L1\';

EXEC p_generate_migration_commands_qtc
  @source_folder = @source_folder,
  @destination_folder = @destination_folder,
  @destination_server = @destination_server,
  @destination_db = N'onesumxdb_qtc',
  @bat_file_folder = @bat_file_folder,
  @bat_file_name = @bat_file_name,
  @out_file_name = @out_file_name;

-- 3.7 BI
SELECT @bat_file_name = N'3_7_BI_' + @release_number + '.bat',
       @out_file_name = @deploy_path2 + N'bat\3_7_BI_' + @release_number + '.log',
	   @source_folder = @source_path + N'15 BI\',
	   @destination_folder = @deploy_path2 + N'15 BI\';

EXEC p_generate_migration_commands_qtc
  @source_folder = @source_folder,
  @destination_folder = @destination_folder,
  @destination_server = @destination_server,
  @destination_db = N'onesumxdb_qtc',
  @bat_file_folder = @bat_file_folder,
  @bat_file_name = @bat_file_name,
  @out_file_name = @out_file_name;

-- 3.8 Historical Data
SELECT @bat_file_name = N'3_8_Historical_Data_' + @release_number + '.bat',
       @out_file_name = @deploy_path2 + N'bat\3_8_Historical_Data_' + @release_number + '.log',
	   @source_folder = @source_path + N'10 Historical Data\',
	   @destination_folder = @deploy_path2 + N'10 Historical Data\';

EXEC p_generate_migration_commands_qtc
  @source_folder = @source_folder,
  @destination_folder = @destination_folder,
  @destination_server = @destination_server,
  @destination_db = N'onesumxdb_qtc',
  @bat_file_folder = @bat_file_folder,
  @bat_file_name = @bat_file_name,
  @out_file_name = @out_file_name;

---------------------------------------------------------------
-- 4. Files

--XCOPY source [destination] [/A | /M] [/D[:date]] [/P] [/S [/E]] [/V] [/W]
--                           [/C] [/I] [/Q] [/F] [/L] [/H] [/R] [/T] [/U]
--                           [/K] [/N] [/O] [/X] [/Y] [/-Y] [/Z]
--                           [/EXCLUDE:file1[+file2][+file3]...]

SELECT @out_file_name = @deploy_path2 + N'bat\4_Files_' + @release_number + '.log';

DELETE FROM t_temp_commands_to_be_executed;

SELECT @source_folder2 = @source_path + N'05 Tools\Portal\';
INSERT INTO #files2 (subdirectory, depth, is_file)
EXEC xp_dirtree @source_folder2, 0, 1;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 0,
       cmd = ''
UNION ALL
SELECT lvl = 1,
       cmd = 'echo %date% %time% > ' + @out_file_name
UNION ALL
SELECT lvl = 2,
       cmd = 'echo start script Files copy>> ' + @out_file_name
UNION ALL
SELECT lvl = 3,
       cmd = N'XCOPY /f /s /e /v /y /i "' + @deploy_path2 + N'05 Tools\Portal\*" "' + @destination_portal + N'" >>' + @out_file_name
UNION ALL
SELECT lvl = 4,
       cmd = 'echo end script Files copy>> ' + @out_file_name
UNION ALL
SELECT lvl = 1999,
       cmd = 'goto :EOF'
UNION ALL
SELECT lvl = 2000,
       cmd = ':error'
UNION ALL
SELECT lvl = 2001,
       cmd = 'echo ERROR'
UNION ALL
SELECT lvl = 2002,
       cmd = 'exit /b';

--SELECT *
--  FROM t_temp_commands_to_be_executed
SELECT @bat_file_name = @bat_file_folder + N'4_Files_' + @release_number + '.bat';
IF EXISTS (
SELECT 1
  FROM #files2)
BEGIN
  EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0;
END;

DELETE FROM #files2;
DELETE FROM t_temp_commands_to_be_executed;

---------------------------------------------------------------
-- 5. Maestro import
SELECT @out_file_name = @deploy_path2 + N'bat\5_Maestro_' + @release_number + '.log';


INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 0,
       cmd = '<?xml version="1.0" encoding="UTF-8" ?>'
UNION ALL
SELECT lvl = 1,
       cmd = '<XmlExchangerCommandFile>'
UNION ALL
SELECT lvl = 2,
       cmd = '  <Imports>'

DECLARE @files TABLE 
(
  solution                 SYSNAME,
  depth                    INT,
  is_file                  BIT
);
SELECT @source_folder = @source_path + N'06 Maestro';
INSERT INTO @files 
(      
       solution,
	   depth,
	   is_file
)
EXEC xp_dirtree @source_folder, 0, 1

DECLARE cur CURSOR FOR
SELECT solution
  FROM @files
 WHERE LOWER(solution) LIKE N'%.xml';

OPEN cur;
FETCH NEXT FROM cur INTO @solution;
WHILE @@FETCH_STATUS = 0
BEGIN
  INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
  SELECT MAX(lvl) + 1 AS lvl,
         N'    <Import>'
    FROM t_temp_commands_to_be_executed
  UNION ALL 
  SELECT MAX(lvl) + 2 AS lvl,
         N'      <Enabled>true</Enabled>'         
    FROM t_temp_commands_to_be_executed
  UNION ALL 
  SELECT MAX(lvl) + 3 AS lvl,
         N'      <Server>' + @destination_server + N'</Server>'
    FROM t_temp_commands_to_be_executed
  UNION ALL 
  SELECT MAX(lvl) + 4 AS lvl,
         N'      <Database>onesumxdb_qtc</Database>'
    FROM t_temp_commands_to_be_executed
 -- UNION ALL 
 -- SELECT MAX(lvl) + 5 AS lvl,
 --        N'      <LogFile>' + REPLACE(REPLACE(@solution, ' ', '_'), '&', 'n') + N'_import_log.xml</LogFile>'
 --   FROM t_temp_commands_to_be_executed
  UNION ALL 
  SELECT MAX(lvl) + 6 AS lvl,
         N'      <ConsoleOn>true</ConsoleOn>'
    FROM t_temp_commands_to_be_executed
  UNION ALL 
  SELECT MAX(lvl) + 7 AS lvl,
         N'      <ReplaceSet>true</ReplaceSet>'
    FROM t_temp_commands_to_be_executed
  UNION ALL 
  SELECT MAX(lvl) + 8 AS lvl,
         N'      <ReplaceSolution>true</ReplaceSolution>'
    FROM t_temp_commands_to_be_executed
  UNION ALL 
  SELECT MAX(lvl) + 9 AS lvl,
         N'      <XmlFile>' + @deploy_path2 + N'06 Maestro\'+@solution +'</XmlFile>'
    FROM t_temp_commands_to_be_executed
  UNION ALL 
  SELECT MAX(lvl) + 10 AS lvl,
         N'    </Import>'
    FROM t_temp_commands_to_be_executed
      
  FETCH NEXT FROM cur INTO @solution;
END;
CLOSE cur;
DEALLOCATE cur;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 3000,
       cmd = '  </Imports>'
UNION ALL
SELECT lvl = 4000,
       cmd = '</XmlExchangerCommandFile>'

--SELECT *
--  FROM t_temp_commands_to_be_executed
SELECT @bat_file_name = @bat_file_folder + N'Maestro_Import.xml';
IF EXISTS (SELECT 1 FROM @files)
BEGIN
  EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0
END;

DELETE FROM t_temp_commands_to_be_executed;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 0,
       cmd = ''
UNION ALL
SELECT lvl = 1,
       cmd = 'echo %date% %time% > ' + @out_file_name
UNION ALL
SELECT lvl = 2,
       cmd = 'echo start script Maestro Import>> ' + @out_file_name
UNION ALL
SELECT lvl = 3,
       cmd = N'"' + @destination_portal + N'\Rules Exchange\Business Rules Engine\Maestro.Main.Exchange.Command.exe" -commandfile "' + @deploy_path2 + N'bat\Maestro_Import.xml" >>' + @out_file_name
UNION ALL
SELECT lvl = 4,
       cmd = 'echo end script Maestro Import>> ' + @out_file_name
UNION ALL
SELECT lvl = 1999,
       cmd = 'goto :EOF'
UNION ALL
SELECT lvl = 2000,
       cmd = ':error'
UNION ALL
SELECT lvl = 2001,
       cmd = 'echo ERROR'
UNION ALL
SELECT lvl = 2002,
       cmd = 'exit /b';

--SELECT *
--  FROM t_temp_commands_to_be_executed
SELECT @bat_file_name = @bat_file_folder + N'5_Maestro_' + @release_number + '.bat';
EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0

DELETE FROM t_temp_commands_to_be_executed;

---------------------------------------------------------------
-- 6. SSRS Reports


SELECT @out_file_name = @deploy_path2 + N'bat\6_SSRS_' + @release_number + '.log';


DELETE FROM t_temp_commands_to_be_executed;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 0,
       cmd = ''
UNION ALL
SELECT lvl = 1,
       cmd = 'echo %date% %time% > ' + @out_file_name
UNION ALL
SELECT lvl = 2,
       cmd = 'echo start script SSRS deploy>> ' + @out_file_name
UNION ALL
SELECT lvl = 3,
       cmd = N'set varServerPath=' + @ssrs_serverpath
UNION ALL
SELECT lvl = 4,
       cmd = N'set varReportFolder=Standard Reports qtc'
UNION ALL
SELECT lvl = 5,
       cmd = N'Set varDatasetFolder=Datasets'
UNION ALL
SELECT lvl = 6,
       cmd = N'set varDataSourceFolder=Data Sources'
UNION ALL
SELECT lvl = 7,
       cmd = N'Set varDataSourcePath=/Data Sources'
UNION ALL
SELECT lvl = 8,
       cmd = N'set varReportName='
UNION ALL
SELECT lvl = 9,
       cmd = N'set varReportFilePath=' + @deploy_path2 + N'08 SSRS reports\SSRS'
UNION ALL
SELECT lvl = 10,
       cmd = N''
UNION ALL
SELECT lvl = 11,
       cmd = N'rs.exe -i "' + @deploy_path2 + '08 SSRS reports\SSRS\Commonscript.rss" -s %varServerPath% -v ReportFolder="%varReportFolder%" -v DataSetFolder="%varDatasetFolder%" -v DataSourceFolder="%varDataSourceFolder%" -v DataSourcePath="%varDataSourcePath%" -v ReportName="%varReportName%"  -v filePath="%varReportFilePath%" -e Mgmt2010>> ' + @out_file_name
UNION ALL
SELECT lvl = 12,
       cmd = 'echo end script SSRS deploy>> ' + @out_file_name
UNION ALL
SELECT lvl = 1999,
       cmd = 'goto :EOF'
UNION ALL
SELECT lvl = 2000,
       cmd = ':error'
UNION ALL
SELECT lvl = 2001,
       cmd = 'echo ERROR'
UNION ALL
SELECT lvl = 2002,
       cmd = 'exit /b';

SELECT @bat_file_name = @bat_file_folder + N'6_SSRS_' + @release_number + '.bat';
EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0

DELETE FROM t_temp_commands_to_be_executed;

SELECT @out_file_name = @deploy_path2 + N'bat\7_Mark_Release_' + @release_number + '.log';

INSERT INTO t_temp_commands_to_be_executed(lvl,cmd)
SELECT 0,N''
UNION ALL
SELECT 1,N'sqlcmd -S "' + @destination_server + N'" -d onesumxdb_qtc -Q "UPDATE t_configuration_qtc SET default_value = N''' + @release_number + N''' WHERE config_name = N''RELEASE_NUMBER''" -E -b -r1';

SELECT @bat_file_name = @bat_file_folder + N'7_Mark_Release_' + @release_number + '.bat';
EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0

DELETE FROM t_temp_commands_to_be_executed;

SELECT @bat_file_name = @bat_file_folder + N'99_1_SQL_Agent_Start_' + @release_number + '.bat',
       @out_file_name = @deploy_path2 + N'bat\99_1_SQL_Agent_' + @release_number + '.log'

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 1,
       N'
echo %date% %time% >' + @out_file_name + N'
echo start script start_agent.ps1>>' + @out_file_name + N'
Powershell.exe -executionpolicy remotesigned -File "' + @deploy_path2 + N'bat\start_agent.ps1" >>' + @out_file_name + N'
echo end script start_agent.ps1>>' + @out_file_name + N'
';

EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0

DELETE FROM t_temp_commands_to_be_executed;

-- 8 SQL Agent
SELECT @bat_file_name = N'99_2_SQL_Agent_' + @release_number + '.bat',
       @out_file_name = @deploy_path2 + N'bat\99_2_SQL_Agent_' + @release_number + '.log',
	   @source_folder = @source_path + N'20 SQL Agent\',
	   @destination_folder = @deploy_path2 + N'20 SQL Agent\';

EXEC p_generate_migration_commands_qtc
  @source_folder = @source_folder,
  @destination_folder = @destination_folder,
  @destination_server = @destination_server,
  @destination_db = N'onesumxdb_qtc',
  @bat_file_folder = @bat_file_folder,
  @bat_file_name = @bat_file_name,
  @out_file_name = @out_file_name,
  @out_indicator = N' >> ';

SELECT @out_file_name = @deploy_path2 + N'bat\97_start_processes_' + @release_number + '.log';

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT lvl = 0,
       cmd = ''
UNION     
SELECT lvl = 1,
       cmd = 'echo %date% %time% > ' + @out_file_name;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 100,
       N'sqlcmd -S "' + @destination_server + N'" -i "' + @deploy_path2 + N'bat\start_processes.sql" -E -b -r1 2>>' + @out_file_name

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 101,
       N'sc \\' + @app_server + N' START qtc_FinStudio.TaskRunner'

SELECT @bat_file_name = @bat_file_folder + N'97_start_processes_' + @release_number + N'.bat';
EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0

DELETE FROM t_temp_commands_to_be_executed;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 1,
       N'USE master;
GO
ALTER DATABASE [onesumxdb_qtc] SET  MULTI_USER WITH NO_WAIT
GO';

SELECT @bat_file_name = @bat_file_folder + N'start_processes.sql';
EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0

DELETE FROM t_temp_commands_to_be_executed;

INSERT INTO t_temp_commands_to_be_executed (lvl,cmd)
SELECT 1,
N'$agentsrvc = get-service -name sqlserveragent -ComputerName STVSQLN02
if ( $agentsrvc.status -eq "Stopped" )
    {
    $agentsrvc.Start()
    $agentsrvc.WaitForStatus( ''Running'', ''00:00:20'' )     
    if ( $agentsrvc.status -ne ''Running'' )
        {
        "SQL Agent not started in time. Status is $agentsrvc.Status"
        exit( 1 )
        }
        else
        { "SQL Agent successfully started" }
    }
    else
    { "SQL Server Agent already $agentsrvc.status" }'

SELECT @bat_file_name = @bat_file_folder + N'start_agent.ps1';
EXEC p_write_resultset
                     @statement              = N'SELECT cmd from t_temp_commands_to_be_executed ORDER BY lvl',
                     @filename               = @bat_file_name,
                     @write_mode             = N'W',
                     @null_value             = N'<NULL>',
                     @add_record_count_ind   = 0,
                     @verbosity_lvl          = 0


DROP TABLE #files2;