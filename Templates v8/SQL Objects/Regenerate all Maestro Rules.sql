   
DECLARE @mex_id d_id;
DECLARE mex CURSOR FOR
SELECT mex_id
  FROM t_mex_type;
  
OPEN mex;

FETCH NEXT FROM mex INTO @mex_id;
WHILE @@FETCH_STATUS = 0
BEGIN 
  EXEC p_mex_gen_cluster @mex_id=@mex_id,
                         @cluster_id=NULL,
                         @debug=0,
                         @show_code_preview=0,
                         @use_encryption=0,
                         @with_recompile=NULL,
                         @use_cursor=0,
                         @print_ind=N'P';
  FETCH NEXT FROM mex INTO @mex_id;
END

CLOSE mex;
DEALLOCATE mex;