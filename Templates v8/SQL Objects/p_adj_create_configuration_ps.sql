USE OneSumX;
GO


IF OBJECT_ID(N'p_adj_create_configuration_ps',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_adj_create_configuration_ps ...';
  DROP PROCEDURE p_adj_create_configuration_ps;
END
GO

PRINT N'Creating procedure p_adj_create_configuration_ps...';
GO
CREATE PROCEDURE dbo.p_adj_create_configuration_ps (  @fda_table			    d_name
												      , @data_fields            d_max_char      = NULL
												      , @type_name			    d_name 
												      , @debug                  d_bit           = 0
                                                      , @execute_immediately    d_bit           = 1
                                                      , @database_name          SYSNAME         = NULL
													  , @recreate_validation    d_bit           = 0
												      )
AS

-- ******************************************************************************
-- * Purpose: Creates the configuration of an adjustment                        *
-- *                                                                            *
-- * Inputs:  @fda_table: The table  name of the table that needs to be        *
-- *                       adjusted.                                            *
-- *          @data_fields: Fields that are enabled to be adjusted              *
-- *                        (should not include PK fields)                      *
-- *          @type_name: Name of the adjustment type                           *
-- *          @debug : Turn on debug statements                                 *
-- *                                                                            *
-- ******************************************************************************

SET NOCOUNT ON;

DECLARE @code_block    NVARCHAR(4000),
        @column_name   d_name,
        @database_id               INT,
        @display_order INT = 1,
        @field         INT,
        @load_proc     NVARCHAR(200),
        @val_proc      NVARCHAR(200),
        @err_proc      NVARCHAR(200),
		@control_type_id INT,
        @proc_name     SYSNAME = OBJECT_NAME(@@PROCID),
        @shadow_table  d_name,
        @val_table     d_name,
        @table_id      INT,
		@system_type	NVARCHAR(500),
		@static_fields d_max_char,
		@statement		d_statement,
		@is_identity   BIT,
		@is_nullable   BIT,
		@is_bit        BIT,
		@default	   NVARCHAR(2000);

SELECT @val_table = @fda_table + N'_validation',
       @val_proc = N'p_dec_' + 
	                CASE WHEN @fda_table LIKE N't_%' THEN SUBSTRING(@fda_table, 3, LEN(@fda_table)) ELSE @fda_table END +
                    N'_val',
       @err_proc = N'p_dec_' + 
	                CASE WHEN @fda_table LIKE N't_%' THEN SUBSTRING(@fda_table, 3, LEN(@fda_table)) ELSE @fda_table END +
                    N'_val_get_errors',
	   @database_name = ISNULL(@database_name,DB_NAME());


SELECT @database_id = database_id
  FROM sys.databases
 WHERE name = @database_name;

DECLARE @pkeys TABLE ( TABLE_QUALIFIER NVARCHAR(356) NOT NULL,
                       TABLE_OWNER     NVARCHAR(356) NOT NULL,
                       TABLE_NAME      NVARCHAR(356) NOT NULL,
                       COLUMN_NAME     NVARCHAR(356) NOT NULL,
                       KEY_SEQ         INT NOT NULL,
                       PK_NAME         NVARCHAR(356) NOT NULL);

DECLARE @syscolumns TABLE ([object_id]	         INT      NOT NULL,
                           name	                 SYSNAME  NULL,
                           column_id	         INT      NOT NULL,
                           system_type_id	     TINYINT  NOT NULL,
                           user_type_id	         INT      NOT NULL,
                           max_length	         SMALLINT NOT NULL,
                           [precision]	         TINYINT  NOT NULL,
                           scale	             TINYINT  NOT NULL,
                           collation_name	     SYSNAME  NULL,
                           is_nullable	         BIT      NULL,
                           is_ansi_padded	     BIT      NOT NULL,
                           is_rowguidcol	     BIT      NOT NULL,
                           is_identity	         BIT      NOT NULL,
                           is_computed	         BIT      NOT NULL,
                           is_filestream	     BIT      NOT NULL,
                           is_replicated	     BIT      NULL,
                           is_non_sql_subscribed BIT      NULL,
                           is_merge_published    BIT      NULL,
                           is_dts_replicated     BIT      NULL,
                           is_xml_document       BIT      NOT NULL,
                           xml_collection_id     INT      NOT NULL,
                           default_object_id     INT      NOT NULL,
                           rule_object_id        INT      NOT NULL,
                           is_sparse             BIT      NULL,
                           is_column_set         BIT      NULL);

CREATE TABLE #cmd (seq_nr INT           NOT NULL IDENTITY(1, 1),  
                   txt    NVARCHAR(MAX) NOT NULL);

INSERT #cmd (txt) SELECT N'SELECT c.[object_id],';
INSERT #cmd (txt) SELECT N'       c.name,';
INSERT #cmd (txt) SELECT N'       c.column_id,';
INSERT #cmd (txt) SELECT N'       c.system_type_id,';
INSERT #cmd (txt) SELECT N'       c.user_type_id,';
INSERT #cmd (txt) SELECT N'       c.max_length,';
INSERT #cmd (txt) SELECT N'       c.precision,';
INSERT #cmd (txt) SELECT N'       c.scale,';
INSERT #cmd (txt) SELECT N'       c.collation_name,';
INSERT #cmd (txt) SELECT N'       c.is_nullable,';
INSERT #cmd (txt) SELECT N'       c.is_ansi_padded,';
INSERT #cmd (txt) SELECT N'       c.is_rowguidcol,';
INSERT #cmd (txt) SELECT N'       c.is_identity,';
INSERT #cmd (txt) SELECT N'       c.is_computed,';
INSERT #cmd (txt) SELECT N'       c.is_filestream,';
INSERT #cmd (txt) SELECT N'       c.is_replicated,';
INSERT #cmd (txt) SELECT N'       c.is_non_sql_subscribed,';
INSERT #cmd (txt) SELECT N'       c.is_merge_published,';
INSERT #cmd (txt) SELECT N'       c.is_dts_replicated,';
INSERT #cmd (txt) SELECT N'       c.is_xml_document,';
INSERT #cmd (txt) SELECT N'       c.xml_collection_id,';
INSERT #cmd (txt) SELECT N'       c.default_object_id,';
INSERT #cmd (txt) SELECT N'       c.rule_object_id,';
INSERT #cmd (txt) SELECT N'       c.is_sparse,';
INSERT #cmd (txt) SELECT N'       c.is_column_set';
INSERT #cmd (txt) SELECT N'  FROM ' + @database_name + N'.sys.columns AS c';
INSERT #cmd (txt) SELECT N'  JOIN ' + @database_name + N'.sys.tables AS t';
INSERT #cmd (txt) SELECT N'    ON c.object_id = t.object_id';
INSERT #cmd (txt) SELECT N' WHERE OBJECT_NAME (c.[object_id],' + CONVERT(NVARCHAR,@database_id) + N') = ''' + @fda_table + N'''';
INSERT #cmd (txt) SELECT N'   AND SCHEMA_NAME(t.schema_id) = N''dbo'';';
INSERT INTO @syscolumns
EXEC p_exec_resultset @stmt = N'SELECT txt FROM #cmd ORDER BY seq_nr';

DELETE FROM #cmd;

INSERT #cmd (txt) SELECT 'USE ' + @database_name;
INSERT #cmd (txt) SELECT 'EXEC (N''sp_pkeys ''''' +  @fda_table + N''''', ''''dbo'''''');';
INSERT INTO @pkeys
EXEC p_exec_resultset @stmt = 'SELECT txt FROM #cmd ORDER BY seq_nr';

DELETE FROM #cmd;

IF @data_fields IS NULL
BEGIN
  SELECT @data_fields = N'';
  SELECT @data_fields = @data_fields + c.name + CASE WHEN c.column_id <> MAX(c.column_id) OVER () THEN N',' ELSE N'' END
    FROM @syscolumns c
    LEFT JOIN @pkeys p
      ON c.name = p.COLUMN_NAME
   WHERE p.COLUMN_NAME IS NULL
     AND c.is_computed = 0
	 AND c.name NOT IN (N'last_modified',N'modified_by')
  ORDER BY c.column_id;
  PRINT N'New value for @data_fields: ' + @data_fields;
END

SELECT @static_fields = N'';
SELECT @static_fields = @static_fields + COLUMN_NAME + CASE WHEN KEY_SEQ <> MAX(KEY_SEQ) OVER () THEN N',' ELSE N'' END
  FROM @pkeys
 ORDER BY KEY_SEQ;

IF NOT EXISTS (SELECT 1
             FROM sys.key_constraints kc
             JOIN sys.indexes i
               ON kc.parent_object_id = i.object_id
              AND kc.name = i.name
             JOIN sys.index_columns ic
               ON i.object_id = ic.object_id
              AND i.index_id = ic.index_id
             JOIN sys.columns c
               ON ic.object_id = c.object_id
              AND ic.column_id = c.column_id
            WHERE kc.type = N'UQ'
              AND kc.parent_object_id = OBJECT_ID(@fda_table)
              AND c.is_computed = 0
			  AND OBJECT_SCHEMA_NAME(c.object_id) = N'dbo'
              )
BEGIN
  SELECT @static_fields = @static_fields + N',' + c.name
    FROM sys.key_constraints kc
    JOIN sys.indexes i
      ON kc.parent_object_id = i.object_id
     AND kc.name = i.name
    JOIN sys.index_columns ic
      ON i.object_id = ic.object_id
     AND i.index_id = ic.index_id
    JOIN sys.columns c
      ON ic.object_id = c.object_id
     AND ic.column_id = c.column_id
   WHERE kc.type = N'UQ'
     AND kc.parent_object_id = OBJECT_ID(@fda_table)
     AND c.is_computed = 0
	 AND OBJECT_SCHEMA_NAME(c.object_id) = N'dbo'
END

IF @recreate_validation = 1 OR OBJECT_ID(@val_table) IS NULL
BEGIN
  EXEC p_dec_create_val_table_ps @fda_table = @fda_table, 
                                  @val_table = @val_table, 
                                  @execute_immediately = @execute_immediately,
                                  @database_name = @database_name,
                                  @debug = @debug;
END;

IF @recreate_validation = 1 OR OBJECT_ID(@val_proc) IS NULL
BEGIN
  EXEC p_dec_gen_val_ps  @val_table = @val_table,
                          @adj_table = @shadow_table,
                          @val_proc = @val_proc,
                          @action = N'V',
		  		          @execute_immediately = @execute_immediately,
                          @debug = @debug,
                          @database_name = @database_name;
END;
IF @recreate_validation = 1 OR OBJECT_ID(@err_proc) IS NULL
BEGIN
  EXEC p_dec_gen_val_ps  @val_table = @val_table,
                          @adj_table = @shadow_table,
                          @val_proc = @err_proc,
                          @action = N'E',
		  	              @execute_immediately = @execute_immediately,
                          @debug = @debug,
                          @database_name = @database_name;
END;

SELECT @table_id = table_id
  FROM t_dec_table 
 WHERE table_code = N'dec_' + @fda_table;

IF @table_id IS NOT NULL
BEGIN
	UPDATE dbo.t_dec_table
	   SET table_name = @fda_table,
		   database_name = IIF(@database_name = DB_NAME(),NULL,@database_name),
		   select_proc = NULL,
		   insert_proc = NULL,
		   update_proc = NULL,
		   delete_proc = NULL,
		   table_desc = @type_name,
		   label = @type_name,
		   allow_insert = 1,
		   allow_update = 1,
		   allow_delete = 1,
		   validation_mode = 1,
		   validation_proc = @val_proc,
		   validation_errors_proc = @err_proc,
		   filter = NULL,
		   default_sort = NULL,
		   default_page_size = NULL,
		   last_modified = CURRENT_TIMESTAMP,
		   modified_by = CURRENT_USER
	 WHERE table_id = @table_id;	
END;
ELSE
BEGIN
	INSERT INTO dbo.t_dec_table
			( table_code ,
			  table_name ,
			  database_name ,
			  select_proc ,
			  insert_proc ,
			  update_proc ,
			  delete_proc ,
			  table_desc ,
			  label ,
			  allow_insert ,
			  allow_update ,
			  allow_delete ,
			  validation_mode ,
			  validation_proc ,
			  validation_errors_proc ,
			  filter ,
			  default_sort ,
			  default_page_size ,
			  last_modified ,
			  modified_by
			)
	VALUES  ( 'dec_' + @fda_table , -- table_code - d_name
			  @fda_table , -- table_name - sysname
			  IIF(@database_name = DB_NAME(),NULL,@database_name) , -- database_name - sysname
			  NULL , -- select_proc - sysname
			  NULL , -- insert_proc - sysname
			  NULL , -- update_proc - sysname
			  NULL , -- delete_proc - sysname
			  @type_name , -- table_desc - d_description
			  @type_name , -- label - d_description
			  1 , -- allow_insert - d_bit
			  1 , -- allow_update - d_bit
			  1 , -- allow_delete - d_bit
			  1 , -- validation_mode - d_id
			  @val_proc , -- validation_proc - sysname
			  @err_proc , -- validation_errors_proc - sysname
			  NULL , -- filter - d_cond_value
			  NULL , -- default_sort - d_cond_value
			  NULL , -- default_page_size - d_id
			  CURRENT_TIMESTAMP , -- last_modified - d_last_modified
			  CURRENT_USER  -- modified_by - d_user
			);
END;

SELECT @table_id = table_id
  FROM t_dec_table 
 WHERE table_code = N'dec_' + @fda_table;

DELETE 
  FROM dbo.t_dec_column
 WHERE table_id = @table_id;

DECLARE column_cursor CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT c.name,
	   CASE WHEN p.COLUMN_NAME IS NOT NULL
			  THEN 1
			WHEN f.value IS NOT NULL
			  THEN 2
			ELSE 0 END
  FROM @syscolumns c
  LEFT JOIN @pkeys p
    ON c.name = p.COLUMN_NAME
  LEFT JOIN fn_parse_single_dimension_list(@data_fields,N',') f
    ON c.name = LTRIM(RTRIM(f.value))

OPEN column_cursor;

FETCH NEXT FROM column_cursor INTO @column_name, @field;
WHILE @@FETCH_STATUS = 0
BEGIN
     
	SET @control_type_id = NULL;
	SET @statement = NULL;

    SELECT @is_identity = c.is_identity,
	       @is_nullable = c.is_nullable,
		   @is_bit = CASE WHEN t.name IS NOT NULL THEN 1 ELSE 0 END,
		   @display_order = c.column_id,
		   @system_type = TYPE_NAME(c.system_type_id)
	  FROM sys.columns c
	  LEFT JOIN sys.types t
	    ON c.user_type_id = t.user_type_id
	   AND t.name IN ('d_bit','bit')
	 WHERE c.name = @column_name
	   AND c.object_id = OBJECT_ID(@fda_table);

	IF EXISTS ( SELECT 1
                  FROM sys.columns c
                  JOIN sys.foreign_key_columns fkc
                    ON c.object_id = fkc.parent_object_id
                   AND c.column_id = fkc.parent_column_id
                  JOIN sys.objects o
                    ON fkc.referenced_object_id = o.object_id
                  JOIN sys.columns c2
                    ON o.object_id = c2.object_id
                   AND fkc.referenced_column_id = c2.column_id
	             WHERE c.name = @column_name
	               AND c.object_id = OBJECT_ID(@fda_table)
				   AND OBJECT_SCHEMA_NAME(c.object_id) = N'dbo')
	BEGIN
		SELECT @control_type_id = control_type_id
		  FROM dbo.t_fsp_control_type
		 WHERE control_type_name = N'DropDownEditor';
	END;
	
	SELECT @control_type_id = control_type_id
	  FROM dbo.t_fsp_control_type
	 WHERE control_type_name = N'CheckBox'
	   AND @is_bit = 1
	   AND @control_type_id IS NULL;

	SELECT @control_type_id = control_type_id
	  FROM dbo.t_fsp_control_type
	 WHERE control_type_name = N'Calendar'
	   AND @system_type IN (N'datetimeoffset',N'datetime2',N'datetime',N'time',N'smalldatetime',N'date')
	   AND @control_type_id IS NULL;	

	SET @control_type_id = ISNULL(@control_type_id,1);

	IF EXISTS (SELECT 1   
				 FROM sys.default_constraints dc
				 JOIN sys.columns c
				   ON c.object_id = dc.parent_object_id
				  AND dc.parent_column_id = c.column_id
				WHERE dc.parent_object_id = OBJECT_ID(@fda_table)
				  AND c.name = @column_name
				  AND (c.name LIKE N'char_cust_element%'
				   OR c.name LIKE N'num_cust_element%'))
	BEGIN
		SELECT @default = N'SELECT ' + REPLACE(dc.definition,''''')','''.'')')
		  FROM sys.default_constraints dc
		  JOIN sys.columns c
		    ON c.object_id = dc.parent_object_id
		   AND dc.parent_column_id = c.column_id
		 WHERE dc.parent_object_id = OBJECT_ID(@fda_table)
		   AND c.name = @column_name
		   AND (c.name LIKE N'char_cust_element%'
		    OR c.name LIKE N'num_cust_element%');
	END;
	ELSE
	BEGIN
		SET @default = IIF(@is_identity = 1,'0',NULL)
    END;

	SELECT @statement = N'SELECT ' + c2.name + N' FROM ' + o.name
	  FROM sys.columns c
	  JOIN sys.foreign_key_columns fkc
	    ON c.object_id = fkc.parent_object_id
	   AND c.column_id = fkc.parent_column_id
	  JOIN sys.objects o
	    ON fkc.referenced_object_id = o.object_id
	  JOIN sys.columns c2
	    ON o.object_id = c2.object_id
	   AND fkc.referenced_column_id = c2.column_id
	 WHERE c.name = @column_name
	   AND c.object_id = OBJECT_ID(@fda_table)
	   AND OBJECT_SCHEMA_NAME(c.object_id) = N'dbo';

	IF @is_nullable = 1
	BEGIN
		SELECT @statement = @statement + ' UNION SELECT N'' ''';
	END;

	INSERT INTO dbo.t_dec_column
	        ( table_id ,
	          column_name ,
	          column_desc ,
	          label ,
	          display_order ,
	          is_readonly ,
	          is_mandatory ,
	          is_visible ,
	          control_type_id ,
	          default_value ,
	          statement ,
	          format ,
	          last_modified ,
	          modified_by
	        )
	VALUES  ( (SELECT table_id FROM t_dec_table WHERE table_code = N'dec_' + @fda_table ) , -- table_id - d_id
	          @column_name , -- column_name - d_name
	          dbo.fn_cvt_proper_ps (REPLACE(@column_name,N'_',N' '),1) , -- column_desc - d_description
	          dbo.fn_cvt_proper_ps (REPLACE(@column_name,N'_',N' '),1) , -- label - d_column_label
	          @display_order , -- display_order - d_display_nbr
	          CASE WHEN @is_identity = 1
					 THEN 1
				   WHEN @field = 0 
				     THEN 1 
				   ELSE 0 END , -- is_readonly - bit
	          CASE WHEN @is_identity = 1
					 THEN 0
				   WHEN @field = 0 
					 THEN 0
				   WHEN ISNULL(@is_nullable,1) = 1
					 THEN 0
				   WHEN EXISTS (SELECT 1   
					              FROM sys.default_constraints dc
					              JOIN sys.columns c
					                ON c.object_id = dc.parent_object_id
					               AND dc.parent_column_id = c.column_id
					             WHERE dc.parent_object_id = OBJECT_ID(@fda_table)
					               AND c.name = @column_name
					               AND (c.name LIKE N'char_cust_element%'
					                OR c.name LIKE N'num_cust_element%'))
					 THEN 0
				   ELSE 1 END , -- is_mandatory - tinyint
	          1 , -- is_visible - bit
	          @control_type_id , -- control_type_id - d_id
	          @default , -- default_value - d_statement
	          @statement , -- statement - d_statement
	          NULL , -- format - d_url
	          CURRENT_TIMESTAMP , -- last_modified - d_last_modified
	          CURRENT_USER  -- modified_by - d_user
	        )
	SELECT @display_order = @display_order + 1;
	FETCH NEXT FROM column_cursor INTO @column_name, @field;
END

CLOSE column_cursor;
DEALLOCATE column_cursor;

DROP TABLE #cmd;

RETURN(0);

GO


IF OBJECT_ID(N'p_adj_create_configuration_ps',N'P') IS NOT NULL
  PRINT N'PROCEDURE p_adj_create_configuration_ps has been created...';
ELSE
  PRINT N'PROCEDURE p_adj_create_configuration_ps has NOT been created due to errors...';

GO
