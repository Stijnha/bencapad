USE OneSumX;
GO

IF OBJECT_ID (N'p_dec_create_val_table_ps',N'P') IS NOT NULL
BEGIN
  PRINT 'Dropping procedure p_dec_create_val_table_ps...';
  DROP PROCEDURE p_dec_create_val_table_ps;
END
GO
PRINT 'Creating procedure p_dec_create_val_table_ps...';
GO
  
/**************************************************************************************************************  
 * Purpose: Generate data editor validation table creation script for @fda_table                              *  
 *                                                                                                            *  
 **************************************************************************************************************
  
*/  
CREATE PROCEDURE [dbo].p_dec_create_val_table_ps(  
		    @fda_table                  NVARCHAR(200),
            @val_table					NVARCHAR(200)   = NULL,  
            @execute_immediately        BIT             = 0,  
            @database_name              SYSNAME         = NULL,
            @debug                      BIT             = 0)
AS  
SET NOCOUNT ON;
  
---- declare local variables  
DECLARE @cdefault                  INT,
        @code_block                NVARCHAR(4000), 
        @col_name_start_pos        BIGINT = 2,
        @colorder                  SMALLINT,
        @column_name               NVARCHAR(MAX),
        @column_type               NVARCHAR(256),  
        @column_type2              NVARCHAR(256), 
        @database_id               INT,
        @datatype_start_pos        BIGINT = 50,
        @def                       NVARCHAR(200),
        @isnullable                BIT,
        @null_start_pos            BIGINT = 100,
        @spaces                    NVARCHAR(200),   
        @writeline                 NVARCHAR(MAX);

SELECT @database_id = database_id
  FROM sys.databases
 WHERE name = @database_name;
 
CREATE TABLE #code ([id]       INT            IDENTITY(1,1)  NOT NULL,  
                    code_block NVARCHAR(4000)                NOT NULL);

DECLARE @sysobjects TABLE (name                SYSNAME      NOT NULL,
                           [object_id]         INT          NOT NULL,
                           principal_id	       INT          NULL,
                           [schema_id]         INT          NOT NULL,
                           parent_object_id	   INT          NOT NULL,
                           [type]              CHAR(2)      NOT NULL,
                           type_desc           NVARCHAR(60) NULL,
                           create_date         DATETIME     NOT NULL,
                           modify_date         DATETIME     NOT NULL,
                           is_ms_shipped       BIT          NOT NULL,
                           is_published        BIT          NOT NULL,
                           is_schema_published BIT          NOT NULL);

INSERT #code (code_block) SELECT N'SELECT name,';
INSERT #code (code_block) SELECT N'       [object_id],';
INSERT #code (code_block) SELECT N'       principal_id,';
INSERT #code (code_block) SELECT N'       [schema_id],';
INSERT #code (code_block) SELECT N'       parent_object_id,';
INSERT #code (code_block) SELECT N'       type,';
INSERT #code (code_block) SELECT N'       type_desc,';
INSERT #code (code_block) SELECT N'       create_date,';
INSERT #code (code_block) SELECT N'       modify_date,';
INSERT #code (code_block) SELECT N'       is_ms_shipped,';
INSERT #code (code_block) SELECT N'       is_published,';
INSERT #code (code_block) SELECT N'       is_schema_published';
INSERT #code (code_block) SELECT N'  FROM ' + @database_name + '.sys.objects';
INSERT #code (code_block) SELECT N' WHERE name = ''' + @fda_table + ''';';
INSERT INTO @sysobjects
EXEC p_exec_resultset @stmt = N'SELECT code_block FROM #code ORDER BY id';

DELETE FROM #code;

-- we'll use @spaces and the input parameters to garantee that in the table   
-- creation script columnnames, datatypes and nullable information always   
-- start on the same position  
-- init @spaces  
SELECT @spaces = '                                                                                                                                                                                                                                             
          '  ;
  
-- ******************************************************************************  
-- * Perform some initial checks                                                *  
-- ******************************************************************************  
    
-- check if @fda_table exists  
IF NOT EXISTS (SELECT * FROM @sysobjects)  
  BEGIN  
    RAISERROR(N'Table %s does not exist.  No table creation code has been generated...', 16,-1, @fda_table);
    RETURN (-1);
  END  
  
SELECT @val_table = ISNULL(@val_table, @fda_table + N'_validation');
  
-- ******************************************************************************  
-- * Generate header                                                            *  
-- ******************************************************************************  
INSERT INTO #code VALUES(N'IF OBJECT_ID(N''' + @val_table + ''',N''U'') IS NOT NULL');
INSERT INTO #code VALUES(N'BEGIN');
INSERT INTO #code VALUES(N'  PRINT N''Dropping table ' + @val_table + '...'';');
INSERT INTO #code VALUES(N'  DROP TABLE ' + @val_table + ';' );
INSERT INTO #code VALUES(N'END');
INSERT INTO #code VALUES(N'GO');
INSERT INTO #code VALUES(N'');

INSERT INTO #code VALUES(N'PRINT N''Creating table ' + @val_table + '...'';'); 
INSERT INTO #code VALUES(N'');
INSERT INTO #code VALUES(N'CREATE TABLE [dbo].' + @val_table + '  ('  );

SELECT @writeline = SUBSTRING(@spaces, 1, @col_name_start_pos) + N'[line_nr]'  
                    + SUBSTRING(@spaces, 0, @datatype_start_pos - @col_name_start_pos - LEN('[line_nr]')) + N'd_id'  
                    + SUBSTRING(@spaces, 0, @null_start_pos - @datatype_start_pos - LEN('d_id')) + N'NOT NULL,'; 
INSERT INTO #code VALUES(@writeline);

SELECT @writeline = SUBSTRING(@spaces, 1, @col_name_start_pos) + N'[column_name]'  
                    + SUBSTRING(@spaces, 0, @datatype_start_pos - @col_name_start_pos - LEN('[column_name]')) + N'sysname'  
                    + SUBSTRING(@spaces, 0, @null_start_pos - @datatype_start_pos - LEN('sysname' ))  + N'NOT NULL,'; 
INSERT INTO #code VALUES(@writeline);

SELECT @writeline = SUBSTRING(@spaces, 1, @col_name_start_pos) + N'[message_type]'  
                    + SUBSTRING(@spaces, 0, @datatype_start_pos - @col_name_start_pos - LEN('[message_type]')) + N'd_id'  
                    + SUBSTRING(@spaces, 0, @null_start_pos - @datatype_start_pos - LEN('d_id')) + N'NOT NULL,';
INSERT INTO #code VALUES(@writeline);
    
SELECT @writeline = SUBSTRING(@spaces, 1, @col_name_start_pos) + N'[message]'  
                    + SUBSTRING(@spaces, 0, @datatype_start_pos - @col_name_start_pos - LEN('[message]')) + N'd_string'  
                    + SUBSTRING(@spaces, 0, @null_start_pos - @datatype_start_pos - LEN('d_string')) + N'NOT NULL,';
INSERT INTO #code VALUES(@writeline); 

SELECT @writeline = SUBSTRING(@spaces, 1, @col_name_start_pos) + N'[last_modified]'  
                    + SUBSTRING(@spaces, 0, @datatype_start_pos - @col_name_start_pos - LEN('[last_modified]')) + N'd_last_modified'  
                    + SUBSTRING(@spaces, 0, @null_start_pos - @datatype_start_pos - LEN('d_last_modified' ))  + N'NOT NULL'  
                    + N' CONSTRAINT df_' + CONVERT(NVARCHAR(200),@val_table) + N'_last_modified DEFAULT (getdate()),';
INSERT INTO #code VALUES(@writeline);

SELECT @writeline = SUBSTRING(@spaces, 1, @col_name_start_pos) + N'[modified_by]'  
                    + SUBSTRING(@spaces, 0, @datatype_start_pos - @col_name_start_pos - LEN('[modified_by]')) + N'd_user'  
                    + SUBSTRING(@spaces, 0, @null_start_pos - @datatype_start_pos - LEN('d_user' ))  + N'NOT NULL'  
                    + N' CONSTRAINT df_' + CONVERT(NVARCHAR(200),@val_table) + N'_modified_by DEFAULT SYSTEM_USER,';
INSERT INTO #code VALUES(@writeline);

INSERT INTO #code VALUES(SUBSTRING(@spaces, 1, @col_name_start_pos) + N'CONSTRAINT pk_' + @val_table + N' PRIMARY KEY CLUSTERED');
INSERT INTO #code VALUES(SUBSTRING(@spaces, 1, @col_name_start_pos) + N'(');
INSERT INTO #code VALUES(SUBSTRING(@spaces, 1, @col_name_start_pos) + N'[line_nr], [column_name]');
INSERT INTO #code VALUES(SUBSTRING(@spaces, 1, @col_name_start_pos) + N')');
INSERT INTO #code VALUES(N'  );');
INSERT INTO #code VALUES(N'');
INSERT INTO #code VALUES('  ');
-- ******************************************************************************  
-- * Generate footer of table creation script                                   *  
-- ******************************************************************************  
INSERT INTO #code VALUES(N'IF OBJECT_ID(N''' + @val_table + ''',N''U'') IS NOT NULL');
INSERT INTO #code VALUES(N'  PRINT N''Table ' + @val_table + ' has been created...'';');
INSERT INTO #code VALUES(N'ELSE');
INSERT INTO #code VALUES(N'  PRINT N''Error: table ' + @val_table + ' has NOT been created due to errors...'';');
INSERT INTO #code VALUES(N'');
INSERT INTO #code VALUES(N'GO')  
INSERT INTO #code VALUES(N'')  
IF @debug = 1  
BEGIN
  DECLARE c_code CURSOR LOCAL FAST_FORWARD   
  FOR SELECT code_block FROM #code ORDER BY [id];
  
  OPEN c_code;
  FETCH NEXT FROM c_code INTO @code_block;
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    PRINT @code_block;
    FETCH NEXT FROM c_code INTO @code_block;
  END;
  CLOSE c_code;
  DEALLOCATE c_code;
END  

IF @execute_immediately = 1  
BEGIN  
  EXEC p_exec_resultset 'SELECT code_block FROM #code ORDER BY [id]'; 
END  
  
RETURN(0); 
GO

IF  EXISTS (SELECT 1 FROM sys.objects WHERE [name] = 'p_dec_create_val_table_ps' AND [type] IN ('P', 'PC'))
  PRINT 'Procedure p_dec_create_val_table_ps has been created...';
ELSE
  PRINT 'Error: procedure p_dec_create_val_table_ps has NOT been created due to errors...';
GO