

IF OBJECT_ID (N'p_rpt_finale_details_ps',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_rpt_finale_details_ps...';
  DROP PROCEDURE p_rpt_finale_details_ps;
END
GO

PRINT N'Creating procedure p_rpt_finale_details_ps...';
GO
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************

CREATE PROCEDURE dbo.p_rpt_finale_details_ps (@table_name          NVARCHAR(200),
                                               @activity_date       DATETIME,
                                               @activity            NVARCHAR(200),
                                               @debug               BIT = 0,
								               @execute_immediately BIT = 1,
                                               @show_data           BIT)

AS

SET NOCOUNT ON;

DECLARE @code_block            NVARCHAR(4000) = N'',
        @ErrorSeverity         INT,
		@ErrorState            INT,
		@Msg                   NVARCHAR(4000),
		@ProcName              SYSNAME = OBJECT_NAME(@@PROCID),
        @Raiserror             INT,
        @ale_table_name        NVARCHAR(200),
		@ale_reporting_function_name NVARCHAR(200);

SELECT @ale_table_name = ale_history_table_name,
       @ale_reporting_function_name = ale_reporting_function_name
  FROM t_ale_config_table
 WHERE table_name = @table_name;

CREATE TABLE #code  (id  INT           NOT NULL IDENTITY(1, 1),  
                     txt NVARCHAR(MAX) NOT NULL);

DECLARE @tab TABLE (table_name SYSNAME,
                    column_name SYSNAME,
					key_ordinal SMALLINT,
					column_id SMALLINT,
					pk_name NVARCHAR(500));

INSERT INTO @tab                    
       (
	   table_name,
	   column_name,
	   key_ordinal,
	   column_id,
	   pk_name
	   )
SELECT @table_name AS table_name,
       c.name AS column_name,
	   ic.key_ordinal AS key_ordinal,
	   c.column_id,
	   kc.name AS pk_name
  FROM sys.columns c
  JOIN sys.key_constraints kc
    ON c.object_id = kc.parent_object_id
   AND kc.type = 'PK'
  JOIN sys.indexes i
    ON c.object_id = i.object_id
   AND kc.name = i.name
  LEFT JOIN sys.index_columns ic
    ON i.index_id = ic.index_id
   AND c.object_id = ic.object_id
   AND c.column_id = ic.column_id
 WHERE c.object_id = OBJECT_ID(@table_name)
 ORDER BY c.column_id;

IF @show_data = 1
BEGIN
    DECLARE @pk_string NVARCHAR(MAX) = N'';

    SELECT @pk_string = @pk_string + t2.column_name + CASE WHEN t2.key_ordinal <> MAX(t2.key_ordinal) OVER () THEN N',' ELSE N'' END
      FROM @tab t2 
     WHERE key_ordinal IS NOT NULL
     ORDER BY key_ordinal

    INSERT #code (txt) SELECT N'WITH cte AS (';
	INSERT #code (txt) SELECT N'SELECT CONVERT(NVARCHAR, ale.ale_modification_timestamp, 107) AS Date,';
	INSERT #code (txt) SELECT N'       CASE WHEN CHARINDEX (N<q>I<q>, ale_modification_indicator )> 0 THEN N<q>Insert<q>';
	INSERT #code (txt) SELECT N'            WHEN CHARINDEX (N<q>D<q>, ale_modification_indicator )> 0 THEN N<q>Delete<q>';
	INSERT #code (txt) SELECT N'			WHEN CHARINDEX (N<q>U<q>, ale_modification_indicator )> 0 THEN N<q>Update<q>';
	INSERT #code (txt) SELECT N'       END AS Action,';
	INSERT #code (txt) SELECT N'	   CASE WHEN CHARINDEX (N<q>U<q>, ale_modification_indicator )> 0 THEN N<q>Before<q> ELSE N<q><q> END AS SubAction,';
	INSERT #code (txt) SELECT N'       ale.ale_modification_timestamp,';
	INSERT #code (txt) SELECT N'       u.first_name + N'' '' + u.last_name			AS [user],';
	INSERT #code (txt) SELECT CASE WHEN column_name = N'last_modified'
	                                 THEN N'       ale.ale_modification_timestamp AS last_modified'
								   WHEN column_name = N'modified_by'
								     THEN N'       COALESCE(ale.modified_by, au.username, fn.modified_by) AS modified_by'
								   WHEN key_ordinal IS NOT NULL
								     THEN N'       ale.' + column_name 
								   ELSE N'       fn.' + column_name 
							  END +
	                          CASE WHEN column_id <> MAX(column_id) OVER () THEN N',' ELSE N'' END
	  FROM @tab
	 ORDER BY column_id;
	INSERT #code (txt) SELECT N'  FROM ' + @ale_table_name + N' ale';
	INSERT #code (txt) SELECT N'  CROSS APPLY dbo.' + @ale_reporting_function_name + N' (';
	INSERT #code (txt) SELECT N'  		CASE WHEN CHARINDEX(N''I'',ale.ale_modification_indicator) > 0 ';
	INSERT #code (txt) SELECT N'  				THEN DATEADD(MILLISECOND,5,ale.ale_modification_timestamp)';
	INSERT #code (txt) SELECT N'  		ELSE ale.ale_modification_timestamp';
	INSERT #code (txt) SELECT N'  		END,';
	INSERT #code (txt) SELECT N'       ale.' + column_name +
	                          CASE WHEN key_ordinal <> MAX(key_ordinal) OVER () THEN N',' ELSE N') fn' END
	  FROM sys.parameters p
	  JOIN @tab t
		ON p.name = N'@' + t.column_name
	 WHERE OBJECT_NAME(p.object_id) = @ale_reporting_function_name
	   AND t.key_ordinal IS NOT NULL
	 ORDER BY p.parameter_id;

	INSERT #code (txt) SELECT N'  LEFT JOIN t_ale_event_log ael';
	INSERT #code (txt) SELECT N'    ON ale.event_log_id = ael.event_log_id';
	INSERT #code (txt) SELECT N'  LEFT JOIN t_ale_user au';
	INSERT #code (txt) SELECT N'    ON ael.ale_user_id = au.ale_user_id';
	INSERT #code (txt) SELECT N'  LEFT JOIN t_sec_users u';
	INSERT #code (txt) SELECT N'    ON UPPER(au.username) = u.user_name_ucase';
	INSERT #code (txt) SELECT N' WHERE CONVERT(NVARCHAR, ale.ale_modification_timestamp, 107) = N<q>' + CONVERT(NVARCHAR,@activity_date,107) + N'<q>';
	INSERT #code (txt) SELECT N' UNION ALL';
	INSERT #code (txt) SELECT N'SELECT CONVERT(NVARCHAR, ale.ale_modification_timestamp, 107) AS Date,';
	INSERT #code (txt) SELECT N'       N<q>Update<q> AS Action,';
	INSERT #code (txt) SELECT N'	   N<q>After<q> AS SubAction,';
	INSERT #code (txt) SELECT N'       DATEADD(MILLISECOND,5,ale.ale_modification_timestamp) AS ale_modification_timestamp,';
	INSERT #code (txt) SELECT N'       u.first_name + N'' '' + u.last_name			AS [user],';
	INSERT #code (txt) SELECT CASE WHEN column_name = N'last_modified'
	                                 THEN N'       ale.ale_modification_timestamp AS last_modified'
								   WHEN column_name = N'modified_by'
								     THEN N'       COALESCE(ale.modified_by, au.username, fn.modified_by) AS modified_by'
								   WHEN key_ordinal IS NOT NULL
								     THEN N'       ale.' + column_name 
								   ELSE N'       fn.' + column_name 
							  END +
	                          CASE WHEN column_id <> MAX(column_id) OVER () THEN N',' ELSE N'' END
	  FROM @tab
	 ORDER BY column_id;
	INSERT #code (txt) SELECT N'  FROM ' + @ale_table_name + N' ale';
	INSERT #code (txt) SELECT N'  CROSS APPLY dbo.' + @ale_reporting_function_name + N' (';
	INSERT #code (txt) SELECT N'  		DATEADD(MILLISECOND,5,ale.ale_modification_timestamp),';
	INSERT #code (txt) SELECT N'        ale.' + column_name +
	                          CASE WHEN key_ordinal <> MAX(key_ordinal) OVER () THEN N',' ELSE N') fn' END
	  FROM sys.parameters p
	  JOIN @tab t
		ON p.name = N'@' + t.column_name
	 WHERE OBJECT_NAME(p.object_id) = @ale_reporting_function_name
	   AND t.key_ordinal IS NOT NULL
	 ORDER BY p.parameter_id
	INSERT #code (txt) SELECT N'  LEFT JOIN t_ale_event_log ael';
	INSERT #code (txt) SELECT N'    ON ale.event_log_id = ael.event_log_id';
	INSERT #code (txt) SELECT N'  LEFT JOIN t_ale_user au';
	INSERT #code (txt) SELECT N'    ON ael.ale_user_id = au.ale_user_id';
	INSERT #code (txt) SELECT N'  LEFT JOIN t_sec_users u';
	INSERT #code (txt) SELECT N'    ON UPPER(au.username) = u.user_name_ucase';
	INSERT #code (txt) SELECT N' WHERE CONVERT(NVARCHAR, ale.ale_modification_timestamp, 107) = N<q>' + CONVERT(NVARCHAR,@activity_date,107) + N'<q>';
	INSERT #code (txt) SELECT N'   AND CHARINDEX (N<q>U<q>, ale_modification_indicator)> 0';
	INSERT #code (txt) SELECT N')';
	INSERT #code (txt) SELECT N'';
	INSERT #code (txt) SELECT N'SELECT Date,';
	INSERT #code (txt) SELECT N'       Action,';
	INSERT #code (txt) SELECT N'       SubAction,';
	INSERT #code (txt) SELECT N'       ale_modification_timestamp,';
	INSERT #code (txt) SELECT N'       [user],';
	INSERT #code (txt) SELECT N'       ' + column_name + N' AS column' + CONVERT(NVARCHAR(5),column_id) +
	                          CASE WHEN column_id < 50 THEN N',' ELSE N'' END
	  FROM @tab
	 ORDER BY column_id;

    WITH cte AS (
    SELECT COUNT(*) + 1 AS cnt
      FROM @tab
    UNION ALL
    SELECT cnt + 1
      FROM cte
     WHERE cnt <50)

	INSERT #code (txt) SELECT N'       <q><q> AS column' + CONVERT(NVARCHAR(5),cnt) + CASE WHEN cnt < 50 THEN N',' ELSE N'' END
	  FROM cte;

	INSERT #code (txt) SELECT N'  FROM (';
	INSERT #code (txt) SELECT N'       SELECT Date,';
	INSERT #code (txt) SELECT N'              Action,';
	INSERT #code (txt) SELECT N'              SubAction,';
	INSERT #code (txt) SELECT N'              ale_modification_timestamp,';
	INSERT #code (txt) SELECT N'              [user],';

    WITH cte AS (
    SELECT 1 AS cnt
    UNION ALL
    SELECT cnt + 1 AS cnt
      FROM cte
     WHERE cnt < 10)

	INSERT #code (txt) 
    SELECT cmd + CASE WHEN ROW_NUMBER() OVER (ORDER BY column_id, cnt) < COUNT(*) OVER () THEN N',' ELSE N'' END
	  FROM (
	SELECT N'              c.' + column_name + N' AS ' + column_name AS cmd,
		   column_id,
		   0 AS cnt
	  FROM @tab
     WHERE 1=1
	    OR column_name IN (N'last_modified',N'modified_by')
	) sub
	 ORDER BY column_id, cnt;

	INSERT #code (txt) SELECT N'         FROM cte c';
	INSERT #code (txt) SELECT N'       ) sub';
	INSERT #code (txt) SELECT N' WHERE sub.Action = <q>' + @activity 
	+ N'<q>';

    INSERT #code (txt) SELECT CASE WHEN key_ordinal = MIN(key_ordinal) OVER () THEN N' ORDER BY sub.' + column_name
	                          ELSE N'          sub.' + column_name END +
							  N','
	  FROM @tab
	 WHERE key_ordinal IS NOT NULL
	 ORDER BY key_ordinal;
	INSERT #code (txt) SELECT N'          sub.ale_modification_timestamp;';
END
ELSE -- IF @show_data = 1
BEGIN
	INSERT #code (txt) SELECT N'SELECT N<q>Date<q> AS Date,';
	INSERT #code (txt) SELECT N'       N<q>Action<q> AS Action,';
	INSERT #code (txt) SELECT N'       N<q>SubAction<q> AS SubAction,';
	INSERT #code (txt) SELECT N'       N<q>ale_modification_timestamp<q> AS ale_modification_timestamp,';
	INSERT #code (txt) SELECT N'       N<q>user<q> AS [user],';
	INSERT #code (txt) SELECT N'       N<q>' + column_name + N'<q> AS column' + CONVERT(NVARCHAR(5), column_id) +
	                          CASE WHEN column_id < 50
								     THEN N',' ELSE N'' END
	  FROM @tab
	 ORDER BY column_id;

    WITH cte AS (
	SELECT CONVERT(SYSNAME,N'') AS column_name,
	       CONVERT(SMALLINT,MAX(column_id) + 1) AS column_id
	  FROM @tab
	UNION ALL
	SELECT CONVERT(SYSNAME,N'') AS column_name,
	       CONVERT(SMALLINT,column_id + 1) AS column_id
	  FROM cte
	 WHERE column_id < 50)


	INSERT #code (txt) 
	SELECT N'       N<q>' + column_name + N'<q> AS column' + CONVERT(NVARCHAR(5), column_id)+
	                          CASE WHEN column_id < 50
								     THEN N',' ELSE N';' END
	   FROM cte
	 ORDER BY column_id;
END
UPDATE #code SET txt = REPLACE(txt, '<q>', '''')  

IF @debug = 1  
BEGIN
  DECLARE c_code CURSOR LOCAL FAST_FORWARD   
  FOR SELECT txt FROM #code ORDER BY [id];
  
  OPEN c_code;
  FETCH NEXT FROM c_code INTO @code_block;
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    PRINT @code_block;
    FETCH NEXT FROM c_code INTO @code_block;
  END;
  CLOSE c_code;
  DEALLOCATE c_code;
END  

IF @execute_immediately = 1  
BEGIN  
  EXEC p_exec_resultset @stmt = N'SELECT txt FROM #code ORDER BY id;';
END  

DROP TABLE #code;

RETURN(0)  

GO

IF OBJECT_ID (N'p_rpt_finale_details_ps',N'P') IS NOT NULL
  PRINT N'Procedure p_rpt_finale_details_ps has been created';
ELSE
  PRINT N'Procedure p_rpt_finale_details_ps has not been created due to errors';

GO
