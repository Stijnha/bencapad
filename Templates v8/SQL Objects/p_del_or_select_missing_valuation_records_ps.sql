
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_del_or_select_missing_valuation_records_ps' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_del_or_select_missing_valuation_records_ps] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_del_or_select_missing_valuation_records_ps] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_del_or_select_missing_valuation_records_ps]...'
GO
ALTER PROCEDURE [dbo].[p_del_or_select_missing_valuation_records_ps]
		(@etl_table_name SYSNAME ,
        @adj_id INT=0,
		@cob_date DATETIME,
		@debug BIT = 1,
		@delete_or_select NVARCHAR(10)='S',
		@execute_immediately BIT = 0)

AS

SET NOCOUNT ON;

-- local parameters
DECLARE @fda_table_name SYSNAME;
DECLARE @cmd_text NVARCHAR(MAX)
DECLARE @column_name NVARCHAR(MAX)

CREATE TABLE #sql 
(
	id INT IDENTITY (1,1),
	cmd NVARCHAR(MAX)
);

SET @fda_table_name = REPLACE(@etl_table_name,'t_etl_adj_data_','t_');

-- check if it's a valuation table
IF @fda_table_name NOT LIKE N'%valuation' and @fda_table_name NOT LIKE N'%back_office_balance%'
BEGIN
	PRINT ' Do nothing';
	GOTO end_of_script;
END;

IF @delete_or_select NOT IN ( 'S','D')
BEGIN
	PRINT ' Do nothing';
	GOTO end_of_script;
END;


IF @delete_or_select = 'D'
BEGIN
	INSERT INTO #sql(cmd) VALUES ('DELETE ' + @fda_table_name)
END

IF @delete_or_select = 'S'
BEGIN
	INSERT INTO #sql(cmd) VALUES ('SELECT * ')
END

INSERT INTO #sql(cmd) VALUES ('  FROM ' + @fda_table_name + ' fda');
INSERT INTO #sql(cmd) VALUES ('  LEFT JOIN ' + @etl_table_name + N' etl');
INSERT INTO #sql(cmd) VALUES ('    ON 1=1');
INSERT INTO #sql(cmd) VALUES ('   AND etl.adjustment_id='+CAST(@adj_id AS VARCHAR));

-- This needs to dynamically look for the PK columns in the fda tables
BEGIN
	DECLARE pk_cur CURSOR FOR
	SELECT COL_NAME(ic.OBJECT_ID, COLUMN_ID) 
	  FROM sys.index_columns ic
	  JOIN sys.indexes i 
	    ON ic.object_id=i.object_id
	 WHERE i.object_id=OBJECT_ID(@fda_table_name)
       AND i.is_primary_key=1;

	OPEN pk_cur;
	FETCH NEXT FROM pk_cur INTO @column_name;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--	PRINT @column_name
		INSERT INTO #sql(cmd) VALUES ('   AND fda.'+@column_name+' = etl.'+@column_name);
		FETCH NEXT FROM pk_cur INTO @column_name;
	END;
	CLOSE pk_cur;
	DEALLOCATE pk_cur;
END


IF @fda_table_name LIKE N'%valuation' 
BEGIN
	INSERT INTO #sql(cmd) VALUES (' WHERE fda.valuation_date = ' +''''+ CONVERT(varchar,@cob_date,112)+''''); 
END
IF @fda_table_name LIKE N'%back_office_balance%' 
BEGIN
	INSERT INTO #sql(cmd) VALUES (' WHERE fda.balance_date = ' + ''''+ CONVERT(varchar,@cob_date,112)+'''');  -- put something specific for bob 
END

INSERT INTO #sql(cmd) VALUES ('   AND etl.line_nr IS NULL');

end_of_script:

IF @debug = 1
BEGIN
	DECLARE cur CURSOR FOR
	SELECT cmd
	  FROM #sql
	 ORDER BY id;
	OPEN cur;
	FETCH NEXT FROM cur INTO @cmd_text;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT @cmd_text
		FETCH NEXT FROM cur INTO @cmd_text;
	END;
	CLOSE cur;
	DEALLOCATE cur;
END

IF @execute_immediately = 1
BEGIN
	EXEC p_exec_resultset @stmt = 'SELECT cmd FROM #sql ORDER BY id'
END

DROP TABLE #sql;



GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_del_or_select_missing_valuation_records_ps' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_del_or_select_missing_valuation_records_ps] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_del_or_select_missing_valuation_records_ps] has NOT been altered due to errors!'
END
GO
