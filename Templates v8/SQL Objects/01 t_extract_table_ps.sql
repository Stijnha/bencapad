--USE OneSumX;
GO

IF OBJECT_ID('t_extract_table_ps','U') IS NOT NULL
BEGIN
  PRINT 'Dropping table t_extract_table_ps...';
  DROP TABLE [dbo].[t_extract_table_ps]
END
GO

PRINT 'Creating table t_extract_table_ps...';
-- ****************************************************************************************
-- * Purpose: This is a mapping table between the source system and source staging tables *
-- *																					  *
-- * Notes:																				  *
-- ****************************************************************************************

CREATE TABLE [dbo].[t_extract_table_ps](
    extract_id			d_id IDENTITY (1,1)	NOT NULL,
	extract_description	d_value				NOT NULL,
	source_system		d_source_system		NULL,
	source_name			d_value				NOT NULL,
	source_format		d_value				NOT NULL,
	source_delimiter	d_value				NOT NULL,
	source_column_list	d_value				NOT NULL,
	staging_database	d_value				NOT NULL,
	staging_table		d_value				NOT NULL,
	staging_column_list	NVARCHAR(MAX)		NOT NULL,
	is_enabled			d_bit				NOT NULL,
	execution_order		d_id				NOT NULL,
	last_modified		DATETIME			NOT NULL CONSTRAINT df1_extract_table_ps DEFAULT CURRENT_TIMESTAMP,
	modified_by			d_user				NOT NULL CONSTRAINT df2_extract_table_ps DEFAULT (dbo.fn_user_default()),
	CONSTRAINT pk_extract_table_ps PRIMARY KEY CLUSTERED (extract_id)
);

ALTER TABLE dbo.[t_extract_table_ps] ADD CONSTRAINT uq1_extract_table_ps UNIQUE (extract_description);
GO

IF OBJECT_ID('tr_extract_table_ps', 'TR') IS NOT NULL
	DROP Trigger tr_extract_table_ps;
GO
CREATE TRIGGER tr_extract_table_ps ON [t_extract_table_ps]
AFTER INSERT, UPDATE
AS
BEGIN
	IF @@ROWCOUNT = 0 OR (UPDATE(is_enabled) AND NOT UPDATE(staging_column_list))
	  RETURN;

	DECLARE @tab NVARCHAR(2000);

	DECLARE cur_stag CURSOR FOR
	SELECT staging_table
	  FROM Inserted;

	OPEN cur_stag;
	FETCH NEXT FROM cur_stag INTO @tab;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC p_gen_staging_ps @table_name = @tab;
		FETCH NEXT FROM cur_stag INTO @tab;
	END;
	CLOSE cur_stag;
	DEALLOCATE cur_stag;



END
GO


IF OBJECT_ID('t_extract_table_ps','U') IS NOT NULL
  PRINT 'Table t_extract_table_ps has been created...';
ELSE
  PRINT 'Table t_extract_table_ps has NOT been created due to errors...';

GO
