USE OneSumX;
GO

IF OBJECT_ID('t_extract_etl_ps','U') IS NOT NULL
BEGIN
  PRINT 'Dropping table t_extract_etl_ps...';
  DROP TABLE [dbo].[t_extract_etl_ps]
END
GO

PRINT 'Creating table t_extract_etl_ps...';
-- ****************************************************************************************
-- * Purpose: This is a mapping table between the source system and source staging tables *
-- *																					  *
-- * Notes:																				  *
-- ****************************************************************************************

CREATE TABLE [dbo].[t_extract_etl_ps](
    extract_id			d_id				NOT NULL,
	[type_id]			d_id				NOT NULL,
	is_enabled			d_bit				NOT NULL,
	execution_order		d_id				NOT NULL,
	last_modified		DATETIME			NOT NULL CONSTRAINT df1_extract_etl_ps DEFAULT CURRENT_TIMESTAMP,
	modified_by			d_user				NOT NULL CONSTRAINT df2_extract_etl_ps DEFAULT (dbo.fn_user_default()),
	CONSTRAINT pk_extract_etl_ps PRIMARY KEY CLUSTERED (extract_id, [type_id])
);

IF OBJECT_ID('t_extract_etl_ps','U') IS NOT NULL
  PRINT 'Table t_extract_etl_ps has been created...';
ELSE
  PRINT 'Table t_extract_etl_ps has NOT been created due to errors...';

GO
