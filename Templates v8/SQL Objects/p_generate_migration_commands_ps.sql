USE <Database>;
GO

IF OBJECT_ID (N'dbo.<procedure_name>',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure <procedure_name>...';
  DROP PROCEDURE dbo.<procedure_name>;
END
GO

PRINT N'Creating procedure <procedure_name>...';
GO

-- **************************************************************************************************************************
-- * Purpose: Procedure to generate sqlcmd scripts based on the.															*
-- *																														*
-- * Input  : @source_folder		: Root folder of the SQL source files.													*
-- *          @destination_server	: Name of the server the scripts should be deployed to.									*
-- *          @destination_db		: Name of the database the script should be	deployed to.								*
-- *          @bat_file_folder		: Name of the folder where you want to generate the bat files							*
-- *		  @bat_file_name		: Name of the bat file to be created													*
-- *		  @out_file_name		: Name of the log file to be generated													*
-- **************************************************************************************************************************

CREATE PROCEDURE dbo.p_generate_migration_commands_ps
(
	@source_folder        NVARCHAR(256),
	@destination_folder   NVARCHAR(256),
	@destination_server   NVARCHAR(128),
	@destination_db       NVARCHAR(128),
	@bat_file_folder      NVARCHAR(256) = NULL,
	@bat_file_name        NVARCHAR(256) = NULL,
	@out_file_name        NVARCHAR(256) = N'log.txt'
)

AS

SET NOCOUNT ON;

DECLARE @code_block				NVARCHAR(4000) = N'',
		@ErrorSeverity			INT,
		@ErrorState				INT,
		@Msg					NVARCHAR(4000),
		@ProcName				SYSNAME = OBJECT_NAME(@@PROCID),
		@Raiserror				INT,
		@Rowcount				INT,
		@step_guid				UNIQUEIDENTIFIER,
		@event_log_guid			UNIQUEIDENTIFIER,
		@xml_log				XML,
		@correlation_guid		UNIQUEIDENTIFIER,
		@cob_date				DATETIME,
		@bat_full_path			NVARCHAR(256);
           
BEGIN TRY

	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE id = OBJECT_ID(N'tempdb..#files'))
	BEGIN
		DROP TABLE #files;
	END

	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE id = OBJECT_ID(N'tempdb..#full_files'))
	BEGIN
		DROP TABLE #full_files;
	END

	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE id = OBJECT_ID(N'tempdb..#commands'))
	BEGIN
		DROP TABLE #commands;
	END

	CREATE TABLE #files
	(
		id              INT IDENTITY(1, 1),
		subdirectory    SYSNAME,
		parent_id       INT NULL,
		depth           INT,
		is_file         BIT
	);

	CREATE TABLE #commands
	(
		lvl				INT NULL,
		cmd				NVARCHAR(MAX) NULL
	);

	-- init local variables
	IF @bat_file_folder IS NOT NULL
		SELECT @bat_full_path = @bat_file_folder + @bat_file_name;
	ELSE
		SELECT @bat_full_path = @source_folder + @bat_file_name;

	-- Do work

	/*
		Step 1: List all objects in the given folder (including subfolders)
	*/

	PRINT 'Reading source file';   

	INSERT INTO #files (subdirectory, depth, is_file)
	EXEC xp_dirtree @source_folder, 0, 1;


	/*
		Step 2: Create a parent-child hierarchy that represents the folder structure
	*/

	PRINT 'Creating source file hierarchy';

	UPDATE #files SET parent_id = parents.parent_id
	FROM #files f
	JOIN (  SELECT id, MAX(parent_id) as parent_id
			  FROM (SELECT f.id, f.subdirectory, p.id as parent_id, f.depth, f.is_file 
					  from #files f
					  left join #files p
						on p.depth = f.depth - 1
					   and f.id > p.id
				   ) data
			 group by id, subdirectory, depth, is_file
		  ) as parents
	on f.id = parents.id;


	/*
		Step 3: Recursively construct the folder path for the files
	*/

	PRINT 'Constructing full file path';

	WITH files
		AS
		 ( SELECT t.id,
				  CASE WHEN t.is_file = 0 THEN t.subdirectory ELSE '' END as subdirectory,
				  CASE WHEN t.is_file = 1 THEN t.subdirectory ELSE '' END as file_name,
				  t.depth,
				  t.parent_id,
				  t.is_file
			 FROM #files AS t
			WHERE parent_id IS NULL

			UNION ALL

		   SELECT t.id,
				  CASE WHEN t.is_file = 0 THEN CAST(parent.subdirectory + '\' + t.subdirectory AS sysname) ELSE parent.subdirectory END as subdirectory,
				  CASE WHEN t.is_file = 1 THEN t.subdirectory ELSE '' END as file_name,
				  t.depth,
				  t.parent_id,
				  t.is_file
			 FROM #files AS t
			 JOIN files AS parent
			   ON t.parent_id = parent.id
		 )
     
	SELECT subdirectory as subdirectory, file_name
	  INTO #full_files
	  FROM files 
	 WHERE is_file = 1
	 ORDER BY id;


	/*
		Step 4a: For tables and initial data scripts, order by dependency.
	*/

	PRINT 'Creating table dependencies';

	INSERT INTO #commands (lvl,cmd)
	SELECT lvl = 0,
		   cmd = N''

	UNION ALL

	SELECT lvl = 1,
		   cmd = N'echo %date% %time% > ' + @out_file_name;



	WITH fks AS ( -- Only foreign keys that are related to site-specific tables are relevant

		SELECT DISTINCT 
			   on_table       = on_table.name,
			   against_table  = against_table.name
		  FROM sysforeignkeys fk
		  JOIN sysobjects on_table
			ON fk.fkeyid = on_table.id
		  JOIN sysobjects against_table
			ON fk.rkeyid = against_table.id
		  JOIN #full_files f
			ON (   ( on_table.name = REPLACE(f.file_name, '.sql', '')
					 AND f.file_name like 't_%')
				OR ( on_table.name = REPLACE(REPLACE(f.file_name, '.sql', ''), 'dat_', 't_')
					 AND f.file_name like 'dat_%'))
		 WHERE 1 = 1
		   AND against_table.type = 'U'
		   AND on_table.type = 'U'
		   AND on_table.name <> against_table.name
		)

	,mydata as ( -- All Summix tables as baseline (there might be FKs to standard tables)

		SELECT on_table      = o.name,
			   against_table = fks.against_table
		  FROM sys.objects o
		  LEFT JOIN fks
			ON  o.name = fks.on_table
		 WHERE 1 = 1
		   AND o.type = 'U'
		)

	,myrecursion as (

		-- base case
		SELECT table_name  = on_table,
			   level       = 1
		  FROM mydata
		 WHERE against_table IS NULL

		-- recursive case
		UNION ALL 
    
		SELECT table_name  = on_table,
			   level       = r.level + 1
		  FROM mydata d
		  JOIN myrecursion r
			ON d.against_table = r.table_name
	)

	/*
		Step 5: Create the commands, based on dependency for tables and just a listing for other objects
	*/
	, cte AS (
		SELECT DISTINCT 3000 + DENSE_RANK() OVER (ORDER BY MAX(level), file_name) AS lvl,
			   CASE p.a WHEN 1 THEN 'echo start script ' + file_name + '>> ' + @out_file_name
						WHEN 2 THEN REPLACE('sqlcmd -S "' + @destination_server + '" -d ' + @destination_db + ' -i "' + @destination_folder + subdirectory + '\' + file_name + '"' + @out_file_name, '\\', '\')
						WHEN 3 THEN 'echo end script ' + file_name + '>> ' + @out_file_name
						WHEN 4 THEN 'echo **************************************************************' + '>> ' + @out_file_name END AS fle_name,
			   p.a AS cnt
		  FROM (SELECT file_name, subdirectory FROM #full_files) f
		  LEFT JOIN myrecursion
			ON table_name = REPLACE(f.file_name, '.sql', '')

		 CROSS APPLY (VALUES (1),(2),(3),(4)) p(a)
		 WHERE @destination_db = db_name()
		   AND f.file_name LIKE 't_%'
		 GROUP BY f.file_name,
			   f.subdirectory,
			   p.a
		UNION ALL
		SELECT DISTINCT 4000 + DENSE_RANK() OVER (ORDER BY MAX(level), file_name) AS lvl,
			   CASE p.a WHEN 1 THEN 'echo start script ' + file_name + '>> ' + @out_file_name
						WHEN 2 THEN REPLACE('sqlcmd -S "' + @destination_server + '" -d ' + @destination_db + ' -i "' + @destination_folder + subdirectory + '\' + file_name + '"' + @out_file_name, '\\', '\')
						WHEN 3 THEN 'echo end script ' + file_name + '>> ' + @out_file_name
						WHEN 4 THEN 'echo **************************************************************' + '>> ' + @out_file_name END AS fle_name,
			   p.a AS cnt
	   
		  FROM myrecursion
			-- Filter based on the relevant scripts only
		  JOIN (SELECT file_name, subdirectory FROM #full_files) f
			ON 1=1
		   AND table_name = REPLACE(REPLACE(f.file_name, '.sql', ''), 'dat_', 't_')
		   AND f.file_name LIKE 'dat_%'
		 CROSS APPLY (VALUES (1),(2),(3),(4)) p(a)
		 WHERE @destination_db = db_name()
		 GROUP BY f.file_name,
			   f.subdirectory,
			   p.a
	 );
 
	INSERT INTO #commands (lvl,cmd)
	SELECT lvl + ROW_NUMBER() OVER (ORDER BY lvl, cnt), fle_name
	  FROM cte
	 ORDER BY lvl, cnt;

	PRINT 'Adding non-table results';

	WITH cte AS (
	SELECT CASE WHEN file_name like 'd[_]%'  THEN 1000 -- data types
				WHEN file_name like 'sq[_]%' THEN 2000 -- sequences
				WHEN file_name like 'idx%'   THEN 5000 -- indexes
				WHEN file_name like 'fn[_]%' THEN 6000 -- functions
				WHEN file_name like 'v[_]%'  THEN 7000 -- views
				WHEN file_name like 'p[_]%'  THEN 8000 -- procedures
				WHEN file_name like 's[_]%'  THEN 9000 -- scripts
				WHEN file_name like 'tr[_]%' THEN 10000 -- triggers
				ELSE 20000 END + DENSE_RANK() OVER (ORDER BY file_name) AS lvl,
		   CASE p.a WHEN 1 THEN 'echo start script ' + file_name + '>> ' + @out_file_name
					WHEN 2 THEN REPLACE('sqlcmd -S "' + @destination_server + '" -d ' + @destination_db + ' -i "' + @destination_folder + subdirectory + '\' + file_name + '"' + @out_file_name, '\\', '\')
					WHEN 3 THEN 'echo end script ' + file_name + '>> ' + @out_file_name
					WHEN 4 THEN 'echo **************************************************************' + '>> ' + @out_file_name END AS file_name,
		   p.a AS cnt
	  FROM #full_files
	 CROSS APPLY (VALUES (1),(2),(3),(4)) p(a)
	 WHERE ((file_name NOT LIKE 't[_]%' 
	   AND file_name NOT LIKE 'dat[_]%'
	   AND file_name LIKE N'%.sql'
	   AND @destination_db = db_name())
		OR @destination_db <> db_name())
	   AND file_name LIKE N'%.sql');

	INSERT INTO #commands (lvl,cmd)
	SELECT lvl + ROW_NUMBER() OVER (ORDER BY lvl, cnt), file_name
	  FROM cte
	 ORDER BY lvl, cnt;

	PRINT 'Preparing results';

	PRINT 'Adding error handling';

	INSERT INTO #commands (lvl,cmd)
	SELECT 29999 AS lvl, 'goto :EOF' AS cmd
	UNION
	SELECT 30000, ':error'
	UNION ALL
	SELECT 30001, 'echo ERROR'
	UNION ALL
	SELECT 30002, 'exit /b';

	--SELECT * from #commands  ORDER BY lvl;

	IF EXISTS (
	SELECT 1
	  FROM #commands 
	 WHERE lvl NOT IN (0,1,29999,30000,30001,30002))
	BEGIN

		IF @bat_file_name IS NULL
			SELECT cmd from #commands  ORDER BY lvl;
		ELSE
		BEGIN
			EXEC p_write_resultset
							 @statement              = N'SELECT cmd from #commands ORDER BY lvl',
							 @filename               = @bat_full_path,
							 @write_mode             = N'W',
							 @null_value             = N'<NULL>',
							 @add_record_count_ind   = 0,
							 @verbosity_lvl          = 0
                     
			PRINT 'Output file saved as ' + @bat_full_path;
		END;
	END;

	DROP TABLE #commands;
	GOTO FINISH;


END TRY
BEGIN CATCH 
	SELECT @Msg = ERROR_MESSAGE()
	RAISERROR (@Msg, 18, 1); 

	GOTO FINISH_ERROR;
  
END CATCH

FINISH:                     
	RETURN(0);

FINISH_ERROR:
	RETURN(1);


GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_generate_migration_commands_qtc' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_generate_migration_commands_qtc] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_generate_migration_commands_qtc] has NOT been altered due to errors!'
END
GO
