SET NOCOUNT ON;

SELECT *
  FROM t_fsp_application
 WHERE application_name = N'OneSumX';

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_application',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'application_name = N''OneSumX''',
     @field_existence_check          = N'application_name',
     @field_ignore                   = N'application_id, last_modified, modified_by',
     @sql_after                      = N'UPDATE t_fsp_application SET is_default = 0 WHERE application_name <> N''OneSumX'';';

SELECT *
  FROM t_fsp_menu_item
 WHERE application_id IN (SELECT application_id FROM t_fsp_application WHERE application_name = N'OneSumX')
ORDER BY parent_id;





EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_menu_item',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'application_id IN (SELECT application_id FROM t_fsp_application WHERE application_name = N''OneSumX'')',
     @field_search                   = N'application_id; parent_id = item_id',
     @table_search                   = N't_fsp_application; t_fsp_menu_item',
     @field_replace                  = N'application_name; item_code',
	 @field_replace_allow_doubles    = 1,
     @field_ignore                   = N'item_id, last_modified, modified_by',
	 @field_existence_check          = N'item_code, application_id',
	 @order_by                       = N'parent_id'; 
/* Empty
SELECT *
  FROM t_fsp_content
 WHERE content_name IN (SELECT action_value
                          FROM t_fsp_menu_item fmi
						  JOIN t_fsp_application fa
						    ON fmi.application_id = fa.application_id
                         WHERE fa.application_name = N'OneSumX'
						   AND fmi.action_type = N'Content');

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_content',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'content_name IN (SELECT action_value
                          FROM t_fsp_menu_item fmi
						  JOIN t_fsp_application fa
						    ON fmi.application_id = fa.application_id
                         WHERE fa.application_name = N''OneSumX''
						   AND fmi.action_type = N''Content'')',
     @field_ignore                   = N'content_id, last_modified, modified_by',
	 @field_existence_check          = N'content_name';
*/
/* empty
SELECT *
  FROM t_fsp_linkset
 WHERE linkset_id IN (SELECT object_id
                        FROM t_fsp_channel ch
						JOIN t_fsp_content_item ci
						  ON ch.channel_id = ci.channel_id
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
						JOIN t_fsp_channel_type fct
						  ON ch.channel_type = fct.channel_type
                       WHERE fa.application_name = N'OneSumX'
						 AND fmi.action_type = N'Content'
                         AND fct.name = N'FALinks');

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_linkset',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'linkset_id IN (SELECT object_id
                        FROM t_fsp_channel ch
						JOIN t_fsp_content_item ci
						  ON ch.channel_id = ci.channel_id
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
						JOIN t_fsp_channel_type fct
						  ON ch.channel_type = fct.channel_type
                       WHERE fa.application_name = N''OneSumX''
						 AND fmi.action_type = N''Content''
                         AND fct.name = N''FALinks'')',
     @field_existence_check          = N'linkset_name',
     @field_ignore                   = N'linkset_id, last_modified, modified_by';
*/
/*empty
SELECT *
  FROM t_fsp_statement
 WHERE statement_id IN (SELECT select_id
                          FROM t_fsp_tree t
						  JOIN t_fsp_channel ch
						    ON t.tree_id = ch.object_id
					      JOIN t_fsp_content_item ci
					        ON ch.channel_id = ci.channel_id
					      JOIN t_fsp_content c
					        ON ci.content_id = c.content_id
					      JOIN t_fsp_menu_item fmi
					        ON c.content_name = fmi.action_value
					      JOIN t_fsp_application fa
					        ON fmi.application_id = fa.application_id
					      JOIN t_fsp_channel_type fct
					        ON ch.channel_type = fct.channel_type
                         WHERE fa.application_name = N'OneSumX'
					       AND fmi.action_type = N'Content'
                           AND fct.name = N'FATree'
						UNION
						SELECT g.select_id
						  FROM t_fsp_grid g
                          JOIN t_fsp_channel ch
						    ON g.grid_id = ch.object_id
					      JOIN t_fsp_content_item ci
 					        ON ch.channel_id = ci.channel_id
					      JOIN t_fsp_content c
					        ON ci.content_id = c.content_id
					      JOIN t_fsp_menu_item fmi
					        ON c.content_name = fmi.action_value
					      JOIN t_fsp_application fa
					        ON fmi.application_id = fa.application_id
					      JOIN t_fsp_channel_type fct
					        ON ch.channel_type = fct.channel_type
                         WHERE fa.application_name = N'OneSumX'
					       AND fmi.action_type = N'Content'
                           AND fct.name = N'FAGridView');

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_statement',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'statement_id IN (SELECT select_id
                          FROM t_fsp_tree t
						  JOIN t_fsp_channel ch
						    ON t.tree_id = ch.object_id
					      JOIN t_fsp_content_item ci
					        ON ch.channel_id = ci.channel_id
					      JOIN t_fsp_content c
					        ON ci.content_id = c.content_id
					      JOIN t_fsp_menu_item fmi
					        ON c.content_name = fmi.action_value
					      JOIN t_fsp_application fa
					        ON fmi.application_id = fa.application_id
					      JOIN t_fsp_channel_type fct
					        ON ch.channel_type = fct.channel_type
                         WHERE fa.application_name = N''OneSumX''
					       AND fmi.action_type = N''Content''
                           AND fct.name = N''FATree''
						 UNION
						SELECT g.select_id
						  FROM t_fsp_grid g
                          JOIN t_fsp_channel ch
						    ON g.grid_id = ch.object_id
					      JOIN t_fsp_content_item ci
 					        ON ch.channel_id = ci.channel_id
					      JOIN t_fsp_content c
					        ON ci.content_id = c.content_id
					      JOIN t_fsp_menu_item fmi
					        ON c.content_name = fmi.action_value
					      JOIN t_fsp_application fa
					        ON fmi.application_id = fa.application_id
					      JOIN t_fsp_channel_type fct
					        ON ch.channel_type = fct.channel_type
                         WHERE fa.application_name = N''OneSumX''
					       AND fmi.action_type = N''Content''
                           AND fct.name = N''FAGridView'')',
     @field_existence_check          = N'statement_name',
     @field_ignore                   = N'statement_id, last_modified, modified_by';
*/
/* empty
SELECT *
  FROM t_fsp_tree
 WHERE tree_id IN (SELECT object_id
                     FROM t_fsp_channel ch
					 JOIN t_fsp_content_item ci
					   ON ch.channel_id = ci.channel_id
					 JOIN t_fsp_content c
					   ON ci.content_id = c.content_id
					 JOIN t_fsp_menu_item fmi
					   ON c.content_name = fmi.action_value
					 JOIN t_fsp_application fa
					   ON fmi.application_id = fa.application_id
					 JOIN t_fsp_channel_type fct
					   ON ch.channel_type = fct.channel_type
                    WHERE fa.application_name = N'OneSumX'
					  AND fmi.action_type = N'Content'
                      AND fct.name = N'FATree');

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_tree',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'tree_id IN (SELECT object_id
                     FROM t_fsp_channel ch
					 JOIN t_fsp_content_item ci
					   ON ch.channel_id = ci.channel_id
					 JOIN t_fsp_content c
					   ON ci.content_id = c.content_id
					 JOIN t_fsp_menu_item fmi
					   ON c.content_name = fmi.action_value
					 JOIN t_fsp_application fa
					   ON fmi.application_id = fa.application_id
					 JOIN t_fsp_channel_type fct
					   ON ch.channel_type = fct.channel_type
                    WHERE fa.application_name = N''OneSumX''
					  AND fmi.action_type = N''Content''
                      AND fct.name = N''FATree'')',
     @field_existence_check          = N'tree_name',
     @field_search                   = N'select_id = statement_id',
     @table_search                   = N't_fsp_statement',
     @field_replace                  = N'statement_name',
     @field_ignore                   = N'tree_id, last_modified, modified_by';
*/
/* empty
SELECT *
  FROM t_fsp_grid
 WHERE grid_id IN (SELECT object_id
                     FROM t_fsp_channel ch
					 JOIN t_fsp_content_item ci
					   ON ch.channel_id = ci.channel_id
					 JOIN t_fsp_content c
					   ON ci.content_id = c.content_id
					 JOIN t_fsp_menu_item fmi
					   ON c.content_name = fmi.action_value
					 JOIN t_fsp_application fa
					   ON fmi.application_id = fa.application_id
					 JOIN t_fsp_channel_type fct
					   ON ch.channel_type = fct.channel_type
                    WHERE fa.application_name = N'OneSumX'
					  AND fmi.action_type = N'Content'
                      AND fct.name = N'FAGridView');

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_grid',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'grid_id IN (SELECT object_id
                     FROM t_fsp_channel ch
					 JOIN t_fsp_content_item ci
					   ON ch.channel_id = ci.channel_id
					 JOIN t_fsp_content c
					   ON ci.content_id = c.content_id
					 JOIN t_fsp_menu_item fmi
					   ON c.content_name = fmi.action_value
					 JOIN t_fsp_application fa
					   ON fmi.application_id = fa.application_id
					 JOIN t_fsp_channel_type fct
					   ON ch.channel_type = fct.channel_type
                    WHERE fa.application_name = N''OneSumX''
					  AND fmi.action_type = N''Content''
                      AND fct.name = N''FAGridView'')',
     @field_existence_check          = N'grid_name',
     @field_search                   = N'select_id = statement_id',
     @table_search                   = N't_fsp_statement',
     @field_replace                  = N'statement_name',
     @field_ignore                   = N'grid_id, last_modified, modified_by';
*/
/* empty
SELECT *
  FROM t_fsp_channel
 WHERE channel_id IN (SELECT ci.channel_id
                        FROM t_fsp_content_item ci
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N'OneSumX'
						 AND fmi.action_type = N'Content')
   AND channel_type = (SELECT channel_type FROM t_fsp_channel_type WHERE name = N'FALinks')

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_channel',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'channel_id IN (SELECT ci.channel_id
                        FROM t_fsp_content_item ci
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N''OneSumX''
						 AND fmi.action_type = N''Content'')
						               AND channel_type = (SELECT channel_type FROM t_fsp_channel_type WHERE name = N''FALinks'')',
     @field_search                   = N'object_id = linkset_id; channel_type',
     @table_search                   = N't_fsp_linkset; t_fsp_channel_type',
     @field_replace                  = N'linkset_name; name',
     @field_existence_check          = N'channel_name',
     @field_ignore                   = N'channel_id, last_modified, modified_by';
*/
/* empty
SELECT *
  FROM t_fsp_channel
 WHERE channel_id IN (SELECT ci.channel_id
                        FROM t_fsp_content_item ci
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N'OneSumX'
						 AND fmi.action_type = N'Content')
   AND channel_type = (SELECT channel_type FROM t_fsp_channel_type WHERE name = N'FATree')

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_channel',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'channel_id IN (SELECT ci.channel_id
                        FROM t_fsp_content_item ci
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N''OneSumX''
						 AND fmi.action_type = N''Content'')
						               AND channel_type = (SELECT channel_type FROM t_fsp_channel_type WHERE name = N''FATree'')',
     @field_search                   = N'object_id = tree_id; channel_type',
     @table_search                   = N't_fsp_tree; t_fsp_channel_type',
     @field_replace                  = N'tree_name; name',
     @field_existence_check          = N'channel_name',
     @field_ignore                   = N'channel_id, last_modified, modified_by';
*/
/* empty
SELECT *
  FROM t_fsp_channel
 WHERE channel_id IN (SELECT ci.channel_id
                        FROM t_fsp_content_item ci
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N'OneSumX'
						 AND fmi.action_type = N'Content')
   AND channel_type = (SELECT channel_type FROM t_fsp_channel_type WHERE name = N'FAGridView')

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_channel',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'channel_id IN (SELECT ci.channel_id
                        FROM t_fsp_content_item ci
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N''OneSumX''
						 AND fmi.action_type = N''Content'')
						               AND channel_type = (SELECT channel_type FROM t_fsp_channel_type WHERE name = N''FAGridView'')',
     @field_search                   = N'object_id = grid_id; channel_type',
     @table_search                   = N't_fsp_grid; t_fsp_channel_type',
     @field_replace                  = N'grid_name; name',
     @field_existence_check          = N'channel_name',
     @field_ignore                   = N'channel_id, last_modified, modified_by';
*/
/*
SELECT *
  FROM t_fsp_channel
 WHERE channel_id IN (SELECT ci.channel_id
                        FROM t_fsp_content_item ci
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N'OneSumX'
						 AND fmi.action_type = N'Content')
   AND object_id IS NULL

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_channel',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'channel_id IN (SELECT ci.channel_id
                        FROM t_fsp_content_item ci
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N''OneSumX''
						 AND fmi.action_type = N''Content'')
						                AND object_id IS NULL',
     @field_search                   = N'channel_type',
     @table_search                   = N't_fsp_channel_type',
     @field_replace                  = N'name',
     @field_existence_check          = N'channel_name',
     @field_ignore                   = N'channel_id, last_modified, modified_by';
*/
/* empty
SELECT *
  FROM t_fsp_content_item
 WHERE content_id IN (SELECT c.content_id
                        FROM t_fsp_content c
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N'OneSumX'
						 AND fmi.action_type = N'Content')

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_content_item',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'content_id IN (SELECT c.content_id
                        FROM t_fsp_content c
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N''OneSumX''
						 AND fmi.action_type = N''Content'')',
     @field_search                   = N'content_id; channel_id',
     @table_search                   = N't_fsp_content; t_fsp_channel',
     @field_replace                  = N'content_name; channel_name',
     @field_existence_check          = N'content_id, channel_id',
     @field_ignore                   = N'item_id, last_modified, modified_by';
*/

SELECT *
  FROM t_fsp_page
 WHERE page_code IN (SELECT action_value
                       FROM t_fsp_menu_item fmi
				       JOIN t_fsp_application fa
						 ON fmi.application_id = fa.application_id
                      WHERE fa.application_name = N'OneSumX'
						AND fmi.action_type = N'Page')

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_page',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'page_code IN (SELECT action_value
                       FROM t_fsp_menu_item fmi
				       JOIN t_fsp_application fa
						 ON fmi.application_id = fa.application_id
                      WHERE fa.application_name = N''OneSumX''
						AND fmi.action_type = N''Page'')',
     @field_existence_check          = N'page_code',
     @field_ignore                   = N'page_id, last_modified, modified_by';

/* empty
SELECT *
  FROM t_fsp_html_text
 WHERE channel_id IN (SELECT ci.channel_id
                        FROM t_fsp_content_item ci
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N'OneSumX'
						 AND fmi.action_type = N'Content')

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_html_text',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'channel_id IN (SELECT ci.channel_id
                        FROM t_fsp_content_item ci
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
                       WHERE fa.application_name = N''OneSumX''
						 AND fmi.action_type = N''Content'')',
     @field_ignore                   = N'last_modified, modified_by',
     @field_search                   = N'channel_id',
     @table_search                   = N't_fsp_channel',
     @field_replace                  = N'channel_name';
*/

SELECT *
  FROM t_fsp_widget
 WHERE widget_id IN (SELECT fpi.widget_id
                       FROM t_fsp_page_item fpi
					   JOIN t_fsp_page fp
					     ON fpi.page_id = fp.page_id
					   JOIN t_fsp_menu_item fmi
					     ON fp.page_code = fmi.action_value
					   JOIN t_fsp_application fa
						 ON fmi.application_id = fa.application_id
                      WHERE fa.application_name = N'OneSumX'
						AND fmi.action_type = N'Page');

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_widget',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'widget_id IN (SELECT fpi.widget_id
                       FROM t_fsp_page_item fpi
					   JOIN t_fsp_page fp
					     ON fpi.page_id = fp.page_id
					   JOIN t_fsp_menu_item fmi
					     ON fp.page_code = fmi.action_value
					   JOIN t_fsp_application fa
						 ON fmi.application_id = fa.application_id
                      WHERE fa.application_name = N''OneSumX''
						AND fmi.action_type = N''Page'')',
     @field_ignore                   = N'widget_id, last_modified, modified_by',
     @field_existence_check          = N'widget_code';

SELECT *
  FROM t_fsp_widget_user_preference
 WHERE widget_id IN (SELECT fpi.widget_id
                       FROM t_fsp_page_item fpi
					   JOIN t_fsp_page fp
					     ON fpi.page_id = fp.page_id
					   JOIN t_fsp_menu_item fmi
					     ON fp.page_code = fmi.action_value
					   JOIN t_fsp_application fa
						 ON fmi.application_id = fa.application_id
                      WHERE fa.application_name = N'OneSumX'
						AND fmi.action_type = N'Page');

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_widget_user_preference',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'widget_id IN (SELECT fpi.widget_id
                       FROM t_fsp_page_item fpi
					   JOIN t_fsp_page fp
					     ON fpi.page_id = fp.page_id
					   JOIN t_fsp_menu_item fmi
					     ON fp.page_code = fmi.action_value
					   JOIN t_fsp_application fa
						 ON fmi.application_id = fa.application_id
                      WHERE fa.application_name = N''OneSumX''
						AND fmi.action_type = N''Page'')',
     @field_ignore                   = N'widget_user_preference_id, last_modified, modified_by',
     @field_existence_check          = N'widget_id, username',
     @field_search                   = N'widget_id',
     @table_search                   = N't_fsp_widget',
     @field_replace                  = N'widget_code';

SELECT *
  FROM t_fsp_page_item
 WHERE page_id IN (SELECT pg.page_id
                     FROM t_fsp_page pg
					 JOIN t_fsp_menu_item fmi
                       ON pg.page_code = fmi.action_value
                     JOIN t_fsp_application fa
					   ON fmi.application_id = fa.application_id
                    WHERE fa.application_name = N'OneSumX'
					  AND fmi.action_type = N'Page');

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_page_item',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @data_filter                    = N'page_id IN (SELECT pg.page_id
                     FROM t_fsp_page pg
					 JOIN t_fsp_menu_item fmi
                       ON pg.page_code = fmi.action_value
                     JOIN t_fsp_application fa
					   ON fmi.application_id = fa.application_id
                    WHERE fa.application_name = N''OneSumX''
					  AND fmi.action_type = N''Page'')',
     @field_ignore                   = N'last_modified, modified_by',
     @field_search                   = N'widget_id; page_id',
     @table_search                   = N't_fsp_widget; t_fsp_page',
     @field_replace                  = N'widget_code; page_code',
	 @sql_before                     = 'DELETE FROM t_fsp_page_item WHERE page_id IN (SELECT pg.page_id
                     FROM t_fsp_page pg
					 JOIN t_fsp_menu_item fmi
                       ON pg.page_code = fmi.action_value
                     JOIN t_fsp_application fa
					   ON fmi.application_id = fa.application_id
                    WHERE fa.application_name = N''OneSumX''
					  AND fmi.action_type = N''Page'')';

SELECT *
  FROM t_fsp_rvw_report

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_rvw_report',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @field_ignore                   = N'report_id, last_modified, modified_by',
	 @field_existence_check          = N'report_code';

SELECT *
  FROM t_fsp_rvw_report_handler

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_rvw_report_handler',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @field_ignore                   = N'last_modified, modified_by',
     @field_search                   = N'report_id; handler_id',
     @table_search                   = N't_fsp_rvw_report; t_fsp_rvw_handler',
     @field_replace                  = N'report_code; handler_code';

SELECT *
  FROM t_fsp_rvw_report_property

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_rvw_report_property',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @field_ignore                   = N'last_modified, modified_by',
     @field_search                   = N'report_id; property_id',
     @table_search                   = N't_fsp_rvw_report; t_fsp_rvw_handler_property',
     @field_replace                  = N'report_code; handler_id, property_code';

SELECT *
  FROM t_fsp_rvw_group

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_rvw_group',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @field_ignore                   = N'group_id, last_modified, modified_by',
     @field_search                   = N'report_id',
     @table_search                   = N't_fsp_rvw_report',
     @field_replace                  = N'report_code',
	 @field_existence_check          = N'report_id, group_code';


SELECT *
  FROM t_fsp_rvw_parameter

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_rvw_parameter',
     @add_existence_check            = 1,
     @add_update                     = 1,
     @field_ignore                   = N'parameter_id, last_modified, modified_by',
     @field_search                   = N't_fsp_rvw_group.report_id;group_id; control_type_id',
     @table_search                   = N't_fsp_rvw_report; t_fsp_rvw_group; t_fsp_control_type',
     @field_replace                  = N'report_code;group_code, report_id; control_type_name',
	 @field_existence_check          = N'parameter_code, group_id';

SELECT *
  FROM t_fsp_rvw_parameter_dependency

IF OBJECT_ID(N'fn_show_parameter_ps',N'FN') IS NOT NULL
  DROP FUNCTION dbo.fn_show_parameter_ps;
GO

CREATE FUNCTION dbo.fn_show_parameter_ps (@parameter_id INT)
RETURNS NVARCHAR(MAX)
AS
BEGIN
  RETURN (
  SELECT DISTINCT frp.parameter_code + N''',N''' + frg.group_code + N''',N''' + frr.report_code
    FROM t_fsp_rvw_parameter frp
    JOIN t_fsp_rvw_group frg
      ON frp.group_id = frg.group_id
    JOIN t_fsp_rvw_report frr
      ON frg.report_id = frr.report_id
   WHERE frp.parameter_id = @parameter_id
  );
END;
GO

EXEC p_createpopulationscript 
     @object_name                    = N't_fsp_rvw_parameter_dependency',
     @add_existence_check            = 1,
     @field_ignore                   = N'last_modified, modified_by',
     @field_search                   = N'origin_parameter_id; dependent_parameter_id',
     @table_search                   = N'function; function',
     @field_replace                  = N'fn_show_parameter_ps(@origin_parameter_id)|fn_get_parameter_ps;fn_show_parameter_ps(@dependent_parameter_id)|fn_get_parameter_ps',
	 @sql_before                     = N'
IF OBJECT_ID(N''fn_get_parameter_ps'',N''FN'') IS NOT NULL
  DROP FUNCTION dbo.fn_get_parameter_ps;

GO

CREATE FUNCTION dbo.fn_get_parameter_ps (@parameter_code d_name,
                                          @group_code     d_name,
										  @report_code    d_name)
RETURNS INT
AS
BEGIN
  RETURN (
  SELECT frp.parameter_id
    FROM t_fsp_rvw_parameter frp
    JOIN t_fsp_rvw_group frg
      ON frp.group_id = frg.group_id
    JOIN t_fsp_rvw_report frr
      ON frg.report_id = frr.report_id
   WHERE frp.parameter_code = @parameter_code
     AND frg.group_code = @group_code
	 AND frr.report_code = @report_code
  );
END;
GO';

/* empty
SELECT *
  FROM t_fsp_linkset_item
 WHERE linkset_id IN (SELECT l.linkset_id
                        FROM t_fsp_linkset l
						JOIN t_fsp_channel ch
						  ON l.linkset_id = ch.object_id
						JOIN t_fsp_content_item ci
						  ON ch.channel_id = ci.channel_id
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
						JOIN t_fsp_channel_type fct
						  ON ch.channel_type = fct.channel_type
                       WHERE fa.application_name = N'OneSumX'
						 AND fmi.action_type = N'Content'
                         AND fct.name = N'FALinks');

EXEC p_createpopulationscript 
     @object_name = 't_fsp_linkset_item',
     @add_existence_check = 1,
     @add_update = 1,   
     @data_filter = 'linkset_id IN (SELECT l.linkset_id
                        FROM t_fsp_linkset l
						JOIN t_fsp_channel ch
						  ON l.linkset_id = ch.object_id
						JOIN t_fsp_content_item ci
						  ON ch.channel_id = ci.channel_id
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
						JOIN t_fsp_channel_type fct
						  ON ch.channel_type = fct.channel_type
                       WHERE fa.application_name = N''OneSumX''
						 AND fmi.action_type = N''Content''
                         AND fct.name = N''FALinks'')',
     @field_search = 'linkset_id; action_type',
     @table_search = 't_fsp_linkset; t_fsp_link_action_type',
     @field_replace = 'linkset_name; description',
     @field_existence_check = 'item_name',
     @field_ignore = 'item_id,last_modified,modified_by',
     @sql_after = '
DECLARE @action_value d_action_value;
SELECT @action_value = value + N''/Pages/ReportViewer.aspx?%2fQTC+Reports%2f''
  FROM v_fsp_setting
 WHERE keyword = ''ReportServer'';

UPDATE t_fsp_linkset_item
   SET action_value = @action_value + item_tooltip + ''&rs:Command=Render''
  FROM t_fsp_linkset_item fli
  JOIN t_fsp_link_action_type lat
    ON fli.action_type = lat.action_type
 WHERE linkset_id IN (SELECT l.linkset_id
                        FROM t_fsp_linkset l
						JOIN t_fsp_channel ch
						  ON l.linkset_id = ch.object_id
						JOIN t_fsp_content_item ci
						  ON ch.channel_id = ci.channel_id
						JOIN t_fsp_content c
						  ON ci.content_id = c.content_id
						JOIN t_fsp_menu_item fmi
						  ON c.content_name = fmi.action_value
						JOIN t_fsp_application fa
						  ON fmi.application_id = fa.application_id
						JOIN t_fsp_channel_type fct
						  ON ch.channel_type = fct.channel_type
                       WHERE fa.application_name = N''OneSumX''
						 AND fmi.action_type = N''Content''
                         AND fct.name = N''FALinks'')
   AND lat.description = ''GotoUrl'';';
*/
