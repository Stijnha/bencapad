USE [OneSumX_archive];
GO

DECLARE @osx_db					SYSNAME;

SELECT @osx_db = platform_property_value
	  FROM dbo.t_sys_platform_property
	 WHERE platform_property_name = N'archive_sibling_db';

IF EXISTS (SELECT 1
				 FROM dbo.t_arcm_table arc
				 JOIN dbo.t_arcm_type at
				   ON at.type_id = arc.type_id
				 JOIN dbo.t_arcm_category c
				   ON c.category_id = at.category_id
				 LEFT JOIN sys.objects o
				   ON arc.table_name = o.name
				WHERE table_name NOT LIKE N'=%'
				  AND c.category_code = N'PS'
				  AND (o.object_id IS NULL
				   OR arc.table_name LIKE N'%ps'
				   OR at.type_code = N'ETL'))
	BEGIN
		CREATE TABLE #tab
		(
			id		INT IDENTITY(1,1),
			txt		NVARCHAR(MAX)
		)

		INSERT INTO #tab(txt)
		SELECT N'USE ' + @osx_db;

		INSERT INTO #tab(txt)
		SELECT N'PRINT N''USE ' + DB_NAME() + N''''

		INSERT INTO #tab(txt) SELECT N'PRINT N''IF NOT EXISTS (SELECT 1''';
		INSERT INTO #tab(txt) SELECT N'PRINT N''                 FROM sys.types''';
		INSERT INTO #tab(txt) SELECT N'PRINT N''                WHERE name = N''''d_numeric'''')''';
		INSERT INTO #tab(txt) SELECT N'PRINT N''BEGIN''';
		INSERT INTO #tab(txt) SELECT N'PRINT N''  CREATE TYPE [dbo].[d_numeric] FROM [numeric](30, 15) NULL;''';
		INSERT INTO #tab(txt) SELECT N'PRINT N''END;''';

	
		INSERT INTO #tab(txt)
		SELECT DISTINCT N'EXEC dbo.p_createtablescript @table_name = N''' + arc.table_name + N''', @force_development_conventions = 0, @use_if_not_exists_exec = 1, @silent_mode = 0, @omit_fk = 1, @create_statement_only = 0'
		  FROM dbo.t_arcm_table arc
		  JOIN dbo.t_arcm_type at
			ON at.type_id = arc.type_id
		  JOIN dbo.t_arcm_category c
			ON c.category_id = at.category_id
		  LEFT JOIN sys.objects o
			ON arc.table_name = o.name
		 WHERE table_name NOT LIKE N'=%'
		   AND c.category_code = N'ANZ'
		   --AND at.type_code = @type_code
		   AND (o.object_id IS NULL
			OR arc.table_name LIKE N'%anz'
			OR at.type_code = N'ETL')
		 ORDER BY 1

		SELECT *
		  FROM #tab
		 ORDER BY id;

		DROP TABLE #tab;
	END;