USE master;

IF EXISTS (SELECT 1
             FROM sys.databases
			WHERE name = N'OneSumX_archive'
			  AND is_trustworthy_on = 1)
BEGIN
	ALTER DATABASE summix_archive SET TRUSTWORTHY OFF;
END;


USE [OneSumX_archive]
GO

IF EXISTS (SELECT 1
             FROM sys.databases
			WHERE name = N'OneSumX_archive'
			  AND SUSER_NAME(owner_sid) <> N'sa')
BEGIN
	ALTER AUTHORIZATION ON DATABASE::[OneSumX_archive] TO [sa];
END;

GO

IF EXISTS (SELECT 1
             FROM sys.databases
            WHERE name = N'OneSumX_archive'
              AND recovery_model_desc = N'FULL')
BEGIN
	ALTER DATABASE [OneSumX_archive] SET RECOVERY SIMPLE WITH NO_WAIT;
END;

