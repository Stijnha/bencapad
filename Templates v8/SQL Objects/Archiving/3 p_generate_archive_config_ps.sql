USE [OneSumX_archive];

IF OBJECT_ID (N'dbo.p_generate_archive_config_ps',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_generate_archive_config_ps...';
  DROP PROCEDURE dbo.p_generate_archive_config_ps;
END
GO

PRINT N'Creating procedure p_generate_archive_config_ps...';
GO
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************

CREATE PROCEDURE dbo.p_generate_archive_config_ps
(
		@category_code				d_name,
        @category_description		d_description,
		@type_code					d_name,
		@type_description			d_name,
		@sql_init					d_sql = NULL,
	    @sql_before					d_sql = NULL,
        @sql_after					d_sql = NULL,
		@debug						BIT = 0,
		@execute_immediately		BIT = 0
)
AS

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @table_list	TABLE
	(
		table_name		d_name,
		filter_template	d_sql,
		copy_rows		TINYINT,
		delete_rows		TINYINT,
		clear_fks		TINYINT,
		apply_id_offset	TINYINT
	);
	DECLARE @fk_table TABLE
	(
		table_name		d_name,
		parent			NVARCHAR(128),
		lvl				INT	
	);

	DECLARE @code_block            NVARCHAR(4000) = N'',
			@ErrorSeverity         INT,
			@ErrorState            INT,
			@Msg                   NVARCHAR(4000),
			@ProcName              SYSNAME = OBJECT_NAME(@@PROCID),
			@Raiserror             INT,
			@Rowcount              INT;

	DECLARE @sql					NVARCHAR(MAX),
			@channel_name			d_name = N'ARC_' + @category_code,
			@content_name			d_name = @category_code + N' Archive',
			@report_code			d_name = N'arcm_' + @type_code,
			@table_id				d_id,
			@category_id			d_id,
			@osx_db					SYSNAME;

	CREATE TABLE #code ([id] INT            IDENTITY(1,1)  NOT NULL,  
						txt  NVARCHAR(4000)                NOT NULL);


	SELECT @osx_db = platform_property_value
	  FROM dbo.t_sys_platform_property
	 WHERE platform_property_name = N'archive_sibling_db';


	IF NOT EXISTS (
					SELECT 1
					  FROM dbo.t_arcm_category
					 WHERE category_code = @category_code)
	BEGIN
		INSERT INTO dbo.t_arcm_category
				( category_code ,
				  category_name ,
				  category_desc ,
				  last_modified ,
				  modified_by
				)
		VALUES  ( @category_code, -- category_code - d_name
				  @category_code , -- category_name - d_name
				  @category_description , -- category_desc - d_description
				  CURRENT_TIMESTAMP , -- last_modified - d_last_modified
				  CURRENT_USER  -- modified_by - d_user
				)
	END;

	SELECT @category_id = category_id
	  FROM dbo.t_arcm_category
	 WHERE category_code = @category_code;

	IF NOT EXISTS ( 
					SELECT 1
					  FROM dbo.t_arcm_type at
					  JOIN dbo.t_arcm_category ac
						ON ac.category_id = at.category_id
					 WHERE ac.category_code = @category_code
					   AND at.type_code = @type_code)
	BEGIN
		INSERT INTO dbo.t_arcm_type
				( category_id ,
				  type_code ,
				  type_name ,
				  type_desc ,
				  is_predefined ,
				  sql_init ,
				  sql_before ,
				  sql_after ,
				  last_modified ,
				  modified_by
				)
		VALUES  ( (SELECT category_id FROM dbo.t_arcm_category WHERE category_code = @category_code) , -- category_id - d_id
				  @type_code , -- type_code - d_name
				  @type_code , -- type_name - d_name
				  @type_description , -- type_desc - d_description
				  0 , -- is_predefined - d_bit
				  @sql_init , -- sql_init - d_sql
				  @sql_before , -- sql_before - d_sql
				  @sql_after , -- sql_after - d_sql
				  CURRENT_TIMESTAMP , -- last_modified - d_last_modified
				  CURRENT_USER  -- modified_by - d_user
				)
	END
	ELSE
	BEGIN
		WITH cte AS
		(
			SELECT (SELECT category_id FROM dbo.t_arcm_category WHERE category_code = @category_code) AS category_id,
					  @type_code AS type_code,
					  @type_code AS type_name,
					  @type_description AS type_desc,
					  0 AS is_predefined,
					  @sql_init AS sql_init,
					  @sql_before AS sql_before,
					  @sql_after AS sql_after,
					  CURRENT_TIMESTAMP AS last_modified,
					  CURRENT_USER AS modified_by
		)
		UPDATE at
		   SET at.type_name = c.type_name,
	         at.type_desc = c.type_desc,
			at.is_predefined = c.is_predefined,
			at.sql_init = c.sql_init,
			at.sql_before = c.sql_before,
			at.sql_after = c.sql_after,
			at.last_modified = c.last_modified,
			at.modified_by = c.modified_by
		 FROM dbo.t_arcm_type at
		 JOIN cte c
		   ON c.category_id = at.category_id
		  AND c.type_code = at.type_code
	END


	IF @type_code = N'ETL'
	BEGIN

		/*
		Generate 3 entries per ETL table:
		- The ETL table for records that passed validation without warning or error (line_status = 0): These records can simply be deleted
		- The ETL table for records that passed validation with warning or error (line_status <> 0): These records are to be moved from live to archive
		- The ETL validation table records will all be moved from live to archive
		*/


		SELECT @table_id = MAX(table_id)
		  FROM t_arcm_table;

		INSERT INTO #code VALUES(N'	WITH cte AS');
		INSERT INTO #code VALUES(N'	(');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   ' + CONVERT(NVARCHAR,@table_id) + N' + ROW_NUMBER() OVER (ORDER BY at.type_name, fn.a)				AS table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 3 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END																		AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN N''line_status = 0''');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN N''line_status <> 0 AND line_date < (SELECT arc_date FROM #arc_etl_date WHERE arc_date_type = N''''ETL'''')''');
		INSERT INTO #code VALUES(N'				WHEN 3 THEN N''last_modified < (SELECT arc_date FROM #arc_etl_date WHERE arc_date_type = N''''ETL'''')''');
		INSERT INTO #code VALUES(N'			   END																		AS filter_template,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN 0');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN 1');
		INSERT INTO #code VALUES(N'				WHEN 3 THEN 1');
		INSERT INTO #code VALUES(N'			   END																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN 1');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN 1');
		INSERT INTO #code VALUES(N'				WHEN 3 THEN 1');
		INSERT INTO #code VALUES(N'			   END																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'		  FROM ' + @osx_db + N'.dbo.t_adj_type at');
		INSERT INTO #code VALUES(N'		  CROSS APPLY (VALUES(1),(2),(3)) AS fn(a)');
		INSERT INTO #code VALUES(N'		  LEFT JOIN ' + @osx_db + N'.sys.tables t');
		INSERT INTO #code VALUES(N'			ON CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 3 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END = t.name');
		INSERT INTO #code VALUES(N'		 WHERE t.name IS NOT NULL -- make sure the tables exist');
		INSERT INTO #code VALUES(N'	)');
		INSERT INTO #code VALUES(N'	INSERT INTO dbo.t_arcm_table');
		INSERT INTO #code VALUES(N'			( type_id ,');
		INSERT INTO #code VALUES(N'			  table_id ,');
		INSERT INTO #code VALUES(N'			  table_name ,');
		INSERT INTO #code VALUES(N'			  parent_table_id ,');
		INSERT INTO #code VALUES(N'			  filter_template ,');
		INSERT INTO #code VALUES(N'			  copy_rows ,');
		INSERT INTO #code VALUES(N'			  delete_rows ,');
		INSERT INTO #code VALUES(N'			  clear_fks ,');
		INSERT INTO #code VALUES(N'			  apply_id_offset ,');
		INSERT INTO #code VALUES(N'			  last_modified ,');
		INSERT INTO #code VALUES(N'			  modified_by');
		INSERT INTO #code VALUES(N'			)');
		INSERT INTO #code VALUES(N'	SELECT cte.*');
		INSERT INTO #code VALUES(N'	  FROM cte');
		INSERT INTO #code VALUES(N'	  LEFT JOIN dbo.t_arcm_table at');
		INSERT INTO #code VALUES(N'		ON cte.type_id = at.type_id');
		INSERT INTO #code VALUES(N'	   AND cte.table_name = at.table_name');
		INSERT INTO #code VALUES(N'	   AND cte.filter_template = at.filter_template');
		INSERT INTO #code VALUES(N'	 WHERE at.table_id IS NULL;');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'	WITH cte AS');
		INSERT INTO #code VALUES(N'	(');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   ' + CONVERT(NVARCHAR,@table_id) + N' + ROW_NUMBER() OVER (ORDER BY at.type_name, fn.a)				AS table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 3 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END																		AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN N''line_status = 0''');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN N''line_status <> 0 AND line_date < (SELECT arc_date FROM #arc_etl_date WHERE arc_date_type = N''''ETL'''')''');
		INSERT INTO #code VALUES(N'				WHEN 3 THEN N''last_modified < (SELECT arc_date FROM #arc_etl_date WHERE arc_date_type = N''''ETL'''')''');
		INSERT INTO #code VALUES(N'			   END																		AS filter_template,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN 0');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN 1');
		INSERT INTO #code VALUES(N'				WHEN 3 THEN 1');
		INSERT INTO #code VALUES(N'			   END																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN 1');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN 1');
		INSERT INTO #code VALUES(N'				WHEN 3 THEN 1');
		INSERT INTO #code VALUES(N'			   END																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'		  FROM ' + @osx_db + N'.dbo.t_adj_type at');
		INSERT INTO #code VALUES(N'		  CROSS APPLY (VALUES(1),(2),(3)) AS fn(a)');
		INSERT INTO #code VALUES(N'		  LEFT JOIN ' + @osx_db + N'.sys.tables t');
		INSERT INTO #code VALUES(N'			ON CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 3 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END = t.name');
		INSERT INTO #code VALUES(N'		 WHERE t.name IS NOT NULL -- make sure the tables exist');
		INSERT INTO #code VALUES(N'	)');
		INSERT INTO #code VALUES(N'	UPDATE at');
		INSERT INTO #code VALUES(N'	   SET at.table_name = cte.table_name,');
		INSERT INTO #code VALUES(N'		   at.parent_table_id  = cte.parent_table_id,');
		INSERT INTO #code VALUES(N'		   at.filter_template  = cte.filter_template,');
		INSERT INTO #code VALUES(N'		   at.copy_rows  = cte.copy_rows,');
		INSERT INTO #code VALUES(N'		   at.delete_rows  = cte.delete_rows,');
		INSERT INTO #code VALUES(N'		   at.clear_fks  = cte.clear_fks,');
		INSERT INTO #code VALUES(N'		   at.apply_id_offset  = cte.apply_id_offset,');
		INSERT INTO #code VALUES(N'		   at.last_modified  = cte.last_modified,');
		INSERT INTO #code VALUES(N'		   at.modified_by  = cte.modified_by');
		INSERT INTO #code VALUES(N'	  FROM cte');
		INSERT INTO #code VALUES(N'	  JOIN dbo.t_arcm_table at');
		INSERT INTO #code VALUES(N'		ON cte.type_id = at.type_id');
		INSERT INTO #code VALUES(N'	   AND cte.table_name = at.table_name');
		INSERT INTO #code VALUES(N'	   AND cte.filter_template = at.filter_template;');

		EXEC p_exec_resultset N'SELECT txt FROM #code ORDER BY [id]'; 
		DELETE FROM #code;
		/*
		Create the sequences. There will be a sequence for every ETL validation table
		*/
		SET @sql = N'SELECT N''CREATE SEQUENCE seq_'' + RIGHT(at.table_name,(LEN(at.table_name)-2)) + N'' AS INT INCREMENT BY 1;''
		  FROM dbo.t_arcm_table at
		  JOIN dbo.t_arcm_type aty
			ON aty.type_id = at.type_id
		  LEFT JOIN sys.sequences s
			ON s.name = N''seq_'' + RIGHT(at.table_name,(LEN(at.table_name)-2))
		 WHERE aty.type_code = N''' + @type_code + N'''
		   AND at.table_name LIKE N''%validation''
		   AND s.name IS NULL';
		EXEC dbo.p_exec_resultset @stmt = @sql;

		/*
		We will override the values for record_id as this could be refreshed every time.
		The record_id column is a pure technical column to make the record unique, so no impact is expected
		*/

		INSERT INTO dbo.t_arcm_column_mapping
				( type_id ,
				  table_name ,
				  column_name ,
				  value ,
				  last_modified ,
				  modified_by
				)
		SELECT at.type_id,
			   at.table_name,
			   N'record_id'									AS column_name,
			   N'NEXT VALUE FOR dbo.' + s.name				AS value,
			   CURRENT_TIMESTAMP,
			   CURRENT_USER
		  FROM dbo.t_arcm_table at
		  JOIN dbo.t_arcm_type aty
			ON aty.type_id = at.type_id
		  JOIN sys.sequences s
			ON s.name = N'seq_' + RIGHT(at.table_name,(LEN(at.table_name)-2))
		  LEFT JOIN dbo.t_arcm_column_mapping acm
			ON acm.type_id = aty.type_id
		   AND acm.table_name = at.table_name
		   AND acm.column_name = N'record_id'
		 WHERE aty.type_code = @type_code
		   AND at.table_name LIKE N'%validation'
		   AND acm.column_mapping_id IS NULL;
	END;

	IF @type_code = N'ETL_PUR_W'
	BEGIN

		/*
		Generate 3 entries per ETL table:
		- The ETL table for records that passed validation without warning or error (line_status = 0): These records can simply be deleted
		- The ETL table for records that passed validation with warning or error (line_status <> 0): These records are to be moved from live to archive
		- The ETL validation table records will all be moved from live to archive
		*/


		SELECT @table_id = MAX(table_id)
		  FROM t_arcm_table;

		INSERT INTO #code VALUES(N'	WITH cte AS');
		INSERT INTO #code VALUES(N'	(');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   ' + CONVERT(NVARCHAR,@table_id) + N' + ROW_NUMBER() OVER (ORDER BY at.type_name, fn.a)				AS table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END																		AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN N''line_status < 0 AND line_date < (SELECT arc_date FROM #arc_etl_date_pur_w WHERE arc_date_type = N''''WARNINGS'''')''');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN N''validation_severity_level < 0 AND last_modified < (SELECT arc_date FROM #arc_etl_date_pur_w WHERE arc_date_type = N''''WARNINGS'''')''');
		INSERT INTO #code VALUES(N'			   END																		AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'		  FROM ' + @osx_db + N'.dbo.t_adj_type at');
		INSERT INTO #code VALUES(N'		  CROSS APPLY (VALUES(1),(2)) AS fn(a)');
		INSERT INTO #code VALUES(N'		  LEFT JOIN ' + @osx_db + N'.sys.tables t');
		INSERT INTO #code VALUES(N'			ON CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END = t.name');
		INSERT INTO #code VALUES(N'		 WHERE t.name IS NOT NULL -- make sure the tables exist');
		INSERT INTO #code VALUES(N'	)');
		INSERT INTO #code VALUES(N'	INSERT INTO dbo.t_arcm_table');
		INSERT INTO #code VALUES(N'			( type_id ,');
		INSERT INTO #code VALUES(N'			  table_id ,');
		INSERT INTO #code VALUES(N'			  table_name ,');
		INSERT INTO #code VALUES(N'			  parent_table_id ,');
		INSERT INTO #code VALUES(N'			  filter_template ,');
		INSERT INTO #code VALUES(N'			  copy_rows ,');
		INSERT INTO #code VALUES(N'			  delete_rows ,');
		INSERT INTO #code VALUES(N'			  clear_fks ,');
		INSERT INTO #code VALUES(N'			  apply_id_offset ,');
		INSERT INTO #code VALUES(N'			  last_modified ,');
		INSERT INTO #code VALUES(N'			  modified_by');
		INSERT INTO #code VALUES(N'			)');
		INSERT INTO #code VALUES(N'	SELECT cte.*');
		INSERT INTO #code VALUES(N'	  FROM cte');
		INSERT INTO #code VALUES(N'	  LEFT JOIN dbo.t_arcm_table at');
		INSERT INTO #code VALUES(N'		ON cte.type_id = at.type_id');
		INSERT INTO #code VALUES(N'	   AND cte.table_name = at.table_name');
		INSERT INTO #code VALUES(N'	   AND cte.filter_template = at.filter_template');
		INSERT INTO #code VALUES(N'	 WHERE at.table_id IS NULL;');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'	WITH cte AS');
		INSERT INTO #code VALUES(N'	(');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   ' + CONVERT(NVARCHAR,@table_id) + N' + ROW_NUMBER() OVER (ORDER BY at.type_name, fn.a)				AS table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END																		AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN N''line_status < 0 AND line_date < (SELECT arc_date FROM #arc_etl_date_pur_w WHERE arc_date_type = N''''WARNINGS'''')''');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN N''validation_severity_level < 0 AND last_modified < (SELECT arc_date FROM #arc_etl_date_pur_w WHERE arc_date_type = N''''WARNINGS'''')''');
		INSERT INTO #code VALUES(N'			   END																		AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'		  FROM ' + @osx_db + N'.dbo.t_adj_type at');
		INSERT INTO #code VALUES(N'		  CROSS APPLY (VALUES(1),(2)) AS fn(a)');
		INSERT INTO #code VALUES(N'		  LEFT JOIN ' + @osx_db + N'.sys.tables t');
		INSERT INTO #code VALUES(N'			ON CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END = t.name');
		INSERT INTO #code VALUES(N'		 WHERE t.name IS NOT NULL -- make sure the tables exist');
		INSERT INTO #code VALUES(N'	)');
		INSERT INTO #code VALUES(N'	UPDATE at');
		INSERT INTO #code VALUES(N'	   SET at.table_name = cte.table_name,');
		INSERT INTO #code VALUES(N'		   at.parent_table_id  = cte.parent_table_id,');
		INSERT INTO #code VALUES(N'		   at.filter_template  = cte.filter_template,');
		INSERT INTO #code VALUES(N'		   at.copy_rows  = cte.copy_rows,');
		INSERT INTO #code VALUES(N'		   at.delete_rows  = cte.delete_rows,');
		INSERT INTO #code VALUES(N'		   at.clear_fks  = cte.clear_fks,');
		INSERT INTO #code VALUES(N'		   at.apply_id_offset  = cte.apply_id_offset,');
		INSERT INTO #code VALUES(N'		   at.last_modified  = cte.last_modified,');
		INSERT INTO #code VALUES(N'		   at.modified_by  = cte.modified_by');
		INSERT INTO #code VALUES(N'	  FROM cte');
		INSERT INTO #code VALUES(N'	  JOIN dbo.t_arcm_table at');
		INSERT INTO #code VALUES(N'		ON cte.type_id = at.type_id');
		INSERT INTO #code VALUES(N'	   AND cte.table_name = at.table_name');
		INSERT INTO #code VALUES(N'	   AND cte.filter_template = at.filter_template;');

		EXEC p_exec_resultset N'SELECT txt FROM #code ORDER BY [id]'; 
		DELETE FROM #code;
	END;

	IF @type_code = N'ETL_PUR_E'
	BEGIN

		/*
		Generate 3 entries per ETL table:
		- The ETL table for records that passed validation without warning or error (line_status = 0): These records can simply be deleted
		- The ETL table for records that passed validation with warning or error (line_status <> 0): These records are to be moved from live to archive
		- The ETL validation table records will all be moved from live to archive
		*/


		SELECT @table_id = MAX(table_id)
		  FROM t_arcm_table;

		INSERT INTO #code VALUES(N'	WITH cte AS');
		INSERT INTO #code VALUES(N'	(');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   ' + CONVERT(NVARCHAR,@table_id) + N' + ROW_NUMBER() OVER (ORDER BY at.type_name, fn.a)				AS table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END																		AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN N''line_status > 0 AND line_date < (SELECT arc_date FROM #arc_etl_date_pur_e WHERE arc_date_type = N''''ERRORS'''')''');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN N''validation_severity_level > 0 AND last_modified < (SELECT arc_date FROM #arc_etl_date_pur_e WHERE arc_date_type = N''''ERRORS'''')''');
		INSERT INTO #code VALUES(N'			   END																		AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'		  FROM ' + @osx_db + N'.dbo.t_adj_type at');
		INSERT INTO #code VALUES(N'		  CROSS APPLY (VALUES(1),(2)) AS fn(a)');
		INSERT INTO #code VALUES(N'		  LEFT JOIN ' + @osx_db + N'.sys.tables t');
		INSERT INTO #code VALUES(N'			ON CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END = t.name');
		INSERT INTO #code VALUES(N'		 WHERE t.name IS NOT NULL -- make sure the tables exist');
		INSERT INTO #code VALUES(N'	)');
		INSERT INTO #code VALUES(N'	INSERT INTO dbo.t_arcm_table');
		INSERT INTO #code VALUES(N'			( type_id ,');
		INSERT INTO #code VALUES(N'			  table_id ,');
		INSERT INTO #code VALUES(N'			  table_name ,');
		INSERT INTO #code VALUES(N'			  parent_table_id ,');
		INSERT INTO #code VALUES(N'			  filter_template ,');
		INSERT INTO #code VALUES(N'			  copy_rows ,');
		INSERT INTO #code VALUES(N'			  delete_rows ,');
		INSERT INTO #code VALUES(N'			  clear_fks ,');
		INSERT INTO #code VALUES(N'			  apply_id_offset ,');
		INSERT INTO #code VALUES(N'			  last_modified ,');
		INSERT INTO #code VALUES(N'			  modified_by');
		INSERT INTO #code VALUES(N'			)');
		INSERT INTO #code VALUES(N'	SELECT cte.*');
		INSERT INTO #code VALUES(N'	  FROM cte');
		INSERT INTO #code VALUES(N'	  LEFT JOIN dbo.t_arcm_table at');
		INSERT INTO #code VALUES(N'		ON cte.type_id = at.type_id');
		INSERT INTO #code VALUES(N'	   AND cte.table_name = at.table_name');
		INSERT INTO #code VALUES(N'	   AND cte.filter_template = at.filter_template');
		INSERT INTO #code VALUES(N'	 WHERE at.table_id IS NULL;');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'	WITH cte AS');
		INSERT INTO #code VALUES(N'	(');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   ' + CONVERT(NVARCHAR,@table_id) + N' + ROW_NUMBER() OVER (ORDER BY at.type_name, fn.a)				AS table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END																		AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN N''line_status > 0 AND line_date < (SELECT arc_date FROM #arc_etl_date_pur_e WHERE arc_date_type = N''''ERRORS'''')''');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN N''validation_severity_level > 0 AND last_modified < (SELECT arc_date FROM #arc_etl_date_pur_e WHERE arc_date_type = N''''ERRORS'''')''');
		INSERT INTO #code VALUES(N'			   END																		AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'		  FROM ' + @osx_db + N'.dbo.t_adj_type at');
		INSERT INTO #code VALUES(N'		  CROSS APPLY (VALUES(1),(2)) AS fn(a)');
		INSERT INTO #code VALUES(N'		  LEFT JOIN ' + @osx_db + N'.sys.tables t');
		INSERT INTO #code VALUES(N'			ON CASE fn.a');
		INSERT INTO #code VALUES(N'				WHEN 1 THEN at.type_name');
		INSERT INTO #code VALUES(N'				WHEN 2 THEN at.type_name + N''_validation''');
		INSERT INTO #code VALUES(N'			   END = t.name');
		INSERT INTO #code VALUES(N'		 WHERE t.name IS NOT NULL -- make sure the tables exist');
		INSERT INTO #code VALUES(N'	)');
		INSERT INTO #code VALUES(N'	UPDATE at');
		INSERT INTO #code VALUES(N'	   SET at.table_name = cte.table_name,');
		INSERT INTO #code VALUES(N'		   at.parent_table_id  = cte.parent_table_id,');
		INSERT INTO #code VALUES(N'		   at.filter_template  = cte.filter_template,');
		INSERT INTO #code VALUES(N'		   at.copy_rows  = cte.copy_rows,');
		INSERT INTO #code VALUES(N'		   at.delete_rows  = cte.delete_rows,');
		INSERT INTO #code VALUES(N'		   at.clear_fks  = cte.clear_fks,');
		INSERT INTO #code VALUES(N'		   at.apply_id_offset  = cte.apply_id_offset,');
		INSERT INTO #code VALUES(N'		   at.last_modified  = cte.last_modified,');
		INSERT INTO #code VALUES(N'		   at.modified_by  = cte.modified_by');
		INSERT INTO #code VALUES(N'	  FROM cte');
		INSERT INTO #code VALUES(N'	  JOIN dbo.t_arcm_table at');
		INSERT INTO #code VALUES(N'		ON cte.type_id = at.type_id');
		INSERT INTO #code VALUES(N'	   AND cte.table_name = at.table_name');
		INSERT INTO #code VALUES(N'	   AND cte.filter_template = at.filter_template;');

		EXEC p_exec_resultset N'SELECT txt FROM #code ORDER BY [id]'; 
		DELETE FROM #code;
	END;

	IF @type_code = N'LOGS'
	BEGIN
		INSERT INTO #code VALUES(N'	DECLARE @table_id INT;');
		INSERT INTO #code VALUES(N'	SELECT @table_id = MAX(table_id)');
		INSERT INTO #code VALUES(N'	  FROM t_arcm_table;');	  	
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'	WITH cte AS');
		INSERT INTO #code VALUES(N'	(');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_mex_step_log''														AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.job_log_id IN (SELECT mjl.job_log_id FROM t_mex_job_log mjl WHERE mjl.run_date < (SELECT arc_date FROM #arc_log_date WHERE arc_date_type = N''''logs''''))''			AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_mex_step_log_property''														AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.step_log_id IN (SELECT msl.step_log_id FROM t_mex_step_log msl WHERE msl.job_log_id IN (SELECT mjl.job_log_id FROM t_mex_job_log mjl WHERE mjl.run_date < (SELECT arc_date FROM #arc_log_date WHERE arc_date_type = N''''logs'''')))''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_mex_job_log''														AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.run_date < (SELECT arc_date FROM #arc_log_date WHERE arc_date_type = N''''logs'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_ale_event_log''												AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.timestamp < (SELECT arc_date FROM #arc_log_date WHERE arc_date_type = N''''logs'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_ale_event_log_detail''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.timestamp < (SELECT arc_date FROM #arc_log_date WHERE arc_date_type = N''''logs'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N' )');
		INSERT INTO #code VALUES(N'	INSERT INTO dbo.t_arcm_table');
		INSERT INTO #code VALUES(N'			( type_id ,');
		INSERT INTO #code VALUES(N'			  table_id ,');
		INSERT INTO #code VALUES(N'			  table_name ,');
		INSERT INTO #code VALUES(N'			  parent_table_id ,');
		INSERT INTO #code VALUES(N'			  filter_template ,');
		INSERT INTO #code VALUES(N'			  copy_rows ,');
		INSERT INTO #code VALUES(N'			  delete_rows ,');
		INSERT INTO #code VALUES(N'			  clear_fks ,');
		INSERT INTO #code VALUES(N'			  apply_id_offset ,');
		INSERT INTO #code VALUES(N'			  last_modified ,');
		INSERT INTO #code VALUES(N'			  modified_by');
		INSERT INTO #code VALUES(N'			)');
		INSERT INTO #code VALUES(N'	SELECT cte.type_id,');
		INSERT INTO #code VALUES(N'	       @table_id + ROW_NUMBER() OVER (ORDER BY cte.table_name)	AS table_id,');
		INSERT INTO #code VALUES(N'	       cte.table_name,');
		INSERT INTO #code VALUES(N'		   cte.parent_table_id,');
		INSERT INTO #code VALUES(N'		   cte.filter_template,');
		INSERT INTO #code VALUES(N'		   cte.copy_rows,');
		INSERT INTO #code VALUES(N'		   cte.delete_rows,');
		INSERT INTO #code VALUES(N'		   cte.clear_fks,');
		INSERT INTO #code VALUES(N'		   cte.apply_id_offset,');
		INSERT INTO #code VALUES(N'		   cte.last_modified,');
		INSERT INTO #code VALUES(N'		   cte.modified_by');
		INSERT INTO #code VALUES(N'	  FROM cte');
		INSERT INTO #code VALUES(N'	  LEFT JOIN dbo.t_arcm_table at');
		INSERT INTO #code VALUES(N'		ON cte.type_id = at.type_id');
		INSERT INTO #code VALUES(N'	   AND cte.table_name = at.table_name');
		INSERT INTO #code VALUES(N'	 WHERE at.table_id IS NULL');
		INSERT INTO #code VALUES(N'	   AND cte.filter_template IS NOT NULL;');
		INSERT INTO #code VALUES(N'');

		EXEC p_exec_resultset N'SELECT txt FROM #code ORDER BY [id]'; 
		DELETE FROM #code;
		/*
		Create the sequence for t_ale_event_log.
		*/
		IF NOT EXISTS (SELECT *
		                 FROM sys.sequences
						WHERE name = N'seq_ale_event_log_ps')
		BEGIN
			CREATE SEQUENCE dbo.seq_ale_event_log_ps AS INT INCREMENT BY 1;
		END
		
		/*
		We will override the values for record_id as this could be refreshed every time.
		The record_id column is a pure technical column to make the record unique, so no impact is expected
		*/

		IF NOT EXISTS (SELECT *
		                 FROM dbo.t_arcm_column_mapping
						WHERE table_name = N't_ale_event_log'
						  AND column_name = N'event_log_id'
					)
		BEGIN
			INSERT INTO dbo.t_arcm_column_mapping
					( type_id ,
					  table_name ,
					  column_name ,
					  value ,
					  last_modified ,
					  modified_by
					)
			SELECT at.type_id,
				   N't_ale_event_log',
				   N'event_log_id'									AS column_name,
				   N'NEXT VALUE FOR dbo.seq_ale_event_log_ps'			AS value,
				   CURRENT_TIMESTAMP,
				   CURRENT_USER
			  FROM dbo.t_arcm_type at
			 WHERE at.type_code = @type_code
			   AND at.category_id = @category_id;
		END;
	END;

	IF @type_code = N'FDA'
	BEGIN

		SELECT @table_id = MAX(table_id)
		  FROM t_arcm_table;

		INSERT INTO #code VALUES(N'	WITH cte AS');
		INSERT INTO #code VALUES(N'	(');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_daily_ed_balance''													AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.effective_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_tfi_trn_equity_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_tfi_trn_future_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_swap_ir_leg_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_tfi_trn_bond_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_tfi_trn_commodity_valuation''										AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_tfi_trn_option_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_credit_derivatives_valuation''									AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_forex_valuation''												AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_fra_valuation''													AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_loan_valuation''												AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_repo_style_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_swap_fx_fx_leg_valuation''										AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_swap_fx_valuation''												AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_swap_ir_valuation''												AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   1																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'	)');
		INSERT INTO #code VALUES(N'	INSERT INTO dbo.t_arcm_table');
		INSERT INTO #code VALUES(N'			( type_id ,');
		INSERT INTO #code VALUES(N'			  table_id ,');
		INSERT INTO #code VALUES(N'			  table_name ,');
		INSERT INTO #code VALUES(N'			  parent_table_id ,');
		INSERT INTO #code VALUES(N'			  filter_template ,');
		INSERT INTO #code VALUES(N'			  copy_rows ,');
		INSERT INTO #code VALUES(N'			  delete_rows ,');
		INSERT INTO #code VALUES(N'			  clear_fks ,');
		INSERT INTO #code VALUES(N'			  apply_id_offset ,');
		INSERT INTO #code VALUES(N'			  last_modified ,');
		INSERT INTO #code VALUES(N'			  modified_by');
		INSERT INTO #code VALUES(N'			)');
		INSERT INTO #code VALUES(N'	SELECT cte.type_id,');
		INSERT INTO #code VALUES(N'	       ' + CONVERT(NVARCHAR,@table_id) + N' + ROW_NUMBER() OVER (ORDER BY cte.table_name)	AS table_id,');
		INSERT INTO #code VALUES(N'	       cte.table_name,');
		INSERT INTO #code VALUES(N'		   cte.parent_table_id,');
		INSERT INTO #code VALUES(N'		   cte.filter_template,');
		INSERT INTO #code VALUES(N'		   cte.copy_rows,');
		INSERT INTO #code VALUES(N'		   cte.delete_rows,');
		INSERT INTO #code VALUES(N'		   cte.clear_fks,');
		INSERT INTO #code VALUES(N'		   cte.apply_id_offset,');
		INSERT INTO #code VALUES(N'		   cte.last_modified,');
		INSERT INTO #code VALUES(N'		   cte.modified_by');
		INSERT INTO #code VALUES(N'	  FROM cte');
		INSERT INTO #code VALUES(N'	  LEFT JOIN dbo.t_arcm_table at');
		INSERT INTO #code VALUES(N'		ON cte.type_id = at.type_id');
		INSERT INTO #code VALUES(N'	   AND cte.table_name = at.table_name');
		INSERT INTO #code VALUES(N'	 WHERE at.table_id IS NULL');
		
		EXEC p_exec_resultset N'SELECT txt FROM #code ORDER BY [id]'; 
		DELETE FROM #code;


		SELECT @table_id = MAX(table_id)
			  FROM t_arcm_table;

			WITH cte AS
			(
				SELECT at.table_name,
					   0					AS lvl
				  FROM dbo.t_arcm_table at
				  JOIN dbo.t_arcm_type ty
					ON ty.type_id = at.type_id
				 WHERE ty.type_code = @type_code
				   AND ty.category_id = @category_id

				 UNION ALL

				SELECT t2.name,
					   c.lvl+1
				  FROM cte c
				  JOIN sys.tables t
					ON c.table_name = t.name
				  JOIN sys.foreign_keys fk
					ON t.object_id = fk.parent_object_id
				   AND fk.parent_object_id <> fk.referenced_object_id
				  JOIN sys.tables t2
					ON fk.referenced_object_id = t2.object_id
			)
	 
			INSERT INTO dbo.t_arcm_table
					  ( type_id ,
						table_id ,
						table_name ,
						parent_table_id ,
						filter_template ,
						copy_rows ,
						delete_rows ,
						clear_fks ,
						apply_id_offset ,
						last_modified ,
						modified_by
					)
			 SELECT DISTINCT 
					(SELECT type_id FROM dbo.t_arcm_type WHERE type_code = @type_code AND category_id = @category_id)	AS type_id,
					DENSE_RANK() OVER (ORDER BY fk.table_name) + @table_id												AS table_id,
					fk.table_name,
					NULL																								AS parent_table_id,
					NULL																								AS filter_template,
					1																									AS copy_rows,
					0																									AS delete_rows,
					0																									AS clear_fks,
					0																									AS apply_id_offset,
					CURRENT_TIMESTAMP																					AS last_modified,
					CURRENT_USER																						AS modified_by
			   FROM cte fk
			   LEFT JOIN dbo.t_arcm_table at
				 ON at.table_name = fk.table_name
				AND at.type_id IN (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = @type_code AND category_id = @category_id)
			  WHERE at.table_id IS NULL;

			WITH cte AS 
			(
				SELECT at.table_name,
					   MIN(at2.table_id)				AS parent_id
			   
				  FROM dbo.t_arcm_table at
				  JOIN dbo.t_arcm_type aty
					ON aty.type_id = at.type_id
				  JOIN sys.tables t
					ON at.table_name = t.name
				  JOIN sys.foreign_keys fk
					ON t.object_id = fk.parent_object_id
				   AND fk.parent_object_id <> fk.referenced_object_id
				  JOIN sys.tables t2
					ON t2.object_id = fk.referenced_object_id
				  JOIN dbo.t_arcm_table at2
					ON t2.name = at2.table_name
				   AND aty.type_id = at2.type_id
				 WHERE aty.type_code = @type_code
				   AND aty.category_id = @category_id
				 GROUP BY at.table_name
			)
	
			UPDATE at  
			   SET at.parent_table_id = cte.parent_id
			  FROM dbo.t_arcm_table at
			  JOIN dbo.t_arcm_type aty
				ON aty.type_id = at.type_id
			  JOIN cte
				ON cte.table_name = at.table_name
			 WHERE aty.type_code = @type_code
			   AND aty.category_id = @category_id;

			WITH cte AS
			(
				SELECT at.type_id,
					   at.table_id,
					   ROW_NUMBER() OVER (PARTITION BY at.type_id,
													   at.table_id
											  ORDER BY c1.name)			AS sequence_no,
										   
					   N'this.' + c1.name								AS this_expr_template,
					   N'parent.' + c2.name								AS parent_expr_template,
					   CURRENT_TIMESTAMP								AS last_modified,
					   CURRENT_USER										AS modified_by
		   
				  FROM dbo.t_arcm_table at
				  JOIN dbo.t_arcm_type aty
					ON aty.type_id = at.type_id
				  JOIN dbo.t_arcm_table at2
					ON at2.type_id = at.type_id 
				   AND at.parent_table_id = at2.table_id
				  JOIN sys.foreign_keys fk
					ON fk.parent_object_id = OBJECT_ID(at.table_name)
				   AND fk.referenced_object_id = OBJECT_ID(at2.table_name)
				  JOIN sys.foreign_key_columns fkc
					ON fk.object_id = fkc.constraint_object_id
				  JOIN sys.columns c1
					ON c1.object_id = fkc.parent_object_id
				   AND c1.column_id = fkc.parent_column_id
				  JOIN sys.columns c2
					ON c2.object_id = fkc.referenced_object_id
				   AND c2.column_id = fkc.referenced_column_id
				 WHERE aty.type_code = @type_code
				   AND aty.category_id = @category_id 
			)
			INSERT INTO dbo.t_arcm_table_link
					( type_id ,
						table_id ,
						sequence_no ,
						this_expr_template ,
						parent_expr_template ,
						last_modified ,
						modified_by
					)
			SELECT c.*
			  FROM cte c
			  LEFT JOIN dbo.t_arcm_table_link atl
				ON c.type_id = atl.type_id
			   AND c.table_id = atl.table_id
			   AND c.sequence_no = atl.sequence_no
			 WHERE atl.link_id IS NULL;

	END;
	
	IF @type_code = N'FDA_PUR'
	BEGIN

		SELECT @table_id = MAX(table_id)
		  FROM t_arcm_table;

		INSERT INTO #code VALUES(N'	WITH cte AS');
		INSERT INTO #code VALUES(N'	(');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_daily_ed_balance''													AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.effective_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_tfi_trn_equity_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_tfi_trn_future_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_swap_ir_leg_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_tfi_trn_bond_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_tfi_trn_commodity_valuation''										AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_tfi_trn_option_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_credit_derivatives_valuation''									AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_forex_valuation''												AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_fra_valuation''													AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_loan_valuation''												AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_repo_style_valuation''											AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_swap_fx_fx_leg_valuation''										AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_swap_fx_valuation''												AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		UNION ALL');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_trn_swap_ir_valuation''												AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''this.valuation_date < (SELECT arc_date FROM #arc_fda_date_pur WHERE arc_date_type = N''''Valuations'''')''										AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'	)');
		INSERT INTO #code VALUES(N'	INSERT INTO dbo.t_arcm_table');
		INSERT INTO #code VALUES(N'			( type_id ,');
		INSERT INTO #code VALUES(N'			  table_id ,');
		INSERT INTO #code VALUES(N'			  table_name ,');
		INSERT INTO #code VALUES(N'			  parent_table_id ,');
		INSERT INTO #code VALUES(N'			  filter_template ,');
		INSERT INTO #code VALUES(N'			  copy_rows ,');
		INSERT INTO #code VALUES(N'			  delete_rows ,');
		INSERT INTO #code VALUES(N'			  clear_fks ,');
		INSERT INTO #code VALUES(N'			  apply_id_offset ,');
		INSERT INTO #code VALUES(N'			  last_modified ,');
		INSERT INTO #code VALUES(N'			  modified_by');
		INSERT INTO #code VALUES(N'			)');
		INSERT INTO #code VALUES(N'	SELECT cte.type_id,');
		INSERT INTO #code VALUES(N'	       ' + CONVERT(NVARCHAR,@table_id) + N' + ROW_NUMBER() OVER (ORDER BY cte.table_name)	AS table_id,');
		INSERT INTO #code VALUES(N'	       cte.table_name,');
		INSERT INTO #code VALUES(N'		   cte.parent_table_id,');
		INSERT INTO #code VALUES(N'		   cte.filter_template,');
		INSERT INTO #code VALUES(N'		   cte.copy_rows,');
		INSERT INTO #code VALUES(N'		   cte.delete_rows,');
		INSERT INTO #code VALUES(N'		   cte.clear_fks,');
		INSERT INTO #code VALUES(N'		   cte.apply_id_offset,');
		INSERT INTO #code VALUES(N'		   cte.last_modified,');
		INSERT INTO #code VALUES(N'		   cte.modified_by');
		INSERT INTO #code VALUES(N'	  FROM cte');
		INSERT INTO #code VALUES(N'	  LEFT JOIN dbo.t_arcm_table at');
		INSERT INTO #code VALUES(N'		ON cte.type_id = at.type_id');
		INSERT INTO #code VALUES(N'	   AND cte.table_name = at.table_name');
		INSERT INTO #code VALUES(N'	 WHERE at.table_id IS NULL');
		
		EXEC p_exec_resultset N'SELECT txt FROM #code ORDER BY [id]'; 
		DELETE FROM #code;

	END;

	
	IF @type_code = N'FIN_PUR'
	BEGIN

		SELECT @table_id = MAX(table_id)
		  FROM t_arcm_table;

		INSERT INTO #code VALUES(N'	WITH cte AS');
		INSERT INTO #code VALUES(N'	(');
		INSERT INTO #code VALUES(N'		SELECT (SELECT type_id FROM dbo.t_arcm_type WHERE type_code = ''' + @type_code + N''' AND category_id = ' + CONVERT(NVARCHAR(20),@category_id) + N')			AS type_id,');
		INSERT INTO #code VALUES(N'			   N''t_deal_balance''													    AS table_name,');
		INSERT INTO #code VALUES(N'			   NULL																		AS parent_table_id,');
		INSERT INTO #code VALUES(N'			   N''(this.year < (SELECT archive_year FROM #db_mapping) OR (this.year = (SELECT archive_year FROM #db_mapping) AND this.period < (SELECT archive_period FROM #db_mapping)))''		AS filter_template,');
		INSERT INTO #code VALUES(N'			   0																		AS copy_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS delete_rows,');
		INSERT INTO #code VALUES(N'			   1																		AS clear_fks,');
		INSERT INTO #code VALUES(N'			   0																		AS apply_id_offset,');
		INSERT INTO #code VALUES(N'			   CURRENT_TIMESTAMP														AS last_modified,');
		INSERT INTO #code VALUES(N'			   CURRENT_USER																AS modified_by');
		INSERT INTO #code VALUES(N'');
		INSERT INTO #code VALUES(N'	)');
		INSERT INTO #code VALUES(N'	INSERT INTO dbo.t_arcm_table');
		INSERT INTO #code VALUES(N'			( type_id ,');
		INSERT INTO #code VALUES(N'			  table_id ,');
		INSERT INTO #code VALUES(N'			  table_name ,');
		INSERT INTO #code VALUES(N'			  parent_table_id ,');
		INSERT INTO #code VALUES(N'			  filter_template ,');
		INSERT INTO #code VALUES(N'			  copy_rows ,');
		INSERT INTO #code VALUES(N'			  delete_rows ,');
		INSERT INTO #code VALUES(N'			  clear_fks ,');
		INSERT INTO #code VALUES(N'			  apply_id_offset ,');
		INSERT INTO #code VALUES(N'			  last_modified ,');
		INSERT INTO #code VALUES(N'			  modified_by');
		INSERT INTO #code VALUES(N'			)');
		INSERT INTO #code VALUES(N'	SELECT cte.type_id,');
		INSERT INTO #code VALUES(N'	       ' + CONVERT(NVARCHAR,@table_id) + N' + ROW_NUMBER() OVER (ORDER BY cte.table_name)	AS table_id,');
		INSERT INTO #code VALUES(N'	       cte.table_name,');
		INSERT INTO #code VALUES(N'		   cte.parent_table_id,');
		INSERT INTO #code VALUES(N'		   cte.filter_template,');
		INSERT INTO #code VALUES(N'		   cte.copy_rows,');
		INSERT INTO #code VALUES(N'		   cte.delete_rows,');
		INSERT INTO #code VALUES(N'		   cte.clear_fks,');
		INSERT INTO #code VALUES(N'		   cte.apply_id_offset,');
		INSERT INTO #code VALUES(N'		   cte.last_modified,');
		INSERT INTO #code VALUES(N'		   cte.modified_by');
		INSERT INTO #code VALUES(N'	  FROM cte');
		INSERT INTO #code VALUES(N'	  LEFT JOIN dbo.t_arcm_table at');
		INSERT INTO #code VALUES(N'		ON cte.type_id = at.type_id');
		INSERT INTO #code VALUES(N'	   AND cte.table_name = at.table_name');
		INSERT INTO #code VALUES(N'	 WHERE at.table_id IS NULL');
		
		EXEC p_exec_resultset N'SELECT txt FROM #code ORDER BY [id]'; 
		DELETE FROM #code;

		WITH cte AS 
			(
				SELECT at.table_name,
					   MIN(at2.table_id)				AS parent_id
			   
				  FROM dbo.t_arcm_table at
				  JOIN dbo.t_arcm_type aty
					ON aty.type_id = at.type_id
				  JOIN sys.tables t
					ON at.table_name = t.name
				  JOIN sys.foreign_keys fk
					ON t.object_id = fk.parent_object_id
				   AND fk.parent_object_id <> fk.referenced_object_id
				  JOIN sys.tables t2
					ON t2.object_id = fk.referenced_object_id
				  JOIN dbo.t_arcm_table at2
					ON t2.name = at2.table_name
				   AND aty.type_id = at2.type_id
				 WHERE aty.type_code = @type_code
				   AND aty.category_id = @category_id
				 GROUP BY at.table_name
			)
	
			UPDATE at  
			   SET at.parent_table_id = cte.parent_id
			  FROM dbo.t_arcm_table at
			  JOIN dbo.t_arcm_type aty
				ON aty.type_id = at.type_id
			  JOIN cte
				ON cte.table_name = at.table_name
			 WHERE aty.type_code = @type_code
			   AND aty.category_id = @category_id;

			WITH cte AS
			(
				SELECT at.type_id,
					   at.table_id,
					   ROW_NUMBER() OVER (PARTITION BY at.type_id,
													   at.table_id
											  ORDER BY c1.name)			AS sequence_no,
										   
					   N'this.' + c1.name								AS this_expr_template,
					   N'parent.' + c2.name								AS parent_expr_template,
					   CURRENT_TIMESTAMP								AS last_modified,
					   CURRENT_USER										AS modified_by
		   
				  FROM dbo.t_arcm_table at
				  JOIN dbo.t_arcm_type aty
					ON aty.type_id = at.type_id
				  JOIN dbo.t_arcm_table at2
					ON at2.type_id = at.type_id 
				   AND at.parent_table_id = at2.table_id
				  JOIN sys.foreign_keys fk
					ON fk.parent_object_id = OBJECT_ID(at.table_name)
				   AND fk.referenced_object_id = OBJECT_ID(at2.table_name)
				  JOIN sys.foreign_key_columns fkc
					ON fk.object_id = fkc.constraint_object_id
				  JOIN sys.columns c1
					ON c1.object_id = fkc.parent_object_id
				   AND c1.column_id = fkc.parent_column_id
				  JOIN sys.columns c2
					ON c2.object_id = fkc.referenced_object_id
				   AND c2.column_id = fkc.referenced_column_id
				 WHERE aty.type_code = @type_code
				   AND aty.category_id = @category_id 
			)
			INSERT INTO dbo.t_arcm_table_link
					( type_id ,
						table_id ,
						sequence_no ,
						this_expr_template ,
						parent_expr_template ,
						last_modified ,
						modified_by
					)
			SELECT c.*
			  FROM cte c
			  LEFT JOIN dbo.t_arcm_table_link atl
				ON c.type_id = atl.type_id
			   AND c.table_id = atl.table_id
			   AND c.sequence_no = atl.sequence_no
			 WHERE atl.link_id IS NULL;
	END;

	
		
	IF NOT EXISTS (
					SELECT *
					  FROM t_fsp_channel fc
					  JOIN dbo.t_arcm_type at
						ON fc.channel_type = 6000
					   AND fc.object_id = at.type_id
					 WHERE at.type_code = @type_code
				)
	BEGIN
		INSERT INTO dbo.t_fsp_channel
				( channel_name ,
				  channel_title ,
				  channel_type ,
				  object_id ,
				  cssclass_title ,
				  cssclass_frame ,
				  style ,
				  is_header_visible ,
				  is_collapsible ,
				  is_collapsed ,
				  enabled ,
				  owner_type ,
				  owner ,
				  show_mobile ,
				  channel_icon ,
				  width ,
				  height ,
				  last_modified ,
				  modified_by
				)
		SELECT N'ARC_' + @type_code					AS channel_name,
			   @type_description					AS channel_title,
			   6000									AS channel_type,
			   type_id								AS object_id,
			   NULL									AS cssclass_title,
			   NULL									AS cssclass_frame,
			   NULL									AS style,
			   1									AS is_header_visible,
			   1									AS is_collapsible,
			   0									AS is_collapsed,
			   1									AS enabled,
			   0									AS owner_type,
			   N'SYSTEM'							AS owner,
			   0									AS show_mobile,
			   N''									AS channel_icon,
			   N'auto'								AS width,
			   NULL									AS height,
			   CURRENT_TIMESTAMP,
			   CURRENT_USER
		  FROM dbo.t_arcm_type
		 WHERE type_code = @type_code
	END;

	IF NOT EXISTS (
					SELECT 1
					  FROM dbo.t_fsp_content
					 WHERE content_name = @content_name
				)
	BEGIN
		INSERT INTO dbo.t_fsp_content
				( content_name ,
				  layout_name ,
				  enabled ,
				  last_modified ,
				  modified_by
				)
		VALUES  ( @content_name, -- content_name - d_name
				  N'OneColumn' , -- layout_name - d_name
				  1 , -- enabled - bit
				  CURRENT_TIMESTAMP , -- last_modified - datetime
				  CURRENT_USER  -- modified_by - d_user
				)
	END;

	IF NOT EXISTS (
					SELECT 1
					  FROM dbo.t_fsp_content_item fci
					  JOIN dbo.t_fsp_content fc
						ON fc.content_id = fci.content_id  
					  JOIN dbo.t_fsp_channel fch
						ON fch.channel_id = fci.channel_id
					 WHERE fc.content_name = @content_name
					   AND fch.channel_name = N'ARC_' + @type_code
					)
	BEGIN

		INSERT INTO dbo.t_fsp_content_item
				( content_id ,
				  channel_id ,
				  display_order ,
				  display_pane ,
				  enabled ,
				  last_modified ,
				  modified_by
				)
		SELECT (SELECT content_id FROM t_fsp_content WHERE content_name = @content_name)	AS content_id,
			   fc.channel_id,
			   1							AS display_order,
			   N'CenterPane'				AS display_pane,
			   1							AS enabled,
			   CURRENT_TIMESTAMP,
			   CURRENT_USER
		  FROM dbo.t_fsp_channel fc
		 WHERE fc.channel_name = N'ARC_' + @type_code
	END;

	IF NOT EXISTS (
					SELECT 1
					   FROM dbo.t_fsp_rvw_report
					  WHERE report_code = @report_code
				)
	BEGIN
		INSERT INTO dbo.t_fsp_rvw_report
				( report_code ,
				  report_name ,
				  report_desc ,
				  is_dashboard_report ,
				  is_editable ,
				  last_modified ,
				  modified_by
				)
		VALUES  ( @report_code , -- report_code - d_name
				  @report_code , -- report_name - d_name
				  @type_description , -- report_desc - d_description
				  0 , -- is_dashboard_report - d_bit
				  1 , -- is_editable - d_bit
				  CURRENT_TIMESTAMP , -- last_modified - d_last_modified
				  CURRENT_USER  -- modified_by - d_user
				)
	END;

	IF NOT EXISTS (
					SELECT 1
					  FROM dbo.t_fsp_rvw_report_handler frrh
					  JOIN dbo.t_fsp_rvw_report frr
						ON frr.report_id = frrh.report_id
					 WHERE frr.report_code = @report_code
					)
	BEGIN
		INSERT INTO dbo.t_fsp_rvw_report_handler
				( report_id ,
				  handler_id ,
				  display_order ,
				  last_modified ,
				  modified_by
				)
		VALUES  ( (SELECT report_id FROM t_fsp_rvw_report WHERE report_code = @report_code), -- report_id - d_id
				  (SELECT handler_id FROM dbo.t_fsp_rvw_handler WHERE handler_code = N'GenericProcedure'), -- handler_id - d_id
				  1 , -- display_order - d_display_nbr
				  CURRENT_TIMESTAMP , -- last_modified - d_last_modified
				  CURRENT_USER  -- modified_by - d_user
				)
	END;


	IF NOT EXISTS (
					SELECT 1
					  FROM dbo.t_fsp_rvw_report_property frrp
					  JOIN dbo.t_fsp_rvw_report frr
						ON frr.report_id = frrp.report_id
					  JOIN dbo.t_fsp_rvw_handler_property frhp
						ON frhp.property_id = frrp.property_id
					 WHERE frr.report_code = @report_code
				)
	BEGIN
		INSERT INTO dbo.t_fsp_rvw_report_property
				( report_id ,
				  property_id ,
				  property_value ,
				  last_modified ,
				  modified_by
				)
		VALUES  ( (SELECT report_id FROM dbo.t_fsp_rvw_report WHERE report_code = @report_code) , -- report_id - d_id
				  900 , -- property_id - d_id
				  N'p_arcm_handle_store_policy' , -- property_value - d_context_value
				  CURRENT_TIMESTAMP , -- last_modified - d_last_modified
				  CURRENT_USER  -- modified_by - d_user
				);

		INSERT INTO dbo.t_fsp_rvw_report_property
				( report_id ,
				  property_id ,
				  property_value ,
				  last_modified ,
				  modified_by
				)
		VALUES  ( (SELECT report_id FROM dbo.t_fsp_rvw_report WHERE report_code = @report_code) , -- report_id - d_id
				  901 , -- property_id - d_id
				  N'0' , -- property_value - d_context_value
				  CURRENT_TIMESTAMP , -- last_modified - d_last_modified
				  CURRENT_USER  -- modified_by - d_user
				);

	END;


	IF NOT EXISTS (
					SELECT 1
					  FROM t_fsp_rvw_group frg
					  JOIN t_fsp_rvw_report frr
						ON frr.report_id = frg.report_id
					 WHERE frr.report_code = @report_code
				)
	BEGIN
		INSERT INTO dbo.t_fsp_rvw_group
				( report_id ,
				  group_code ,
				  group_name ,
				  group_desc ,
				  display_order ,
				  is_collapsible ,
				  is_collapsed ,
				  mandatory_count ,
				  last_modified ,
				  modified_by
				)
		VALUES  ( (SELECT report_id FROM t_fsp_rvw_report WHERE report_code = @report_code) , -- report_id - d_id
				  @type_description , -- group_code - d_name
				  @type_description , -- group_name - d_name
				  @type_description , -- group_desc - d_description
				  0 , -- display_order - d_display_nbr
				  1 , -- is_collapsible - d_bit
				  0 , -- is_collapsed - d_bit
				  0 , -- mandatory_count - d_count
				  CURRENT_TIMESTAMP , -- last_modified - d_last_modified
				  CURRENT_USER  -- modified_by - d_user
				)
	END;
 
	WITH par AS
	(
		SELECT N'policy_code'			AS parameter_code,
			   'Archive policy'			AS parameter_name,
			   'Archive policy'			AS parameter_desc,
			   NULL						AS context_table_name,
			   NULL						AS context_column_name,
			   1						AS control_type_id,
			   N'nvarchar(128)'			AS data_type,
			   NULL						AS format,
			   NULL						AS statement,
			   NULL						AS statement_header,
			   NULL						AS default_value,
			   1						AS display_order,
			   0						AS is_readonly,
			   1						AS is_mandatory,
			   1						AS is_visible,
			   1						AS send_to_handler

		UNION ALL

		SELECT N'policy_id'				AS parameter_code,
			   'Policy ID'				AS parameter_name,
			   'Policy ID'				AS parameter_desc,
			   NULL						AS context_table_name,
			   NULL						AS context_column_name,
			   3						AS control_type_id,
			   N'int'					AS data_type,
			   NULL						AS format,
			   NULL						AS statement,
			   NULL						AS statement_header,
			   0						AS default_value,
			   2						AS display_order,
			   1						AS is_readonly,
			   1						AS is_mandatory,
			   0						AS is_visible,
			   1						AS send_to_handler

		UNION ALL

		SELECT N'type_id'				AS parameter_code,
			   'Type ID'				AS parameter_name,
			   'Type ID'				AS parameter_desc,
			   NULL						AS context_table_name,
			   NULL						AS context_column_name,
			   3						AS control_type_id,
			   N'int'					AS data_type,
			   NULL						AS format,
			   NULL						AS statement,
			   NULL						AS statement_header,
			   0						AS default_value,
			   3						AS display_order,
			   1						AS is_readonly,
			   1						AS is_mandatory,
			   0						AS is_visible,
			   1						AS send_to_handler

		UNION ALL

		SELECT N'schedule_date'			AS parameter_code,
			   'Schedule Date'			AS parameter_name,
			   'Schedule Date'			AS parameter_desc,
			   NULL						AS context_table_name,
			   NULL						AS context_column_name,
			   6						AS control_type_id,
			   N'datetime'				AS data_type,
			   NULL						AS format,
			   NULL						AS statement,
			   NULL						AS statement_header,
			   NULL						AS default_value,
			   4						AS display_order,
			   0						AS is_readonly,
			   0						AS is_mandatory,
			   1						AS is_visible,
			   1						AS send_to_handler

		UNION ALL

		SELECT N'schedule_number'		AS parameter_code,
			   'Schedule Number'		AS parameter_name,
			   'Schedule Number'		AS parameter_desc,
			   NULL						AS context_table_name,
			   NULL						AS context_column_name,
			   3						AS control_type_id,
			   N'int'					AS data_type,
			   NULL						AS format,
			   NULL						AS statement,
			   NULL						AS statement_header,
			   NULL						AS default_value,
			   5						AS display_order,
			   0						AS is_readonly,
			   0						AS is_mandatory,
			   1						AS is_visible,
			   1						AS send_to_handler

		UNION ALL

		SELECT N'schedule_period'		AS parameter_code,
			   'Schedule Period'		AS parameter_name,
			   'Schedule Period'		AS parameter_desc,
			   NULL						AS context_table_name,
			   NULL						AS context_column_name,
			   7						AS control_type_id,
			   N'nvarchar(128)'			AS data_type,
			   NULL						AS format,
			   N'SELECT period_code, period_name FROM t_arcm_period'	AS statement,
			   N'SELECT ''period_name'', ''Period'''					AS statement_header,
			   NULL						AS default_value,
			   6						AS display_order,
			   0						AS is_readonly,
			   0						AS is_mandatory,
			   1						AS is_visible,
			   1						AS send_to_handler

		UNION ALL

		SELECT N'reference_date'				AS parameter_code,
				'Retention reference date'			AS parameter_name,
				'Date to start calculating retention date from'			AS parameter_desc,
				NULL						AS context_table_name,
				NULL						AS context_column_name,
				6						AS control_type_id,
				N'datetime'		AS data_type,
				NULL						AS format,
				NULL						AS statement,
				NULL						AS statement_header,
				NULL						AS default_value,
				7						AS display_order,
				0						AS is_readonly,
				0						AS is_mandatory,
				1						AS is_visible,
				1						AS send_to_handler

		UNION ALL

		SELECT N'arc_entity'			AS parameter_code,
				'Entity'					AS parameter_name,
				'Entity'					AS parameter_desc,
				NULL						AS context_table_name,
				NULL						AS context_column_name,
				7						AS control_type_id,
				N'd_entity' 				AS data_type,
				NULL						AS format,
				'SELECT entity,description = entity + '' ('' + description + '')''  FROM [%archive_sibling_instance_server%].[%archive_sibling_db%].dbo.t_entity ORDER BY 1, 2'						AS statement,
				NULL						AS statement_header,
				NULL						AS default_value,
				8						AS display_order,
				0						AS is_readonly,
				0						AS is_mandatory,
				1						AS is_visible,
				1						AS send_to_handler

		UNION ALL

		SELECT N'product_retention_nr'	AS parameter_code,
				'Retention number'		AS parameter_name,
				'Together with the retention period, determines the offset compared to the reference date to retain the product transactions. Deals that have an older maturity will be archived together with their related tables.'	AS parameter_desc,
				NULL						AS context_table_name,
				NULL						AS context_column_name,
				3						AS control_type_id,
				N'int' 					AS data_type,
				NULL						AS format,
				NULL						AS statement,
				NULL						AS statement_header,
				NULL						AS default_value,
				9						AS display_order,
				0						AS is_readonly,
				0						AS is_mandatory,
				1						AS is_visible,
				1						AS send_to_handler

		UNION ALL

		SELECT N'product_retention_period'	AS parameter_code,
				'Retention Period'	AS parameter_name,
				'Together with the retention number, determines the offset compared to the reference date to retain the product transactions. Deals that have an older maturity will be archived together with their related tables.'	AS parameter_desc,
				NULL						AS context_table_name,
				NULL						AS context_column_name,
				7						AS control_type_id,
				N'nvarchar(128)' 		AS data_type,
				NULL						AS format,
				N'SELECT period_code, period_name FROM t_arcm_period'	AS statement,
				N'SELECT ''period_name'', ''Period'''	AS statement_header,
				NULL						AS default_value,
				10						AS display_order,
				0						AS is_readonly,
				0						AS is_mandatory,
				1						AS is_visible,
				1						AS send_to_handler

	)

	INSERT INTO dbo.t_fsp_rvw_parameter
			( group_id ,
			  parameter_code ,
			  parameter_name ,
			  parameter_desc ,
			  context_table_name ,
			  context_column_name ,
			  control_type_id ,
			  data_type ,
			  format ,
			  statement ,
			  statement_header ,
			  default_value ,
			  display_order ,
			  is_readonly ,
			  is_mandatory ,
			  is_visible ,
			  send_to_handler ,
			  last_modified ,
			  modified_by
			)
	SELECT frg.group_id,
		   par.*,
		   CURRENT_TIMESTAMP,
		   CURRENT_USER
	  FROM dbo.t_fsp_rvw_group frg
	  JOIN par
		ON 1=1
	  LEFT JOIN dbo.t_fsp_rvw_parameter frp
		ON frp.group_id = frg.group_id
	   AND frp.parameter_code = par.parameter_code
	 WHERE group_code = @type_description
	   AND frp.parameter_id IS NULL;

	IF NOT EXISTS ( 
					SELECT 1
					  FROM dbo.t_fsp_menu_item fmi
					  JOIN dbo.t_fsp_application fa
						ON fa.application_id = fmi.application_id
					  JOIN dbo.t_fsp_menu_item fmi2
						ON fmi2.application_id = fa.application_id
					   AND fmi2.parent_id = fmi.item_id
					 WHERE fa.application_name = N'FSP'
					   AND fmi.item_code = N'FAArch'
					   AND fmi2.item_code = @category_code
					)
	BEGIN
		INSERT INTO dbo.t_fsp_menu_item
				( application_id ,
				  item_code ,
				  item_name ,
				  item_desc ,
				  parent_id ,
				  display_order ,
				  is_default ,
				  action_type ,
				  action_value ,
				  action_properties ,
				  target ,
				  icon ,
				  display_icon_only ,
				  last_modified ,
				  modified_by
				)
		VALUES  ( (SELECT application_id FROM dbo.t_fsp_application WHERE application_name = N'FSP') , -- application_id - d_id
				  @category_code , -- item_code - d_code
				  @category_description , -- item_name - d_name
				  @category_description , -- item_desc - d_description
				  (SELECT item_id FROM dbo.t_fsp_menu_item WHERE item_code = N'FAArch'),
				  99 , -- display_order - d_display_nbr
				  0 , -- is_default - d_bit
				  N'Content' , -- action_type - d_action_type
				  @content_name , -- action_value - d_action_value
				  NULL , -- action_properties - d_action_properties
				  N'' , -- target - d_target
				  NULL , -- icon - d_path
				  0 , -- display_icon_only - d_bit
				  CURRENT_TIMESTAMP , -- last_modified - d_last_modified
				  CURRENT_USER  -- modified_by - d_user
				)
	END;				 

	/*
	Tables need to go because the same table name exists on another schema
	*/

	/*
	Tables with a table name length of more than 70 characters can't be included
	*/


	DROP TABLE #code
END TRY

BEGIN CATCH
	SELECT	@Raiserror = 300000 + ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@Msg = ERROR_MESSAGE();

	RAISERROR (@Msg, @ErrorSeverity, @ErrorState);
	DROP TABLE #code;
	RETURN @Raiserror;
END CATCH
  
RETURN(0); 
GO

IF OBJECT_ID (N'p_generate_archive_config_ps',N'P') IS NOT NULL
	PRINT N'Procedure p_generate_archive_config_ps has been created';
ELSE
	PRINT N'Procedure p_generate_archive_config_ps has not been created due to errors';

GO
