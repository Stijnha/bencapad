USE OneSumX_archive
GO

SET NOCOUNT ON;

DECLARE @platform_install_id INT;

IF NOT EXISTS (
			SELECT 1
			  FROM dbo.t_sys_platform_property
			 WHERE platform_property_name = N'archive_sibling_iis_url'
			 )
BEGIN

	SELECT @platform_install_id = MAX(platform_install_id)
	  FROM dbo.t_sys_platform;
	
	INSERT INTO dbo.t_sys_platform_property
	        ( platform_install_id ,
	          platform_property_name ,
	          platform_property_value ,
	          last_modified ,
	          modified_by
	        )
	SELECT @platform_install_id,
	       'archive_sibling_iis_url' AS platform_property_name,
		   platform_property_value,
		   CURRENT_TIMESTAMP,
		   CURRENT_USER
      FROM OneSumX_archive.dbo.t_sys_platform_property
	 WHERE platform_property_name = N'iis_url'
	   AND last_modified = (SELECT MAX(last_modified)
	                          FROM OneSumX_archive.dbo.t_sys_platform_property
							 WHERE platform_property_name = N'iis_url')
END;


USE OneSumX_archive;
GO

IF NOT EXISTS (
			SELECT 1
			  FROM dbo.t_arcm_period
			 WHERE period_code = N'W'
			)
BEGIN
	INSERT INTO dbo.t_arcm_period
	        ( period_code ,
	          period_name ,
	          last_modified ,
	          modified_by
	        )
	VALUES  ( N'W' , -- period_code - d_name
	          N'Weeks' , -- period_name - d_name
	          CURRENT_TIMESTAMP , -- last_modified - d_last_modified
	          CURRENT_USER  -- modified_by - d_user
	        )
END;

IF NOT EXISTS (
			SELECT 1
			  FROM dbo.t_arcm_period
			 WHERE period_code = N'D'
			)
BEGIN
	INSERT INTO dbo.t_arcm_period
	        ( period_code ,
	          period_name ,
	          last_modified ,
	          modified_by
	        )
	VALUES  ( N'D' , -- period_code - d_name
	          N'Days' , -- period_name - d_name
	          CURRENT_TIMESTAMP , -- last_modified - d_last_modified
	          CURRENT_USER  -- modified_by - d_user
	        )
END;

INSERT INTO dbo.t_domain
        ( domain_id ,
          domain_name ,
          domain_desc ,
          is_rolled_to_next_period ,
          period_roll_to_domain_id ,
          is_rolled_to_next_year ,
          year_roll_to_domain_id ,
          ed_num_periods_forward ,
          ed_num_periods_back ,
          vd_num_periods_forward ,
          vd_num_periods_back ,
          last_modified ,
          modified_by
        )
SELECT DISTINCT d.*
  FROM OneSumX.dbo.t_journal_code a
  JOIN OneSumX.dbo.t_domain d
    ON d.domain_id = a.domain_id
  LEFT JOIN t_journal_code jc
    ON a.journal_code = jc.journal_code
  LEFT JOIN dbo.t_domain d2
    ON d2.domain_id = d.domain_id
 WHERE jc.journal_code IS NULL
   AND d2.domain_id IS NULL

INSERT INTO t_journal_code
SELECT a.*
  FROM OneSumX.dbo.t_journal_code a
  LEFT JOIN t_journal_code jc
    ON a.journal_code = jc.journal_code
 WHERE jc.journal_code IS NULL
