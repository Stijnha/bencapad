USE [OneSumX_archive];
GO

SET NOCOUNT ON

DELETE atl
  FROM dbo.t_arcm_category ac
  JOIN dbo.t_arcm_type at
    ON at.category_id = ac.category_id
  JOIN dbo.t_arcm_table ata
    ON at.type_id = ata.type_id
  JOIN dbo.t_arcm_table_link atl
    ON atl.type_id = ata.type_id 
   AND atl.table_id = ata.table_id
 WHERE ac.category_code = N'Accounting'
   AND at.type_code = N'BALANCES'
   AND ata.table_name IN (N't_srp_key','t_srp_key_ed')

DELETE acm
  FROM dbo.t_arcm_category ac
  JOIN dbo.t_arcm_type at
    ON at.category_id = ac.category_id
  JOIN dbo.t_arcm_table ata
    ON at.type_id = ata.type_id
  JOIN dbo.t_arcm_column_mapping acm
    ON acm.type_id = at.type_id
   AND acm.table_name = ata.table_name
 WHERE ac.category_code = N'Accounting'
   AND at.type_code = N'BALANCES'
   AND ata.table_name IN (N't_srp_key','t_srp_key_ed')

DELETE ata
  FROM dbo.t_arcm_category ac
  JOIN dbo.t_arcm_type at
    ON at.category_id = ac.category_id
  JOIN dbo.t_arcm_table ata
    ON at.type_id = ata.type_id
 WHERE ac.category_code = N'Accounting'
   AND at.type_code = N'BALANCES'
   AND ata.table_name IN (N't_srp_key','t_srp_key_ed')

 USE OneSumX;

UPDATE t_entity
   SET ed_num_periods_back = 15
 WHERE ed_num_periods_back <> 15
   AND entity <> N'001'
   
