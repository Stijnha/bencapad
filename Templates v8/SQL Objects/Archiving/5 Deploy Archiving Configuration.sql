USE master;
GO

SET NOCOUNT ON

ALTER DATABASE [OneSumX_archive] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
GO

USE [OneSumX_archive];
GO

EXEC p_generate_archive_config_ps
		@category_code				= N'PS',
        @category_description		= N'PS Archive',
		@type_code					= N'ETL',
		@type_description			= N'PS ETL Archive',
		@sql_init					= 'IF @reference_date IS NULL
BEGIN
    SELECT @reference_date = cob_date
      FROM OneSumX.dbo.t_entity
     WHERE entity = @arc_entity;
    SELECT @reference_date = ISNULL(@reference_date, dbo.fn_datepart(CURRENT_TIMESTAMP));
END
IF OBJECT_ID(''tempdb..#arc_etl_date'') IS NOT NULL
  DROP TABLE #arc_etl_date;
CREATE TABLE #arc_etl_date (arc_date_type nvarchar(50), arc_date datetime);
IF @product_retention_period = N''W''
BEGIN
	INSERT INTO #arc_etl_date (arc_date_type, arc_date) 
	SELECT ''ETL'', DATEADD(WEEK,@product_retention_nr*-1,@reference_date)
END;
ELSE
BEGIN
	IF @product_retention_period = N''D''
	BEGIN
		INSERT INTO #arc_etl_date (arc_date_type, arc_date) 
		SELECT ''ETL'', DATEADD(DAY,@product_retention_nr*-1,@reference_date)
	END;
	ELSE
	BEGIN
		INSERT INTO #arc_etl_date (arc_date_type, arc_date) 
		SELECT ''ETL'', ISNULL(dbo.fn_arcm_subtract_period(@reference_date, ISNULL(@product_retention_nr, 0), ISNULL(@product_retention_period, '''')), ''19000101'')
	END
END',
	    @sql_before					= NULL,
        @sql_after					= N'IF OBJECT_ID(''tempdb..#arc_etl_date'') IS NOT NULL       DROP TABLE #arc_etl_date';

EXEC p_generate_archive_config_ps
		@category_code				= N'PS',
        @category_description		= N'PS Archive',
		@type_code					= N'FDA',
		@type_description			= N'PS FDA Archive',
		@sql_init					= 'IF @reference_date IS NULL
BEGIN
    SELECT @reference_date = cob_date
      FROM OneSumX.dbo.t_entity
     WHERE entity = @arc_entity;
    SELECT @reference_date = ISNULL(@reference_date, dbo.fn_datepart(CURRENT_TIMESTAMP));
END
IF OBJECT_ID(''tempdb..#arc_fda_date'') IS NOT NULL
  DROP TABLE #arc_fda_date;
CREATE TABLE #arc_fda_date (arc_date_type nvarchar(50), arc_date datetime);
IF @product_retention_period = N''W''
BEGIN
	INSERT INTO #arc_fda_date (arc_date_type, arc_date) 
	SELECT ''Valuations'', DATEADD(WEEK,@product_retention_nr*-1,@reference_date)
END;
ELSE
BEGIN
	IF @product_retention_period = N''D''
	BEGIN
		INSERT INTO #arc_fda_date (arc_date_type, arc_date) 
		SELECT ''Valuations'', DATEADD(DAY,@product_retention_nr*-1,@reference_date)
	END;
	ELSE
	BEGIN
		INSERT INTO #arc_fda_date (arc_date_type, arc_date) 
		SELECT ''Valuations'', ISNULL(dbo.fn_arcm_subtract_period(@reference_date, ISNULL(@product_retention_nr, 0), ISNULL(@product_retention_period, '''')), ''19000101'')
	END
END',
		@sql_before					= NULL,
        @sql_after					= N'IF OBJECT_ID(''tempdb..#arc_fda_date'') IS NOT NULL       DROP TABLE #arc_fda_date';

EXEC p_generate_archive_config_ps
		@category_code				= N'PS',
        @category_description		= N'PS Archive',
		@type_code					= N'FDA_PUR',
		@type_description			= N'PS FDA Archive Purge',
		@sql_init					= 'IF @reference_date IS NULL
BEGIN
    SELECT @reference_date = cob_date
      FROM OneSumX.dbo.t_entity
     WHERE entity = @arc_entity;
    SELECT @reference_date = ISNULL(@reference_date, dbo.fn_datepart(CURRENT_TIMESTAMP));
END
IF OBJECT_ID(''tempdb..#arc_fda_date_pur'') IS NOT NULL
  DROP TABLE #arc_fda_date_pur;
CREATE TABLE #arc_fda_date_pur (arc_date_type nvarchar(50), arc_date datetime);
IF @product_retention_period = N''W''
BEGIN
	INSERT INTO #arc_fda_date_pur (arc_date_type, arc_date) 
	SELECT ''Valuations'', DATEADD(WEEK,@product_retention_nr*-1,@reference_date)
END;
ELSE
BEGIN
	IF @product_retention_period = N''D''
	BEGIN
		INSERT INTO #arc_fda_date_pur (arc_date_type, arc_date) 
		SELECT ''Valuations'', DATEADD(DAY,@product_retention_nr*-1,@reference_date)
	END;
	ELSE
	BEGIN
		INSERT INTO #arc_fda_date_pur (arc_date_type, arc_date) 
		SELECT ''Valuations'', ISNULL(dbo.fn_arcm_subtract_period(@reference_date, ISNULL(@product_retention_nr, 0), ISNULL(@product_retention_period, '''')), ''19000101'')
	END
END',
		@sql_before					= NULL,
        @sql_after					= N'IF OBJECT_ID(''tempdb..#arc_fda_date_pur'') IS NOT NULL       DROP TABLE #arc_fda_date_pur';


EXEC p_generate_archive_config_ps
		@category_code				= N'PS',
        @category_description		= N'PS Archive',
		@type_code					= N'ETL_PUR_W',
		@type_description			= N'PS ETL Warnings Archive Purge',
		@sql_init					= 'IF @reference_date IS NULL
BEGIN
    SELECT @reference_date = cob_date
      FROM OneSumX.dbo.t_entity
     WHERE entity = @arc_entity;
    SELECT @reference_date = ISNULL(@reference_date, dbo.fn_datepart(CURRENT_TIMESTAMP));
END
IF OBJECT_ID(''tempdb..#arc_etl_date_pur_w'') IS NOT NULL
  DROP TABLE #arc_etl_date_pur_w;
CREATE TABLE #arc_etl_date_pur_w (arc_date_type nvarchar(50), arc_date datetime);
IF @product_retention_period = N''W''
BEGIN
	INSERT INTO #arc_etl_date_pur_w (arc_date_type, arc_date) 
	SELECT ''WARNINGS'', DATEADD(WEEK,@product_retention_nr*-1,@reference_date)
END;
ELSE
BEGIN
	IF @product_retention_period = N''D''
	BEGIN
		INSERT INTO #arc_etl_date_pur_w (arc_date_type, arc_date) 
		SELECT ''WARNINGS'', DATEADD(DAY,@product_retention_nr*-1,@reference_date)
	END;
	ELSE
	BEGIN
		INSERT INTO #arc_etl_date_pur_w (arc_date_type, arc_date) 
		SELECT ''WARNINGS'', ISNULL(dbo.fn_arcm_subtract_period(@reference_date, ISNULL(@product_retention_nr, 0), ISNULL(@product_retention_period, '''')), ''19000101'')
	END
END',
		@sql_before					= NULL,
        @sql_after					= N'IF OBJECT_ID(''tempdb..#arc_etl_date_pur_w'') IS NOT NULL       DROP TABLE #arc_etl_date_pur_w';

EXEC p_generate_archive_config_ps
		@category_code				= N'PS',
        @category_description		= N'PS Archive',
		@type_code					= N'ETL_PUR_E',
		@type_description			= N'PS ETL Errors Archive Purge',
		@sql_init					= 'IF @reference_date IS NULL
BEGIN
    SELECT @reference_date = cob_date
      FROM OneSumX.dbo.t_entity
     WHERE entity = @arc_entity;
    SELECT @reference_date = ISNULL(@reference_date, dbo.fn_datepart(CURRENT_TIMESTAMP));
END
IF OBJECT_ID(''tempdb..#arc_etl_date_pur_e'') IS NOT NULL
  DROP TABLE #arc_etl_date_pur_e;
CREATE TABLE #arc_etl_date_pur_e (arc_date_type nvarchar(50), arc_date datetime);
IF @product_retention_period = N''W''
BEGIN
	INSERT INTO #arc_etl_date_pur_e (arc_date_type, arc_date) 
	SELECT ''ERRORS'', DATEADD(WEEK,@product_retention_nr*-1,@reference_date)
END;
ELSE
BEGIN
	IF @product_retention_period = N''D''
	BEGIN
		INSERT INTO #arc_etl_date_pur_e (arc_date_type, arc_date) 
		SELECT ''ERRORS'', DATEADD(DAY,@product_retention_nr*-1,@reference_date)
	END;
	ELSE
	BEGIN
		INSERT INTO #arc_etl_date_pur_e (arc_date_type, arc_date) 
		SELECT ''ERRORS'', ISNULL(dbo.fn_arcm_subtract_period(@reference_date, ISNULL(@product_retention_nr, 0), ISNULL(@product_retention_period, '''')), ''19000101'')
	END
END',
		@sql_before					= NULL,
        @sql_after					= N'IF OBJECT_ID(''tempdb..#arc_etl_date_pur_e'') IS NOT NULL       DROP TABLE #arc_etl_date_pur_e';


EXEC p_generate_archive_config_ps
		@category_code				= N'PS',
        @category_description		= N'PS Archive',
		@type_code					= N'FIN_PUR',
		@type_description			= N'PS Finance Archive Purge',
		@sql_init					= 'DECLARE @archive_date datetime 
SELECT @archive_date = CONVERT(DATE,dbo.fn_arcm_subtract_period(GETDATE(), ISNULL(@product_retention_nr, 0), @product_retention_period));

IF OBJECT_ID(''tempdb..#arc_postings'') IS NOT NULL  
	DROP TABLE #arc_postings;

IF OBJECT_ID(''tempdb..#db_mapping'') IS NOT NULL  
	DROP TABLE #db_mapping;

WITH CTE_post (journal_code, journal_hdr_nr, last_action_date) AS 
(
	SELECT ph.journal_code, ph.journal_hdr_nr, last_action_date = MAX(p.effective_date)    
	  FROM dbo.t_posted_header ph   
	  JOIN dbo.t_posted p     
	    ON p.journal_code = ph.journal_code     
	   AND p.journal_hdr_nr = ph.journal_hdr_nr  
	 GROUP BY ph.journal_code, ph.journal_hdr_nr 
) 

SELECT cte.*    
  INTO #arc_postings   
  FROM CTE_post cte   
 WHERE last_action_date < @archive_date 

SELECT year		AS archive_year,
       period	AS archive_period
  INTO #db_mapping
  FROM [<source_server>].[<source_fsdb>].dbo.t_calendar_period
 WHERE @archive_date BETWEEN period_start_date AND period_end_date
   AND calendar = N''Australia''',
		@sql_before					= NULL,
        @sql_after					= N'IF OBJECT_ID(''tempdb..#arc_postings'') IS NOT NULL       DROP TABLE #arc_postings
IF OBJECT_ID(''tempdb..#db_mapping'') IS NOT NULL       DROP TABLE #db_mapping';

-- Add the Finance Balance policies to the PS page
DECLARE @channel_id INT,
        @content_id INT;

SELECT @channel_id = fc.channel_id
  FROM dbo.t_fsp_channel fc
  JOIN t_arcm_type at
    ON fc.channel_type = 6000
   AND fc.object_id = at.type_id
 WHERE at.type_code = 'BALANCES';

SELECT @content_id = content_id
  FROM dbo.t_fsp_content
 WHERE content_name = N'PS Archive';

IF NOT EXISTS (SELECT 1
				 FROM dbo.t_fsp_content_item
				WHERE content_id = @content_id
				  AND channel_id = @channel_id)
BEGIN
	INSERT INTO dbo.t_fsp_content_item (   content_id ,
	                                       channel_id ,
	                                       display_order ,
	                                       display_pane ,
	                                       enabled ,
	                                       last_modified ,
	                                       modified_by
	                                   )
	VALUES (   @content_id ,      -- content_id - d_id
	           @channel_id ,      -- channel_id - d_id
	           1 ,         -- display_order - int
	           N'CenterPane' ,      -- display_pane - d_name
	           1 ,      -- enabled - bit
	           CURRENT_TIMESTAMP , -- last_modified - datetime
	           CURRENT_USER        -- modified_by - d_user
	       )
END;


SELECT @channel_id = fc.channel_id
  FROM dbo.t_fsp_channel fc
  JOIN t_arcm_type at
    ON fc.channel_type = 6000
   AND fc.object_id = at.type_id
 WHERE at.type_code = 'POSTINGS';

IF NOT EXISTS (SELECT 1
				 FROM dbo.t_fsp_content_item
				WHERE content_id = @content_id
				  AND channel_id = @channel_id)
BEGIN
	INSERT INTO dbo.t_fsp_content_item (   content_id ,
	                                       channel_id ,
	                                       display_order ,
	                                       display_pane ,
	                                       enabled ,
	                                       last_modified ,
	                                       modified_by
	                                   )
	VALUES (   @content_id ,      -- content_id - d_id
	           @channel_id ,      -- channel_id - d_id
	           1 ,         -- display_order - int
	           N'CenterPane' ,      -- display_pane - d_name
	           1 ,      -- enabled - bit
	           CURRENT_TIMESTAMP , -- last_modified - datetime
	           CURRENT_USER        -- modified_by - d_user
	       )
END;

WITH CTE AS
(
	SELECT fci.content_id,
		   fc.channel_id,
		   ROW_NUMBER() OVER (ORDER BY fc.channel_title)	AS rwnbr
	  FROM dbo.t_fsp_content_item fci
	  JOIN dbo.t_fsp_channel fc
		ON fc.channel_id = fci.channel_id
	 WHERE fci.content_id = @content_id
)
UPDATE fci
   SET fci.display_order = c.rwnbr
  FROM CTE c
  JOIN dbo.t_fsp_content_item fci
    ON fci.channel_id = c.channel_id
   AND fci.content_id = c.content_id


USE master;
GO

ALTER DATABASE [OneSumX_archive] SET MULTI_USER;
GO
