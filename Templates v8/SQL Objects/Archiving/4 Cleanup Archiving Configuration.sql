USE [OneSumX_archive];
GO

SET NOCOUNT ON;

-- SELECT *
DELETE
  FROM t_fsp_menu_item
 WHERE item_code IN (N'PS_Purge','PS','ARC_PS')

-- SELECT *
DELETE
  FROM t_fsp_rvw_history_detail
 WHERE parameter_id IN (
					SELECT parameter_id
					  FROM t_fsp_rvw_parameter
					 WHERE group_id IN (
										 SELECT group_id
										  FROM t_fsp_rvw_group
										 WHERE group_code IN ('PS ETL Archive','PS FDA Archive','PS FDA Archive Purge','PS ETL Warnings Archive Purge','PS ETL Errors Archive Purge','PS Finance Archive Purge')))

-- SELECT *
DELETE
  FROM t_fsp_rvw_parameter
 WHERE group_id IN (
					 SELECT group_id
					  FROM t_fsp_rvw_group
					 WHERE group_code IN ('PS ETL Archive','PS FDA Archive','PS FDA Archive Purge','PS ETL Warnings Archive Purge','PS ETL Errors Archive Purge','PS Finance Archive Purge'))

-- SELECT *
DELETE
  FROM t_fsp_rvw_group
 WHERE group_code IN ('PS ETL Archive','PS FDA Archive','PS FDA Archive Purge','PS ETL Warnings Archive Purge','PS ETL Errors Archive Purge','PS Finance Archive Purge')

 -- SELECT *
DELETE
  FROM t_fsp_rvw_report_property
 WHERE report_id IN (SELECT report_id
					  FROM t_fsp_rvw_report
					 WHERE report_code IN (N'arcm_ETL','arcm_FDA','arcm_FDA_PUR','arcm_ETL_PUR_W','arcm_ETL_PUR_E','ARC_FIN_PUR'))

-- SELECT *
DELETE
  FROM t_fsp_rvw_report_handler
 WHERE report_id IN (SELECT report_id
					  FROM t_fsp_rvw_report
					 WHERE report_code IN (N'arcm_ETL','arcm_FDA','arcm_FDA_PUR','arcm_ETL_PUR_W','arcm_ETL_PUR_E','ARC_FIN_PUR'))

-- SELECT *
DELETE
  FROM t_fsp_rvw_history
 WHERE report_id IN (SELECT report_id
					  FROM t_fsp_rvw_report
					 WHERE report_code IN (N'arcm_ETL','arcm_FDA','arcm_FDA_PUR','arcm_ETL_PUR_W','arcm_ETL_PUR_E','ARC_FIN_PUR'))

-- SELECT *
DELETE
  FROM t_fsp_rvw_report
 WHERE report_code IN (N'arcm_ETL','arcm_FDA','arcm_FDA_PUR','arcm_ETL_PUR_W','arcm_ETL_PUR_E','ARC_FIN_PUR')

-- SELECT *
DELETE
  FROM t_fsp_content_item
 WHERE content_id IN (SELECT content_id
					  FROM t_fsp_content
					 WHERE content_name IN ('PS Archive','PS_Purge Archive'))

-- SELECT *
DELETE
  FROM t_fsp_content
 WHERE content_name IN ('PS Archive','PS_Purge Archive')

-- SELECT *
DELETE
  FROM t_fsp_channel 
 WHERE channel_name IN (N'ARC_ETL','ARC_FDA','ARC_FDA_PUR','ARC_ETL_PUR_W','ARC_ETL_PUR_E','ARC_FIN_PUR')

-- SELECT *
DELETE
  FROM t_arcm_table_link
 WHERE type_id IN (SELECT type_id
					  FROM t_arcm_type
					 WHERE category_id IN (SELECT category_id
											  FROM t_arcm_category
											 WHERE category_code IN ('PS','PS_Purge')))
-- SELECT *
DELETE
  FROM t_arcm_table
 WHERE type_id IN (SELECT type_id
					  FROM t_arcm_type
					 WHERE category_id IN (SELECT category_id
											  FROM t_arcm_category
											 WHERE category_code IN ('PS','PS_Purge')))

-- SELECT *
DELETE
  FROM t_arcm_column_mapping
 WHERE type_id IN (SELECT type_id
					  FROM t_arcm_type
					 WHERE category_id IN (SELECT category_id
											  FROM t_arcm_category
											 WHERE category_code IN ('PS','PS_Purge')))

-- SELECT *
DELETE
  FROM t_arcm_policy
 WHERE type_id IN (SELECT type_id
					  FROM t_arcm_type
					 WHERE category_id IN (SELECT category_id
											  FROM t_arcm_category
											 WHERE category_code IN ('PS','PS_Purge')))

-- SELECT *
DELETE
  FROM t_arcm_type
 WHERE category_id IN (SELECT category_id
						  FROM t_arcm_category
						 WHERE category_code IN ('PS','PS_Purge'))

-- SELECT *
DELETE
  FROM t_arcm_category
 WHERE category_code IN ('PS','PS_Purge')




