USE OneSumX;
GO

IF OBJECT_ID (N'p_rpt_finale_ps',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_rpt_finale_ps...';
  DROP PROCEDURE p_rpt_finale_ps;
END
GO

PRINT N'Creating procedure p_rpt_finale_ps...';
GO
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ************************************************

CREATE PROCEDURE dbo.p_rpt_finale_ps (@activity_date       DATETIME = NULL,
                                       @table_name          NVARCHAR(500) = NULL,
                                       @debug               BIT = 0,
									   @execute_immediately BIT = 1)

AS

SET NOCOUNT ON;

DECLARE @code_block            NVARCHAR(4000) = N'',
        @ErrorSeverity         INT,
		@ErrorState            INT,
		@Msg                   NVARCHAR(4000),
		@ProcName              SYSNAME = OBJECT_NAME(@@PROCID),
        @Raiserror             INT,
		@Rowcount              INT,
        @table_name2            NVARCHAR(200),
        @ale_history_table_name NVARCHAR(200);

IF OBJECT_ID(N'tempdb..#finale') IS NOT NULL
	DROP TABLE #finale;

CREATE TABLE #finale (
  table_name                 SYSNAME,
  [date]                     NVARCHAR(100),
  [user]					 NVARCHAR(200) NULL,
  [action]                   NVARCHAR(100),
  records                    BIGINT);

CREATE TABLE #code ([id] INT            IDENTITY(1,1)  NOT NULL,  
                    txt  NVARCHAR(4000)                NOT NULL);

BEGIN TRY
  DECLARE cur CURSOR FOR
  SELECT table_name, ale_history_table_name
    FROM t_ale_config_table
   WHERE ale_enabled_ind = 1
     AND ale_history_table_name <> N''
     AND ale_history_table_name IS NOT NULL
	 AND table_name = ISNULL(@table_name,table_name);

  OPEN cur;
  FETCH NEXT FROM cur into @table_name2, @ale_history_table_name;

  WHILE @@FETCH_STATUS = 0
  BEGIN
    INSERT INTO #code(txt) SELECT N'INSERT INTO #finale (';
    INSERT INTO #code(txt) SELECT N'       table_name,';
    INSERT INTO #code(txt) SELECT N'       [date],';
	INSERT INTO #code(txt) SELECT N'       [user],';
    INSERT INTO #code(txt) SELECT N'       [action],';
    INSERT INTO #code(txt) SELECT N'       records)';
    INSERT INTO #code(txt) SELECT N'SELECT N''' + @table_name2 + ''' AS fda_table,';
    INSERT INTO #code(txt) SELECT N'       CONVERT(NVARCHAR, ale.ale_modification_timestamp, 107) AS ale_timestamp,';
	INSERT INTO #code(txt) SELECT N'       u.first_name + N'' '' + u.last_name AS ale_user,';
    INSERT INTO #code(txt) SELECT N'       CASE WHEN CHARINDEX (N''U'', ale_modification_indicator) > 0 THEN N''Update''';
    INSERT INTO #code(txt) SELECT N'            WHEN CHARINDEX (N''I'', ale_modification_indicator )> 0 THEN N''Insert''';
    INSERT INTO #code(txt) SELECT N'            WHEN CHARINDEX (N''D'', ale_modification_indicator )> 0 THEN N''Delete''';
    INSERT INTO #code(txt) SELECT N'       END AS action,';
    INSERT INTO #code(txt) SELECT N'       COUNT(*) AS records';
    INSERT INTO #code(txt) SELECT N'  FROM ' + @ale_history_table_name + ' ale';
	INSERT INTO #code(txt) SELECT N'  LEFT JOIN dbo.t_ale_event_log l';
	INSERT INTO #code(txt) SELECT N'    ON ale.event_log_id = l.event_log_id';
	INSERT INTO #code(txt) SELECT N'  LEFT JOIN t_ale_user au';
	INSERT INTO #code(txt) SELECT N'    ON l.ale_user_id = au.ale_user_id';
	INSERT INTO #code(txt) SELECT N'  LEFT JOIN t_sec_users u';
	INSERT INTO #code(txt) SELECT N'    ON UPPER(au.username) = u.user_name_ucase';

    IF @activity_date IS NOT NULL
    BEGIN
      INSERT INTO #code(txt) SELECT N' WHERE ale.ale_modification_timestamp BETWEEN ''' + CONVERT(NVARCHAR(100),@activity_date) + ''' AND ''' + CONVERT(NVARCHAR(100),@activity_date + 1) + '''';
    END;
    INSERT INTO #code(txt) SELECT N' GROUP BY CONVERT(NVARCHAR, ale.ale_modification_timestamp, 107),';
	INSERT INTO #code(txt) SELECT N'       u.first_name,';
	INSERT INTO #code(txt) SELECT N'       u.last_name,';
    INSERT INTO #code(txt) SELECT N'       CASE WHEN CHARINDEX (N''U'', ale_modification_indicator) > 0 THEN N''Update''';
    INSERT INTO #code(txt) SELECT N'            WHEN CHARINDEX (N''I'', ale_modification_indicator )> 0 THEN N''Insert''';
    INSERT INTO #code(txt) SELECT N'            WHEN CHARINDEX (N''D'', ale_modification_indicator )> 0 THEN N''Delete''';
    INSERT INTO #code(txt) SELECT N'       END;';
    INSERT INTO #code(txt) SELECT N'';
    FETCH NEXT FROM cur into @table_name2, @ale_history_table_name;

  END;

  INSERT INTO #code(txt) SELECT N'SELECT *';
  INSERT INTO #code(txt) SELECT N'  FROM #finale';
  INSERT INTO #code(txt) SELECT N' ORDER BY CONVERT(DATETIME,date) DESC,';
  INSERT INTO #code(txt) SELECT N'       table_name ASC;';

  CLOSE cur;
  DEALLOCATE cur;

  IF @debug = 1  
  BEGIN
    DECLARE c_code CURSOR LOCAL FAST_FORWARD   
    FOR SELECT txt FROM #code ORDER BY [id];
  
    OPEN c_code;
    FETCH NEXT FROM c_code INTO @code_block;
    WHILE @@FETCH_STATUS = 0  
    BEGIN  
      PRINT @code_block;
      FETCH NEXT FROM c_code INTO @code_block;
    END;
    CLOSE c_code;
    DEALLOCATE c_code;
  END  

  IF @execute_immediately = 1  
  BEGIN  
    EXEC p_exec_resultset N'SELECT txt FROM #code ORDER BY [id]'; 
  END;  

  DROP TABLE #finale;
  DROP TABLE #code;

END TRY

BEGIN CATCH
  SELECT @Raiserror = 300000 + ERROR_NUMBER(),
         @ErrorSeverity = ERROR_SEVERITY(),
         @ErrorState = ERROR_STATE(),
         @Msg = ERROR_MESSAGE();
  RAISERROR (@Msg, @ErrorSeverity, @ErrorState);
  RETURN @Raiserror;
END CATCH
  
RETURN(0); 
GO

IF OBJECT_ID (N'p_rpt_finale_ps',N'P') IS NOT NULL
  PRINT N'Procedure p_rpt_finale_ps has been created';
ELSE
  PRINT N'Procedure p_rpt_finale_ps has not been created due to errors';

GO
