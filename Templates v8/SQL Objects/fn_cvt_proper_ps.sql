USE OneSumX;
GO

IF OBJECT_ID(N'fn_cvt_proper_ps', N'FN') IS NOT NULL
BEGIN
  PRINT N'Dropping function fn_cvt_proper_ps';
  DROP FUNCTION dbo.fn_cvt_proper_ps;
END
GO

PRINT N'Creating function fn_cvt_proper_ps';
GO
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************

CREATE FUNCTION dbo.fn_cvt_proper_ps
(
        @input                  NVARCHAR(MAX),
		@convert_all            BIT
)
RETURNS NVARCHAR(MAX)
AS

BEGIN
  DECLARE @converted NVARCHAR(MAX);
  IF @convert_all = 0
  BEGIN
    SELECT @converted = UPPER(LEFT(@input,1)) + LOWER (RIGHT(@input,LEN(@input)-1));
  END
  ELSE -- IF @convert_all = 0
  BEGIN
    SELECT @converted = N'';
    SELECT @converted = @converted + UPPER(LEFT(value,1)) + LOWER (RIGHT(value,LEN(value)-1)) + N' '
      FROM dbo.fn_parse_list (@input, N'',N' ');    
  END
  RETURN @converted;
END
GO

IF OBJECT_ID (N'fn_cvt_proper_ps', N'FN') IS NOT NULL
  PRINT N'Function fn_cvt_proper_ps has been created';
ELSE
  PRINT N'Function fn_cvt_proper_ps has not been created due to errors';

GO


