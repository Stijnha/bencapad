USE OneSumX;
GO

SET NOCOUNT ON;

SELECT *
  FROM t_sys_configuration_core scc
 WHERE config_id IN (SELECT config_id
                       FROM t_sys_configuration
                      WHERE frp_component = N'FOUNDATION - SECURITYENGINE'
                        AND config_name IN (N'BYPASS_SECURITY_FOR_SYSADMIN',N'REPLICATE_FS_SECURITY_TO_SQL'));


EXEC p_createpopulationscript
	@object_name = N't_sys_configuration_core',
	@add_existence_check = 1,
	@add_update = 1,
	@data_filter = N'config_id IN (SELECT config_id
                       FROM t_sys_configuration
                      WHERE frp_component = N''FOUNDATION - SECURITYENGINE''
                        AND config_name IN (N''BYPASS_SECURITY_FOR_SYSADMIN'',N''REPLICATE_FS_SECURITY_TO_SQL''))',
	@field_search = N'config_id',
	@table_search = N't_sys_configuration',
	@field_replace = N'frp_component, config_name',
	@do_not_generate_insert = 1;

SELECT *
  FROM t_sec_roles;

EXEC p_createpopulationscript
	@object_name = N't_sec_roles',
	@add_existence_check = 1,
	@add_update = 0,
	@field_id = 'role_id',
	@field_existence_check = N'role_name';

SELECT *
  FROM t_sec_settings
 WHERE sec_type = 1;

EXEC p_createpopulationscript
	@object_name = N't_sec_settings',
	@add_existence_check = 1,
	@add_update = 1,
	@data_filter = N'sec_type = 1',
	@field_search = N'sec_id = role_id;permission_id',
	@table_search = N't_sec_roles; t_sec_permissions',
	@field_replace = N'role_name; description';

SELECT *
  FROM t_sec_active_directory_group

IF @@ROWCOUNT > 0
BEGIN

	EXEC p_createpopulationscript
		@object_name = N't_sec_active_directory_group',
		@add_existence_check = 1,
		@add_update = 1,
		@field_id = 'ads_group_id',
		@field_existence_check = N'ads_distinguished_name';
END;

SELECT *
  FROM t_sec_active_directory_group_role_mapping

IF @@ROWCOUNT > 0
BEGIN
	EXEC p_createpopulationscript
		@object_name = N't_sec_active_directory_group_role_mapping',
		@add_existence_check = 1,
		@add_update = 1,
		@field_search = N'role_id;ads_group_id',
		@table_search = N't_sec_roles; t_sec_active_directory_group',
		@field_replace = N'role_name; ads_distinguished_name';
END;
/*
SELECT *
  FROM t_sec_users
 WHERE is_application_user = 0;

EXEC p_createpopulationscript
	@object_name = N't_sec_users',
	@add_existence_check = 1,
	@add_update = 1,
	@data_filter = N'is_application_user = 0',
	@field_id = 'user_id',
	@field_existence_check = N'user_name_ucase';

SELECT *
  FROM t_sec_members
 WHERE user_id IN (SELECT user_id FROM t_sec_users WHERE is_application_user = 0);

EXEC p_createpopulationscript
	@object_name = N't_sec_members',
	@add_existence_check = 1,
	@add_update = 1,
	@data_filter = N'user_id IN (SELECT user_id FROM t_sec_users WHERE is_application_user = 0)',
	@field_search = N'role_id;user_id',
	@table_search = N't_sec_roles; t_sec_users',
	@field_replace = N'role_name; user_name_ucase';
*/