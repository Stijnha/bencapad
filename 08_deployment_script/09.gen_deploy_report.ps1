﻿#requires -version 3.0

#create a SQL Server report

[cmdletbinding()]

Param(
[string]$computername=$env:computername
)
#cls

#get ini file contents
#set local variables
$loc=split-path -parent $MyInvocation.MyCommand.Definition
$timestamp=get-date -f yyyyMMdd_hhmmss
$ini_path = $loc + "\env.ini"

Get-Content $ini_path | foreach-object -begin {$h=@{}} -process { $k = [regex]::split($_,'='); if(($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) { $h.Add($k[0], $k[1]) } }

$computername=$env:computername
$content=get-content $ini_path
$ServerName=$content[0]
$OSXDataBase=$content[1]
$RGDatabase=$content[2]
$master=$content[3]
$Package=$content[4]
$Project =$content[5]

cd $loc
$curr = get-location

$path="$curr\sqlrpt.htm"


#define an empty array to hold all of the HTML fragments
$fragments=@()

#save current location so I can set it back after importing SQL module


#import the SQL module
#Import-Module SQLPS -DisableNameChecking
#change the location back



#get uptime
Write-Verbose "Getting SQL Server uptime"
$starttime = Invoke-Sqlcmd -Query 'SELECT CURRENT_TIMESTAMP' -ServerInstance "$ServerName" -database master 
$version = Invoke-Sqlcmd -Query 'Select @@version AS Version' -ServerInstance "$ServerName" -database master
$user = Invoke-Sqlcmd -Query 'SELECT SYSTEM_USER' -ServerInstance "$ServerName" -database master 


#create an object
$uptime = New-Object -TypeName PSObject -Property @{
 'Install Date' = $starttime.Item(0)
 'Installed By' = $user.item(0)
 'Package No' = $Package
 'Project Name' = $Project 
 }
 
$tmp = $uptime | ConvertTo-HTML -fragment -AS List
$fragments += $tmp.replace("|","<br>")

#get services
$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server=$ServerName;Database=$OSXDataBase;Integrated Security=True"
$SqlConnection.Open()
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = "select top 1 platform_property_value from t_sys_platform_property
where platform_property_name = 'iis_computer'
order by platform_install_id desc"
$SqlCmd.Connection = $SqlConnection
$iismac = $SqlCmd.ExecuteScalar()
$SqlConnection.Close()


Write-Verbose "Querying services"
$services1 = Get-WMIObject Win32_Service -Computer $ServerName | Where-Object {($_.Name -like 'MSSQLSERVER') -or ($_.Name -like '*ReportServer')}
$services2=Get-WMIObject Win32_Service -Computer $iismac | Where-Object {($_.Name -like '*Tomcat7') -or ($_.Name -like '*TaskRunner') -or ($_.Name -like '*W3SVC') }

#$services2 = Get-Service -DisplayName *Offline* -ComputerName $computername
$fragments += "<h3>Database Services on: $ServerName</h3>"
$services= $services1 | Select Name,State,Status,StartMode
$fragments += $services | ConvertTo-HTML -Fragment

$fragments += "<h3>Web Services on: $iismac</h3>"
$services = $services2  | Select Name,State,Status,StartMode
#$fragments += "<h3>OnesumX Services</h3>"
$fragments += $services | ConvertTo-HTML -Fragment



#get deployment status
$contents = Invoke-Sqlcmd -Query 'SELECT object, type, status,execution_date, modified_by   FROM t_deployment_ps order by status' -ServerInstance "$ServerName" -database $OSXDataBase
$fragments += "<h3>Deployment Result</h3>"
$fragments +=  $contents | Select object,type, status,execution_date, modified_by |ConvertTo-HTML -fragment


#get databases
Write-Verbose "Querying databases"

$dbpath1 = Invoke-Sqlcmd -Query 'SELECT a.NAME, a.FILENAME, [FILE_SIZE_MB] = convert(decimal(12,2),round(a.size/128.000,2)),[SPACE_USED_MB] = convert(decimal(12,2),round(fileproperty(a.name,''SpaceUsed'')/128.000,2)), [FREE_SPACE_MB] = convert(decimal(12,2),round((a.size-fileproperty(a.name,''SpaceUsed''))/128.000,2)) , [FREE_SPACE_%] = convert(decimal(12,2),(convert(decimal(12,2),round((a.size-fileproperty(a.name,''SpaceUsed''))/128.000,2))/ convert(decimal(12,2),round(a.size/128.000,2)) * 100))FROM dbo.sysfiles a ORDER BY [Name]' -ServerInstance "$ServerName" -database $OSXDataBase
$dbpath2 = Invoke-Sqlcmd -Query 'SELECT a.NAME, a.FILENAME, [FILE_SIZE_MB] = convert(decimal(12,2),round(a.size/128.000,2)),[SPACE_USED_MB] = convert(decimal(12,2),round(fileproperty(a.name,''SpaceUsed'')/128.000,2)), [FREE_SPACE_MB] = convert(decimal(12,2),round((a.size-fileproperty(a.name,''SpaceUsed''))/128.000,2)) , [FREE_SPACE_%] = convert(decimal(12,2),(convert(decimal(12,2),round((a.size-fileproperty(a.name,''SpaceUsed''))/128.000,2))/ convert(decimal(12,2),round(a.size/128.000,2)) * 100))FROM dbo.sysfiles a ORDER BY [Name]' -ServerInstance "$ServerName" -database $RGDatabase

$dbpath = $dbpath1 + $dbpath2

$fragments += "<h3>Database Utilization</h3>"
#$fragments +=  $dbpath | Select Name,Size,DataSpaceUsage,SpaceAvailable,
#@{Name="PercentFree";Expression={ [math]::Round((($_.SpaceAvailable/1kb)/$_.size)*100,2) }} | 
#Sort PercentFree | ConvertTo-HTML -fragment
$fragments +=  $dbpath | Select NAME,FILENAME,FILE_SIZE_MB,SPACE_USED_MB,FREE_SPACE_MB,FREE_SPACE_% |ConvertTo-HTML -fragment


#define the HTML style
Write-Verbose "preparing report"
$imagefile = "$curr\client.png"
$ImageBits = [Convert]::ToBase64String((Get-Content $imagefile -Encoding Byte))
$ImageHTML = "<img src=data:image/png;base64,$($ImageBits) alt='db utilization'/>"

$head = @"
<style>
body { background-color:#FAFAFA;
       font-family:calibri;
       font-size:11pt; }
td, th { border:1px solid black; 
         border-collapse:collapse; }
th { color:white;
     background-color:black; }
table, tr, td, th { padding: 2px; margin: 0px }
tr:nth-child(odd) {background-color: lightgreen}
table { margin-left:50px; }
img
{
float:left;
margin: 0px 25px;
}
.danger {background-color: red}
.warn {background-color: yellow}
</style>
$imagehtml
<H2>EFS Deployment: $Computername</H2>
<br>
"@

#create the HTML document
ConvertTo-HTML -Head $head -Body $fragments -PostContent "<i>report generated: $(Get-Date)</i>" |
Out-File -FilePath $path -Encoding ascii

Write-Verbose "Opening report"
Invoke-Item $path

Write-host "END OF PROCESS"

#end of script