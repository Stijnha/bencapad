
#cls
#set local variables
$loc=split-path -parent $MyInvocation.MyCommand.Definition
$timestamp=get-date -f yyyyMMdd_hhmmss
$logFile="$loc\logs\osx_sqldeploy_summary_$timestamp.log"
$sumlog="$loc\logs\osx_sqldeploy_detailed_$timestamp.log"
$filecount=0
$success=0
$failed=0

#get ini file contents
$ini_path = $loc + "\env.ini"

Get-Content $ini_path | foreach-object -begin {$h=@{}} -process { $k = [regex]::split($_,'='); if(($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) { $h.Add($k[0], $k[1]) } }

$content=get-content $ini_path
$ServerName=$content[0]
$OSXDataBase=$content[1]
$RGDatabase=$content[2]
$master=$content[3]


#initiate log file
mkdir -path $loc\logs -ErrorAction silentlycontinue
get-date | out-file $logFile

#get list of SQL files to be deployed
cd $loc
cd ..\02_onesumxdb

$sqlloc=Get-Location
Write-host "[OSX DB] START OF PROCESS"

#check if any sql file exists
$files = Get-ChildItem -path $sqlloc -recurse | where {$_.extension -eq ".sql"}
If ($files -eq $null)
{
	write-host "NO SQL files found"
    "***********              NO SQL files found            *************" | out-file $logFile -Append	 
}
else  
{

"----------------------------------------------------------------------------------" | out-file $logFile -Append

"***********              Deployment of SQL Files Started             *************" | out-file $logFile -Append

write-host "----------------------------------------------------------------------------------"
write-host "***********              Deployment of SQL Files Started             *************"


"***********              Listing SQL files to be executed            *************" | out-file $logFile -Append
write-host "***********              Listing SQL files to be executed            *************"

$sqlfiles = get-childitem "${sqlloc}" -recurse | where {$_.extension -eq ".sql"} | % {$_.FullName}


foreach($runfile in $sqlfiles)
{

$filecount++
$filename=split-path $runfile -Leaf


"---- Executing file ....... [$filecount]. $filename" | out-file $sumlog -append


Invoke-Sqlcmd -server "$ServerName" -Database "$OSXDataBase" -InputFile "$runfile" -ErrorAction Continue -QueryTimeout([int]::MaxValue) -Verbose 4>&1>> $sumlog


    if($?)
        {
            $success++           
            write-host "Deployment successful for:" $filename -ForegroundColor Green
             "Deployment successful for: ${filename}" | out-file $logfile -append
            $sql="INSERT INTO $OSXDataBase.dbo.t_deployment_ps (object, type, status, execution_date, modified_by) VALUES (N'${runfile}', N'Sql Object', N'SUCCESS', CURRENT_TIMESTAMP, dbo.fn_user())"
            Invoke-Sqlcmd -Query $sql  -server "$ServerName" -Database "$OSXDataBase"
            #write-host $sql
            $sql=''
    
        }
    else 
        {

            $failed++
            $Error[0].Exception | out-file $sumlog -Append
            
            $sql="INSERT INTO dbo.t_deployment_ps (object, type, status, execution_date, modified_by) VALUES (N'${runfile}', N'Sql Object', N'FAILED', CURRENT_TIMESTAMP, dbo.fn_user())"
            Invoke-Sqlcmd -Query $sql  -server "$ServerName" -Database "$OSXDataBase"
        
            "Deployment FAILED for: ${filename}" | out-file $logfile -append
            write-host "Deployment FAILED for:" $filename -ForegroundColor RED
            
            $sql=''       

         }

}



#Disconnection from MS SQL Server 
"***********              Deployment of SQL Files Completed             *************" | out-file $logfile -append
write-host "***********              Deployment of SQL Files Completed             *************" | out-file $logfile -append


"Files successfully deployed: $success" | out-file $logfile -append
"Files failed deployment: $failed" | out-file $logfile -append
"Total files deployment: $filecount" | out-file $logfile -append


Write-Host "Files successfully deployed: $success"
Write-Host "Files failed deployment: $failed" 
Write-Host "Total files deployment: $filecount" 

}

Function end-proc {

write-host '***End of Process***'
end 
} 

Write-host "END OF PROCESS"

#end of script