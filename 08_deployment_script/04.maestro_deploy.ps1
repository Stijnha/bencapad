﻿
#cls
#set local variables
$loc=split-path -parent $MyInvocation.MyCommand.Definition
$timestamp=get-date -f yyyyMMdd_hhmmss
$logFile="$loc\logs\maestro_deploy_$timestamp.log"


#get ini file contents
$ini_path = $loc + "\env.ini"


Get-Content $ini_path | foreach-object -begin {$h=@{}} -process { $k = [regex]::split($_,'='); if(($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) { $h.Add($k[0], $k[1]) } }

$content=get-content $ini_path
$ServerName=$content[0]
$OSXDataBase=$content[1]
$RGDatabase=$content[2]
$master=$content[3]

#initiate log file
mkdir -path $loc\logs -ErrorAction silentlycontinue
get-date | out-file $logFile


#get remote IIS directory
$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server=$ServerName;Database=$OSXDataBase;Integrated Security=True"
$SqlConnection.Open()
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = "select top 1 platform_property_value +'\Rules Exchange\Business Rules Engine' from t_sys_platform_property
where platform_property_name = 'iis_remotepath'
order by platform_install_id desc"
$SqlCmd.Connection = $SqlConnection
$exe_loc = $SqlCmd.ExecuteScalar()
$SqlConnection.Close()

$ldm="[$RGDatabase]"

#write-host "ISS Remote is " $exe_loc
#$exe_loc="\\ddvosx01\c$\inetpub\wwwroot\BENONESUMXD_Portal\Rules Exchange\Business Rules Engine"


#cd to maestro source
cd $loc
cd ..\04_maestro
$xmlloc=Get-Location

#initiate import file
mkdir -path ${xmlloc}\command_file -ErrorAction silentlycontinue

Write-host "[MAESTRO] START OF PROCESS"

$files = Get-ChildItem -path $xmlloc -recurse | where {$_.extension -eq ".xml"}
If ($files -eq $null)
{
	write-host "NO Maestro files found"
    "***********              NO Maestro files found            *************" | out-file $logFile -Append
	 
}
else  
{


"----------------------------------------------------------------------------------" | out-file $logFile -Append

"***********              Deployment of Maestro Files Started             *************" | out-file $logFile -Append

write-host "----------------------------------------------------------------------------------"
write-host "***********              Deployment of Maestro Files Started             *************"


"***********              Listing Maestro files to be executed            *************" | out-file $logFile -Append
write-host "***********              Listing Maestro files to be executed            *************"

$fullname = get-childitem "${xmlloc}" | where {$_.extension -eq ".xml"} | % {$_.FullName}
$xmlfiles = get-childitem "${xmlloc}" | where {$_.extension -eq ".xml"} | % {$_.BaseName}


foreach($xmlfile in $xmlfiles)
{
    
    Copy-Item -Path .\${xmlfile}.xml -Destination .\${xmlfile}.bak

    "Executing Maestro ${xmlfile}.xml" | out-file $logFile -append
    #step 1 check compile catalog for each file
    $file="${xmlloc}\${xmlfile}.xml"

    write-host "XML File is:" $file

    #change compile catalog
    $compile_catalog = GC ${file} | Select-String -Pattern 'compile_catalog' | select-object -First 1

    if ($compile_catalog -ne $null) 
    { 
    (Get-Content $file) -replace $compile_catalog, "          <compile_catalog>${OSXDataBase}</compile_catalog>" | Set-Content $file

    # change file formatting back UTF8Encoding
    $MyFile = Get-Content $file
    $Utf8NoBomEncoding = New-Object System.Text.UTF8Encoding $False
    [System.IO.File]::WriteAllLines($file, $MyFile, $Utf8NoBomEncoding)
    }


    #change AUA reference    
    $AUA = GC ${file} | Select-String -Pattern '\[AUA_LDM\]' | select-object -First 1

    if ($AUA -ne $null)

    { 
     (Get-Content ${file}) -replace '\[AUA_LDM\]', ${ldm}| Set-Content ${file}
        # change file formatting back UTF8Encoding
    $MyFile = Get-Content $file
    $Utf8NoBomEncoding = New-Object System.Text.UTF8Encoding $False
    [System.IO.File]::WriteAllLines($file, $MyFile, $Utf8NoBomEncoding)
    }


    #Create the import xml file
    #Assign the CSV and XML Output File Paths
     $XML_Path ="${xmlloc}\command_file\import_${xmlfile}.xml"

     write-host ${XML_Path}

     # Delete if file already exists

      if (Test-Path $XML_Path) 
        {
        Remove-Item $XML_Path
        }
                  
     #Create the XML File Tags
     $xmlWriter = New-Object System.XMl.XmlTextWriter($XML_Path,$Null)
     $xmlWriter.Formatting = 'Indented'
     $xmlWriter.Indentation = 1
     $XmlWriter.IndentChar = "`t"
     $xmlWriter.WriteStartDocument()
     $xmlWriter.WriteComment('Get the Information about the maestro file')
     $xmlWriter.WriteStartElement('XmlExchangerCommandFile')
     $xmlWriter.WriteEndElement()
     $xmlWriter.WriteEndDocument()
     $xmlWriter.Flush()
     $xmlWriter.Close()
     
      
     # Create the Initial  Node
     $xmlDoc = [System.Xml.XmlDocument](Get-Content $XML_Path);
     $importCollectionNode = $xmlDoc.CreateElement("Imports") 
     $xmlDoc.SelectSingleNode("//XmlExchangerCommandFile").AppendChild($importCollectionNode) | Out-Null
     $xmlDoc.Save($XML_Path)
            
     $xmlDoc = [System.Xml.XmlDocument](Get-Content $XML_Path);
     $importCollectionNode = $xmlDoc.CreateElement("Import")
     $xmlDoc.SelectSingleNode("//XmlExchangerCommandFile/Imports").AppendChild($importCollectionNode) | Out-Null
    
     $enableNode = $importCollectionNode.AppendChild($xmlDoc.CreateElement("Enabled"));
     $enableTextNode = $enableNode.AppendChild($xmlDoc.CreateTextNode("true"));

     $serverNode = $importCollectionNode.AppendChild($xmlDoc.CreateElement("Server"));
     $serverTextNode = $serverNode.AppendChild($xmlDoc.CreateTextNode("${ServerName}"));


     $dbNode = $importCollectionNode.AppendChild($xmlDoc.CreateElement("Database"));
     $dbTextNode = $dbNode.AppendChild($xmlDoc.CreateTextNode("${OSXDataBase}"));


     $LogFileNode = $importCollectionNode.AppendChild($xmlDoc.CreateElement("LogFile"));
     $LogFileTextNode = $LogFileNode.AppendChild($xmlDoc.CreateTextNode("${loc}\logs\${xmlfile}_import_result.xml"));

     $ConsoleOnNode = $importCollectionNode.AppendChild($xmlDoc.CreateElement("ConsoleOn"));
     $ConsoleOnTextNode = $ConsoleOnNode.AppendChild($xmlDoc.CreateTextNode("true"));

     $ReplaceSetNode = $importCollectionNode.AppendChild($xmlDoc.CreateElement("ReplaceSet"));
     $ReplaceSetTextNode = $ReplaceSetNode.AppendChild($xmlDoc.CreateTextNode("true"));

     $ReplaceSolutionNode = $importCollectionNode.AppendChild($xmlDoc.CreateElement("ReplaceSolution"));
     $ReplaceSolutionTextNode = $ReplaceSolutionNode.AppendChild($xmlDoc.CreateTextNode("true"));

     $XmlFileNode = $importCollectionNode.AppendChild($xmlDoc.CreateElement("XmlFile"));
     $XmlFileTextNode = $XmlFileNode.AppendChild($xmlDoc.CreateTextNode("${xmlloc}\${xmlfile}.xml"));

        
     $xmlDoc.Save($XML_Path)

    # change file formatting to UTF8Encoding
    $xml = New-Object -TypeName XML
    $xml.Load($XML_Path)

    $utf8WithoutBom = New-Object System.Text.UTF8Encoding($false)
    $sw = New-Object System.IO.StreamWriter($XML_Path, $false, $utf8WithoutBom)

    $xml.Save( $sw )
    $sw.Close()


	#Step 3 Finally import new maestro

	#write-host .\Maestro.Main.Exchange.Command.exe -commandfile ${XML_Path}
	& ${exe_loc}\Maestro.Main.Exchange.Command.exe "-commandfile" "${XML_Path}"

    if($?)
        {
            write-host "Deployment successful for:" $file -ForegroundColor Green
            "Deployment successful for: ${file}" | out-file $logfile -append
            $sql="INSERT INTO $OSXDataBase.dbo.t_deployment_ps (object, type, status, execution_date, modified_by) VALUES (N'${file}', N'Maestro Business Rules', N'SUCCESS', CURRENT_TIMESTAMP, dbo.fn_user())"
            Invoke-Sqlcmd -Query $sql  -server "$ServerName" -Database "$OSXDataBase"
            $sql=''
    
        }
    else 
        {

            $Error[0].Exception | out-file $logFile -Append
            "Deployment FAILED for: ${file}" | out-file $logfile -append
            write-host "Deployment FAILED for:" $file -ForegroundColor RED
            $sql="INSERT INTO $OSXDataBase.dbo.t_deployment_ps (object, type, status, execution_date, modified_by) VALUES (N'${file}', N'Maestro Business Rules', N'FAILED', CURRENT_TIMESTAMP, dbo.fn_user())"
            Invoke-Sqlcmd -Query $sql  -server "$ServerName" -Database "$OSXDataBase"
            $sql=''          

         }
   Copy-Item -Path ${xmlloc}\${xmlfile}.bak -Destination ${xmlloc}\${xmlfile}.xml   
}


}


Write-host "END OF PROCESS"

#end of script

