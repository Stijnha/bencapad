﻿
#cls
#set local variables

$loc=split-path -parent $MyInvocation.MyCommand.Definition
$timestamp=get-date -f yyyyMMdd_hhmmss
$logFile="$loc\logs\05_ssis_deploy_$timestamp.log"


#get ini file contents
$ini_path = $loc + "\env.ini"

Get-Content $ini_path | foreach-object -begin {$h=@{}} -process { $k = [regex]::split($_,'='); if(($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) { $h.Add($k[0], $k[1]) } }

$content=get-content $ini_path
$ServerName=$content[0]
$OSXDataBase=$content[1]
$RGDatabase=$content[2]
$master=$content[3]
$SSISCatalog=$content[4]

write-host "server name is" $ServerName

#set SSIS variable
$SSISCatalog="SSISDB"
$CatalogPwd = "P@ssw0rd1"
$FolderName = "ETL"
$EnvironmentName = "ETL"
cd $loc
cd ..\05_ssis
$ssisloc=Get-Location
$ProjectFilePath = get-childitem "${ssisloc}" -Recurse | where {$_.extension -eq ".ispac"} | % {$_.FullName}
$ProjectName = get-childitem "${ssisloc}\" -Recurse | where {$_.extension -eq ".sln"} | % {$_.BaseName}
$filename=get-childitem "${ssisloc}" -Recurse | where {$_.extension -eq ".ispac"} | % {$_.FullName}

#write-host 'ISPAC IS:' $ProjectFilePath
#write-host 'Project name is' $ProjectName

Write-host "[SSIS] START OF PROCESS"

If ($ProjectFilePath -eq $null)
{
	write-host "NO SSIS ISPAC file found files found"
    "***********              NO SSIS ISPAC file found            *************" | out-file $logFile -Append

	 
}
else

{



#initiate log file
mkdir -path $loc\logs -ErrorAction silentlycontinue
get-date | out-file $logFile

"***********              SSIS Deployment Started             *************" | out-file $logFile -Append

# Load the IntegrationServices Assembly
[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Management.IntegrationServices")

# Store the IntegrationServices Assembly namespace to avoid typing it every time
$ISNamespace = "Microsoft.SqlServer.Management.IntegrationServices"

Write-Host "Connecting to server ..."

# Create a connection to the server
$sqlConnectionString = "Data Source=$ServerName;Initial Catalog=master;Integrated Security=SSPI;"
$sqlConnection = New-Object System.Data.SqlClient.SqlConnection $sqlConnectionString
$connection = "Data Source=$ServerName;Initial Catalog=master;Integrated Security=True;"


Write-Host $connection
$integrationServices = New-Object "$ISNamespace.IntegrationServices" $sqlConnection

$catalog = $integrationServices.Catalogs[$SSISCatalog]


# Create the Integration Services object if it does not exist
if (!$catalog) {
    # Provision a new SSIS Catalog
    Write-Host "Creating SSIS Catalog ..."

    $catalog = New-Object 'Microsoft.SqlServer.Management.IntegrationServices.Catalog' ($integrationServices, $SSISCatalog, $CatalogPwd)    
    $catalog.Create()
}

$folder = $catalog.Folders[$FolderName]

if (!$folder) 
{
    #Create a folder in SSISDB
    Write-Host "Creating Folder ..."
    $folder = New-Object "$ISNamespace.CatalogFolder" ($catalog, $FolderName, $FolderName)        
    $folder.Create()  
}

# Read the project file, and deploy it to the folder
Write-Host "Deploying Project ..."
[byte[]] $projectFile = [System.IO.File]::ReadAllBytes($ProjectFilePath)
$folder.DeployProject($ProjectName, $projectFile)

$environment = $folder.Environments[$EnvironmentName]

if (!$environment)
{
    Write-Host "Creating environment ..." 
    $environment = New-Object "$ISNamespace.EnvironmentInfo" ($folder, $EnvironmentName, $EnvironmentName)
    $environment.Create()     
}

$project = $folder.Projects[$ProjectName]
$ref = $project.References[$EnvironmentName, $folder.Name]

if (!$ref)
{
    # making project refer to this environment
    Write-Host "Adding environment reference to project ..."
    $project.References.Add($EnvironmentName, $folder.Name)
    $project.Alter() 
}

# Adding variable to our environment
# Constructor args: variable name, type, default value, sensitivity, description
$connection_string = $environment.Variables["connection_string"];

if (!$connection_string)
{
    Write-Host "Adding environment variables ..."
    $environment.Variables.Add(
        “connection_string”, 
        [System.TypeCode]::String, $connection, $false, “connection_string”)
    $environment.Alter()
    $connection_string = $environment.Variables["connection_string"];
}

$project.Parameters["connection_string"].Set(
    [Microsoft.SqlServer.Management.IntegrationServices.ParameterInfo+ParameterValueType]::Referenced, 
    $connection_string.Name)
$project.Alter()  



 if($?)
        {
            write-host "SSIS Deployment successful for: ${ProjectFilePath}" -ForegroundColor Green
            "SSIS Deployment successful for: ${ProjectFilePath}" | out-file $logfile -append
            $sql="INSERT INTO $OSXDataBase.dbo.t_deployment_ps (object, type, status, execution_date, modified_by) VALUES (N'${filename}', N'SSIS Catalog', N'SUCCESS', CURRENT_TIMESTAMP, dbo.fn_user())"
            Invoke-Sqlcmd -Query $sql  -server "$ServerName" -Database "$OSXDataBase"
            #write-host $sql
            $sql=''
           
    
        }
    else 
        {
                
            $Error[0].Exception | out-file $log -Append
            write-host "SSIS Deployment FAILED for: ${ProjectFilePath}" -ForegroundColor RED
            "SSIS Deployment FAILED for: ${ProjectFilePath}" | out-file $logfile -append
            $sql="INSERT INTO $OSXDataBase.dbo.t_deployment_ps (object, type, status, execution_date, modified_by) VALUES (N'${filename}', N'SSIS', N'FAILED', CURRENT_TIMESTAMP, dbo.fn_user())"
            Invoke-Sqlcmd -Query $sql  -server "$ServerName" -Database "$OSXDataBase"
            $sql=''

         }
}

Write-host "END OF PROCESS"

#end of script

#end of script
