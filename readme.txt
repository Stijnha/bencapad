This is the main repository folder for the osx development
The sub-folder structure will be as follows

source_code
|->01_database /* this will include sql that are to be run on the master database */
|->02_onesumxdb /* changes done on osx db objects */
    |->01_config_scripts_before /*sql that needs to be executed before deployment */
    |->02_core_objects /* changes to osx out of the box objects */
    |->03_data_type /* data type changes */
    
