USE BENONESUMX;
GO

IF OBJECT_ID (N'p_gen_staging_ps',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_gen_staging_ps...';
  DROP PROCEDURE p_gen_staging_ps;
END
GO

PRINT N'Creating procedure p_gen_staging_ps...';
GO
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************

CREATE PROCEDURE dbo.p_gen_staging_ps
(
        @table_name				NVARCHAR(2000) = NULL,
		@debug                  BIT = 0,
		@execute_immediately    BIT = 1
)
AS

SET NOCOUNT ON;

DECLARE @code_block            NVARCHAR(4000) = N'',
        @ErrorSeverity         INT,
		@ErrorState            INT,
		@Msg                   NVARCHAR(4000),
		@ProcName              SYSNAME = OBJECT_NAME(@@PROCID),
        @Raiserror             INT,
		@Rowcount              INT,
		@db					   NVARCHAR(100),
        @tab				   NVARCHAR(2000),
		@cols			       NVARCHAR(MAX);

CREATE TABLE #code ([id] INT            IDENTITY(1,1)  NOT NULL,  
                    txt  NVARCHAR(4000)                NOT NULL);

BEGIN TRY
	DECLARE cur CURSOR FOR
	SELECT staging_database,staging_table,staging_column_list
	  FROM t_extract_table_ps
	 WHERE ISNULL(@table_name,staging_table) = staging_table
	   
	OPEN cur;
	FETCH NEXT FROM cur INTO @db,@tab,@cols;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #code(txt) SELECT N'IF OBJECT_ID(''' + @db + N'..' + @tab + N''',''U'') IS NOT NULL'
		INSERT INTO #code(txt) SELECT N'BEGIN';
		INSERT INTO #code(txt) SELECT N'	DROP TABLE [' + @db + N'].[dbo].[' + @tab + N'];';
		INSERT INTO #code(txt) SELECT N'END;';
		INSERT INTO #code(txt) SELECT N'GO';
		INSERT INTO #code(txt) SELECT N'';
		INSERT INTO #code(txt) SELECT N'CREATE TABLE [' + @db + N'].[dbo].[' + @tab + N']';
		INSERT INTO #code(txt) SELECT N'(';
		INSERT INTO #code ( txt ) SELECT REPLICATE(N' ',4) + 
										 section + 
										 REPLICATE(N' ',50-LEN(section)) + 
										 N'NVARCHAR(MAX)' + 
										 REPLICATE(N' ',10) + 
										 N' NULL' +
										 IIF(row_id = MAX(row_id) OVER (),N'',N',')
		  FROM dbo.fn_split_list(@cols,N',',N'')
		INSERT INTO #code(txt) SELECT N')';
		INSERT INTO #code(txt) SELECT N'';
		INSERT INTO #code(txt) SELECT N'GO';
		INSERT INTO #code(txt) SELECT N'';

		FETCH NEXT FROM cur INTO @db,@tab,@cols;
	END;

	CLOSE cur;
	DEALLOCATE cur;
	IF @debug = 1  
	BEGIN
	  DECLARE c_code CURSOR LOCAL FAST_FORWARD   
	  FOR SELECT txt FROM #code ORDER BY [id];
  
	  OPEN c_code;
	  FETCH NEXT FROM c_code INTO @code_block;
	  WHILE @@FETCH_STATUS = 0  
	  BEGIN  
		PRINT @code_block;
		FETCH NEXT FROM c_code INTO @code_block;
	  END;
	  CLOSE c_code;
	  DEALLOCATE c_code;
	END  

	IF @execute_immediately = 1  
	BEGIN  
	  EXEC p_exec_resultset N'SELECT txt FROM #code ORDER BY [id]'; 
	END  
	DROP TABLE #code;  
END TRY

BEGIN CATCH
  SELECT @Raiserror = 300000 + ERROR_NUMBER(),
         @ErrorSeverity = ERROR_SEVERITY(),
         @ErrorState = ERROR_STATE(),
         @Msg = ERROR_MESSAGE();
  DROP TABLE #code; 
  CLOSE cur;
  DEALLOCATE cur;

  RAISERROR (@Msg, @ErrorSeverity, @ErrorState);
  RETURN @Raiserror;
END CATCH


RETURN(0); 
GO


IF OBJECT_ID (N'p_gen_staging_ps',N'P') IS NOT NULL
  PRINT N'Procedure p_gen_staging_ps has been created';
ELSE
  PRINT N'Procedure p_gen_staging_ps has not been created due to errors';

GO