USE BENONESUMX;
GO

IF OBJECT_ID (N'p_dec_gen_val_ps',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_dec_gen_val_ps...';
  DROP PROCEDURE p_dec_gen_val_ps;
END
GO
PRINT N'Creating procedure p_dec_gen_val_ps...';
GO


/****************************************************************************************  
-- * Purpose: Generates data editor validation procedure  
-- *          
-- *   
-- *  Author: Nils Bertels
-- *    Date: 05 Nov 2015
-- *   
This procedure generates a stored procedure that is used to validate the data in the adjustment table.
****************************************************************************************/  

CREATE PROCEDURE dbo.p_dec_gen_val_ps
         (  
           @val_table             NVARCHAR(200),  
           @adj_table             NVARCHAR(200),  
           @val_proc              NVARCHAR(200),  
           @action                NVARCHAR(1) ,-- V(alidate) or E(rror)
           @execute_immediately   BIT          = 1,  
           @debug                 BIT          = 0,  
           @database_name         SYSNAME      = NULL
         )  
AS  
  
SET NOCOUNT ON;

DECLARE @code_block     NVARCHAR(4000),  
        @proc_name      NVARCHAR(200) = OBJECT_NAME(@@PROCID);

DECLARE @sysobjects TABLE (name                SYSNAME      NOT NULL,
                           [object_id]         INT          NOT NULL,
                           principal_id	       INT          NULL,
                           [schema_id]         INT          NOT NULL,
                           parent_object_id	   INT          NOT NULL,
                           [type]              CHAR(2)      NOT NULL,
                           type_desc           NVARCHAR(60) NULL,
                           create_date         DATETIME     NOT NULL,
                           modify_date         DATETIME     NOT NULL,
                           is_ms_shipped       BIT          NOT NULL,
                           is_published        BIT          NOT NULL,
                           is_schema_published BIT          NOT NULL);

CREATE TABLE #sql  (seq_nr    INT           NOT NULL IDENTITY(1, 1),  
                    txt       NVARCHAR(MAX) NOT NULL); 
  
CREATE TABLE #sqlw (seq_nr    INT           NOT NULL IDENTITY(1, 1),  
                    txt       NVARCHAR(MAX) NOT NULL); 

INSERT #sql (txt) SELECT N'SELECT name,';
INSERT #sql (txt) SELECT N'       [object_id],';
INSERT #sql (txt) SELECT N'       principal_id,';
INSERT #sql (txt) SELECT N'       [schema_id],';
INSERT #sql (txt) SELECT N'       parent_object_id,';
INSERT #sql (txt) SELECT N'       type,';
INSERT #sql (txt) SELECT N'       type_desc,';
INSERT #sql (txt) SELECT N'       create_date,';
INSERT #sql (txt) SELECT N'       modify_date,';
INSERT #sql (txt) SELECT N'       is_ms_shipped,';
INSERT #sql (txt) SELECT N'       is_published,';
INSERT #sql (txt) SELECT N'       is_schema_published';
INSERT #sql (txt) SELECT N'  FROM ' + @database_name + '.sys.objects';
INSERT #sql (txt) SELECT N' WHERE name = ''' + @val_table + ''';';
INSERT INTO @sysobjects
EXEC p_exec_resultset @stmt = N'SELECT txt FROM #sql ORDER BY seq_nr';

DELETE FROM #sql;

---- check source object exists.  
--IF NOT EXISTS (SELECT * FROM sys.objects WHERE name = @adj_table AND type IN ('U', 'V'))  
--BEGIN  
--  RAISERROR (N'%s: load type source_object configuration invalid. Object ''%s'' does not exist or is not a table or view.', 16, -1, @proc_name, @adj_table); 
--  RETURN (-1);
--END  
  
---- check target table exists.  
--IF NOT EXISTS (SELECT * FROM @sysobjects WHERE name = @val_table AND type IN ('U'))  
--BEGIN  
--  RAISERROR (N'%s: load type target_table configuration invalid. Object ''%s'' does not exist or is not a table.', 16, -1, @proc_name, @val_table);
--  RETURN (-1);
--END  
  
-- ****************************************************************************************  
-- * Build Load Procedure DROP Section  
-- ****************************************************************************************  
  
INSERT #sql (txt) SELECT N'IF OBJECT_ID(N<q><val_proc><q>,N<q>P<q>) IS NOT NULL';
INSERT #sql (txt) SELECT N'BEGIN'  
INSERT #sql (txt) SELECT N'  PRINT N<q>Dropping procedure <val_proc>...<q>;';
INSERT #sql (txt) SELECT N'  DROP PROCEDURE dbo.<val_proc>;';
INSERT #sql (txt) SELECT N'END';
INSERT #sql (txt) SELECT N'GO';
INSERT #sql (txt) SELECT N'';
INSERT #sql (txt) SELECT N'PRINT N<q>Creating procedure <val_proc>...<q>;';  
INSERT #sql (txt) SELECT N'GO';    
INSERT #sql (txt) SELECT N'';
INSERT #sql (txt) SELECT N'CREATE PROCEDURE dbo.<val_proc>';
INSERT #sql (txt) SELECT N'(';
INSERT #sql (txt) SELECT N'  @statement           d_statement   = NULL,';
INSERT #sql (txt) SELECT N'  @dec_table_code      d_name        = NULL,';
INSERT #sql (txt) SELECT N'  @debug               BIT           = 0';
INSERT #sql (txt) SELECT N')';
INSERT #sql (txt) SELECT N'AS';
INSERT #sql (txt) SELECT N'-- Message types:';
INSERT #sql (txt) SELECT N'-- -1 : warning';
INSERT #sql (txt) SELECT N'--  0 : information';
INSERT #sql (txt) SELECT N'--  1 : error';
INSERT #sql (txt) SELECT N'';
INSERT #sql (txt) SELECT N'SET NOCOUNT ON;'
INSERT #sql (txt) SELECT N'SET DATEFORMAT YMD;';  
  
-- ****************************************************************************************  
-- * Build Load Procedure HEADER Section  
-- ****************************************************************************************  
  
INSERT #sql (txt) SELECT N'';
INSERT #sql (txt) SELECT N'-- ******************************************************************************';
INSERT #sql (txt) SELECT N'-- * THIS PROCEDURE HAS BEEN AUTO-GENERATED                                     *';  
INSERT #sql (txt) SELECT N'-- ******************************************************************************';  
INSERT #sql (txt) SELECT N''; 
INSERT #sql (txt) SELECT N'-- ******************************************************************************';
INSERT #sql (txt) SELECT N'-- * WARNING: DO NOT UPDATE THIS PROCEDURE DIRECTLY. ALL CHANGES WILL BE LOST   *';  
INSERT #sql (txt) SELECT N'-- * IN FUTURE RE-GENERATION.                                                   *';  
INSERT #sql (txt) SELECT N'-- ******************************************************************************';  
INSERT #sql (txt) SELECT N'';  
INSERT #sql (txt) SELECT N'-- ******************************************************************************';
INSERT #sql (txt) SELECT N'-- * Generated On: ' + CONVERT(VARCHAR,CURRENT_TIMESTAMP,113); 
INSERT #sql (txt) SELECT N'-- * Generated By: ' + @proc_name; 
INSERT #sql (txt) SELECT N'-- ******************************************************************************';
INSERT #sql (txt) SELECT N'';
  
-- ****************************************************************************************  
-- * Build Load Procedure DECLARATIONS Section  
-- ****************************************************************************************  
  
INSERT #sql (txt) SELECT N''; 
INSERT #sql (txt) SELECT N'-- Declare error/debug variables'; 
INSERT #sql (txt) SELECT N'DECLARE @proc_name       sysname, -- procedure name';
INSERT #sql (txt) SELECT N'        @status          INT, -- return status';
INSERT #sql (txt) SELECT N'        @error           INT, -- saved error context';
INSERT #sql (txt) SELECT N'        @rowcount        INT  -- saved rowcount context';

INSERT #sql (txt) SELECT N'';
INSERT #sql (txt) SELECT N'-- Initialise error/debug variables'; 
INSERT #sql (txt) SELECT N'SELECT @proc_name = OBJECT_NAME( @@PROCID ),'; 
INSERT #sql (txt) SELECT N'       @status     = 0,'; 
INSERT #sql (txt) SELECT N'       @error      = 0,'; 
INSERT #sql (txt) SELECT N'       @rowcount   = 0'; 

INSERT #sql (txt) SELECT N'';
INSERT #sql (txt) SELECT N'-- Declare local variables';
INSERT #sql (txt) SELECT N'IF (@statement = <q><q> OR @statement IS NULL  OR @statement = N<q>1 = 1 AND (("line_nr" = null))<q>)';
INSERT #sql (txt) SELECT N'  SELECT @statement = <q>1=1<q>;';

IF @action = N'V'
BEGIN
    INSERT #sql (txt) SELECT N'';
    INSERT #sql (txt) SELECT N'-- Clean up existing validation messages.'; 
    INSERT #sql (txt) SELECT N'EXEC (<q>'; 
    INSERT #sql (txt) SELECT N'  DELETE FROM <val_table>'; 
    INSERT #sql (txt) SELECT N'  WHERE line_nr IN ('; 
    INSERT #sql (txt) SELECT N'    SELECT line_nr'; 
    INSERT #sql (txt) SELECT N'      FROM <adj_table>'; 
    INSERT #sql (txt) SELECT N'     WHERE <q> + @statement + <q>'; 
    INSERT #sql (txt) SELECT N'  )<q>);'; 
END

INSERT #sql (txt) SELECT N'';
INSERT #sql (txt) SELECT N'-- ****************************************************************************************';
INSERT #sql (txt) SELECT N'-- * Start';
INSERT #sql (txt) SELECT N'-- ****************************************************************************************';

IF @action = N'V'
BEGIN
    INSERT #sql (txt) SELECT N'';
    INSERT #sql (txt) SELECT N'-- Place validation checks here';

    INSERT #sql (txt) SELECT N'';
    INSERT #sql (txt) SELECT N'--IF (@debug = 1) PRINT <q>1. 1 cannot be equal to 2.<q>;';
    INSERT #sql (txt) SELECT N'--EXEC (<q>';
    INSERT #sql (txt) SELECT N'--  INSERT INTO <val_table> (line_nr, column_name, message_type, message)';
    INSERT #sql (txt) SELECT N'--  SELECT  line_nr,';
    INSERT #sql (txt) SELECT N'--         <q><q>line_nr<q><q>,';
    INSERT #sql (txt) SELECT N'--         1,';
    INSERT #sql (txt) SELECT N'--         <q><q>1. 1 cannot be equal to 2.<q><q>';
    INSERT #sql (txt) SELECT N'--    FROM <adj_table>';
    INSERT #sql (txt) SELECT N'--   WHERE <q> + @statement + <q>';
    INSERT #sql (txt) SELECT N'--     AND (1=2)<q>);';
END

IF @action = N'E'
BEGIN
    INSERT #sql (txt) SELECT N'';
    INSERT #sql (txt) SELECT N'-- Load messages from the validation errors table';
    INSERT #sql (txt) SELECT N'EXEC (<q>';
    INSERT #sql (txt) SELECT N'  SELECT line_nr, column_name, message_type, message';
    INSERT #sql (txt) SELECT N'    FROM <val_table>';
    INSERT #sql (txt) SELECT N'   WHERE <q> + @statement);';
END

INSERT #sql (txt) SELECT N'';
INSERT #sql (txt) SELECT N'-- ****************************************************************************************';
INSERT #sql (txt) SELECT N'-- * End';
INSERT #sql (txt) SELECT N'-- ****************************************************************************************';
INSERT #sql (txt) SELECT N'';
INSERT #sql (txt) SELECT N'RETURN (0);';
INSERT #sql (txt) SELECT N'GO';
INSERT #sql (txt) SELECT N'';

INSERT #sql (txt) SELECT N'IF OBJECT_ID (N<q><val_proc><q>,N<q>P<q>) IS NOT NULL';
INSERT #sql (txt) SELECT N'  PRINT N<q>Procedure <val_proc> has been created...<q>;';
INSERT #sql (txt) SELECT N'ELSE';  
INSERT #sql (txt) SELECT N'  PRINT N<q>Error: procedure <val_proc> has NOT been created due to errors...<q>;';
INSERT #sql (txt) SELECT N'GO';
  
-- perform replacement of all remaining tokens  
UPDATE #sql SET txt = REPLACE(txt, N'<q>', '''');    
UPDATE #sql SET txt = REPLACE(txt, N'<adj_table>', ISNULL(@database_name + N'.dbo.' + @adj_table,N'@adj_table'));
UPDATE #sql SET txt = REPLACE(txt, N'<val_table>', ISNULL(@database_name + N'.dbo.' + @val_table,N'@val_table'));
UPDATE #sql SET txt = REPLACE(txt, N'<val_proc>', ISNULL(@val_proc,N'@val_proc'));
  
-- ****************************************************************************************  
-- * Generate Procedure  
-- ****************************************************************************************  
  
IF @debug = 1  
BEGIN
  DECLARE c_code CURSOR LOCAL FAST_FORWARD   
  FOR SELECT txt FROM #sql ORDER BY seq_nr;    
  
  OPEN c_code;
  FETCH NEXT FROM c_code INTO @code_block;
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    PRINT @code_block;
    FETCH NEXT FROM c_code INTO @code_block;
  END  
  CLOSE c_code;
  DEALLOCATE c_code;
END  
  
IF @execute_immediately = 1
BEGIN  
  EXEC p_exec_resultset @stmt = 'SELECT txt FROM #sql ORDER BY seq_nr;';
END  
  
  
--IF NOT EXISTS (SELECT * FROM sys.objects WHERE name = @val_proc AND type = 'P')  
--BEGIN  
--  RAISERROR (N'%s: GENERATION OF VALIDATION TYPE PROCEDURE ''%s'' FAILED. REVIEW LOG FOR DETAILS.', 16, -1, @proc_name, @val_proc);
--  RETURN (-1);
--END  
  
RETURN(0);

GO

IF OBJECT_ID (N'p_dec_gen_val_ps',N'P') IS NOT NULL
  PRINT N'Procedure p_dec_gen_val_ps has been created';
ELSE
  PRINT N'Procedure p_dec_gen_val_ps has not been created due to errors';


GO