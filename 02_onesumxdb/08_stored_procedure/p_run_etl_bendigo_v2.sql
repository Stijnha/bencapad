
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_run_etl_bendigo_v2' AND schema_id = SCHEMA_ID('dbo'))


  BEGIN


    PRINT N'Creating procedure [dbo].[p_run_etl_bendigo_v2] ...'


    EXEC (N'CREATE PROCEDURE [dbo].[p_run_etl_bendigo_v2] AS RETURN(0)')


    WAITFOR DELAY N'00:00:00.003'


  END


PRINT N'Altering procedure [dbo].[p_run_etl_bendigo_v2]...'


GO


ALTER PROCEDURE [dbo].[p_run_etl_bendigo_v2]


(


	   @entity						nvarchar(10),       


       @status						int       = NULL OUTPUT


)


AS



SET NOCOUNT ON



DECLARE @sql nvarchar(max)


DECLARE @con_str nvarchar(200) = N'Data Source=BENONESUMXU1-DB,4101;Initial Catalog=BENONESUMXU1;Integrated Security=True;'


DECLARE @etl_table_name d_name


DECLARE @fda_table_name d_name


DECLARE @message nvarchar(max)


DECLARE @start_time nvarchar(30)


DECLARE @end_time nvarchar(30)



SET @start_time=(select FORMAT(GETDATE(),'yyyy MMM dd hh:MM:ss'))



Print 'Executing ETL for Entity: ' + @entity


Print 'Start time: ' + @start_time



DECLARE table_cur CURSOR  


FOR 


SELECT at.type_name AS etl_table_name,


       REPLACE(at.type_name,N't_etl_adj_data_',N't_') AS fda_table_name    


  FROM t_extract_etl_ps ee


  JOIN t_adj_type at


    ON ee.type_id = at.type_id


 WHERE 1=1


   AND is_enabled = 1


ORDER BY execution_order;



OPEN table_cur  



FETCH NEXT FROM table_cur


INTO @etl_table_name, @fda_table_name  



WHILE @@FETCH_STATUS = 0  


BEGIN  


    PRINT ' '  


    SELECT @message = 'ETL Processing for table: ' + @fda_table_name + ' adj table name: ' + @etl_table_name + CHAR(13) 


	PRINT  @message  



	FETCH NEXT FROM table_cur


	INTO @etl_table_name, @fda_table_name


END   


CLOSE table_cur


DEALLOCATE table_cur



PRINT 'SSIS ETL Package Running....' + CHAR(13) + CHAR(13) + CHAR(13)



SET @sql = N'DECLARE @execution_id bigint


DECLARE @entity nvarchar(24)=N'''+ @entity +'''' +


+ CHAR(13) +


'DECLARE @con_str nvarchar(200) =N''' +@con_str + ''''


+ CHAR(13) +



'EXEC [SSISDB].[catalog].[create_execution]


	  @package_name		= N''Main.dtsx''


	 ,@execution_id		= @execution_id OUTPUT


	 ,@folder_name		= N''ETL''


	 ,@project_name     = N''AUA_SSIS''


	 ,@use32bitruntime	= False


	 ,@reference_id		= NULL



/* Set entity */



EXEC [SSISDB].[catalog].[set_execution_parameter_value]  


		 @execution_id 


		,@object_type=20


		,@parameter_name=''entity''


		,@parameter_value=@entity



/* Set Connection String*/



EXEC [SSISDB].[catalog].[set_execution_parameter_value]  


		 @execution_id 


		,@object_type=20


		,@parameter_name=N''connection_string''


		,@parameter_value=@con_str



/* Set table names */



--EXEC [SSISDB].[catalog].[set_execution_parameter_value]  


--		 @execution_id 


--		,@object_type=20


--		,@parameter_name=N''table_name''


--		,@parameter_value=@table_name



/* run Synchronous */



EXEC [SSISDB].[catalog].[set_execution_parameter_value]  


		 @execution_id 


		,@object_type=50


		,@parameter_name=N''SYNCHRONIZED''


		,@parameter_value=1



EXEC [SSISDB].[catalog].[start_execution]


	@execution_id



　


IF 7 <> (SELECT [status] FROM [SSISDB].[catalog].[executions] WHERE execution_id = @execution_id)



RAISERROR(''The package failed. Check the SSIS catalog logs for more information'', 16, 1)



ELSE PRINT ''ETL PROCESS SUCCESSFULL''


'



--PRINT @sql


EXEC (@sql)



SET @end_time=(select FORMAT(GETDATE(),'yyyy MMM dd hh:MM:ss'))



PRINT CHAR(13) + 'ETL  Process completed at: ' + @end_time



　


　


　


GO


IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_run_etl_bendigo_v2' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))


BEGIN


    PRINT N'Procedure [dbo].[p_run_etl_bendigo_v2] has been altered...'


END ELSE BEGIN


    PRINT N'Procedure [dbo].[p_run_etl_bendigo_v2] has NOT been altered due to errors!'


END


GO
