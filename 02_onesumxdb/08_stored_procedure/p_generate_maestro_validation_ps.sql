USE BENONESUMX;

-- Checking if there's a rule on the table t_mex_runtime_parameter
-- Rules shouldn't be used any more since SQL 2008...

SET NOCOUNT ON;

DECLARE @datatype SYSNAME = N'd_bit',
        @fda_table SYSNAME,
		@column_name SYSNAME,
		@SQL NVARCHAR(MAX) = N'',
		@debug BIT = 1;

IF EXISTS (SELECT *
             FROM sys.types
            WHERE name = @datatype
              AND rule_object_id <> 0)
BEGIN       
  EXEC dbo.sp_unbindrule @objname=N'[dbo].[d_bit]';
END;

DECLARE cur CURSOR FOR
SELECT OBJECT_NAME(c.object_id) AS fda_table,
       c.name AS column_name
  FROM sys.columns c
  JOIN sys.tables ta
    ON c.object_id = ta.object_id
  JOIN sys.types t
    ON c.user_type_id = t.user_type_id
  LEFT JOIN sys.check_constraints cc
    ON c.object_id = cc.parent_object_id
   AND c.column_id = cc.parent_column_id
 WHERE t.name = @datatype
   AND cc.object_id IS NULL;

OPEN cur;

FETCH NEXT FROM cur INTO @fda_table, @column_name;

WHILE @@FETCH_STATUS = 0
BEGIN
  IF @debug = 1
    PRINT N'@fda_table: ' + @fda_table + '; @column_name: ' + @column_name;

  SELECT @SQL = N'ALTER TABLE ' + @fda_table + N' WITH CHECK
  ADD CONSTRAINT ck_' + RIGHT(@fda_table,LEN(@fda_table)-2) + N'_' + @column_name + N'_ps
  CHECK (' + @column_name + N' IN (0,1));';
  
  IF @debug = 1
    PRINT @SQL;

  EXEC sp_executesql @statement = @SQL;
  FETCH NEXT FROM cur INTO @fda_table, @column_name;
END;

CLOSE cur;
DEALLOCATE cur;

GO

IF OBJECT_ID (N'p_generate_maestro_validation_ps',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_generate_maestro_validation_ps...';
  DROP PROCEDURE p_generate_maestro_validation_ps;
END
GO

PRINT N'Creating procedure p_generate_maestro_validation_ps...';
GO
-- ***************************************************************************************************************************************
-- * Purpose: This procedure generates a Maestro validation rule that can be used as template in the ETL validation process              *
-- *          It will                                                                                                                    *
-- *            1. generate a new application set if needed by copying the previous version or copy the template ETL validation rule     *
-- *               This application set is linked to the solution "ETL Validation Rules"                                                 *
-- *            2. Create a cluster named ETL Validation Rules if needed                                                                 *
-- *            3. Generate the runtime parameters if needed                                                                             *
-- *            4. Create a job named ETL Validation if needed containing                                                                *
-- *               a. The ETL validation table as output location                                                                        *
-- *               b. A validation job using the output location with the following possible steps                                       *
-- *                    - Duplicates check                                                                                               *
-- *                    - Null checks                                                                                                    *
-- *                    - Foreign key checks (1 step per foreign key constraint)                                                         *
-- *                    - unique constraint checks (1 step per unique constraint)                                                        *
-- *                    - Check constraints (1 step per check constraint)                                                                *
-- *                                                                                                                                     *
-- * Parameters: @fda_table: Name of the FDA table for which you want to generate the template                                           *
-- *             @cob_date: Date indicator from when the rule should be valid                                                            *
-- *             @build: Automatically build the application set? Default No (0)                                                         *
-- *                                                                                                                                     *
-- * Dependencies: - fn_cvt_proper_ps: Function to trasnform strings to Proper format                                                   *
-- *               - ETL Validation Template in Maestro (may contain no objects)                                                         *
-- *                                                                                                                                     *
-- * Notes: This should only be used by WKFS professional services                                                                       *
-- *        This script is not intended for production                                                                                   *
-- *        The script will not delete any objects. It might duplicate tasks.                                                            *
-- ***************************************************************************************************************************************

CREATE PROCEDURE dbo.p_generate_maestro_validation_ps
(
        @fda_table              SYSNAME,
		@cob_date               DATETIME,
		@build                  BIT = 0
)
AS

SET NOCOUNT ON;


--DECLARE @fda_table SYSNAME = N't_mex_step',
--        @cob_date DATETIME = N'1 Aug 2015';

DECLARE 
        @aggregation_id d_id,
		@cluster_id d_id,
		@cur_version_id d_id,
		@default_source_filter_id d_id,
		@dup_formula_id d_id,
		@dup_location_id d_id,
		@dup_object_id d_id,
		@etl_location_id d_id,
		@etl_object_id d_id,
		@etl_table SYSNAME,
		@etl_validation_table SYSNAME,
		@is_nullable NVARCHAR(10),
		@fk_cnt SMALLINT = 1,
		@fk_column NVARCHAR(1000),
		@fk_constraint_id INT, 
		@fk_location_id d_id,
		@fk_object_id d_id,
		@fk_reference_column NVARCHAR(1000),  
		@fk_reference_table NVARCHAR(1000),
		@fk_table SYSNAME,
		@job_id d_id,
		@mex_id d_id,
		@mex_type d_id,
		@mex_type_desc d_description,
		@mex_type_name d_name,
		@msg NVARCHAR(1000),
		@out_location_id d_id,
		@out_object_id d_id,
		@parameter_id d_id,
		@source_field_id d_id,
		@step_id d_id,
		@tpl_mex_type d_id,
        @tpl_version_id d_id,
		@version_id d_id,
		@uq INT;

SELECT @mex_type_name = dbo.fn_cvt_proper_ps (REPLACE (RIGHT(@fda_table,LEN(@fda_table)-2),'_',' '),1),
       @mex_type_desc = N'Validation Rules for ' + @mex_type_name,
	   @etl_table = N't_etl_adj_data_' + RIGHT(@fda_table,LEN(@fda_table)-2),
	   @etl_validation_table = @etl_table + N'_validation';

DECLARE @syscolumns TABLE (
        column_id INT NOT NULL,
		name NVARCHAR(1000) NOT NULL,
		is_nullable BIT NOT NULL,
		pk_column_id INT NULL,
		uq_column_id INT NULL,
		fk_reference_column NVARCHAR(1000) NULL,
		fk_reference_table NVARCHAR(1000) NULL,
		fk_constraint_id INT NULL,
		cc_condition NVARCHAR(MAX) NULL);

PRINT N'FDA Table:			' + @fda_table;

SELECT @msg = N'The %s table %s does not exist in the database %s ';

IF OBJECT_ID(@fda_table) IS NULL
BEGIN
  SELECT @msg = FORMATMESSAGE (@msg, N'FDA', @fda_table , N'OneSumX');
  THROW 50000, @msg, 0;
END;

IF OBJECT_ID(@etl_table) IS NULL
BEGIN
  SELECT @msg = FORMATMESSAGE (@msg, N'ETL', @etl_table , N'OneSumX');
  THROW 50000, @msg, 0;
END;

IF OBJECT_ID(@etl_validation_table) IS NULL
BEGIN
  SELECT @msg = FORMATMESSAGE (@msg, N'ETL Validation', @etl_validation_table , N'OneSumX');
  THROW 50000, @msg, 0;
END;

INSERT INTO @syscolumns (
       column_id,
       name,
       is_nullable,
       pk_column_id,
       uq_column_id,
       fk_reference_column,
       fk_reference_table,
       fk_constraint_id,
       cc_condition)
SELECT 6 + c.column_id AS column_id,
       c.name,
       c.is_nullable,
	   icpk.index_column_id AS pk_column_id,
	   uq.uq_constraint_id,
	   fk.fk_reference_column,
	   fk.fk_reference_table,
	   fk.fk_constraint_id,
	   cc.definition AS cc_condition
  FROM sys.columns c 
  LEFT JOIN sys.key_constraints kcpk -- PK
    ON c.object_id = kcpk.parent_object_id
   AND kcpk.type = N'PK'
  LEFT JOIN sys.indexes ipk
    ON c.object_id = ipk.object_id
   AND kcpk.name = ipk.name
  LEFT JOIN sys.index_columns icpk
    ON c.object_id = icpk.object_id
   AND ipk.index_id = icpk.index_id
   AND c.column_id = icpk.column_id
  LEFT JOIN (SELECT cuq.name AS uq_column,
                    kcuq.object_id AS uq_constraint_id
			   FROM sys.key_constraints kcuq
			   JOIN sys.indexes iuq
			     ON kcuq.parent_object_id = iuq.object_id
			    AND kcuq.name = iuq.name
			   JOIN sys.index_columns icuq
			     ON kcuq.parent_object_id = icuq.object_id
			    AND iuq.index_id = icuq.index_id
			   JOIN sys.columns cuq
			     ON kcuq.parent_object_id = cuq.object_id
			    AND icuq.column_id = cuq.column_id
			  WHERE kcuq.parent_object_id = OBJECT_ID(@fda_table)
			    AND kcuq.type = N'UQ') uq
    ON c.name = uq.uq_column
  LEFT JOIN (SELECT cfkp.name AS parent_column,
	                cfkr.name AS fk_reference_column,
                    OBJECT_NAME(cfkr.object_id) AS fk_reference_table,
                    fk.object_id AS fk_constraint_id
               FROM sys.foreign_keys fk
               JOIN sys.foreign_key_columns fkc
                 ON fk.object_id = fkc.constraint_object_id
               JOIN sys.columns cfkp
                 ON fkc.parent_object_id = cfkp.object_id
                AND fkc.parent_column_id = cfkp.column_id
               JOIN sys.columns cfkr
                 ON fkc.referenced_object_id = cfkr.object_id
                AND fkc.referenced_column_id = cfkr.column_id
              WHERE fk.parent_object_id = OBJECT_ID (@fda_table)
			  UNION
			 SELECT N'customer_nr' AS parent_column,
			        N'customer_nr' AS fk_reference_column,
					N't_customer' AS fk_reference_table,
					NULL AS fk_constraint_id
			  WHERE COL_LENGTH(@fda_table,N'customer_nr') IS NOT NULL		  
			    AND @fda_table <> N't_customer' 
			  UNION
			 SELECT N'currency' AS parent_column,
			        N'currency' AS fk_reference_column,
					N't_currency_code' AS fk_reference_table,
					NULL AS fk_constraint_id
			  WHERE COL_LENGTH(@fda_table,N'currency') IS NOT NULL		  
			    AND @fda_table <> N't_currency_code' 
			  UNION
			 SELECT N'settlement_currency' AS parent_column,
			        N'currency' AS fk_reference_column,
					N't_currency_code' AS fk_reference_table,
					NULL AS fk_constraint_id
			  WHERE COL_LENGTH(@fda_table,N'settlement_currency') IS NOT NULL		  
			    AND @fda_table <> N't_currency_code' 
			  UNION
			 SELECT N'nationality' AS parent_column,
			        N'country_code' AS fk_reference_column,
					N't_country' AS fk_reference_table,
					NULL AS fk_constraint_id
			  WHERE COL_LENGTH(@fda_table,N'nationality') IS NOT NULL		  
			    AND @fda_table <> N't_country' 
			  UNION
			 SELECT N'domicile' AS parent_column,
			        N'country_code' AS fk_reference_column,
					N't_country' AS fk_reference_table,
					NULL AS fk_constraint_id
			  WHERE COL_LENGTH(@fda_table,N'domicile') IS NOT NULL		  
			    AND @fda_table <> N't_country' 
			  UNION
			 SELECT N'deal_type' AS parent_column,
			        N'deal_type' AS fk_reference_column,
					N't_deal_type' AS fk_reference_table,
					NULL AS fk_constraint_id
			  WHERE COL_LENGTH(@fda_table,N'deal_type') IS NOT NULL		  
			    AND @fda_table <> N't_deal_type' 
			  UNION
			 SELECT N'deal_subtype' AS parent_column,
			        N'deal_subtype' AS fk_reference_column,
					N't_deal_subtype' AS fk_reference_table,
					NULL AS fk_constraint_id
			  WHERE COL_LENGTH(@fda_table,N'deal_subtype') IS NOT NULL		  
			    AND @fda_table <> N't_deal_subtype' 
			  UNION
			 SELECT N'book_code' AS parent_column,
			        N'book_code' AS fk_reference_column,
					N't_book_code' AS fk_reference_table,
					NULL AS fk_constraint_id
			  WHERE COL_LENGTH(@fda_table,N'book_code') IS NOT NULL		  
			    AND @fda_table <> N't_book_code' 
			  ) fk
    ON c.name = fk.parent_column
  LEFT JOIN sys.check_constraints cc
    ON c.column_id = cc.parent_column_id
   AND c.object_id = cc.parent_object_id
 WHERE c.object_id = OBJECT_ID (@fda_table)
   AND c.is_identity = 0
 ORDER BY column_id;

INSERT INTO @syscolumns (
       column_id,
       name,
       is_nullable,
       pk_column_id,
       uq_column_id,
       fk_reference_column,
       fk_reference_table,
       fk_constraint_id,
       cc_condition)
SELECT ce.column_id,
       ce.name,
       1 AS is_nullable,
	   NULL AS pk_column_id,
	   NULL AS uq_column_id,
	   NULL AS fk_reference_column,
	   NULL AS fk_reference_table,
	   NULL AS fk_constraint_id,
	   NULL AS cc_condition
  FROM sys.columns ce
  LEFT JOIN @syscolumns c
    ON ce.name = c.name
 WHERE ce.object_id = OBJECT_ID (@etl_table)
   AND c.name IS NULL
 ORDER BY ce.column_id;

--SELECT *
--  FROM @syscolumns
-- ORDER BY column_id;

-- Part 1: Generate the application set


IF EXISTS(SELECT 1
  FROM t_mex_type
 WHERE mex_type_name = @mex_type_name
   AND is_current = 1)
BEGIN
  PRINT N'A current application set for the FDA table ' + @fda_table + N' already exists';
  IF EXISTS (SELECT 1
    FROM t_mex_type
   WHERE mex_type_name = @mex_type_name
     AND CONVERT(DATE,start_validity) = @cob_date)
  BEGIN
    PRINT N'There is an existing application set for the FDA table ' + @fda_table + ' with validity date ' + CONVERT (NVARCHAR(100),@cob_date,106) + N'. Reusing the existing.';
    SELECT @mex_type = mex_type,
           @version_id = version_id
      FROM t_mex_type
     WHERE mex_type_name = @mex_type_name
       AND CONVERT(DATE,start_validity) = @cob_date;
  END;
  ELSE
  BEGIN
    SELECT @mex_type = mex_type,
           @cur_version_id = version_id
      FROM t_mex_type
     WHERE mex_type_name = @mex_type_name
       AND is_current = 1;

    SELECT @version_id = MAX(version_id) + 1
      FROM t_mex_type
     WHERE mex_type = @mex_type;

    EXEC p_mex_copy_type @mex_type = @mex_type,
                         @version_id = @cur_version_id,
                         @target_type = @mex_type,
                         @target_version_id = @version_id,
                         @target_name = @mex_type_name,
                         @target_start_validity = @cob_date;

    UPDATE t_mex_type
	   SET is_current = 0
	 WHERE mex_type = @mex_type
	   AND version_id <> @version_id
	   AND is_current = 1;

    UPDATE t_mex_type
	   SET is_current = 1,
	       mex_type_desc = @mex_type_desc
	 WHERE mex_type = @mex_type
	   AND version_id = @version_id
	   AND is_current = 0;
  PRINT N'A copy of the existing application set for the FDA table ' + @fda_table + N' has been made.';
  END;
END;
ELSE -- IF EXISTS(SELECT 1 FROM t_mex_type WHERE mex_type_name = @mex_type_name AND is_current = 1)
BEGIN
  SELECT @tpl_mex_type = mex_type,
         @tpl_version_id = version_id
    FROM t_mex_type
   WHERE mex_type_name = N'Validation Template'
     AND is_current = 1;
  
  SELECT @mex_type = MAX(mex_type) + 1,
         @version_id = 1
    FROM t_mex_type
   WHERE mex_type BETWEEN @tpl_mex_type AND 42054000;

  EXEC p_mex_copy_type @mex_type = @tpl_mex_type,
                       @version_id = @tpl_version_id,
                       @target_type = @mex_type,
                       @target_version_id = @version_id,
                       @target_name = @mex_type_name,
                       @target_start_validity = @cob_date;

  UPDATE t_mex_type
     SET mex_type_desc = @mex_type_desc
   WHERE mex_type_name = @mex_type_name;

   PRINT N'A new application set has been created.';     
END; --ELSE IF EXISTS(SELECT 1 FROM t_mex_type WHERE mex_type_name = @mex_type_name AND is_current = 1)

SELECT @mex_id = mex_id
  FROM t_mex_type
 WHERE mex_type = @mex_type
   AND version_id = @version_id;

IF NOT EXISTS (SELECT 1
                 FROM t_mex_solution_member msm
				 JOIN t_mex_solution ms
				   ON msm.solution_id = ms.solution_id
				 JOIN t_mex_type mt
				   ON msm.mex_type = mt.mex_type
				WHERE ms.solution_name = N'ETL Validation Rules'
				  AND mt.mex_id = @mex_id)
BEGIN
  INSERT INTO t_mex_solution_member (
         solution_id,
         mex_type,
         execution_sequence,
         enabled,
         last_modified,
         modified_by)
  SELECT s.solution_id,
         @mex_type AS mex_type,
         ISNULL(MAX(execution_sequence),0) + 1 AS execution_sequence,
	     1 AS enabled,
  	     CURRENT_TIMESTAMP,
	     N'Automatic generation' AS modified_by
    FROM t_mex_solution s
    LEFT JOIN t_mex_solution_member sm
	  ON s.solution_id = sm.solution_id
    LEFT JOIN t_mex_type t
	  ON sm.mex_type = t.mex_type
     AND t.mex_type = @mex_type
   WHERE s.solution_name = N'ETL Validation Rules'
     AND t.mex_type IS NULL
   GROUP BY s.solution_id;    
END

PRINT N'mex id:				' + CONVERT (NVARCHAR(100),@mex_id);
PRINT N'mex type:			' + CONVERT (NVARCHAR(100),@mex_type);
PRINT N'version id:			' + CONVERT (NVARCHAR(100),@version_id);
PRINT N'Type Name:			' + @mex_type_name;
PRINT N'Type Description:	' + @mex_type_desc;

-- Part 2: Generate the Cluster

SELECT @cluster_id = cluster_id
  FROM t_mex_cluster
 WHERE mex_id = @mex_id
   AND cluster_name = N'ETL Validation Rules';

IF @cluster_id IS NULL
BEGIN
  INSERT INTO t_mex_cluster (
         mex_id,
         enabled,
         cluster_name,
         cluster_desc,
         parent_cluster,
         cluster_sequence,
         flow_control_type,
         max_iterations_enabled,
         max_iterations,
         max_elapsed_time_enabled,
         max_elapsed_time,
         sleep_enabled,
         sleep,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         1 AS [enabled],
		 N'ETL Validation Rules' AS cluster_name,
		 N'ETL Validation Rules' AS cluster_desc,
		 0 AS parent_cluster,
		 ISNULL(MAX(cluster_sequence),0) + 1 AS cluster_sequence,
		 NULL AS flow_control_type,
		 0 AS max_iterations_enabled,
         0 AS max_iterations,
         0 AS max_elapsed_time_enabled,
         NULL AS max_elapsed_time,
         0 AS sleep_enabled,
         NULL AS sleep,
         CURRENT_TIMESTAMP AS last_modified,
         N'Automatic generation' AS modified_by
    FROM t_mex_cluster
   WHERE mex_id = @mex_id;

  SELECT @cluster_id = cluster_id
    FROM t_mex_cluster
   WHERE mex_id = @mex_id
     AND cluster_name = N'ETL Validation Rules';
END; -- IF @cluster_id IS NULL

-- Step 3: Generate the runtime parameters
WITH CTE AS (
SELECT N'@adjustment_id' AS runtime_parameter_name,
       N'Adjustment ID Parameter' AS runtime_parameter_desc,
	   N'd_id' AS datatype,
	   N'' AS default_value,
	   0 AS is_local,
	   0 AS is_for_output,
	   0 AS is_system_generated,
	   NULL AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@job_log_id' AS runtime_parameter_name,
       N'This variable parameter is used for logging jobs & for drillback purposes' AS runtime_parameter_desc,
	   N'int' AS datatype,
	   N'<auto-generated>' AS default_value,
	   1 AS is_local,
	   0 AS is_for_output,
	   1 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@step_log_id' AS runtime_parameter_name,
       N'This variable parameter is used for logging steps & for drillback purposes' AS runtime_parameter_desc,
	   N'int' AS datatype,
	   N'<auto-generated>' AS default_value,
	   1 AS is_local,
	   0 AS is_for_output,
	   1 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@print_ind' AS runtime_parameter_name,
       N'This variable parameter determines how messages should be printed at runtime' AS runtime_parameter_desc,
	   N'char(1)' AS datatype,
	   N'<auto-generated>' AS default_value,
	   0 AS is_local,
	   0 AS is_for_output,
	   1 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@rowcount' AS runtime_parameter_name,
       N'Stores number of rows affected' AS runtime_parameter_desc,
	   N'int' AS datatype,
	   N'0' AS default_value,
	   1 AS is_local,
	   0 AS is_for_output,
	   1 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@elapsed_time' AS runtime_parameter_name,
       N'Elapsed time' AS runtime_parameter_desc,
	   N'datetime' AS datatype,
	   N'NULL' AS default_value,
	   0 AS is_local,
	   1 AS is_for_output,
	   1 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind)

 MERGE INTO t_mex_runtime_parameter AS tgt
 USING CTE AS src
    ON tgt.runtime_parameter_name = src.runtime_parameter_name
   AND tgt.mex_id = @mex_id
  WHEN MATCHED THEN UPDATE
   SET tgt.runtime_parameter_desc = src.runtime_parameter_desc,
       tgt.datatype = src.datatype,
       tgt.default_value = src.default_value,
       tgt.is_local = src.is_local,
       tgt.is_for_output = src.is_for_output,
       tgt.is_system_generated = src.is_system_generated,
       tgt.domain_list = src.domain_list,
       tgt.limit_to_list_ind = src.limit_to_list_ind,
       tgt.allow_blank_ind = src.allow_blank_ind,
       tgt.hide_ind = src.hide_ind,
       tgt.locked_ind = src.locked_ind,
	   tgt.last_modified = CURRENT_TIMESTAMP,
	   tgt.modified_by = N'Automatic generation'
  WHEN NOT MATCHED THEN INSERT (
       mex_id,
       runtime_parameter_name,
       runtime_parameter_desc,
       datatype,
       default_value,
       is_local,
       is_for_output,
       is_system_generated,
       domain_list,
       limit_to_list_ind,
       allow_blank_ind,
       hide_ind,
       locked_ind,
       last_modified,
       modified_by)
VALUES (
       @mex_id,
       src.runtime_parameter_name,
       src.runtime_parameter_desc,
       src.datatype,
       src.default_value,
       src.is_local,
       src.is_for_output,
       src.is_system_generated,
       src.domain_list,
       src.limit_to_list_ind,
       src.allow_blank_ind,
       src.hide_ind,
       src.locked_ind,
	   CURRENT_TIMESTAMP,
	   N'Automatic generation');

-- Step 4: Generate the locations

-- Step 4.1: Generate the output location

SELECT @out_location_id = location_id
  FROM t_mex_location
 WHERE mex_id = @mex_id
   AND location_type = N'OUT'
   AND location_name = N'ETL Validation Table';

IF @out_location_id IS NULL
BEGIN
  INSERT INTO t_mex_location (
         mex_id,
         location_name,
         location_desc,
         location_type,
         union_id,
         distinct_records,
         use_for_drillthrough,
         expose_as_odata_feed,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         N'ETL Validation Table' AS location_name,
		 N'ETL Validation Table' AS location_desc,
		 N'OUT' AS location_type,
		 NULL AS union_id,
		 0 AS distinct_records,
		 0 AS use_for_drillthrough,
		 0 AS expose_as_odata_feed,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;
  
  SELECT @out_location_id = location_id
    FROM t_mex_location
   WHERE mex_id = @mex_id
     AND location_type = N'OUT'
     AND location_name = N'ETL Validation Table';

  INSERT INTO t_mex_location_table (
         mex_id,
         location_id,
         object_server,
         object_catalog,
         object_owner,
         object_name,
         object_desc,
         object_alias,
         object_type,
         join_type,
         join_hint,
         object_sequence,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @out_location_id AS location_id,
		 N'' AS object_server,
		 N'' AS object_catalog,
		 N'' AS object_owner,
		 @etl_validation_table AS object_name,
		 N'ETL Validation Table' AS object_desc,
		 N'[etl]' AS object_alias,
		 N'U' AS object_type,
		 1 AS join_type,
		 0 AS join_hint,
		 1 AS object_sequence,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;

  SELECT @out_object_id = [object_id]
    FROM t_mex_location_table
   WHERE mex_id = @mex_id
     AND location_id = @out_location_id;

  INSERT INTO t_mex_output_field (
         mex_id,
         output_location_id,
         object_id,
         field_sequence,
         output_field_name,
         output_friendly_name1,
         output_friendly_name2,
         output_friendly_name3,
         is_mapping_candidate,
         domain_list,
         limit_to_list_ind,
         allow_blank_ind,
         hide_ind,
         locked_ind,
         default_output_expression,
         use_for_drillback,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @out_location_id AS output_location_id,
		 @out_object_id AS object_id,
		 c.column_id AS field_sequence,
		 c.name AS output_field_name,
		 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS output_friendly_name1,
		 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS output_friendly_name2,
		 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS output_friendly_name3,
		 1 AS is_mapping_candidate,
		 NULL AS domain_list,
		 0 AS limit_to_list_ind,
		 0 AS allow_blank_ind,
		 0 AS hide_ind,
		 0 AS locked_ind,
		 N'' AS default_output_expression,
		 0 AS use_for_drillback,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM sys.columns c
   WHERE c.object_id = OBJECT_ID(@etl_validation_table);
END; --IF @out_location_id IS NULL

-- Step 4.2: Generate the input locations

-- Step 4.2.1: Generate the duplicates input location
IF EXISTS (SELECT 1
             FROM @syscolumns
			WHERE pk_column_id IS NOT NULL)
BEGIN

	SELECT @dup_location_id = location_id
	  FROM t_mex_location
	 WHERE mex_id = @mex_id
	   AND location_type = N'SRC'
	   AND location_name = N'Duplicates';

	IF @dup_location_id IS NULL
	BEGIN
	  INSERT INTO t_mex_location (
			 mex_id,
			 location_name,
			 location_desc,
			 location_type,
			 union_id,
			 distinct_records,
			 use_for_drillthrough,
			 expose_as_odata_feed,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 N'Duplicates' AS location_name,
			 N'ETL input location used to spot duplicates' AS location_desc,
			 N'SRC' AS location_type,
			 NULL AS union_id,
			 0 AS distinct_records,
			 0 AS use_for_drillthrough,
			 0 AS expose_as_odata_feed,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by;
  
	  SELECT @dup_location_id = location_id
		FROM t_mex_location
	   WHERE mex_id = @mex_id
		 AND location_type = N'SRC'
		 AND location_name = N'Duplicates';

	  INSERT INTO t_mex_location_table (
			 mex_id,
			 location_id,
			 object_server,
			 object_catalog,
			 object_owner,
			 object_name,
			 object_desc,
			 object_alias,
			 object_type,
			 join_type,
			 join_hint,
			 object_sequence,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @dup_location_id AS location_id,
			 N'' AS object_server,
			 N'' AS object_catalog,
			 N'' AS object_owner,
			 @etl_table AS object_name,
			 N'ETL Table used to spot duplicates' AS object_desc,
			 N'[dup]' AS object_alias,
			 N'U' AS object_type,
			 1 AS join_type,
			 0 AS join_hint,
			 1 AS object_sequence,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by;

	  SELECT @dup_object_id = [object_id]
		FROM t_mex_location_table
	   WHERE mex_id = @mex_id
		 AND location_id = @dup_location_id;

	  INSERT INTO t_mex_source_field (
			 mex_id,
			 source_location_id,
			 object_id,
			 field_sequence,
			 source_field_name,
			 source_friendly_name1,
			 source_friendly_name2,
			 source_friendly_name3,
			 is_mapping_candidate,
			 domain_list,
			 include_in_drillback,
			 replace_null_by,
			 writeback_ind,
			 writeback_default,
			 last_modified,
			 modified_by)
	  SELECT DISTINCT @mex_id AS mex_id,
			 @dup_location_id AS source_location_id,
			 @dup_object_id AS object_id,
			 c.column_id AS field_sequence,
			 c.name AS source_field_name,
			 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name1,
			 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name2,
			 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name3,
			 CASE WHEN pk_column_id IS NOT NULL THEN 1
			   ELSE 0
			   END AS is_mapping_candidate,
			 NULL AS domain_list,
			 0 AS include_in_drillback,
			 NULL AS replace_null_by,
			 0 AS writeback_ind,
			 NULL AS writeback_default,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM @syscolumns c
	   ORDER BY column_id;
  
	  INSERT INTO t_mex_formula (
			 mex_id,
			 formula_name,
			 formula_desc,
			 formula_def,
			 original_def,
			 formula_sequence,
			 location_id,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 N'Count_Line_Nr' AS formula_name,
			 N'Count of line_nr used to identify duplicates' AS formula_desc,
			 N'(?1(?2))' AS formula_def,
			 N'(COUNT([dup].[line_nr]))' AS original_def,
			 1 AS formula_sequence,
			 @dup_location_id AS location_id,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by;

	  SELECT @dup_formula_id = formula_id
		FROM t_mex_formula
	   WHERE mex_id = @mex_id
		 AND formula_name = N'Count_Line_Nr';
  
	  SELECT @aggregation_id = aggregation_id
		FROM t_mex_aggregation_method
	   WHERE aggregation_method = N'COUNT';

	  SELECT @source_field_id = source_field_id
		FROM t_mex_source_field
	   WHERE mex_id = @mex_id
		 AND source_location_id = @dup_location_id
		 AND source_field_name = N'line_nr';

	  INSERT INTO t_mex_formula_placeholder (
			 mex_id,
			 formula_id,
			 placeholder_nr,
			 runtime_parameter_id,
			 source_field_id,
			 aggregation_id,
			 group_by_ind,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @dup_formula_id AS formula_id,
			 1 AS placeholder_nr,
			 NULL AS runtime_parameter_id,
			 NULL AS source_field_id,
			 @aggregation_id AS aggregation_id,
			 0 AS group_by_ind,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
	  UNION
	  SELECT @mex_id AS mex_id,
			 @dup_formula_id AS formula_id,
			 2 AS placeholder_nr,
			 NULL AS runtime_parameter_id,
			 @source_field_id AS source_field_id,
			 NULL AS aggregation_id,
			 0 AS group_by_ind,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by;

	  INSERT INTO t_mex_default_source_filter (
			 mex_id,
			 source_location_id,
			 default_source_filter_sequence,
			 default_source_filter_name,
			 default_source_filter_desc,
			 logical_operator,
			 open_brackets,
			 default_source_filter_left_member,
			 original_left_member,
			 comparison_operator,
			 default_source_filter_right_member,
			 original_right_member,
			 close_brackets,
			 filter_group_id,
			 filter_group_item_id,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @dup_location_id AS source_location_id,
			 1 AS default_source_filter_sequence,
			 N'Adjustment id filter' AS default_source_filter_name,
			 N'Filter on adjustment_id' AS default_source_filter_desc,
			 N'AND' AS logical_operator,
			 0 AS open_brackets,
			 N'?2' AS default_source_filter_left_member,
			 N'[dup].adjustment_id' AS original_left_member,
			 N'=' AS comparison_operator,
			 N'?1' AS default_source_filter_right_member,
			 N'@adjustment_id' AS original_right_member,
			 0 AS close_brackets,
			 NULL AS filter_group_id,
			 NULL AS filter_group_item_id,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by;

	  SELECT @default_source_filter_id = default_source_filter_id
		FROM t_mex_default_source_filter
	   WHERE mex_id = @mex_id
		 AND source_location_id = @dup_location_id
		 AND default_source_filter_name = N'Adjustment id filter';

	  SELECT @parameter_id = runtime_parameter_id
		FROM t_mex_runtime_parameter
	   WHERE mex_id = @mex_id
		 AND runtime_parameter_name = N'@adjustment_id';

	  SELECT @source_field_id = source_field_id
		FROM t_mex_source_field
	   WHERE mex_id = @mex_id
		 AND source_location_id = @dup_location_id
		 AND source_field_name = N'adjustment_id';

	  INSERT INTO t_mex_default_source_filter_placeholder (
			 mex_id,
			 source_location_id,
			 default_source_filter_id,
			 placeholder_nr,
			 runtime_parameter_id,
			 source_field_id,
			 formula_id,
			 aggregation_id,
			 group_by_ind,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @dup_location_id AS source_location_id,
			 @default_source_filter_id AS default_source_filter_id,
			 1 AS placeholder_nr,
			 @parameter_id AS runtime_parameter_id,
			 NULL AS source_field_id,
			 NULL AS formula_id,
			 NULL AS aggregation_id,
			 0 AS group_by_ind,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
	  UNION
	  SELECT @mex_id AS mex_id,
			 @dup_location_id AS source_location_id,
			 @default_source_filter_id AS default_source_filter_id,
			 2 AS placeholder_nr,
			 NULL AS runtime_parameter_id,
			 @source_field_id AS source_field_id,
			 NULL AS formula_id,
			 NULL AS aggregation_id,
			 0 AS group_by_ind,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by;

	END; -- IF @dup_location_id IS NULL
END; -- IF EXISTS (SELECT 1
-- Step 4.2.2: Generate the ETL input location
SELECT @etl_location_id = location_id
  FROM t_mex_location
 WHERE mex_id = @mex_id
   AND location_type = N'SRC'
   AND location_name = @etl_table;

IF @etl_location_id IS NULL
BEGIN
  INSERT INTO t_mex_location (
         mex_id,
         location_name,
         location_desc,
         location_type,
         union_id,
         distinct_records,
         use_for_drillthrough,
         expose_as_odata_feed,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @etl_table AS location_name,
		 N'ETL input location' AS location_desc,
		 N'SRC' AS location_type,
		 NULL AS union_id,
		 0 AS distinct_records,
		 0 AS use_for_drillthrough,
		 0 AS expose_as_odata_feed,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;
  
  SELECT @etl_location_id = location_id
    FROM t_mex_location
   WHERE mex_id = @mex_id
     AND location_type = N'SRC'
     AND location_name = @etl_table;

  INSERT INTO t_mex_location_table (
         mex_id,
         location_id,
         object_server,
         object_catalog,
         object_owner,
         object_name,
         object_desc,
         object_alias,
         object_type,
         join_type,
         join_hint,
         object_sequence,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @etl_location_id AS location_id,
		 N'' AS object_server,
		 N'' AS object_catalog,
		 N'' AS object_owner,
		 @etl_table AS object_name,
		 N'ETL Table' AS object_desc,
		 N'[etl]' AS object_alias,
		 N'U' AS object_type,
		 1 AS join_type,
		 0 AS join_hint,
		 1 AS object_sequence,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;

  SELECT @etl_object_id = [object_id]
    FROM t_mex_location_table
   WHERE mex_id = @mex_id
     AND location_id = @etl_location_id;

  INSERT INTO t_mex_source_field (
         mex_id,
         source_location_id,
         object_id,
         field_sequence,
         source_field_name,
         source_friendly_name1,
         source_friendly_name2,
         source_friendly_name3,
         is_mapping_candidate,
         domain_list,
         include_in_drillback,
         replace_null_by,
         writeback_ind,
         writeback_default,
         last_modified,
         modified_by)
  SELECT DISTINCT @mex_id AS mex_id,
         @etl_location_id AS source_location_id,
		 @etl_object_id AS object_id,
		 c.column_id AS field_sequence,
		 c.name AS source_field_name,
		 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name1,
		 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name2,
		 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name3,
		 CASE WHEN is_nullable = 0
		        OR pk_column_id IS NOT NULL
		        OR uq_column_id IS NOT NULL
				OR fk_reference_column IS NOT NULL
				OR cc_condition IS NOT NULL 
				OR c.name IN (N'adjustment_id',N'line_nr') THEN 1
		   ELSE 0
		   END AS is_mapping_candidate,
		 NULL AS domain_list,
		 0 AS include_in_drillback,
		 NULL AS replace_null_by,
		 0 AS writeback_ind,
		 NULL AS writeback_default,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM @syscolumns c
   ORDER BY column_id;

  INSERT INTO t_mex_default_source_filter (
         mex_id,
         source_location_id,
         default_source_filter_sequence,
         default_source_filter_name,
         default_source_filter_desc,
         logical_operator,
         open_brackets,
         default_source_filter_left_member,
         original_left_member,
         comparison_operator,
         default_source_filter_right_member,
         original_right_member,
         close_brackets,
         filter_group_id,
         filter_group_item_id,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @etl_location_id AS source_location_id,
		 1 AS default_source_filter_sequence,
		 N'Adjustment id filter' AS default_source_filter_name,
		 N'Filter on adjustment_id' AS default_source_filter_desc,
		 N'AND' AS logical_operator,
		 0 AS open_brackets,
		 N'?2' AS default_source_filter_left_member,
		 N'[dup].adjustment_id' AS original_left_member,
		 N'=' AS comparison_operator,
		 N'?1' AS default_source_filter_right_member,
		 N'@adjustment_id' AS original_right_member,
		 0 AS close_brackets,
		 NULL AS filter_group_id,
		 NULL AS filter_group_item_id,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;

  SELECT @default_source_filter_id = default_source_filter_id
    FROM t_mex_default_source_filter
   WHERE mex_id = @mex_id
     AND source_location_id = @etl_location_id
	 AND default_source_filter_name = N'Adjustment id filter';

  SELECT @parameter_id = runtime_parameter_id
    FROM t_mex_runtime_parameter
   WHERE mex_id = @mex_id
     AND runtime_parameter_name = N'@adjustment_id';

  SELECT @source_field_id = source_field_id
    FROM t_mex_source_field
   WHERE mex_id = @mex_id
     AND source_location_id = @etl_location_id
     AND source_field_name = N'adjustment_id';

  INSERT INTO t_mex_default_source_filter_placeholder (
         mex_id,
         source_location_id,
         default_source_filter_id,
         placeholder_nr,
         runtime_parameter_id,
         source_field_id,
         formula_id,
         aggregation_id,
         group_by_ind,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @etl_location_id AS source_location_id,
		 @default_source_filter_id AS default_source_filter_id,
		 1 AS placeholder_nr,
		 @parameter_id AS runtime_parameter_id,
		 NULL AS source_field_id,
		 NULL AS formula_id,
		 NULL AS aggregation_id,
		 0 AS group_by_ind,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
  UNION
  SELECT @mex_id AS mex_id,
         @etl_location_id AS source_location_id,
		 @default_source_filter_id AS default_source_filter_id,
		 2 AS placeholder_nr,
		 NULL AS runtime_parameter_id,
		 @source_field_id AS source_field_id,
		 NULL AS formula_id,
		 NULL AS aggregation_id,
		 0 AS group_by_ind,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;
END; -- IF @etl_location_id IS NULL

-- Step 4.2.3: Generate the foreign key input locations

DECLARE cur_ref_tables CURSOR FOR
SELECT DISTINCT fk_reference_table
  FROM @syscolumns
 WHERE fk_reference_table IS NOT NULL;

OPEN cur_ref_tables;
FETCH NEXT FROM cur_ref_tables INTO @fk_table;
WHILE @@FETCH_STATUS = 0
BEGIN
  SELECT @fk_location_id = NULL;
  SELECT @fk_location_id = location_id
    FROM t_mex_location
   WHERE mex_id = @mex_id
	 AND location_type = N'SRC'
	 AND location_name = @fk_table;
	IF @fk_location_id IS NULL
	BEGIN
	  INSERT INTO t_mex_location (
			 mex_id,
			 location_name,
			 location_desc,
			 location_type,
			 union_id,
			 distinct_records,
			 use_for_drillthrough,
			 expose_as_odata_feed,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @fk_table AS location_name,
			 N'Foreign key input location' AS location_desc,
			 N'SRC' AS location_type,
			 NULL AS union_id,
			 0 AS distinct_records,
			 0 AS use_for_drillthrough,
			 0 AS expose_as_odata_feed,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by;
  
	  SELECT @fk_location_id = location_id
		FROM t_mex_location
	   WHERE mex_id = @mex_id
		 AND location_type = N'SRC'
		 AND location_name = @fk_table;

	  INSERT INTO t_mex_location_table (
			 mex_id,
			 location_id,
			 object_server,
			 object_catalog,
			 object_owner,
			 object_name,
			 object_desc,
			 object_alias,
			 object_type,
			 join_type,
			 join_hint,
			 object_sequence,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @fk_location_id AS location_id,
			 N'' AS object_server,
			 N'' AS object_catalog,
			 N'' AS object_owner,
			 @fk_table AS object_name,
			 N'Foreign key Table' AS object_desc,
			 N'[fk' + CONVERT(NVARCHAR(5),@fk_cnt) + N']' AS object_alias,
			 N'U' AS object_type,
			 1 AS join_type,
			 0 AS join_hint,
			 1 AS object_sequence,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by;

	  SELECT @fk_object_id = [object_id]
		FROM t_mex_location_table
	   WHERE mex_id = @mex_id
		 AND location_id = @fk_location_id;

	  INSERT INTO t_mex_source_field (
			 mex_id,
			 source_location_id,
			 object_id,
			 field_sequence,
			 source_field_name,
			 source_friendly_name1,
			 source_friendly_name2,
			 source_friendly_name3,
			 is_mapping_candidate,
			 domain_list,
			 include_in_drillback,
			 replace_null_by,
			 writeback_ind,
			 writeback_default,
			 last_modified,
			 modified_by)
	  SELECT DISTINCT @mex_id AS mex_id,
			 @fk_location_id AS source_location_id,
			 @fk_object_id AS object_id,
			 c.column_id AS field_sequence,
			 c.name AS source_field_name,
			 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name1,
			 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name2,
			 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name3,
			 CASE WHEN cy.name IS NOT NULL THEN 1 ELSE 0 END AS is_mapping_candidate,
			 NULL AS domain_list,
			 0 AS include_in_drillback,
			 NULL AS replace_null_by,
			 0 AS writeback_ind,
			 NULL AS writeback_default,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM sys.columns c
		LEFT JOIN @syscolumns cy
		  ON c.name = cy.fk_reference_column
       WHERE c.object_id = OBJECT_ID (@fk_table)
	   ORDER BY c.column_id;
	END; -- IF @fk_location_id IS NULL
  SELECT @fk_cnt = @fk_cnt + 1; 
  FETCH NEXT FROM cur_ref_tables INTO @fk_table;
END; -- WHILE @@FETCH_STATUS = 0

CLOSE cur_ref_tables;
DEALLOCATE cur_ref_tables;

-- Step 5: Generate the job

SELECT @job_id = job_id
  FROM t_mex_job
 WHERE mex_id = @mex_id
   AND job_name = N'ETL Validation';

IF @job_id IS NULL
BEGIN
  INSERT INTO t_mex_job (
         mex_id,
         nr_of_runs_to_retain,
         run_retention_days,
         last_run_nr,
         enabled,
         job_name,
         job_desc,
         start_step_id,
         application_type,
         run_timeout,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         0 AS nr_of_runs_to_retain,
		 0 AS run_retention_days,
		 0 AS last_run_nr,
		 1 AS [enabled],
		 N'ETL Validation' AS job_name,
		 N'ETL Validation job that validates the data before loading into the FDA' AS job_desc,
		 0 AS start_step_id,
		 4 AS application_type,
		 NULL AS run_timeout,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;

  SELECT @job_id = job_id
    FROM t_mex_job
   WHERE mex_id = @mex_id
     AND job_name = N'ETL Validation';         
END;

-- Step 6: Generate the different job steps

-- Step 6.1: Duplicates
IF EXISTS (SELECT 1
             FROM @syscolumns
			WHERE pk_column_id IS NOT NULL)
BEGIN

	SELECT @step_id = NULL;

	SELECT @step_id = step_id
	  FROM t_mex_step
	 WHERE mex_id = @mex_id
	   AND job_id = @job_id
	   AND step_name = N'Duplicates check';

	IF @step_id IS NULL
	BEGIN
	  INSERT INTO t_mex_step (
			 mex_id,
			 job_id,
			 step_name,
			 step_desc,
			 step_sequence,
			 enabled,
			 warning_ind,
			 top_count,
			 is_for_insert,
			 is_for_update,
			 is_for_exec,
			 on_success_action,
			 on_success_step_id,
			 on_fail_action,
			 on_fail_step_id,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 N'Duplicates check' AS step_name,
			 N'Check on duplicates in the ETL table' AS step_desc,
			 1 AS step_sequence,
			 1 AS [enabled],
			 0 AS warning_ind,
			 NULL AS top_count,
			 1 AS is_for_insert,
			 0 AS is_for_update,
			 0 AS is_for_exec,
			 1 AS on_success_action,
			 -1 AS on_success_step_id,
			 3 AS on_fail_action,
			 -1 AS on_fail_step_id,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by;
  
	  SELECT @step_id = step_id
		FROM t_mex_step
	   WHERE mex_id = @mex_id
		 AND job_id = @job_id
		 AND step_name = N'Duplicates check';

	  INSERT INTO t_mex_field_mapping (
			 mex_id,
			 job_id,
			 step_id,
			 output_field_id,
			 priority,
			 output_expression,
			 original_expression,
			 field_mapping_name,
			 field_mapping_desc,
			 is_conditional,
			 is_identity,
			 seed,
			 increment,
			 mapping_group_id,
			 mapping_group_item_id,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 output_field_id,
			 1 AS priority,
			 CASE WHEN output_field_name = N'record_id'
					THEN N''''''
				  WHEN output_field_name IN (N'job_log_id',N'step_log_id',N'adjustment_id',N'line_nr')
					THEN N'?1'
				  WHEN output_field_name = N'validated_column_list'
					THEN N'''PK fields'''
				  WHEN output_field_name = N'validation_severity_level'
					THEN N'10'
				  WHEN output_field_name = N'validation_desc'
					THEN N'''Duplicates found in the ETL table'''
				  WHEN output_field_name = N'last_modified'
					THEN N'CURRENT_TIMESTAMP'
				  WHEN output_field_name = N'modified_by'
					THEN N'CURRENT_USER'
			 END AS output_expression,
			 CASE WHEN output_field_name = N'record_id'
					THEN N''''''
				  WHEN output_field_name = N'job_log_id'
					THEN N'@job_log_id'
				  WHEN output_field_name = N'step_log_id'
					THEN N'@step_log_id'
				  WHEN output_field_name = N'adjustment_id'
					THEN N'[etl].adjustment_id'
				  WHEN output_field_name = N'line_nr'
					THEN N'[etl].line_nr'
				  WHEN output_field_name = N'validated_column_list'
					THEN N'''PK fields'''
				  WHEN output_field_name = N'validation_severity_level'
					THEN N'10'
				  WHEN output_field_name = N'validation_desc'
					THEN N'''Duplicates found in the ETL table'''
				  WHEN output_field_name = N'last_modified'
					THEN N'CURRENT_TIMESTAMP'
				  WHEN output_field_name = N'modified_by'
					THEN N'CURRENT_USER'
			 END AS original_expression,
			 N'' AS field_mapping_name,
			 N'' AS field_mapping_desc,
			 0 AS is_conditional,
			 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS is_identity,
			 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS seed,
			 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS increment,
			 NULL AS mapping_group_id,
			 NULL AS mapping_group_item_id,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_output_field
	   WHERE mex_id = @mex_id;

	  INSERT INTO t_mex_step_link (
			 mex_id,
			 job_id,
			 step_id,
			 step_link_name,
			 step_link_desc,
			 step_link_sequence,
			 link_to_id,
			 link_type,
			 link_subtype,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 N'' AS step_link_name,
			 N'' AS step_link_desc,
			 1 AS step_link_sequence,
			 location_id AS link_to_id,
			 N'SRC' AS link_type,
			 NULL AS link_subtype,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_location
	   WHERE mex_id = @mex_id
		 AND location_name = @etl_table
	  UNION
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 N'' AS step_link_name,
			 N'' AS step_link_desc,
			 2 AS step_link_sequence,
			 location_id AS link_to_id,
			 N'OUT' AS link_type,
			 NULL AS link_subtype,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_location
	   WHERE mex_id = @mex_id
		 AND location_type = 'OUT'
	  UNION
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 N'' AS step_link_name,
			 N'' AS step_link_desc,
			 3 AS step_link_sequence,
			 location_id AS link_to_id,
			 N'REF' AS link_type,
			 N'INNER' AS link_subtype,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_location
	   WHERE mex_id = @mex_id 
		 AND location_name = N'Duplicates'    
	   ORDER BY step_link_sequence;

	  INSERT INTO t_mex_step_filter (
			 mex_id,
			 job_id,
			 step_id,
			 step_filter_sequence,
			 step_filter_name,
			 step_filter_desc,
			 logical_operator,
			 open_brackets,
			 step_filter_left_member,
			 original_left_member,
			 comparison_operator,
			 step_filter_right_member,
			 original_right_member,
			 close_brackets,
			 destination_clause,
			 display_level,
			 is_system_generated,
			 filter_group_id,
			 filter_group_item_id,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 pk_column_id AS step_filter_sequence,
			 N'' AS step_filter_name,
			 N'' AS step_filter_desc,
			 N'AND' AS logical_operator,
			 0 AS open_brackets,
			 N'?1' AS step_filter_left_member,
			 N'[dup].' + name AS original_left_member,
			 N'=' AS comparison_operator,
			 N'?2' AS step_filter_right_member,
			 N'[etl].' + name AS original_right_member,
			 0 AS close_brackets,
			 2 AS destination_clause,
			 0 AS display_level,
			 0 AS is_system_generated,
			 NULL AS filter_group_id,
			 NULL AS filter_group_item_id,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM @syscolumns
	   WHERE pk_column_id IS NOT NULL
	  UNION
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 MAX(pk_column_id) + 1 AS step_filter_sequence,
			 N'' AS step_filter_name,
			 N'' AS step_filter_desc,
			 N'AND' AS logical_operator,
			 0 AS open_brackets,
			 N'?1' AS step_filter_left_member,
			 N'[REF].Count_Line_Nr' AS original_left_member,
			 N'>' AS comparison_operator,
			 N'1' AS step_filter_right_member,
			 N'1' AS original_right_member,
			 0 AS close_brackets,
			 1 AS destination_clause,
			 0 AS display_level,
			 0 AS is_system_generated,
			 NULL AS filter_group_id,
			 NULL AS filter_group_item_id,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM @syscolumns
	   WHERE pk_column_id IS NOT NULL
	   ORDER BY pk_column_id;

	  INSERT INTO t_mex_field_mapping_placeholder (
			 mex_id,
			 job_id,
			 step_id,
			 output_field_id,
			 field_mapping_id,
			 placeholder_nr,
			 step_link_id,
			 runtime_parameter_id,
			 source_field_id,
			 formula_id,
			 aggregation_id,
			 group_by_ind,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 output_field_id,
			 field_mapping_id,
			 1 AS placeholder_nr,
			 CASE WHEN original_expression LIKE N'_etl%' THEN msl.step_link_id END AS step_link_id,
			 mrp.runtime_parameter_id,
			 msf.source_field_id,
			 NULL AS formula_id,
			 NULL AS aggregation_id,
			 0 AS group_by_ind,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_field_mapping mfm
		JOIN t_mex_step_link msl
		  ON mfm.mex_id = msl.mex_id
		 AND mfm.job_id = msl.job_id
		 AND mfm.step_id = msl.step_id
		 AND msl.link_type = N'SRC'
		LEFT JOIN t_mex_runtime_parameter mrp
		  ON mfm.mex_id = mrp.mex_id
		 AND mfm.original_expression = mrp.runtime_parameter_name
		LEFT JOIN t_mex_source_field msf
		  ON mfm.mex_id = msf.mex_id
		 AND mfm.original_expression = N'[etl].' + msf.source_field_name-- IN (N'adjustment_id',N'line_nr')
		 AND msf.is_mapping_candidate = 1
	   WHERE mfm.mex_id = @mex_id
		 AND mfm.job_id = @job_id
		 AND mfm.step_id = @step_id
		 AND output_expression = N'?1';

	  INSERT INTO t_mex_step_filter_placeholder (
			 mex_id,
			 job_id,
			 step_id,
			 step_filter_id,
			 placeholder_nr,
			 step_link_id,
			 runtime_parameter_id,
			 source_field_id,
			 formula_id,
			 aggregation_id,
			 group_by_ind,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 msf.step_filter_id,
			 CASE WHEN msl.link_type = N'SRC' THEN 2 ELSE 1 END AS placeholder_nr,
			 msl.step_link_id,
			 NULL AS runtime_parameter_id,
			 msfi.source_field_id,
			 NULL AS formula_id,
			 NULL AS aggregation_id,
			 0 AS group_by_ind,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_step_filter msf
		JOIN t_mex_step_link msl
		  ON msf.mex_id = msl.mex_id
		 AND msf.job_id = msl.job_id
		 AND msf.step_id = msl.step_id
		 AND msl.link_type IN (N'SRC',N'REF')
		JOIN t_mex_location ml
		  ON msf.mex_id = ml.mex_id
		 AND msl.link_to_id = ml.location_id
		JOIN t_mex_source_field msfi
		  ON msf.mex_id = msfi.mex_id
		 AND ml.location_id = msfi.source_location_id
		 AND msf.original_left_member = N'[dup].' +msfi.source_field_name
	   WHERE msf.mex_id = @mex_id
		 AND msf.job_id = @job_id
		 AND msf.step_id = @step_id
	  UNION
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 msf.step_filter_id,
			 CASE WHEN msl.link_type = N'SRC' THEN 2 ELSE 1 END AS placeholder_nr,
			 msl.step_link_id,
			 NULL AS runtime_parameter_id,
			 NULL AS source_field_id,
			 mf.formula_id,
			 NULL AS aggregation_id,
			 0 AS group_by_ind,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_step_filter msf
		JOIN t_mex_step_link msl
		  ON msf.mex_id = msl.mex_id
		 AND msf.job_id = msl.job_id
		 AND msf.step_id = msl.step_id
		 AND msl.link_type = N'REF'
		JOIN t_mex_location ml
		  ON msf.mex_id = ml.mex_id
		 AND msl.link_to_id = ml.location_id
		LEFT JOIN t_mex_formula mf
		  ON msf.mex_id = mf.mex_id
		 AND ml.location_id = mf.location_id
	   WHERE msf.mex_id = @mex_id
		 AND msf.job_id = @job_id
		 AND msf.step_id = @step_id
		 AND msf.original_left_member = N'[REF].Count_Line_Nr'	 
	  ORDER BY msf.step_filter_id, msl.step_link_id;

	  INSERT INTO t_mex_step_link_table (
			 mex_id,
			 job_id,
			 step_id,
			 step_link_id,
			 location_id,
			 object_id,
			 object_alias,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 msl.step_link_id,
			 ml.location_id,
			 mlt.object_id,
			 mlt.object_alias,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_step_link msl
		JOIN t_mex_location ml
		  ON msl.mex_id = ml.mex_id
		 AND msl.link_to_id = ml.location_id
		JOIN t_mex_location_table mlt
		  ON msl.mex_id = mlt.mex_id
		 AND ml.location_id = mlt.location_id
	   WHERE msl.mex_id = @mex_id
		 AND msl.job_id = @job_id
		 AND msl.step_id = @step_id
		 AND msl.link_type IN (N'SRC',N'REF');
	END;
END; -- IF EXISTS (SELECT 1
-- Step 6.2: Null checks
SELECT @step_id = NULL;

SELECT @step_id = step_id
  FROM t_mex_step
 WHERE mex_id = @mex_id
   AND job_id = @job_id
   AND step_name = N'Null checks';

IF @step_id IS NULL
BEGIN
  INSERT INTO t_mex_step (
         mex_id,
         job_id,
         step_name,
         step_desc,
         step_sequence,
         enabled,
         warning_ind,
         top_count,
         is_for_insert,
         is_for_update,
         is_for_exec,
         on_success_action,
         on_success_step_id,
         on_fail_action,
         on_fail_step_id,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
		 N'Null checks' AS step_name,
		 N'Check on NULL values for not nullable columns in the ETL table' AS step_desc,
		 ISNULL(MAX(step_sequence),0) + 1 AS step_sequence,
		 1 AS [enabled],
		 0 AS warning_ind,
		 NULL AS top_count,
		 1 AS is_for_insert,
		 0 AS is_for_update,
		 0 AS is_for_exec,
		 1 AS on_success_action,
		 -1 AS on_success_step_id,
		 3 AS on_fail_action,
		 -1 AS on_fail_step_id,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
	FROM t_mex_step
   WHERE mex_id = @mex_id
     AND job_id = @job_id;
  
  SELECT @step_id = step_id
    FROM t_mex_step
   WHERE mex_id = @mex_id
     AND job_id = @job_id
     AND step_name = N'Null checks';

  INSERT INTO t_mex_field_mapping (
         mex_id,
         job_id,
         step_id,
         output_field_id,
         priority,
         output_expression,
         original_expression,
         field_mapping_name,
         field_mapping_desc,
         is_conditional,
         is_identity,
         seed,
         increment,
         mapping_group_id,
         mapping_group_item_id,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
		 @step_id AS step_id,
		 output_field_id,
		 1 AS priority,
		 CASE WHEN output_field_name = N'record_id'
		        THEN N''''''
			  WHEN output_field_name IN (N'job_log_id',N'step_log_id',N'adjustment_id',N'line_nr')
			    THEN N'?1'
			  WHEN output_field_name = N'validated_column_list'
			    THEN N'''NULL values'''
			  WHEN output_field_name = N'validation_severity_level'
			    THEN N'10'
			  WHEN output_field_name = N'validation_desc'
			    THEN N'''NULL values found in the ETL table'''
			  WHEN output_field_name = N'last_modified'
			    THEN N'CURRENT_TIMESTAMP'
			  WHEN output_field_name = N'modified_by'
			    THEN N'CURRENT_USER'
		 END AS output_expression,
		 CASE WHEN output_field_name = N'record_id'
		        THEN N''''''
			  WHEN output_field_name = N'job_log_id'
			    THEN N'@job_log_id'
			  WHEN output_field_name = N'step_log_id'
			    THEN N'@step_log_id'
			  WHEN output_field_name = N'adjustment_id'
			    THEN N'[etl].adjustment_id'
			  WHEN output_field_name = N'line_nr'
			    THEN N'[etl].line_nr'
			  WHEN output_field_name = N'validated_column_list'
			    THEN N'''NULL values'''
			  WHEN output_field_name = N'validation_severity_level'
			    THEN N'10'
			  WHEN output_field_name = N'validation_desc'
			    THEN N'''NULL values found in the ETL table'''
			  WHEN output_field_name = N'last_modified'
			    THEN N'CURRENT_TIMESTAMP'
			  WHEN output_field_name = N'modified_by'
			    THEN N'CURRENT_USER'
		 END AS original_expression,
		 N'' AS field_mapping_name,
		 N'' AS field_mapping_desc,
		 0 AS is_conditional,
		 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS is_identity,
		 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS seed,
		 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS increment,
		 NULL AS mapping_group_id,
		 NULL AS mapping_group_item_id,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM t_mex_output_field
   WHERE mex_id = @mex_id;

  INSERT INTO t_mex_step_link (
         mex_id,
         job_id,
         step_id,
         step_link_name,
         step_link_desc,
         step_link_sequence,
         link_to_id,
         link_type,
         link_subtype,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
		 @step_id AS step_id,
		 N'' AS step_link_name,
		 N'' AS step_link_desc,
		 1 AS step_link_sequence,
		 location_id AS link_to_id,
		 N'SRC' AS link_type,
		 NULL AS link_subtype,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM t_mex_location
   WHERE mex_id = @mex_id
     AND location_name = @etl_table
  UNION
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
		 @step_id AS step_id,
		 N'' AS step_link_name,
		 N'' AS step_link_desc,
		 2 AS step_link_sequence,
		 location_id AS link_to_id,
		 N'OUT' AS link_type,
		 NULL AS link_subtype,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM t_mex_location
   WHERE mex_id = @mex_id
     AND location_type = N'OUT'
   ORDER BY step_link_sequence;

  INSERT INTO t_mex_step_filter (
         mex_id,
         job_id,
         step_id,
         step_filter_sequence,
         step_filter_name,
         step_filter_desc,
         logical_operator,
         open_brackets,
         step_filter_left_member,
         original_left_member,
         comparison_operator,
         step_filter_right_member,
         original_right_member,
         close_brackets,
         destination_clause,
         display_level,
         is_system_generated,
         filter_group_id,
         filter_group_item_id,
         last_modified,
         modified_by)
  SELECT DISTINCT @mex_id AS mex_id,
         @job_id AS job_id,
		 @step_id AS step_id,
		 DENSE_RANK() OVER (ORDER BY column_id) AS step_filter_sequence,
		 N'' AS step_filter_name,
		 N'' AS step_filter_desc,
		 CASE WHEN column_id = MIN(column_id) OVER () THEN N'AND' ELSE N'OR' END AS logical_operator,
		 CASE WHEN column_id = MIN(column_id) OVER () THEN 1 ELSE 0 END AS open_brackets,
		 N'?1' AS step_filter_left_member,
		 N'[etl].' + name AS original_left_member,
		 N'IS NULL' AS comparison_operator,
		 N'' AS step_filter_right_member,
		 N'' + name AS original_right_member,
		 CASE WHEN column_id = MAX(column_id) OVER () THEN 1 ELSE 0 END AS close_brackets,
		 1 AS destination_clause,
		 0 AS display_level,
		 0 AS is_system_generated,
		 NULL AS filter_group_id,
		 NULL AS filter_group_item_id,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM @syscolumns
   WHERE is_nullable = 0
     AND name NOT IN (N'flag',N'adjustment_id',N'line_nr',N'retry_count',N'last_modified',N'modified_by');

  INSERT INTO t_mex_field_mapping_placeholder (
         mex_id,
         job_id,
         step_id,
         output_field_id,
         field_mapping_id,
         placeholder_nr,
         step_link_id,
         runtime_parameter_id,
         source_field_id,
         formula_id,
         aggregation_id,
         group_by_ind,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
		 @step_id AS step_id,
		 mfm.output_field_id,
		 mfm.field_mapping_id,
		 1 AS placeholder_nr,
		 CASE WHEN original_expression LIKE N'_etl%' THEN msl.step_link_id END AS step_link_id,
		 mrp.runtime_parameter_id,
		 msf.source_field_id,
		 NULL AS formula_id,
		 NULL AS aggregation_id,
		 0 AS group_by_ind,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM t_mex_field_mapping mfm
	JOIN t_mex_step_link msl
	  ON mfm.mex_id = msl.mex_id
	 AND mfm.job_id = msl.job_id
	 AND mfm.step_id = msl.step_id
	 AND msl.link_type = N'SRC'
	LEFT JOIN t_mex_runtime_parameter mrp
	  ON mfm.mex_id = mrp.mex_id
	 AND mfm.original_expression = mrp.runtime_parameter_name
    LEFT JOIN t_mex_source_field msf
	  ON mfm.mex_id = msf.mex_id
     AND mfm.original_expression = N'[etl].' + msf.source_field_name-- IN (N'adjustment_id',N'line_nr')
     AND msf.is_mapping_candidate = 1
   WHERE mfm.mex_id = @mex_id
     AND mfm.job_id = @job_id
	 AND mfm.step_id = @step_id
     AND output_expression = N'?1';

  INSERT INTO t_mex_step_filter_placeholder (
         mex_id,
         job_id,
         step_id,
         step_filter_id,
         placeholder_nr,
         step_link_id,
         runtime_parameter_id,
         source_field_id,
         formula_id,
         aggregation_id,
         group_by_ind,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
		 @step_id AS step_id,
		 msf.step_filter_id,
		 1 AS placeholder_nr,
		 msl.step_link_id,
		 NULL AS runtime_parameter_id,
		 msfi.source_field_id,
		 NULL AS formula_id,
		 NULL AS aggregation_id,
		 0 AS group_by_ind,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM t_mex_step_filter msf
	JOIN t_mex_step_link msl
	  ON msf.mex_id = msl.mex_id
	 AND msf.job_id = msl.job_id
	 AND msf.step_id = msl.step_id
	 AND msl.link_type IN (N'SRC',N'REF')
    JOIN t_mex_location ml
	  ON msf.mex_id = ml.mex_id
	 AND msl.link_to_id = ml.location_id
    JOIN t_mex_source_field msfi
	  ON msf.mex_id = msfi.mex_id
	 AND ml.location_id = msfi.source_location_id
	 AND msf.original_left_member = N'[etl].' +msfi.source_field_name
   WHERE msf.mex_id = @mex_id
     AND msf.job_id = @job_id
     AND msf.step_id = @step_id
  ORDER BY msf.step_filter_id, msl.step_link_id;

  INSERT INTO t_mex_step_link_table (
         mex_id,
         job_id,
         step_id,
         step_link_id,
         location_id,
         object_id,
         object_alias,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
		 @step_id AS step_id,
		 msl.step_link_id,
		 ml.location_id,
		 mlt.object_id,
		 mlt.object_alias,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM t_mex_step_link msl
	JOIN t_mex_location ml
	  ON msl.mex_id = ml.mex_id
	 AND msl.link_to_id = ml.location_id
    JOIN t_mex_location_table mlt
	  ON msl.mex_id = mlt.mex_id
	 AND ml.location_id = mlt.location_id
   WHERE msl.mex_id = @mex_id
     AND msl.job_id = @job_id
	 AND msl.step_id = @step_id
     AND msl.link_type IN (N'SRC',N'REF');
END;
-- Step 6.3: Unique key constraints
IF EXISTS (SELECT 1
             FROM @syscolumns
            WHERE uq_column_id IS NOT NULL)
BEGIN
	DECLARE uq CURSOR FOR
	SELECT DISTINCT uq_column_id
	  FROM @syscolumns
	 WHERE uq_column_id IS NOT NULL;
	
	OPEN uq;

	FETCH NEXT FROM uq INTO @uq;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @dup_location_id = NULL;

		SELECT @dup_location_id = location_id
		  FROM t_mex_location
		 WHERE mex_id = @mex_id
		   AND location_type = N'SRC'
		   AND location_name = N'Unique ' + CONVERT(NVARCHAR(10),@uq);

		IF @dup_location_id IS NULL
		BEGIN
		  INSERT INTO t_mex_location (
				 mex_id,
				 location_name,
				 location_desc,
				 location_type,
				 union_id,
				 distinct_records,
				 use_for_drillthrough,
				 expose_as_odata_feed,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 N'Unique ' + CONVERT(NVARCHAR(10),@uq) AS location_name,
				 N'ETL input location used to spot duplicates in unique key constraint ' + CONVERT(NVARCHAR(10),@uq) AS location_desc,
				 N'SRC' AS location_type,
				 NULL AS union_id,
				 0 AS distinct_records,
				 0 AS use_for_drillthrough,
				 0 AS expose_as_odata_feed,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by;
  
		  SELECT @dup_location_id = location_id
			FROM t_mex_location
		   WHERE mex_id = @mex_id
			 AND location_type = N'SRC'
			 AND location_name = N'Unique ' + CONVERT(NVARCHAR(10),@uq);

		  INSERT INTO t_mex_location_table (
				 mex_id,
				 location_id,
				 object_server,
				 object_catalog,
				 object_owner,
				 object_name,
				 object_desc,
				 object_alias,
				 object_type,
				 join_type,
				 join_hint,
				 object_sequence,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @dup_location_id AS location_id,
				 N'' AS object_server,
				 N'' AS object_catalog,
				 N'' AS object_owner,
				 @etl_table AS object_name,
				 N'ETL Table used to spot duplicates' AS object_desc,
				 N'[uq' + CONVERT(NVARCHAR(10),@uq) + N']' AS object_alias,
				 N'U' AS object_type,
				 1 AS join_type,
				 0 AS join_hint,
				 1 AS object_sequence,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by;

		  SELECT @dup_object_id = [object_id]
			FROM t_mex_location_table
		   WHERE mex_id = @mex_id
			 AND location_id = @dup_location_id;

		  INSERT INTO t_mex_source_field (
				 mex_id,
				 source_location_id,
				 object_id,
				 field_sequence,
				 source_field_name,
				 source_friendly_name1,
				 source_friendly_name2,
				 source_friendly_name3,
				 is_mapping_candidate,
				 domain_list,
				 include_in_drillback,
				 replace_null_by,
				 writeback_ind,
				 writeback_default,
				 last_modified,
				 modified_by)
		  SELECT DISTINCT @mex_id AS mex_id,
				 @dup_location_id AS source_location_id,
				 @dup_object_id AS object_id,
				 c.column_id AS field_sequence,
				 c.name AS source_field_name,
				 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name1,
				 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name2,
				 dbo.fn_cvt_proper_ps (REPLACE(c.name,N'_',N' '),1) AS source_friendly_name3,
				 CASE WHEN uq_column_id = @uq THEN 1
				   ELSE 0
				   END AS is_mapping_candidate,
				 NULL AS domain_list,
				 0 AS include_in_drillback,
				 NULL AS replace_null_by,
				 0 AS writeback_ind,
				 NULL AS writeback_default,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM @syscolumns c
		   WHERE @uq = ISNULL(uq_column_id,@uq);		      

		  INSERT INTO t_mex_formula (
				 mex_id,
				 formula_name,
				 formula_desc,
				 formula_def,
				 original_def,
				 formula_sequence,
				 location_id,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 N'Count_Line_Nr_UQ' + CONVERT(NVARCHAR(10),@uq) AS formula_name,
				 N'Count of line_nr used to identify duplicates' AS formula_desc,
				 N'(?1(?2))' AS formula_def,
				 N'(COUNT([uq' + CONVERT(NVARCHAR(10),@uq) + N'].[line_nr]))' AS original_def,
				 1 AS formula_sequence,
				 @dup_location_id AS location_id,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by;

		  SELECT @dup_formula_id = formula_id
			FROM t_mex_formula
		   WHERE mex_id = @mex_id
			 AND formula_name = N'Count_Line_Nr_UQ' + CONVERT(NVARCHAR(10),@uq);
  
		  SELECT @aggregation_id = aggregation_id
			FROM t_mex_aggregation_method
		   WHERE aggregation_method = N'COUNT';

		  SELECT @source_field_id = source_field_id
			FROM t_mex_source_field
		   WHERE mex_id = @mex_id
			 AND source_location_id = @dup_location_id
			 AND source_field_name = N'line_nr';

		  INSERT INTO t_mex_formula_placeholder (
				 mex_id,
				 formula_id,
				 placeholder_nr,
				 runtime_parameter_id,
				 source_field_id,
				 aggregation_id,
				 group_by_ind,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @dup_formula_id AS formula_id,
				 1 AS placeholder_nr,
				 NULL AS runtime_parameter_id,
				 NULL AS source_field_id,
				 @aggregation_id AS aggregation_id,
				 0 AS group_by_ind,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
		  UNION
		  SELECT @mex_id AS mex_id,
				 @dup_formula_id AS formula_id,
				 2 AS placeholder_nr,
				 NULL AS runtime_parameter_id,
				 @source_field_id AS source_field_id,
				 NULL AS aggregation_id,
				 0 AS group_by_ind,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by;

		  INSERT INTO t_mex_default_source_filter (
				 mex_id,
				 source_location_id,
				 default_source_filter_sequence,
				 default_source_filter_name,
				 default_source_filter_desc,
				 logical_operator,
				 open_brackets,
				 default_source_filter_left_member,
				 original_left_member,
				 comparison_operator,
				 default_source_filter_right_member,
				 original_right_member,
				 close_brackets,
				 filter_group_id,
				 filter_group_item_id,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @dup_location_id AS source_location_id,
				 1 AS default_source_filter_sequence,
				 N'Adjustment id filter' AS default_source_filter_name,
				 N'Filter on adjustment_id' AS default_source_filter_desc,
				 N'AND' AS logical_operator,
				 0 AS open_brackets,
				 N'?2' AS default_source_filter_left_member,
				 N'[uq' + CONVERT(NVARCHAR(10),@uq) + N'].adjustment_id' AS original_left_member,
				 N'=' AS comparison_operator,
				 N'?1' AS default_source_filter_right_member,
				 N'@adjustment_id' AS original_right_member,
				 0 AS close_brackets,
				 NULL AS filter_group_id,
				 NULL AS filter_group_item_id,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by;

		  SELECT @default_source_filter_id = default_source_filter_id
			FROM t_mex_default_source_filter
		   WHERE mex_id = @mex_id
			 AND source_location_id = @dup_location_id
			 AND default_source_filter_name = N'Adjustment id filter';

		  SELECT @parameter_id = runtime_parameter_id
			FROM t_mex_runtime_parameter
		   WHERE mex_id = @mex_id
			 AND runtime_parameter_name = N'@adjustment_id';

		  SELECT @source_field_id = source_field_id
			FROM t_mex_source_field
		   WHERE mex_id = @mex_id
			 AND source_location_id = @dup_location_id
			 AND source_field_name = N'adjustment_id';

		  INSERT INTO t_mex_default_source_filter_placeholder (
				 mex_id,
				 source_location_id,
				 default_source_filter_id,
				 placeholder_nr,
				 runtime_parameter_id,
				 source_field_id,
				 formula_id,
				 aggregation_id,
				 group_by_ind,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @dup_location_id AS source_location_id,
				 @default_source_filter_id AS default_source_filter_id,
				 1 AS placeholder_nr,
				 @parameter_id AS runtime_parameter_id,
				 NULL AS source_field_id,
				 NULL AS formula_id,
				 NULL AS aggregation_id,
				 0 AS group_by_ind,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
		  UNION
		  SELECT @mex_id AS mex_id,
				 @dup_location_id AS source_location_id,
				 @default_source_filter_id AS default_source_filter_id,
				 2 AS placeholder_nr,
				 NULL AS runtime_parameter_id,
				 @source_field_id AS source_field_id,
				 NULL AS formula_id,
				 NULL AS aggregation_id,
				 0 AS group_by_ind,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by;

		END; -- IF @dup_location_id IS NULL
		
		SELECT @step_id = NULL;

		SELECT @step_id = step_id
		  FROM t_mex_step
		 WHERE mex_id = @mex_id
		   AND job_id = @job_id
		   AND step_name = N'Unique ' + CONVERT(NVARCHAR(10),@uq);

		IF @step_id IS NULL
		BEGIN
		  INSERT INTO t_mex_step (
				 mex_id,
				 job_id,
				 step_name,
				 step_desc,
				 step_sequence,
				 enabled,
				 warning_ind,
				 top_count,
				 is_for_insert,
				 is_for_update,
				 is_for_exec,
				 on_success_action,
				 on_success_step_id,
				 on_fail_action,
				 on_fail_step_id,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 N'Unique ' + CONVERT(NVARCHAR(10),@uq) AS step_name,
				 N'Unique key ' + CONVERT(NVARCHAR(10),@uq) + N' check' AS step_desc,
				 MAX(step_sequence) + 1 AS step_sequence,
				 1 AS [enabled],
				 0 AS warning_ind,
				 NULL AS top_count,
				 1 AS is_for_insert,
				 0 AS is_for_update,
				 0 AS is_for_exec,
				 1 AS on_success_action,
				 -1 AS on_success_step_id,
				 3 AS on_fail_action,
				 -1 AS on_fail_step_id,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
		    FROM t_mex_step
		   WHERE mex_id = @mex_id
		     AND job_id = @job_id;
  
		  SELECT @step_id = step_id
			FROM t_mex_step
		   WHERE mex_id = @mex_id
			 AND job_id = @job_id
			 AND step_name = N'Unique ' + CONVERT(NVARCHAR(10),@uq);

		  INSERT INTO t_mex_field_mapping (
				 mex_id,
				 job_id,
				 step_id,
				 output_field_id,
				 priority,
				 output_expression,
				 original_expression,
				 field_mapping_name,
				 field_mapping_desc,
				 is_conditional,
				 is_identity,
				 seed,
				 increment,
				 mapping_group_id,
				 mapping_group_item_id,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 output_field_id,
				 1 AS priority,
				 CASE WHEN output_field_name = N'record_id'
						THEN N''''''
					  WHEN output_field_name IN (N'job_log_id',N'step_log_id',N'adjustment_id',N'line_nr')
						THEN N'?1'
					  WHEN output_field_name = N'validated_column_list'
						THEN N'''UQ constraint' + CONVERT(NVARCHAR(10),@uq) + ''''
					  WHEN output_field_name = N'validation_severity_level'
						THEN N'10'
					  WHEN output_field_name = N'validation_desc'
						THEN N'''Unique constraint violations found in the ETL table'''
					  WHEN output_field_name = N'last_modified'
						THEN N'CURRENT_TIMESTAMP'
					  WHEN output_field_name = N'modified_by'
						THEN N'CURRENT_USER'
				 END AS output_expression,
				 CASE WHEN output_field_name = N'record_id'
						THEN N''''''
					  WHEN output_field_name = N'job_log_id'
						THEN N'@job_log_id'
					  WHEN output_field_name = N'step_log_id'
						THEN N'@step_log_id'
					  WHEN output_field_name = N'adjustment_id'
						THEN N'[etl].adjustment_id'
					  WHEN output_field_name = N'line_nr'
						THEN N'[etl].line_nr'
					  WHEN output_field_name = N'validated_column_list'
						THEN N'''UQ constraint' + CONVERT(NVARCHAR(10),@uq) + ''''
					  WHEN output_field_name = N'validation_severity_level'
						THEN N'10'
					  WHEN output_field_name = N'validation_desc'
						THEN N'''Unique constraint violations found in the ETL table'''
					  WHEN output_field_name = N'last_modified'
						THEN N'CURRENT_TIMESTAMP'
					  WHEN output_field_name = N'modified_by'
						THEN N'CURRENT_USER'
				 END AS original_expression,
				 N'' AS field_mapping_name,
				 N'' AS field_mapping_desc,
				 0 AS is_conditional,
				 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS is_identity,
				 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS seed,
				 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS increment,
				 NULL AS mapping_group_id,
				 NULL AS mapping_group_item_id,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_output_field
		   WHERE mex_id = @mex_id;

		  INSERT INTO t_mex_step_link (
				 mex_id,
				 job_id,
				 step_id,
				 step_link_name,
				 step_link_desc,
				 step_link_sequence,
				 link_to_id,
				 link_type,
				 link_subtype,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 N'' AS step_link_name,
				 N'' AS step_link_desc,
				 1 AS step_link_sequence,
				 location_id AS link_to_id,
				 N'SRC' AS link_type,
				 NULL AS link_subtype,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_location
		   WHERE mex_id = @mex_id
			 AND location_name = @etl_table
		  UNION
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 N'' AS step_link_name,
				 N'' AS step_link_desc,
				 2 AS step_link_sequence,
				 location_id AS link_to_id,
				 N'OUT' AS link_type,
				 NULL AS link_subtype,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_location
		   WHERE mex_id = @mex_id
			 AND location_type = 'OUT'
		  UNION
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 N'' AS step_link_name,
				 N'' AS step_link_desc,
				 3 AS step_link_sequence,
				 location_id AS link_to_id,
				 N'REF' AS link_type,
				 N'INNER' AS link_subtype,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_location
		   WHERE mex_id = @mex_id 
			 AND location_name = N'Unique ' + CONVERT(NVARCHAR(10),@uq)
		   ORDER BY step_link_sequence;

		  INSERT INTO t_mex_step_filter (
				 mex_id,
				 job_id,
				 step_id,
				 step_filter_sequence,
				 step_filter_name,
				 step_filter_desc,
				 logical_operator,
				 open_brackets,
				 step_filter_left_member,
				 original_left_member,
				 comparison_operator,
				 step_filter_right_member,
				 original_right_member,
				 close_brackets,
				 destination_clause,
				 display_level,
				 is_system_generated,
				 filter_group_id,
				 filter_group_item_id,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 column_id AS step_filter_sequence,
				 N'' AS step_filter_name,
				 N'' AS step_filter_desc,
				 N'AND' AS logical_operator,
				 0 AS open_brackets,
				 N'?1' AS step_filter_left_member,
				 N'[uq' + CONVERT(NVARCHAR(10),@uq) + N'].' + name AS original_left_member,
				 N'=' AS comparison_operator,
				 N'?2' AS step_filter_right_member,
				 N'[etl].' + name AS original_right_member,
				 0 AS close_brackets,
				 2 AS destination_clause,
				 0 AS display_level,
				 0 AS is_system_generated,
				 NULL AS filter_group_id,
				 NULL AS filter_group_item_id,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM @syscolumns
		   WHERE @uq = uq_column_id
		  UNION
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 MAX(column_id) + 1 AS step_filter_sequence,
				 N'' AS step_filter_name,
				 N'' AS step_filter_desc,
				 N'AND' AS logical_operator,
				 0 AS open_brackets,
				 N'?1' AS step_filter_left_member,
				 N'[REF].Count_Line_Nr' AS original_left_member,
				 N'>' AS comparison_operator,
				 N'1' AS step_filter_right_member,
				 N'1' AS original_right_member,
				 0 AS close_brackets,
				 1 AS destination_clause,
				 0 AS display_level,
				 0 AS is_system_generated,
				 NULL AS filter_group_id,
				 NULL AS filter_group_item_id,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM @syscolumns
		   WHERE @uq = uq_column_id
		   ORDER BY column_id;

		  INSERT INTO t_mex_field_mapping_placeholder (
				 mex_id,
				 job_id,
				 step_id,
				 output_field_id,
				 field_mapping_id,
				 placeholder_nr,
				 step_link_id,
				 runtime_parameter_id,
				 source_field_id,
				 formula_id,
				 aggregation_id,
				 group_by_ind,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 output_field_id,
				 field_mapping_id,
				 1 AS placeholder_nr,
				 CASE WHEN original_expression LIKE N'_etl%' THEN msl.step_link_id END AS step_link_id,
				 mrp.runtime_parameter_id,
				 msf.source_field_id,
				 NULL AS formula_id,
				 NULL AS aggregation_id,
				 0 AS group_by_ind,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_field_mapping mfm
			JOIN t_mex_step_link msl
			  ON mfm.mex_id = msl.mex_id
			 AND mfm.job_id = msl.job_id
			 AND mfm.step_id = msl.step_id
			 AND msl.link_type = N'SRC'
			LEFT JOIN t_mex_runtime_parameter mrp
			  ON mfm.mex_id = mrp.mex_id
			 AND mfm.original_expression = mrp.runtime_parameter_name
			LEFT JOIN t_mex_source_field msf
			  ON mfm.mex_id = msf.mex_id
			 AND mfm.original_expression = N'[etl].' + msf.source_field_name
			 AND msf.is_mapping_candidate = 1
		   WHERE mfm.mex_id = @mex_id
			 AND mfm.job_id = @job_id
			 AND mfm.step_id = @step_id
			 AND output_expression = N'?1';

		  INSERT INTO t_mex_step_filter_placeholder (
				 mex_id,
				 job_id,
				 step_id,
				 step_filter_id,
				 placeholder_nr,
				 step_link_id,
				 runtime_parameter_id,
				 source_field_id,
				 formula_id,
				 aggregation_id,
				 group_by_ind,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 msf.step_filter_id,
				 CASE WHEN msl.link_type = N'SRC' THEN 2 ELSE 1 END AS placeholder_nr,
				 msl.step_link_id,
				 NULL AS runtime_parameter_id,
				 msfi.source_field_id,
				 NULL AS formula_id,
				 NULL AS aggregation_id,
				 0 AS group_by_ind,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_step_filter msf
			JOIN t_mex_step_link msl
			  ON msf.mex_id = msl.mex_id
			 AND msf.job_id = msl.job_id
			 AND msf.step_id = msl.step_id
			 AND msl.link_type IN (N'SRC',N'REF')
			JOIN t_mex_location ml
			  ON msf.mex_id = ml.mex_id
			 AND msl.link_to_id = ml.location_id
			JOIN t_mex_source_field msfi
			  ON msf.mex_id = msfi.mex_id
			 AND ml.location_id = msfi.source_location_id
			 AND msf.original_left_member = N'[uq' + CONVERT(NVARCHAR(10),@uq) + N'].' +msfi.source_field_name
		   WHERE msf.mex_id = @mex_id
			 AND msf.job_id = @job_id
			 AND msf.step_id = @step_id
		  UNION
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 msf.step_filter_id,
				 CASE WHEN msl.link_type = N'SRC' THEN 2 ELSE 1 END AS placeholder_nr,
				 msl.step_link_id,
				 NULL AS runtime_parameter_id,
				 NULL AS source_field_id,
				 mf.formula_id,
				 NULL AS aggregation_id,
				 0 AS group_by_ind,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_step_filter msf
			JOIN t_mex_step_link msl
			  ON msf.mex_id = msl.mex_id
			 AND msf.job_id = msl.job_id
			 AND msf.step_id = msl.step_id
			 AND msl.link_type = N'REF'
			JOIN t_mex_location ml
			  ON msf.mex_id = ml.mex_id
			 AND msl.link_to_id = ml.location_id
			LEFT JOIN t_mex_formula mf
			  ON msf.mex_id = mf.mex_id
			 AND ml.location_id = mf.location_id
		   WHERE msf.mex_id = @mex_id
			 AND msf.job_id = @job_id
			 AND msf.step_id = @step_id
			 AND msf.original_left_member = N'[REF].Count_Line_Nr'	 
		  ORDER BY msf.step_filter_id, msl.step_link_id;

		  INSERT INTO t_mex_step_link_table (
				 mex_id,
				 job_id,
				 step_id,
				 step_link_id,
				 location_id,
				 object_id,
				 object_alias,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 msl.step_link_id,
				 ml.location_id,
				 mlt.object_id,
				 mlt.object_alias,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_step_link msl
			JOIN t_mex_location ml
			  ON msl.mex_id = ml.mex_id
			 AND msl.link_to_id = ml.location_id
			JOIN t_mex_location_table mlt
			  ON msl.mex_id = mlt.mex_id
			 AND ml.location_id = mlt.location_id
		   WHERE msl.mex_id = @mex_id
			 AND msl.job_id = @job_id
			 AND msl.step_id = @step_id
			 AND msl.link_type IN (N'SRC',N'REF');
		END;	

		FETCH NEXT FROM uq INTO @uq;
	END

	CLOSE uq;
	DEALLOCATE uq;
END;
-- Step 6.4: Foreign Key constraints

IF EXISTS (SELECT 1
  FROM @syscolumns
 WHERE fk_reference_column IS NOT NULL)
BEGIN
	SELECT name,
         fk_reference_column,
		 fk_reference_table,
		 CONVERT(NVARCHAR,is_nullable) AS is_nullable,
		 fk_constraint_id
	INTO #temp
    FROM @syscolumns
   WHERE 1=0;

	WITH CTE AS 
	(
	  SELECT name,
			 fk_reference_column,
			 fk_reference_table,
			 is_nullable,
			 fk_constraint_id,
			 COUNT(fk_constraint_id) OVER (PARTITION BY fk_constraint_id)	AS cntr
		FROM @syscolumns
	   WHERE fk_reference_column IS NOT NULL
	)
  	INSERT INTO #temp
	SELECT DISTINCT STUFF((
					SELECT ',' + c1.name
					  FROM CTE c1
					 WHERE c1.fk_constraint_id = c2.fk_constraint_id
					ORDER BY c1.fk_constraint_id
					FOR XML PATH ('')
					),1,1,'') AS name,
		   STUFF((
					SELECT ',' + c1.fk_reference_column
					  FROM CTE c1
					 WHERE c1.fk_constraint_id = c2.fk_constraint_id
					ORDER BY c1.fk_constraint_id
					FOR XML PATH ('')
					),1,1,'') AS fk_reference_column,
		   fk_reference_table,
			STUFF((
					SELECT ',' + CONVERT(NVARCHAR,c1.is_nullable)
					  FROM CTE c1
					 WHERE c1.fk_constraint_id = c2.fk_constraint_id
					ORDER BY c1.fk_constraint_id
					FOR XML PATH ('')
					),1,1,'') AS is_nullable,
		   fk_constraint_id
	  FROM CTE c2
	 WHERE cntr > 1
	UNION ALL
	 SELECT name,
			 fk_reference_column,
			 fk_reference_table,
			 CONVERT(NVARCHAR,is_nullable),
			 fk_constraint_id
		FROM CTE
	   WHERE cntr <= 1
	
  DECLARE fk CURSOR FOR
  SELECT name,
         fk_reference_column,
		 fk_reference_table,
		 is_nullable,
		 fk_constraint_id
    FROM #temp;

	

   OPEN fk;
   FETCH NEXT FROM fk INTO @fk_column, @fk_reference_column, @fk_reference_table, @is_nullable, @fk_constraint_id;
   WHILE @@FETCH_STATUS = 0
   BEGIN
		SELECT @step_id = NULL;

		SELECT @step_id = step_id
		  FROM t_mex_step
		 WHERE mex_id = @mex_id
		   AND job_id = @job_id
		   AND step_name = N'FK on ' + @fk_column;

		IF @step_id IS NULL
		BEGIN
		  INSERT INTO t_mex_step (
				 mex_id,
				 job_id,
				 step_name,
				 step_desc,
				 step_sequence,
				 enabled,
				 warning_ind,
				 top_count,
				 is_for_insert,
				 is_for_update,
				 is_for_exec,
				 on_success_action,
				 on_success_step_id,
				 on_fail_action,
				 on_fail_step_id,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 N'FK on ' + @fk_column AS step_name,
				 CASE WHEN @fk_constraint_id IS NOT NULL THEN N'Check on foreign key violation for the column ' + @fk_column + N' in the ETL table'
				      ELSE N'Functional check on column ' + @fk_column + N' in the ETL table' END AS step_desc,
				 MAX(step_sequence) + 1 AS step_sequence,
				 1 AS [enabled],
				 0 AS warning_ind,
				 NULL AS top_count,
				 1 AS is_for_insert,
				 0 AS is_for_update,
				 0 AS is_for_exec,
				 1 AS on_success_action,
				 -1 AS on_success_step_id,
				 3 AS on_fail_action,
				 -1 AS on_fail_step_id,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_step
		   WHERE mex_id = @mex_id
		     AND job_id = @job_id;
  
		  SELECT @step_id = step_id
			FROM t_mex_step
		   WHERE mex_id = @mex_id
			 AND job_id = @job_id
			 AND step_name = N'FK on ' + @fk_column;

		  INSERT INTO t_mex_field_mapping (
				 mex_id,
				 job_id,
				 step_id,
				 output_field_id,
				 priority,
				 output_expression,
				 original_expression,
				 field_mapping_name,
				 field_mapping_desc,
				 is_conditional,
				 is_identity,
				 seed,
				 increment,
				 mapping_group_id,
				 mapping_group_item_id,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 output_field_id,
				 1 AS priority,
				 CASE WHEN output_field_name = N'record_id'
						THEN N''''''
					  WHEN output_field_name IN (N'job_log_id',N'step_log_id',N'adjustment_id',N'line_nr')
						THEN N'?1'
					  WHEN output_field_name = N'validated_column_list'
						THEN N'''' + @fk_column + N''''
					  WHEN output_field_name = N'validation_severity_level' AND @fk_constraint_id IS NOT NULL
						THEN N'10'
					  WHEN output_field_name = N'validation_severity_level' AND @fk_constraint_id IS NULL
						THEN N'20'
					  WHEN output_field_name = N'validation_desc' AND @fk_constraint_id IS NOT NULL
						THEN N'''Foreign key violation on ' + @fk_column + N'. Record is not found in ' + @fk_reference_table + N'.' + @fk_reference_column + N''''
					  WHEN output_field_name = N'validation_desc' AND @fk_constraint_id IS NULL
						THEN N'''Functional check violation on ' + @fk_column + N'. Record is not found in ' + @fk_reference_table + N'.' + @fk_reference_column + N''''
					  WHEN output_field_name = N'last_modified'
						THEN N'CURRENT_TIMESTAMP'
					  WHEN output_field_name = N'modified_by'
						THEN N'CURRENT_USER'
				 END AS output_expression,
				 CASE WHEN output_field_name = N'record_id'
						THEN N''''''
					  WHEN output_field_name = N'job_log_id'
						THEN N'@job_log_id'
					  WHEN output_field_name = N'step_log_id'
						THEN N'@step_log_id'
					  WHEN output_field_name = N'adjustment_id'
						THEN N'[etl].adjustment_id'
					  WHEN output_field_name = N'line_nr'
						THEN N'[etl].line_nr'
					  WHEN output_field_name = N'validated_column_list'
						THEN N'''' + @fk_column + ''''
					  WHEN output_field_name = N'validation_severity_level' AND @fk_constraint_id IS NOT NULL
						THEN N'10'
					  WHEN output_field_name = N'validation_severity_level' AND @fk_constraint_id IS NULL
						THEN N'20'
					  WHEN output_field_name = N'validation_desc' AND @fk_constraint_id IS NOT NULL
						THEN N'''Foreign key violation on ' + @fk_column + N'. Record is not found in ' + @fk_reference_table + N'.' + @fk_reference_column + N''''
					  WHEN output_field_name = N'validation_desc' AND @fk_constraint_id IS NULL
						THEN N'''Functional check violation on ' + @fk_column + N'. Record is not found in ' + @fk_reference_table + N'.' + @fk_reference_column + N''''
					  WHEN output_field_name = N'last_modified'
						THEN N'CURRENT_TIMESTAMP'
					  WHEN output_field_name = N'modified_by'
						THEN N'CURRENT_USER'
				 END AS original_expression,
				 N'' AS field_mapping_name,
				 N'' AS field_mapping_desc,
				 0 AS is_conditional,
				 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS is_identity,
				 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS seed,
				 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS increment,
				 NULL AS mapping_group_id,
				 NULL AS mapping_group_item_id,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_output_field
		   WHERE mex_id = @mex_id;

		  INSERT INTO t_mex_step_link (
				 mex_id,
				 job_id,
				 step_id,
				 step_link_name,
				 step_link_desc,
				 step_link_sequence,
				 link_to_id,
				 link_type,
				 link_subtype,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 N'' AS step_link_name,
				 N'' AS step_link_desc,
				 1 AS step_link_sequence,
				 location_id AS link_to_id,
				 N'SRC' AS link_type,
				 NULL AS link_subtype,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_location
		   WHERE mex_id = @mex_id
			 AND location_name = @etl_table
		  UNION
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 N'' AS step_link_name,
				 N'' AS step_link_desc,
				 2 AS step_link_sequence,
				 location_id AS link_to_id,
				 N'OUT' AS link_type,
				 NULL AS link_subtype,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_location
		   WHERE mex_id = @mex_id
			 AND location_type = N'OUT'
		  UNION
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 N'' AS step_link_name,
				 N'' AS step_link_desc,
				 3 AS step_link_sequence,
				 location_id AS link_to_id,
				 N'REF' AS link_type,
				 N'LEFTOUTER' AS link_subtype,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_location
		   WHERE mex_id = @mex_id
			 AND location_name = @fk_reference_table
		   ORDER BY step_link_sequence;
--
			IF CHARINDEX(',',@fk_column) = 0
			BEGIN
			  INSERT INTO t_mex_step_filter (
					 mex_id,
					 job_id,
					 step_id,
					 step_filter_sequence,
					 step_filter_name,
					 step_filter_desc,
					 logical_operator,
					 open_brackets,
					 step_filter_left_member,
					 original_left_member,
					 comparison_operator,
					 step_filter_right_member,
					 original_right_member,
					 close_brackets,
					 destination_clause,
					 display_level,
					 is_system_generated,
					 filter_group_id,
					 filter_group_item_id,
					 last_modified,
					 modified_by)
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 1 AS step_filter_sequence,
					 N'' AS step_filter_name,
					 N'' AS step_filter_desc,
					 N'AND' AS logical_operator,
					 0 AS open_brackets,
					 CASE WHEN @is_nullable = '0'
							THEN N'?1' 
						  ELSE N'ISNULL(?1,?2)'
					 END AS step_filter_left_member,--
					 CASE WHEN @is_nullable = '0'
							THEN N'[etl].' + @fk_column 
						  ELSE N'ISNULL([etl].' + @fk_column + N',' + mlt.object_alias + N'.' + @fk_reference_column + N')'
					 END AS original_left_member,--
					 N'=' AS comparison_operator,
					 N'?2' AS step_filter_right_member,
					 mlt.object_alias + N'.' + @fk_reference_column AS original_right_member,
					 0 AS close_brackets,
					 2 AS destination_clause,
					 0 AS display_level,
					 0 AS is_system_generated,
					 NULL AS filter_group_id,
					 NULL AS filter_group_item_id,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_step_link msl
				JOIN t_mex_location ml
				  ON msl.mex_id = ml.mex_id
				 AND msl.link_to_id = ml.location_id
				JOIN t_mex_location_table mlt
				  ON msl.mex_id = mlt.mex_id
				 AND ml.location_id = mlt.location_id
			   WHERE msl.mex_id = @mex_id
				 AND msl.job_id = @job_id
				 AND msl.step_id = @step_id
				 AND msl.link_type = N'REF'
			  UNION
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 2 AS step_filter_sequence,
					 N'Not Found in Ref' AS step_filter_name,
					 N'' AS step_filter_desc,
					 N'AND' AS logical_operator,
					 0 AS open_brackets,
					 N'?1' AS step_filter_left_member,
					 mlt.object_alias + N'.' + @fk_reference_column AS original_left_member,
					 N'IS NULL' AS comparison_operator,
					 N'' AS step_filter_right_member,
					 N'' AS original_right_member,
					 0 AS close_brackets,
					 3 AS destination_clause,
					 0 AS display_level,
					 1 AS is_system_generated,
					 NULL AS filter_group_id,
					 NULL AS filter_group_item_id,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_step_link msl
				JOIN t_mex_location ml
				  ON msl.mex_id = ml.mex_id
				 AND msl.link_to_id = ml.location_id
				JOIN t_mex_location_table mlt
				  ON msl.mex_id = mlt.mex_id
				 AND ml.location_id = mlt.location_id
			   WHERE msl.mex_id = @mex_id
				 AND msl.job_id = @job_id
				 AND msl.step_id = @step_id
				 AND msl.link_type = N'REF';

			  INSERT INTO t_mex_field_mapping_placeholder (
					 mex_id,
					 job_id,
					 step_id,
					 output_field_id,
					 field_mapping_id,
					 placeholder_nr,
					 step_link_id,
					 runtime_parameter_id,
					 source_field_id,
					 formula_id,
					 aggregation_id,
					 group_by_ind,
					 last_modified,
					 modified_by)
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 mfm.output_field_id,
					 mfm.field_mapping_id,
					 1 AS placeholder_nr,
					 CASE WHEN original_expression LIKE N'_etl%' THEN msl.step_link_id END AS step_link_id,
					 mrp.runtime_parameter_id,
					 msf.source_field_id,
					 NULL AS formula_id,
					 NULL AS aggregation_id,
					 0 AS group_by_ind,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_field_mapping mfm
				JOIN t_mex_step_link msl
				  ON mfm.mex_id = msl.mex_id
				 AND mfm.job_id = msl.job_id
				 AND mfm.step_id = msl.step_id
				 AND msl.link_type = N'SRC'
				LEFT JOIN t_mex_runtime_parameter mrp
				  ON mfm.mex_id = mrp.mex_id
				 AND mfm.original_expression = mrp.runtime_parameter_name
				LEFT JOIN t_mex_source_field msf
				  ON mfm.mex_id = msf.mex_id
				 AND mfm.original_expression = N'[etl].' + msf.source_field_name-- IN (N'adjustment_id',N'line_nr')
				 AND msf.is_mapping_candidate = 1
			   WHERE mfm.mex_id = @mex_id
				 AND mfm.job_id = @job_id
				 AND mfm.step_id = @step_id
				 AND output_expression = N'?1';

			  INSERT INTO t_mex_step_filter_placeholder (
					 mex_id,
					 job_id,
					 step_id,
					 step_filter_id,
					 placeholder_nr,
					 step_link_id,
					 runtime_parameter_id,
					 source_field_id,
					 formula_id,
					 aggregation_id,
					 group_by_ind,
					 last_modified,
					 modified_by)
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 msf.step_filter_id,
					 1 AS placeholder_nr,
					 msl.step_link_id,
					 NULL AS runtime_parameter_id,
					 msfi.source_field_id,
					 NULL AS formula_id,
					 NULL AS aggregation_id,
					 0 AS group_by_ind,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_step_filter msf
				JOIN t_mex_step_link msl
				  ON msf.mex_id = msl.mex_id
				 AND msf.job_id = msl.job_id
				 AND msf.step_id = msl.step_id
				 AND msl.link_type = N'SRC'
				JOIN t_mex_location ml
				  ON msf.mex_id = ml.mex_id
				 AND msl.link_to_id = ml.location_id
				JOIN t_mex_source_field msfi
				  ON msf.mex_id = msfi.mex_id
				 AND ml.location_id = msfi.source_location_id
				 AND ((msf.original_left_member = N'[etl].' + msfi.source_field_name
				 AND msf.step_filter_left_member = N'?1')
				  OR (LEFT (msf.original_left_member,CHARINDEX(N',',msf.original_left_member,0)) = N'ISNULL([etl].' + msfi.source_field_name + N','
				 AND msf.step_filter_left_member = N'ISNULL(?1,?2)'))	 

				 --AND msf.original_left_member = N'[etl].' + msfi.source_field_name
			   WHERE msf.mex_id = @mex_id
				 AND msf.job_id = @job_id
				 AND msf.step_id = @step_id
			  UNION
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 msf.step_filter_id,
					 2 AS placeholder_nr,
					 msl.step_link_id,
					 NULL AS runtime_parameter_id,
					 msfi.source_field_id,
					 NULL AS formula_id,
					 NULL AS aggregation_id,
					 0 AS group_by_ind,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_step_filter msf
				JOIN t_mex_step_link msl
				  ON msf.mex_id = msl.mex_id
				 AND msf.job_id = msl.job_id
				 AND msf.step_id = msl.step_id
				 AND msl.link_type = N'REF'
				JOIN t_mex_location ml
				  ON msf.mex_id = ml.mex_id
				 AND msl.link_to_id = ml.location_id
				JOIN t_mex_location_table mlt
				  ON msf.mex_id = mlt.mex_id
				 AND ml.location_id = mlt.location_id
				JOIN t_mex_source_field msfi
				  ON msf.mex_id = msfi.mex_id
				 AND ml.location_id = msfi.source_location_id
				 AND msf.original_right_member =mlt.object_alias + N'.' + msfi.source_field_name
			   WHERE msf.mex_id = @mex_id
				 AND msf.job_id = @job_id
				 AND msf.step_id = @step_id
			  UNION
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 msf.step_filter_id,
					 1 AS placeholder_nr,
					 msl.step_link_id,
					 NULL AS runtime_parameter_id,
					 msfi.source_field_id,
					 NULL AS formula_id,
					 NULL AS aggregation_id,
					 0 AS group_by_ind,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_step_filter msf
				JOIN t_mex_step_link msl
				  ON msf.mex_id = msl.mex_id
				 AND msf.job_id = msl.job_id
				 AND msf.step_id = msl.step_id
				 AND msl.link_type = N'REF'
				JOIN t_mex_location ml
				  ON msf.mex_id = ml.mex_id
				 AND msl.link_to_id = ml.location_id
				JOIN t_mex_location_table mlt
				  ON msf.mex_id = mlt.mex_id
				 AND ml.location_id = mlt.location_id
				JOIN t_mex_source_field msfi
				  ON msf.mex_id = msfi.mex_id
				 AND ml.location_id = msfi.source_location_id
				 AND msf.original_left_member =mlt.object_alias + N'.' + msfi.source_field_name
			   WHERE msf.mex_id = @mex_id
				 AND msf.job_id = @job_id
				 AND msf.step_id = @step_id
				 AND msf.step_filter_name = N'Not Found in Ref'
			  ORDER BY msf.step_filter_id, msl.step_link_id;
			END;
			ELSE
			BEGIN
				WITH  is_nll AS
				(
					SELECT row_id,
					       value
					  FROM dbo.fn_split_string (@is_nullable,',')
				),
				fk_cols AS
				(
					SELECT row_id,
					       value
					  FROM dbo.fn_split_string (@fk_column,',')
				),
				fk_ref AS
				(
					SELECT row_id,
					       value
					  FROM dbo.fn_split_string (@fk_reference_column,',')
				)

				SELECT fk_cols.row_id,
				       fk_cols.value AS fk_column,
				       fk_ref.value AS fk_reference_column,
					   @fk_reference_table AS fk_reference_table, 
					   is_nll.value AS is_nullable,
					   @fk_constraint_id AS fk_constraint_id
				  INTO #temp2
                  FROM is_nll
				  JOIN fk_cols
				    ON is_nll.row_id = fk_cols.row_id
				  JOIN fk_ref
				    ON fk_cols.row_id = fk_ref.row_id;				

				INSERT INTO t_mex_step_filter (
					 mex_id,
					 job_id,
					 step_id,
					 step_filter_sequence,
					 step_filter_name,
					 step_filter_desc,
					 logical_operator,
					 open_brackets,
					 step_filter_left_member,
					 original_left_member,
					 comparison_operator,
					 step_filter_right_member,
					 original_right_member,
					 close_brackets,
					 destination_clause,
					 display_level,
					 is_system_generated,
					 filter_group_id,
					 filter_group_item_id,
					 last_modified,
					 modified_by)
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 t.row_id AS step_filter_sequence,
					 N'' AS step_filter_name,
					 N'' AS step_filter_desc,
					 N'AND' AS logical_operator,
					 0 AS open_brackets,
					 CASE WHEN t.is_nullable = '0'
							THEN N'?1' 
						  ELSE N'ISNULL(?1,?2)'
					 END AS step_filter_left_member,--
					 CASE WHEN t.is_nullable = '0'
							THEN N'[etl].' + t.fk_column 
						  ELSE N'ISNULL([etl].' + t.fk_column + N',' + mlt.object_alias + N'.' + t.fk_reference_column + N')'
					 END AS original_left_member,--
					 N'=' AS comparison_operator,
					 N'?2' AS step_filter_right_member,
					 mlt.object_alias + N'.' + t.fk_reference_column AS original_right_member,
					 0 AS close_brackets,
					 2 AS destination_clause,
					 0 AS display_level,
					 0 AS is_system_generated,
					 NULL AS filter_group_id,
					 NULL AS filter_group_item_id,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_step_link msl
				JOIN t_mex_location ml
				  ON msl.mex_id = ml.mex_id
				 AND msl.link_to_id = ml.location_id
				JOIN t_mex_location_table mlt
				  ON msl.mex_id = mlt.mex_id
				 AND ml.location_id = mlt.location_id
				JOIN #temp2 t
				  ON 1=1 -- can be better, but I'm a busy man
			   WHERE msl.mex_id = @mex_id
				 AND msl.job_id = @job_id
				 AND msl.step_id = @step_id
				 AND msl.link_type = N'REF'
			  UNION
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 (
						SELECT MAX(row_id) + 1 FROM #temp2
					 ) AS step_filter_sequence,
					 N'Not Found in Ref' AS step_filter_name,
					 N'' AS step_filter_desc,
					 N'AND' AS logical_operator,
					 0 AS open_brackets,
					 N'?1' AS step_filter_left_member,
					 mlt.object_alias + N'.' + t.fk_reference_column AS original_left_member,
					 N'IS NULL' AS comparison_operator,
					 N'' AS step_filter_right_member,
					 N'' AS original_right_member,
					 0 AS close_brackets,
					 3 AS destination_clause,
					 0 AS display_level,
					 1 AS is_system_generated,
					 NULL AS filter_group_id,
					 NULL AS filter_group_item_id,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_step_link msl
				JOIN t_mex_location ml
				  ON msl.mex_id = ml.mex_id
				 AND msl.link_to_id = ml.location_id
				JOIN t_mex_location_table mlt
				  ON msl.mex_id = mlt.mex_id
				 AND ml.location_id = mlt.location_id
				JOIN #temp2 t
				  ON t.row_id = 1
			   WHERE msl.mex_id = @mex_id
				 AND msl.job_id = @job_id
				 AND msl.step_id = @step_id
				 AND msl.link_type = N'REF';
				DROP TABLE #temp2;
			  INSERT INTO t_mex_field_mapping_placeholder (
					 mex_id,
					 job_id,
					 step_id,
					 output_field_id,
					 field_mapping_id,
					 placeholder_nr,
					 step_link_id,
					 runtime_parameter_id,
					 source_field_id,
					 formula_id,
					 aggregation_id,
					 group_by_ind,
					 last_modified,
					 modified_by)
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 mfm.output_field_id,
					 mfm.field_mapping_id,
					 1 AS placeholder_nr,
					 CASE WHEN original_expression LIKE N'_etl%' THEN msl.step_link_id END AS step_link_id,
					 mrp.runtime_parameter_id,
					 msf.source_field_id,
					 NULL AS formula_id,
					 NULL AS aggregation_id,
					 0 AS group_by_ind,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_field_mapping mfm
				JOIN t_mex_step_link msl
				  ON mfm.mex_id = msl.mex_id
				 AND mfm.job_id = msl.job_id
				 AND mfm.step_id = msl.step_id
				 AND msl.link_type = N'SRC'
				LEFT JOIN t_mex_runtime_parameter mrp
				  ON mfm.mex_id = mrp.mex_id
				 AND mfm.original_expression = mrp.runtime_parameter_name
				LEFT JOIN t_mex_source_field msf
				  ON mfm.mex_id = msf.mex_id
				 AND mfm.original_expression = N'[etl].' + msf.source_field_name-- IN (N'adjustment_id',N'line_nr')
				 AND msf.is_mapping_candidate = 1
			   WHERE mfm.mex_id = @mex_id
				 AND mfm.job_id = @job_id
				 AND mfm.step_id = @step_id
				 AND output_expression = N'?1';

			  INSERT INTO t_mex_step_filter_placeholder (
					 mex_id,
					 job_id,
					 step_id,
					 step_filter_id,
					 placeholder_nr,
					 step_link_id,
					 runtime_parameter_id,
					 source_field_id,
					 formula_id,
					 aggregation_id,
					 group_by_ind,
					 last_modified,
					 modified_by)
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 msf.step_filter_id,
					 1 AS placeholder_nr,
					 msl.step_link_id,
					 NULL AS runtime_parameter_id,
					 msfi.source_field_id,
					 NULL AS formula_id,
					 NULL AS aggregation_id,
					 0 AS group_by_ind,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_step_filter msf
				JOIN t_mex_step_link msl
				  ON msf.mex_id = msl.mex_id
				 AND msf.job_id = msl.job_id
				 AND msf.step_id = msl.step_id
				 AND msl.link_type = N'SRC'
				JOIN t_mex_location ml
				  ON msf.mex_id = ml.mex_id
				 AND msl.link_to_id = ml.location_id
				JOIN t_mex_source_field msfi
				  ON msf.mex_id = msfi.mex_id
				 AND ml.location_id = msfi.source_location_id
				 AND ((msf.original_left_member = N'[etl].' + msfi.source_field_name
				 AND msf.step_filter_left_member = N'?1')
				  OR (LEFT (msf.original_left_member,CHARINDEX(N',',msf.original_left_member,0)) = N'ISNULL([etl].' + msfi.source_field_name + N','
				 AND msf.step_filter_left_member = N'ISNULL(?1,?2)'))	 

				 --AND msf.original_left_member = N'[etl].' + msfi.source_field_name
			   WHERE msf.mex_id = @mex_id
				 AND msf.job_id = @job_id
				 AND msf.step_id = @step_id
			  UNION
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 msf.step_filter_id,
					 2 AS placeholder_nr,
					 msl.step_link_id,
					 NULL AS runtime_parameter_id,
					 msfi.source_field_id,
					 NULL AS formula_id,
					 NULL AS aggregation_id,
					 0 AS group_by_ind,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_step_filter msf
				JOIN t_mex_step_link msl
				  ON msf.mex_id = msl.mex_id
				 AND msf.job_id = msl.job_id
				 AND msf.step_id = msl.step_id
				 AND msl.link_type = N'REF'
				JOIN t_mex_location ml
				  ON msf.mex_id = ml.mex_id
				 AND msl.link_to_id = ml.location_id
				JOIN t_mex_location_table mlt
				  ON msf.mex_id = mlt.mex_id
				 AND ml.location_id = mlt.location_id
				JOIN t_mex_source_field msfi
				  ON msf.mex_id = msfi.mex_id
				 AND ml.location_id = msfi.source_location_id
				 AND msf.original_right_member =mlt.object_alias + N'.' + msfi.source_field_name
			   WHERE msf.mex_id = @mex_id
				 AND msf.job_id = @job_id
				 AND msf.step_id = @step_id
			  UNION
			  SELECT @mex_id AS mex_id,
					 @job_id AS job_id,
					 @step_id AS step_id,
					 msf.step_filter_id,
					 1 AS placeholder_nr,
					 msl.step_link_id,
					 NULL AS runtime_parameter_id,
					 msfi.source_field_id,
					 NULL AS formula_id,
					 NULL AS aggregation_id,
					 0 AS group_by_ind,
					 CURRENT_TIMESTAMP AS last_modified,
					 N'Automatic generation' AS modified_by
				FROM t_mex_step_filter msf
				JOIN t_mex_step_link msl
				  ON msf.mex_id = msl.mex_id
				 AND msf.job_id = msl.job_id
				 AND msf.step_id = msl.step_id
				 AND msl.link_type = N'REF'
				JOIN t_mex_location ml
				  ON msf.mex_id = ml.mex_id
				 AND msl.link_to_id = ml.location_id
				JOIN t_mex_location_table mlt
				  ON msf.mex_id = mlt.mex_id
				 AND ml.location_id = mlt.location_id
				JOIN t_mex_source_field msfi
				  ON msf.mex_id = msfi.mex_id
				 AND ml.location_id = msfi.source_location_id
				 AND msf.original_left_member =mlt.object_alias + N'.' + msfi.source_field_name
			   WHERE msf.mex_id = @mex_id
				 AND msf.job_id = @job_id
				 AND msf.step_id = @step_id
				 AND msf.step_filter_name = N'Not Found in Ref'
			  ORDER BY msf.step_filter_id, msl.step_link_id;
			END;
		  INSERT INTO t_mex_step_link_table (
				 mex_id,
				 job_id,
				 step_id,
				 step_link_id,
				 location_id,
				 object_id,
				 object_alias,
				 last_modified,
				 modified_by)
		  SELECT @mex_id AS mex_id,
				 @job_id AS job_id,
				 @step_id AS step_id,
				 msl.step_link_id,
				 ml.location_id,
				 mlt.object_id,
				 mlt.object_alias,
				 CURRENT_TIMESTAMP AS last_modified,
				 N'Automatic generation' AS modified_by
			FROM t_mex_step_link msl
			JOIN t_mex_location ml
			  ON msl.mex_id = ml.mex_id
			 AND msl.link_to_id = ml.location_id
			JOIN t_mex_location_table mlt
			  ON msl.mex_id = mlt.mex_id
			 AND ml.location_id = mlt.location_id
		   WHERE msl.mex_id = @mex_id
			 AND msl.job_id = @job_id
			 AND msl.step_id = @step_id
			 AND msl.link_type IN (N'SRC',N'REF');
		END;
     FETCH NEXT FROM fk INTO @fk_column, @fk_reference_column, @fk_reference_table, @is_nullable, @fk_constraint_id;
   END;

   CLOSE fk;
   DEALLOCATE fk;
   DROP TABLE #temp;
END;

-- Step 6.5: Check constraints

IF EXISTS (SELECT 1
             FROM sys.check_constraints
            WHERE parent_object_id = OBJECT_ID (@fda_table)
              AND parent_column_id = 0) OR
   EXISTS (SELECT 1
             FROM @syscolumns
			WHERE cc_condition IS NOT NULL)
BEGIN
	SELECT @step_id = NULL;

	SELECT @step_id = step_id
	  FROM t_mex_step
	 WHERE mex_id = @mex_id
	   AND job_id = @job_id
	   AND step_name = N'Check constraint';

	IF @step_id IS NULL
	BEGIN
	  INSERT INTO t_mex_step (
			 mex_id,
			 job_id,
			 step_name,
			 step_desc,
			 step_sequence,
			 enabled,
			 warning_ind,
			 top_count,
			 is_for_insert,
			 is_for_update,
			 is_for_exec,
			 on_success_action,
			 on_success_step_id,
			 on_fail_action,
			 on_fail_step_id,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 N'Check constraint' AS step_name,
			 N'There is a check constraint on the FDA table' AS step_desc,
			 MAX(step_sequence) + 1 AS step_sequence,
			 1 AS [enabled],
			 0 AS warning_ind,
			 NULL AS top_count,
			 1 AS is_for_insert,
			 0 AS is_for_update,
			 0 AS is_for_exec,
			 1 AS on_success_action,
			 -1 AS on_success_step_id,
			 3 AS on_fail_action,
			 -1 AS on_fail_step_id,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
	    FROM t_mex_step
	   WHERE mex_id = @mex_id
	     AND job_id = @job_id;
  
	  SELECT @step_id = step_id
		FROM t_mex_step
	   WHERE mex_id = @mex_id
		 AND job_id = @job_id
		 AND step_name = N'Check constraint';

	  INSERT INTO t_mex_field_mapping (
			 mex_id,
			 job_id,
			 step_id,
			 output_field_id,
			 priority,
			 output_expression,
			 original_expression,
			 field_mapping_name,
			 field_mapping_desc,
			 is_conditional,
			 is_identity,
			 seed,
			 increment,
			 mapping_group_id,
			 mapping_group_item_id,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 output_field_id,
			 1 AS priority,
			 CASE WHEN output_field_name = N'record_id'
					THEN N''''''
				  WHEN output_field_name IN (N'job_log_id',N'step_log_id',N'adjustment_id',N'line_nr')
					THEN N'?1'
				  WHEN output_field_name = N'validated_column_list'
					THEN N'''Check constraint'''
				  WHEN output_field_name = N'validation_severity_level'
					THEN N'10'
				  WHEN output_field_name = N'validation_desc'
					THEN N'''Check constraint violation found in the ETL table'''
				  WHEN output_field_name = N'last_modified'
					THEN N'CURRENT_TIMESTAMP'
				  WHEN output_field_name = N'modified_by'
					THEN N'CURRENT_USER'
			 END AS output_expression,
			 CASE WHEN output_field_name = N'record_id'
					THEN N''''''
				  WHEN output_field_name = N'job_log_id'
					THEN N'@job_log_id'
				  WHEN output_field_name = N'step_log_id'
					THEN N'@step_log_id'
				  WHEN output_field_name = N'adjustment_id'
					THEN N'[etl].adjustment_id'
				  WHEN output_field_name = N'line_nr'
					THEN N'[etl].line_nr'
				  WHEN output_field_name = N'validated_column_list'
					THEN N'''Check constraint'''
				  WHEN output_field_name = N'validation_severity_level'
					THEN N'10'
				  WHEN output_field_name = N'validation_desc'
					THEN N'''Check constraint violation found in the ETL table'''
				  WHEN output_field_name = N'last_modified'
					THEN N'CURRENT_TIMESTAMP'
				  WHEN output_field_name = N'modified_by'
					THEN N'CURRENT_USER'
			 END AS original_expression,
			 N'' AS field_mapping_name,
			 N'' AS field_mapping_desc,
			 0 AS is_conditional,
			 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS is_identity,
			 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS seed,
			 CASE WHEN output_field_name = N'record_id' THEN 1 ELSE 0 END AS increment,
			 NULL AS mapping_group_id,
			 NULL AS mapping_group_item_id,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_output_field
	   WHERE mex_id = @mex_id;

	  INSERT INTO t_mex_step_link (
			 mex_id,
			 job_id,
			 step_id,
			 step_link_name,
			 step_link_desc,
			 step_link_sequence,
			 link_to_id,
			 link_type,
			 link_subtype,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 N'' AS step_link_name,
			 N'' AS step_link_desc,
			 1 AS step_link_sequence,
			 location_id AS link_to_id,
			 N'SRC' AS link_type,
			 NULL AS link_subtype,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_location
	   WHERE mex_id = @mex_id
		 AND location_name = @etl_table
	  UNION
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 N'' AS step_link_name,
			 N'' AS step_link_desc,
			 2 AS step_link_sequence,
			 location_id AS link_to_id,
			 N'OUT' AS link_type,
			 NULL AS link_subtype,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_location
	   WHERE mex_id = @mex_id
		 AND location_type = N'OUT'
	   ORDER BY step_link_sequence;

      WITH CTE AS (
	  SELECT 
			 N'CASE WHEN ' + definition + N' THEN 1 ELSE 0 END' AS step_filter_left_member,
			 N'CASE WHEN ' + definition + N' THEN 1 ELSE 0 END' AS original_left_member,
			 name AS original_right_member
        FROM sys.check_constraints
       WHERE parent_object_id = OBJECT_ID (@fda_table)
         AND parent_column_id = 0
	  UNION
	  SELECT DISTINCT 
			 N'CASE WHEN ' + cc_condition + N' THEN 1 ELSE 0 END' AS step_filter_left_member,
			 N'CASE WHEN ' + cc_condition + N' THEN 1 ELSE 0 END' AS  original_left_member,
			 name AS original_right_member
		FROM @syscolumns
	   WHERE cc_condition IS NOT NULL)
	  INSERT INTO t_mex_step_filter (
			 mex_id,
			 job_id,
			 step_id,
			 step_filter_sequence,
			 step_filter_name,
			 step_filter_desc,
			 logical_operator,
			 open_brackets,
			 step_filter_left_member,
			 original_left_member,
			 comparison_operator,
			 step_filter_right_member,
			 original_right_member,
			 close_brackets,
			 destination_clause,
			 display_level,
			 is_system_generated,
			 filter_group_id,
			 filter_group_item_id,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
	         @job_id AS job_id,
			 @step_id AS step_id,
			 ROW_NUMBER() OVER (ORDER BY original_right_member) AS step_filter_sequence,
			 N'' AS step_filter_name,
			 N'' AS step_filter_desc,
			 CASE WHEN ROW_NUMBER() OVER (ORDER BY original_right_member) = 1 THEN N'AND' ELSE N'OR' END AS logical_operator,--
			 CASE WHEN ROW_NUMBER() OVER (ORDER BY original_right_member) = 1 THEN N'1' ELSE N'0' END AS open_brackets, --
			 step_filter_left_member,
			 original_left_member,
			 N'<>' AS comparison_operator,
			 N'1' AS step_filter_right_member,
			 original_right_member,
			 CASE WHEN ROW_NUMBER() OVER (ORDER BY original_right_member) = COUNT(1) OVER () THEN N'1' ELSE N'0' END AS close_brackets, --
			 1 AS destination_clause,
			 0 AS display_level,
			 0 AS is_system_generated,
			 NULL AS filter_group_id,
			 NULL AS filter_group_item_id,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
	    FROM CTE;

	  INSERT INTO t_mex_field_mapping_placeholder (
			 mex_id,
			 job_id,
			 step_id,
			 output_field_id,
			 field_mapping_id,
			 placeholder_nr,
			 step_link_id,
			 runtime_parameter_id,
			 source_field_id,
			 formula_id,
			 aggregation_id,
			 group_by_ind,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 mfm.output_field_id,
			 mfm.field_mapping_id,
			 1 AS placeholder_nr,
			 CASE WHEN original_expression LIKE N'_etl%' THEN msl.step_link_id END AS step_link_id,
			 mrp.runtime_parameter_id,
			 msf.source_field_id,
			 NULL AS formula_id,
			 NULL AS aggregation_id,
			 0 AS group_by_ind,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_field_mapping mfm
		JOIN t_mex_step_link msl
		  ON mfm.mex_id = msl.mex_id
		 AND mfm.job_id = msl.job_id
		 AND mfm.step_id = msl.step_id
		 AND msl.link_type = N'SRC'
		LEFT JOIN t_mex_runtime_parameter mrp
		  ON mfm.mex_id = mrp.mex_id
		 AND mfm.original_expression = mrp.runtime_parameter_name
		LEFT JOIN t_mex_source_field msf
		  ON mfm.mex_id = msf.mex_id
		 AND mfm.original_expression = N'[etl].' + msf.source_field_name-- IN (N'adjustment_id',N'line_nr')
		 AND msf.is_mapping_candidate = 1
	   WHERE mfm.mex_id = @mex_id
		 AND mfm.job_id = @job_id
		 AND mfm.step_id = @step_id
		 AND output_expression = N'?1';

	  INSERT INTO t_mex_step_link_table (
			 mex_id,
			 job_id,
			 step_id,
			 step_link_id,
			 location_id,
			 object_id,
			 object_alias,
			 last_modified,
			 modified_by)
	  SELECT @mex_id AS mex_id,
			 @job_id AS job_id,
			 @step_id AS step_id,
			 msl.step_link_id,
			 ml.location_id,
			 mlt.object_id,
			 mlt.object_alias,
			 CURRENT_TIMESTAMP AS last_modified,
			 N'Automatic generation' AS modified_by
		FROM t_mex_step_link msl
		JOIN t_mex_location ml
		  ON msl.mex_id = ml.mex_id
		 AND msl.link_to_id = ml.location_id
		JOIN t_mex_location_table mlt
		  ON msl.mex_id = mlt.mex_id
		 AND ml.location_id = mlt.location_id
	   WHERE msl.mex_id = @mex_id
		 AND msl.job_id = @job_id
		 AND msl.step_id = @step_id
		 AND msl.link_type IN (N'SRC',N'REF');
	END;
END;

-- Step 7: Link the job to the cluster
IF NOT EXISTS (SELECT 1
  FROM t_mex_cluster_job
 WHERE mex_id = @mex_id
   AND cluster_id = @cluster_id
   AND job_id = @job_id)
BEGIN
  INSERT INTO t_mex_cluster_job (
         mex_id,
         cluster_id,
         job_sequence,
         job_id,
         enabled,
         start_step_id,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @cluster_id AS cluster_id,
		 1 AS job_sequence,
		 @job_id AS job_id,
		 1 AS enabled,
		 0 AS start_step_id,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;
END;

--SELECT *
--  FROM @syscolumns

IF @build = 1
BEGIN
  EXEC p_mex_gen_cluster @mex_id = @mex_id;
END;


RETURN(0); 
GO

IF OBJECT_ID (N'p_generate_maestro_validation_ps',N'P') IS NOT NULL
  PRINT N'Procedure p_generate_maestro_validation_ps has been created';
ELSE
  PRINT N'Procedure p_generate_maestro_validation_ps has not been created due to errors';

GO
