USE BENONESUMX;
GO

IF OBJECT_ID (N'p_rpt_etl_validation_details_ps',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_rpt_etl_validation_details_ps...';
  DROP PROCEDURE p_rpt_etl_validation_details_ps;
END
GO

PRINT N'Creating procedure p_rpt_etl_validation_details_ps...';
-- ******************************************************************************
-- * Purpose: This is general ETL validation report                             *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
GO

CREATE PROCEDURE dbo.p_rpt_etl_validation_details_ps (@adjustment_id INT,
                                                     @validation_desc NVARCHAR(500),
                                                     @show_data BIT)
AS   

SET NOCOUNT ON;

-- declare variables
DECLARE @code_block           NVARCHAR(4000),
        @col_name             NVARCHAR (300),
        @colorder             INT,
        @ErrorSeverity        INT,
        @ErrorState           INT,
        @etl_table            SYSNAME,
        @maxcolorder          INT,
        @Msg                  NVARCHAR(MAX),
        @ProcName             SYSNAME = OBJECT_NAME(@@PROCID),
        @Raiserror            INT,
        @rowcount             INT = 0,
        @SQL                  NVARCHAR(MAX),
        @val_table            SYSNAME,
		@NULL_INT			  INT;
  


-- initialise variables
SELECT @etl_table = at.[type_name]
  FROM t_adj_instance ai
  JOIN t_adj_type at
    ON ai.[type_id] = at.[type_id]
 WHERE adjustment_id = @adjustment_id
SELECT @val_table = @etl_table + N'_validation';      

SELECT @maxcolorder = MAX(col.colorder)
  FROM syscolumns col
 WHERE col.id = OBJECT_ID(@etl_table)
   AND col.name NOT IN (N'adjustment_id',N'line_nr',N'line_status',N'retry_count',N'line_date',N'flag');
  
BEGIN TRY

DECLARE cols CURSOR FOR
SELECT col.name, col.colorder
  FROM syscolumns col
 WHERE col.id = OBJECT_ID(@etl_table)
   AND col.name NOT IN (N'adjustment_id',N'line_nr',N'line_status',N'retry_count',N'line_date',N'flag')
 ORDER BY col.colorder;

IF @show_data = 1
  SELECT @SQL = N'SELECT DISTINCT etl.adjustment_id AS adjustment_id,etl.line_nr AS line_nr, ';
ELSE
  SELECT @SQL = N'SELECT TOP 1 N''adjustment_id'' AS adjustment_id, N''line_nr'' AS line_nr, ';
  
OPEN cols;
FETCH NEXT FROM cols into @col_name, @colorder;
WHILE @@FETCH_STATUS = 0
BEGIN
  IF @show_data = 1
  BEGIN
	SELECT @SQL = @SQL + N'ISNULL(CONVERT(NVARCHAR(200),etl.' + @col_name + N'),N''--NULL--'')';
	SELECT @SQL = @SQL + N' AS column' + CONVERT(NVARCHAR(200),@colorder);
    SELECT @SQL = @SQL + CASE WHEN @colorder <> @maxcolorder THEN N', ' ELSE N' ' END;
  END
  ELSE
  BEGIN
	SELECT @SQL = @SQL + N'''' + @col_name + N'''';
	SELECT @SQL = @SQL + N' AS column' + CONVERT(NVARCHAR(200),@colorder);
    SELECT @SQL = @SQL + CASE WHEN @colorder <> @maxcolorder THEN N', ' ELSE N' ' END;
  END
  FETCH NEXT FROM cols into @col_name, @colorder;
END
SELECT @colorder = @colorder + 1;

WHILE @colorder <= 50
BEGIN
  SELECT @SQL = @SQL + N',N'''' AS column' + CONVERT(NVARCHAR(200),@colorder);
  SELECT @colorder = @colorder + 1;
END

IF @show_data = 1
BEGIN
  SELECT @SQL = @SQL + N'
  FROM ' + @etl_table + N' etl
  JOIN ' + @val_table + N' val
    ON etl.adjustment_id = val.adjustment_id
   AND etl.line_nr = val.line_nr
 WHERE etl.adjustment_id = ' + CONVERT(NVARCHAR(200),@adjustment_id) + N'
   AND val.validation_desc = N''' + @validation_desc + N''';';
END
ELSE
BEGIN
  SELECT @SQL = @SQL + N'
    FROM ' + @etl_table + N' etl'

END

--PRINT @SQL
EXEC (@SQL);

CLOSE cols;
DEALLOCATE cols;

END TRY

BEGIN CATCH
  SELECT @Raiserror = 300000 + ERROR_NUMBER(),
         @ErrorSeverity = ERROR_SEVERITY(),
         @ErrorState = ERROR_STATE(),
         @Msg = @ProcName + ' ' + ISNULL(ERROR_MESSAGE(), N'') + N', Error Number = ' + ISNULL(CONVERT(NVARCHAR, ERROR_NUMBER()),N'')
                + N', Error Line = ' + ISNULL(CONVERT(NVARCHAR, ERROR_LINE()),N'N/A');
  RAISERROR (@Msg, @ErrorSeverity, @ErrorState);
  RETURN @Raiserror;
END CATCH
GO

IF OBJECT_ID (N'p_rpt_etl_validation_details_ps',N'P') IS NOT NULL
  PRINT N'PROCEDURE p_rpt_etl_validation_details_ps has been created...';
ELSE
  PRINT N'PROCEDURE p_rpt_etl_validation_details_ps has NOT been created due to errors...';
GO

