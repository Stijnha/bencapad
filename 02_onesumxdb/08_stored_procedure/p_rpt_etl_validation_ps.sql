USE BENONESUMX;

IF OBJECT_ID (N'p_rpt_etl_validation_ps',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_rpt_etl_validation_ps...';
  DROP PROCEDURE p_rpt_etl_validation_ps;
END
GO

PRINT N'Creating procedure p_rpt_etl_validation_ps...';
GO
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************

CREATE PROCEDURE dbo.p_rpt_etl_validation_ps (
												@execution_date DATETIME = NULL,
												@validation_severity_level INT = NULL,
												@show_all BIT = 0,
												@debug BIT = 0)
AS   

SET NOCOUNT ON;

-- declare variables
DECLARE @code_block           NVARCHAR(4000),
        @ErrorSeverity        INT,
        @ErrorState           INT,
        @etl_table            SYSNAME,
        @Msg                  NVARCHAR(MAX),
        @ProcName             SYSNAME = OBJECT_NAME(@@PROCID),
        @Raiserror            INT,
        @rowcount             INT = 0,
        @val_table            SYSNAME;
-- initialise variables

BEGIN TRY

DECLARE val_tables CURSOR FOR
SELECT DISTINCT at.[type_name] + N'_validation', at.[type_name]
  FROM t_adj_type at
  JOIN t_adj_category ac
    ON at.category_id = ac.category_id
  JOIN t_adj_instance ai
    ON at.[type_id] = ai.[type_id]
 WHERE category_name = N'ETL'
   AND OBJECT_ID(at.[type_name]) IS NOT NULL
   AND OBJECT_ID(at.[type_name] + N'_validation') IS NOT NULL
   AND (ai.adjustment_date BETWEEN @execution_date AND @execution_date + 1
    OR @execution_date IS NULL);    

CREATE TABLE #sql(id INT IDENTITY(1,1) NOT NULL,
                  txt NVARCHAR(2000) NOT NULL);

OPEN val_tables;
FETCH NEXT FROM val_tables INTO @val_table,@etl_table;
WHILE @@FETCH_STATUS = 0
BEGIN

  INSERT INTO #sql(txt) SELECT N'SELECT CONVERT(DATE,etl.line_date) AS line_date,';
  INSERT INTO #sql(txt) SELECT N'       val.adjustment_id,';
  INSERT INTO #sql(txt) SELECT N'       N'''+ @etl_table + N''' AS etl_table,';
  INSERT INTO #sql(txt) SELECT N'       val.validated_column_list,';
  INSERT INTO #sql(txt) SELECT N'       val.validation_desc,';
  INSERT INTO #sql(txt) SELECT N'       ISNULL(vsl.severity_level_name,val.validation_severity_level) AS severity_level,';
  INSERT INTO #sql(txt) SELECT N'       COUNT_BIG(*) AS invalid_records';
  INSERT INTO #sql(txt) SELECT N'  FROM ' + @val_table + N' val';
  INSERT INTO #sql(txt) SELECT N'  JOIN ' + @etl_table + N' etl';
  INSERT INTO #sql(txt) SELECT N'    ON val.adjustment_id = etl.adjustment_id';
  INSERT INTO #sql(txt) SELECT N'   AND val.line_nr = etl.line_nr';
  INSERT INTO #sql(txt) SELECT N'  LEFT JOIN t_val_severity_level vsl';
  INSERT INTO #sql(txt) SELECT N'    ON val.validation_severity_level = vsl.validation_severity_level_id';    
  INSERT INTO #sql(txt) SELECT N' WHERE 1=1';
  IF @execution_date IS NOT NULL
    INSERT INTO #sql(txt) SELECT N'   AND etl.line_date BETWEEN N''' + CONVERT(NVARCHAR,@execution_date,107) + N''' AND N''' + CONVERT(NVARCHAR,@execution_date + 1,107) + N'''';
  IF @validation_severity_level IS NOT NULL
    INSERT INTO #sql(txt) SELECT N'   AND val.validation_severity_level = ' + CONVERT(NVARCHAR,@validation_severity_level);
  INSERT INTO #sql(txt) SELECT N' GROUP BY val.adjustment_id,';
  INSERT INTO #sql(txt) SELECT N'       CONVERT(DATE,etl.line_date),';
  INSERT INTO #sql(txt) SELECT N'       val.validated_column_list,';
  INSERT INTO #sql(txt) SELECT N'       val.validation_desc,';
  INSERT INTO #sql(txt) SELECT N'       val.validation_severity_level,';
  INSERT INTO #sql(txt) SELECT N'       vsl.severity_level_name';
  IF @show_all = 1  
  BEGIN
    INSERT INTO #sql(txt) SELECT N'UNION ALL';
    INSERT INTO #sql(txt) SELECT N'SELECT DISTINCT CONVERT(DATE,etl.line_date) AS line_date,';
    INSERT INTO #sql(txt) SELECT N'       etl.adjustment_id,';
    INSERT INTO #sql(txt) SELECT N'       N'''+ @etl_table + N''' AS etl_table,';
    INSERT INTO #sql(txt) SELECT N'       N''None'' AS validated_column_list,';
    INSERT INTO #sql(txt) SELECT N'       N'''' AS validation_desc,';
    INSERT INTO #sql(txt) SELECT N'       N'''' AS severity_level,';
    INSERT INTO #sql(txt) SELECT N'       0 AS ''invalid_records''';
    INSERT INTO #sql(txt) SELECT N'  FROM ' + @etl_table + N' etl';
    INSERT INTO #sql(txt) SELECT N' WHERE NOT EXISTS (SELECT 1'; -- attention on performance!
    INSERT INTO #sql(txt) SELECT N'                     FROM ' + @val_table + N' val';
    INSERT INTO #sql(txt) SELECT N'                    WHERE val.adjustment_id = etl.adjustment_id)';
    IF @execution_date IS NOT NULL
      INSERT INTO #sql(txt) SELECT N'   AND etl.line_date BETWEEN ''' + CONVERT(NVARCHAR,@execution_date,107) + N''' AND ''' + CONVERT(NVARCHAR,@execution_date + 1,107) + N'''';
  END
  INSERT INTO #sql(txt) SELECT N'UNION ALL';

  FETCH NEXT FROM val_tables INTO @val_table,@etl_table;
END

DELETE FROM #sql
WHERE id = (SELECT MAX(id) FROM #sql);  -- could be better

IF @@ROWCOUNT > 0
BEGIN  
  INSERT INTO #sql(txt) SELECT N' ORDER BY etl_table,';
  INSERT INTO #sql(txt) SELECT N'       CONVERT(DATE,line_date) DESC,';
  INSERT INTO #sql(txt) SELECT N'       adjustment_id DESC';
END

CLOSE val_tables;
DEALLOCATE val_tables;

IF @debug = 1  
BEGIN
  DECLARE c_code CURSOR
  FOR SELECT txt FROM #sql ORDER BY id;    
  
  OPEN c_code;
  FETCH NEXT FROM c_code INTO @code_block;
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    PRINT @code_block;
    FETCH NEXT FROM c_code INTO @code_block;
  END  
  CLOSE c_code;
  DEALLOCATE c_code;
END  
  
--SELECT txt FROM #sql ORDER BY id
EXEC p_exec_resultset @stmt = N'SELECT txt FROM #sql ORDER BY id;';



DROP TABLE #sql

       
END TRY

BEGIN CATCH
  SELECT @Raiserror = 300000 + ERROR_NUMBER(),
         @ErrorSeverity = ERROR_SEVERITY(),
         @ErrorState = ERROR_STATE(),
         @Msg = @ProcName + N' ' + ISNULL(ERROR_MESSAGE(), N'') + N', Error Number = ' + ISNULL(CONVERT(NVARCHAR, ERROR_NUMBER()),N'')
                + N', Error Line = ' + ISNULL(CONVERT(NVARCHAR, ERROR_LINE()),N'N/A');             
  RAISERROR (@Msg, @ErrorSeverity, @ErrorState);
  RETURN @Raiserror;
END CATCH

RETURN(0)  

GO

IF OBJECT_ID (N'p_rpt_etl_validation_ps',N'P') IS NOT NULL
  PRINT N'Procedure p_rpt_etl_validation_ps has been created';
ELSE
  PRINT N'Procedure p_rpt_etl_validation_ps has not been created due to errors';

GO
