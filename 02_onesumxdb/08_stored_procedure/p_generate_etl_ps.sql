USE BENONESUMX;
GO

IF OBJECT_ID(N'dbo.p_generate_etl_ps', N'P') IS NOT NULL
    BEGIN
        PRINT N'Dropping procedure p_generate_etl_ps...';
        DROP PROCEDURE dbo.p_generate_etl_ps;
    END;
GO

PRINT N'Creating procedure p_generate_etl_ps...';
GO
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
/*
1. Create staging table
2. create ETL objects
3. Create ETL validation
4. Create ETL transformation
5. Create Data Editor
6. Deploy finale
*/

-- * Notes:                                                                     *
/*
Change history:
05-Apr-2019: Added parameter to add condition on when to generate maestro. 
*/

-- ******************************************************************************
CREATE PROCEDURE dbo.p_generate_etl_ps
       (
                 @fda_table           d_name,
                 @debug               BIT    = 0,
                 @execute_immediately BIT    = 0,
                 @generate_maestro    BIT    = 0--05-Apr-2019 changes
       )
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @code_block    NVARCHAR(4000) = N'',
            @errorseverity INT,
            @errorstate    INT,
            @msg           NVARCHAR(4000),
            @procname      SYSNAME        = OBJECT_NAME(@@procid),
            @raiserror     INT,
            @rowcount      INT,
            @object_id     INT,
            @sql           NVARCHAR(MAX)  = '',
            @today DATE= CONVERT(DATE, CURRENT_TIMESTAMP);

    BEGIN TRY

        -- 1. Create staging
        IF OBJECT_ID(@fda_table) IS NULL
            THROW 50000, 'Table does not exist', 1;

        SELECT @object_id = OBJECT_ID(@fda_table);

		EXEC p_gen_staging_ps 
				@table_name = @fda_table,
				@debug = @debug,
				@execute_immediately = @execute_immediately;

        -- 2. Generate ETL objects
        -- Needs Maestro installed and working
        
		EXEC p_etl_setup_product @table_name = @fda_table,
                                 @execute_immediately = 1,
                                 @use_delta_load = 0,
                                 @generate_trigger = 0;
								 
        IF @generate_maestro = 1 --05-Apr-2019 changes
            BEGIN
                -- 3. Create ETL validation
                EXEC p_generate_maestro_validation_ps @fda_table = @fda_table,
                                                      @cob_date = @today,
                                                      @build = 1;

                -- 4. Create ETL Transformation
                EXEC p_generate_maestro_transformation_ps @fda_table = @fda_table,
                                                          @cob_date = @today;
            END;

        -- 5. Create TMS
        EXEC p_adj_create_configuration_ps @fda_table = @fda_table,
                                           @data_fields = NULL,
                                           @type_name = @fda_table,
                                           @debug = @debug,
                                           @execute_immediately = 1,
                                           @database_name = NULL,
                                           @recreate_validation = 0;

        -- 6. Deploy finale
        EXEC p_ale_config_ps @fda_table = @fda_table,
                             @debug = @debug,
                             @execute_immediately = 1;
    END TRY
    BEGIN CATCH
        SELECT @raiserror = 300000 + ERROR_NUMBER(),
               @errorseverity = ERROR_SEVERITY(),
               @errorstate = ERROR_STATE(),
               @msg = ERROR_MESSAGE();

        RAISERROR(@msg, @errorseverity, @errorstate);

        RETURN @raiserror;
    END CATCH;

    RETURN 0;
END;
GO

IF OBJECT_ID(N'p_generate_etl_ps', N'P') IS NOT NULL
    PRINT N'Procedure p_generate_etl_ps has been created';
ELSE
    PRINT N'Procedure p_generate_etl_ps has not been created due to errors';

GO
