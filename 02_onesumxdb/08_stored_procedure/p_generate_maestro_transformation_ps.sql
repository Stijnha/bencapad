USE BENONESUMX;

IF OBJECT_ID (N'p_generate_maestro_transformation_ps',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_generate_maestro_transformation_ps...';
  DROP PROCEDURE p_generate_maestro_transformation_ps;
END
GO

PRINT N'Creating procedure p_generate_maestro_transformation_ps...';
GO

-- ***************************************************************************************************************************************
-- * Purpose: This procedure generates a Maestro transformation rule that can be used as template in the ETL transformation process      *
-- *          It will                                                                                                                    *
-- *            1. generate a new application set if needed by copying the previous version or copy the template ETL transformation rule *
-- *               This application set is linked to the solution "ETL Transformation Rules"                                             *
-- *            2. Create a cluster named ETL Transformation Rules if needed                                                             *
-- *            3. Generate the runtime parameters if needed                                                                             *
-- *            4. Create a job named ETL Transformation if needed containing                                                            *
-- *               a. The ETL table as output location                                                                                   *
-- *               b. The staging table as input location                               *
-- *               c. An event chain job using the output and input location with 1 default (disabled) step                              *
-- *                  The mapping will contain                                                                                           *
-- *                    - defaults for ETL specific columns                                                                              *
-- *                    - defaults values assigned to the corresponding FDA table                                                        *
-- *                    - The datatype, NULL indicator, foreign key and check constraint information per column in the description       *
-- *                                                                                                                                     *
-- * Parameters: @fda_table: Name of the FDA table for which you want to generate the template                                           *
-- *             @cob_date: Date indicator from when the rule should be valid                                                            *
-- *                                                                                                                                     *
-- * Dependencies: - fn_cvt_proper_ps: Function to trasnform strings to Proper format                                                   *
-- *               - ETL Transformation Template in Maestro (may contain no objects)                                                     *
-- *                                                                                                                                     *
-- * Notes: This should only be used by WKFS professional services                                                                       *
-- *        This script is not intended for production                                                                                   *
-- *        The script will not delete any objects. It might duplicate tasks.                                                            *
-- ***************************************************************************************************************************************

CREATE PROCEDURE dbo.p_generate_maestro_transformation_ps
(
        @fda_table              SYSNAME,
		@cob_date               DATETIME
)
AS

SET NOCOUNT ON;

DECLARE @cluster_id d_id,
		@cur_version_id d_id,
		@def_location_id d_id,
		@def_object_id d_id,
		@etl_table SYSNAME,
		@job_id d_id,
		@mex_id d_id,
		@mex_type d_id,
		@mex_type_desc d_description,
		@mex_type_name d_name,
		@msg NVARCHAR(1000),
		@out_location_id d_id,
		@out_object_id d_id,
		@step_id d_id,
		@tpl_mex_type d_id,
        @tpl_version_id d_id,
		@version_id d_id;

SELECT @mex_type_name = dbo.fn_cvt_proper_ps (REPLACE (RIGHT(@fda_table,LEN(@fda_table)-2),'_',' '),1) + N'Transformation',
       @mex_type_desc = N'Transformation Rules for ' + @mex_type_name,
	   @etl_table = N't_etl_adj_data_' + RIGHT(@fda_table,LEN(@fda_table)-2);

DECLARE @syscolumns TABLE (
        column_id INT NOT NULL,
		name NVARCHAR(1000) NOT NULL,
		datatype NVARCHAR(1000) NULL,
		is_nullable BIT NOT NULL,
		pk_column_id INT NULL,
		uq_column_id INT NULL,
		fk_reference_column NVARCHAR(1000) NULL,
		fk_reference_table NVARCHAR(1000) NULL,
		fk_constraint_id INT NULL,
		cc_condition NVARCHAR(MAX) NULL,
		defaults NVARCHAR(1000) NULL,
		mae_description NVARCHAR(MAX) NULL);

PRINT N'FDA Table:			' + @fda_table;

SELECT @msg = N'The %s table %s does not exist in the database %s ';

IF OBJECT_ID(@fda_table) IS NULL
BEGIN
  SELECT @msg = FORMATMESSAGE (@msg, N'FDA', @fda_table , N'onesumxdb_qtc');
  THROW 50000, @msg, 0;
END;

IF OBJECT_ID(@etl_table) IS NULL
BEGIN
  SELECT @msg = FORMATMESSAGE (@msg, N'ETL', @etl_table , N'onesumxdb_qtc');
  THROW 50000, @msg, 0;
END;

INSERT INTO @syscolumns (
       column_id,
       name,
	   datatype,
       is_nullable,
       pk_column_id,
       uq_column_id,
       fk_reference_column,
       fk_reference_table,
       fk_constraint_id,
       cc_condition,
	   defaults)

SELECT 6 + c.column_id AS column_id,
       c.name,
	   t1.name + ISNULL(N'(' + t2.name + CASE WHEN t1.precision = 0 
	                                            THEN N'(' + CONVERT(NVARCHAR,t1.max_length / 2) + N')' 
											  WHEN t2.name = N'numeric'
											    THEN N'(' + CONVERT(NVARCHAR,t1.precision) + N',' + CONVERT(NVARCHAR,t1.scale) + N')' 
	                                          ELSE '' 
											  END + N')',N'') AS datatype,
       c.is_nullable,
	   icpk.index_column_id AS pk_column_id,
	   uq.uq_constraint_id,
	   fk.fk_reference_column,
	   fk.fk_reference_table,
	   fk.fk_constraint_id,
	   cc.definition AS cc_condition,
	   CASE c.name WHEN N'start_validity_date' THEN '@cob_date'
	               WHEN N'end_validity_date' THEN '''31 Dec 9999'''
				   WHEN N'last_modified' THEN 'CURRENT_TIMESTAMP'
				   WHEN N'modified_by' THEN '@application_set_name'
	     ELSE dc.definition END AS defaults
  FROM sys.columns c 
  LEFT JOIN sys.types t1
    ON c.user_type_id = t1.user_type_id
  LEFT JOIN sys.types t2
    ON t1.system_type_id = t2.user_type_id
  LEFT JOIN sys.key_constraints kcpk -- PK
    ON c.object_id = kcpk.parent_object_id
   AND kcpk.type = N'PK'
  LEFT JOIN sys.indexes ipk
    ON c.object_id = ipk.object_id
   AND kcpk.name = ipk.name
  LEFT JOIN sys.index_columns icpk
    ON c.object_id = icpk.object_id
   AND ipk.index_id = icpk.index_id
   AND c.column_id = icpk.column_id
  LEFT JOIN (SELECT cuq.name AS uq_column,
                    kcuq.object_id AS uq_constraint_id
			   FROM sys.key_constraints kcuq
			   JOIN sys.indexes iuq
			     ON kcuq.parent_object_id = iuq.object_id
			    AND kcuq.name = iuq.name
			   JOIN sys.index_columns icuq
			     ON kcuq.parent_object_id = icuq.object_id
			    AND iuq.index_id = icuq.index_id
			   JOIN sys.columns cuq
			     ON kcuq.parent_object_id = cuq.object_id
			    AND icuq.column_id = cuq.column_id
			  WHERE kcuq.parent_object_id = OBJECT_ID(@fda_table)
			    AND kcuq.type = N'UQ') uq
    ON c.name = uq.uq_column
  LEFT JOIN (SELECT cfkp.name AS parent_column,
	                cfkr.name AS fk_reference_column,
                    OBJECT_NAME(cfkr.object_id) AS fk_reference_table,
                    fk.object_id AS fk_constraint_id
               FROM sys.foreign_keys fk
               JOIN sys.foreign_key_columns fkc
                 ON fk.object_id = fkc.constraint_object_id
               JOIN sys.columns cfkp
                 ON fkc.parent_object_id = cfkp.object_id
                AND fkc.parent_column_id = cfkp.column_id
               JOIN sys.columns cfkr
                 ON fkc.referenced_object_id = cfkr.object_id
                AND fkc.referenced_column_id = cfkr.column_id
              WHERE fk.parent_object_id = OBJECT_ID (@fda_table)) fk
    ON c.name = fk.parent_column
  LEFT JOIN sys.check_constraints cc
    ON c.column_id = cc.parent_column_id
   AND c.object_id = cc.parent_object_id
  LEFT JOIN sys.default_constraints dc
    ON dc.parent_object_id = c.object_id
   AND dc.parent_column_id = c.column_id
   AND c.name NOT IN (N'start_validity_date',N'end_validity_date',N'last_modified',N'modified_by')
 WHERE c.object_id = OBJECT_ID (@fda_table)
   AND c.is_identity = 0
 ORDER BY column_id;

INSERT INTO @syscolumns (
       column_id,
       name,
	   datatype,
       is_nullable,
       pk_column_id,
       uq_column_id,
       fk_reference_column,
       fk_reference_table,
       fk_constraint_id,
       cc_condition,
	   defaults,
	   mae_description)

SELECT ce.column_id,
       ce.name,
	   t1.name + ISNULL(N'(' + t2.name + CASE WHEN t1.precision = 0 
	                                            THEN N'(' + CONVERT(NVARCHAR,t1.max_length / 2) + N')' 
											  WHEN t2.name = N'numeric'
											    THEN N'(' + CONVERT(NVARCHAR,t1.precision) + N',' + CONVERT(NVARCHAR,t1.scale) + N')' 
	                                          ELSE '' 
										END + N')',
                   CASE WHEN t1.precision = 0 AND t1.name LIKE 'N%'
	                       THEN N'(' + CONVERT(NVARCHAR,ce.max_length / 2) + N')' 
                        WHEN t1.precision = 0 AND t1.name NOT LIKE 'N%'
                          THEN N'(' + CONVERT(NVARCHAR,ce.max_length) + N')' 
		                WHEN t1.name = N'numeric'
						  THEN N'(' + CONVERT(NVARCHAR,t1.precision) + N',' + CONVERT(NVARCHAR,t1.scale) + N')' 
	                    ELSE '' END) AS datatype,
       1 AS is_nullable,
	   NULL AS pk_column_id,
	   NULL AS uq_column_id,
	   NULL AS fk_reference_column,
	   NULL AS fk_reference_table,
	   NULL AS fk_constraint_id,
	   NULL AS cc_condition,
	   CASE ce.name 
	     WHEN N'flag' THEN '''U'''
		 WHEN N'adjustment_id' THEN '@adjustment_id'
		 WHEN N'line_nr' THEN ''
		 WHEN N'line_status' THEN '0'
		 WHEN N'retry_count' THEN '1'
		 WHEN N'line_date' THEN 'CURRENT_TIMESTAMP'
	   END AS defaults,
	   CASE ce.name 
	     WHEN N'flag' THEN 'Static U'
		 WHEN N'adjustment_id' THEN 'Adjustment ID Parameter'
		 WHEN N'line_nr' THEN 'Autonumber field'
		 WHEN N'line_status' THEN 'Validation field Static 0'
		 WHEN N'retry_count' THEN 'Static 1'
		 WHEN N'line_date' THEN 'Static time and data of insert'
	   END AS mae_description
  FROM sys.columns ce
  LEFT JOIN sys.types t1
    ON ce.user_type_id = t1.user_type_id
  LEFT JOIN sys.types t2
    ON t1.system_type_id = t2.user_type_id
   AND t1.system_type_id <> t1.user_type_id
  LEFT JOIN @syscolumns c
    ON ce.name = c.name
 WHERE ce.object_id = OBJECT_ID (@etl_table)
   AND c.name IS NULL
 ORDER BY ce.column_id;

UPDATE @syscolumns
   SET mae_description = datatype + ISNULL(N' - ' + mae_description ,N'');

UPDATE @syscolumns
   SET mae_description = mae_description + N' - Not Nullable'
 WHERE is_nullable = 0;

UPDATE @syscolumns
   SET mae_description = mae_description + N' - PK field'
 WHERE pk_column_id IS NOT NULL;

UPDATE @syscolumns
   SET mae_description = mae_description + N' - UQ field'
 WHERE uq_column_id IS NOT NULL;

UPDATE @syscolumns
   SET mae_description = mae_description + N' - FK to ' + fk_reference_table + N'.' + fk_reference_column
 WHERE fk_reference_column IS NOT NULL;

UPDATE @syscolumns
   SET mae_description = mae_description + N' - Default ' + defaults
 WHERE defaults IS NOT NULL;

UPDATE @syscolumns
   SET mae_description = mae_description + N' - Check constraint: ' + cc_condition
 WHERE cc_condition IS NOT NULL;
 
WITH damn AS (
			SELECT name,
				   column_id,
				   MAX(fk_constraint_id) AS max_constraint
			  FROM @syscolumns
			 GROUP BY name,
				   column_id
			HAVING COUNT(*) > 1
			)
DELETE s
  FROM @syscolumns s
  JOIN damn d
    ON s.name = d.name
   AND s.column_id = d.column_id
   AND s.fk_constraint_id = d.max_constraint;

--SELECT *
--  FROM @syscolumns
-- ORDER BY column_id

IF NOT EXISTS (SELECT 1
  FROM t_mex_solution
 WHERE solution_name = N'ETL Transformation Rules')
BEGIN
	INSERT INTO t_mex_solution
	(
		solution_name,
		solution_desc,
		enabled,
		solution_type,
		display_sequence,
		last_modified,
		modified_by
	)
	SELECT N'ETL Transformation Rules',
	       N'ETL Transformation Rules',
		   1,
		   N'ETL',
		   0,
		   CURRENT_TIMESTAMP,
		   N'Automatic generation'
END;


IF EXISTS(SELECT 1
  FROM t_mex_type
 WHERE mex_type_name = @mex_type_name
   AND is_current = 1)
BEGIN
  PRINT N'A current application set for the transformation of the FDA table ' + @fda_table + N' already exists';
  IF EXISTS (SELECT 1
    FROM t_mex_type
   WHERE mex_type_name = @mex_type_name
     AND CONVERT(DATE,start_validity) = @cob_date)
  BEGIN
    PRINT N'There is an existing application set for the transformation of the FDA table ' + @fda_table + ' with validity date ' + CONVERT (NVARCHAR(100),@cob_date,106) + N'. Reusing the existing.';
    SELECT @mex_type = mex_type,
           @version_id = version_id
      FROM t_mex_type
     WHERE mex_type_name = @mex_type_name
       AND CONVERT(DATE,start_validity) = @cob_date;
  END;
  ELSE
  BEGIN
    SELECT @mex_type = mex_type,
           @cur_version_id = version_id
      FROM t_mex_type
     WHERE mex_type_name = @mex_type_name
       AND is_current = 1;

    SELECT @version_id = MAX(version_id) + 1
      FROM t_mex_type
     WHERE mex_type = @mex_type;

    EXEC p_mex_copy_type @mex_type = @mex_type,
                         @version_id = @cur_version_id,
                         @target_type = @mex_type,
                         @target_version_id = @version_id,
                         @target_name = @mex_type_name,
                         @target_start_validity = @cob_date;

    UPDATE t_mex_type
	   SET is_current = 0
	 WHERE mex_type = @mex_type
	   AND version_id <> @version_id
	   AND is_current = 1;

    UPDATE t_mex_type
	   SET is_current = 1,
	       mex_type_desc = @mex_type_desc
	 WHERE mex_type = @mex_type
	   AND version_id = @version_id
	   AND is_current = 0;
  PRINT N'A copy of the existing application set for the FDA table ' + @fda_table + N' has been made.';
  END;
END;
ELSE -- IF EXISTS(SELECT 1 FROM t_mex_type WHERE mex_type_name = @mex_type_name AND is_current = 1)
BEGIN
  SELECT @tpl_mex_type = mex_type,
         @tpl_version_id = version_id
    FROM t_mex_type
   WHERE mex_type_name = N'Transformation Template'
     AND is_current = 1;
  
  SELECT @mex_type = MAX(mex_type) + 1,
         @version_id = 1
    FROM t_mex_type
   WHERE mex_type BETWEEN @tpl_mex_type AND 60000;

  EXEC p_mex_copy_type @mex_type = @tpl_mex_type,
                       @version_id = @tpl_version_id,
                       @target_type = @mex_type,
                       @target_version_id = @version_id,
                       @target_name = @mex_type_name,
                       @target_start_validity = @cob_date;

  UPDATE t_mex_type
     SET mex_type_desc = @mex_type_desc
   WHERE mex_type_name = @mex_type_name;

    
END; --ELSE IF EXISTS(SELECT 1 FROM t_mex_type WHERE mex_type_name = @mex_type_name AND is_current = 1)

IF NOT EXISTS (SELECT 1
                 FROM t_mex_solution s
				 JOIN t_mex_solution_member sm
				   ON s.solution_id = sm.solution_id
				 JOIN t_mex_type t
				   ON sm.mex_type = t.mex_type
				WHERE s.solution_name = N'ETL Transformation Rules'
				  AND t.mex_type = @mex_type)
BEGIN

	INSERT INTO t_mex_solution_member (
			 solution_id,
			 mex_type,
			 execution_sequence,
			 enabled,
			 last_modified,
			 modified_by)
	  SELECT s.solution_id,
			 @mex_type AS mex_type,
			 ISNULL(MAX(execution_sequence),0) + 1 AS execution_sequence,
			 1 AS enabled,
			 CURRENT_TIMESTAMP,
			 N'Automatic generation' AS modified_by
		FROM t_mex_solution s
		LEFT JOIN t_mex_solution_member sm
		  ON s.solution_id = sm.solution_id
		LEFT JOIN t_mex_type t
		  ON sm.mex_type = t.mex_type
		 AND t.mex_type = @mex_type
	   WHERE s.solution_name = N'ETL Transformation Rules'
		 AND t.mex_type IS NULL
	   GROUP BY s.solution_id;    
END;
PRINT N'A new application set has been created. This has been added to the solution "ETL Transformation Rules".';   

SELECT @mex_id = mex_id
  FROM t_mex_type
 WHERE mex_type = @mex_type
   AND version_id = @version_id;

PRINT N'mex id:				' + CONVERT (NVARCHAR(100),@mex_id);
PRINT N'mex type:			' + CONVERT (NVARCHAR(100),@mex_type);
PRINT N'version id:			' + CONVERT (NVARCHAR(100),@version_id);
PRINT N'Type Name:			' + @mex_type_name;
PRINT N'Type Description:	' + @mex_type_desc;

-- Part 2: Generate the Cluster

SELECT @cluster_id = cluster_id
  FROM t_mex_cluster
 WHERE mex_id = @mex_id
   AND cluster_name = N'ETL Transformation Rules';

IF @cluster_id IS NULL
BEGIN
  INSERT INTO t_mex_cluster (
         mex_id,
         enabled,
         cluster_name,
         cluster_desc,
         parent_cluster,
         cluster_sequence,
         flow_control_type,
         max_iterations_enabled,
         max_iterations,
         max_elapsed_time_enabled,
         max_elapsed_time,
         sleep_enabled,
         sleep,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         1 AS [enabled],
		 N'ETL Transformation Rules' AS cluster_name,
		 N'ETL Transformation Rules' AS cluster_desc,
		 0 AS parent_cluster,
		 ISNULL(MAX(cluster_sequence),0) + 1 AS cluster_sequence,
		 NULL AS flow_control_type,
		 0 AS max_iterations_enabled,
         0 AS max_iterations,
         0 AS max_elapsed_time_enabled,
         NULL AS max_elapsed_time,
         0 AS sleep_enabled,
         NULL AS sleep,
         CURRENT_TIMESTAMP AS last_modified,
         N'Automatic generation' AS modified_by
    FROM t_mex_cluster
   WHERE mex_id = @mex_id;

  SELECT @cluster_id = cluster_id
    FROM t_mex_cluster
   WHERE mex_id = @mex_id
     AND cluster_name = N'ETL Transformation Rules';
END; -- IF @cluster_id IS NULL

-- Step 3: Generate the runtime parameters
WITH CTE AS (
SELECT N'@adjustment_id' AS runtime_parameter_name,
       N'Adjustment ID Parameter' AS runtime_parameter_desc,
	   N'd_id' AS datatype,
	   N'' AS default_value,
	   0 AS is_local,
	   0 AS is_for_output,
	   0 AS is_system_generated,
	   NULL AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@job_log_id' AS runtime_parameter_name,
       N'This variable parameter is used for logging jobs & for drillback purposes' AS runtime_parameter_desc,
	   N'int' AS datatype,
	   N'<auto-generated>' AS default_value,
	   1 AS is_local,
	   0 AS is_for_output,
	   1 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@step_log_id' AS runtime_parameter_name,
       N'This variable parameter is used for logging steps & for drillback purposes' AS runtime_parameter_desc,
	   N'int' AS datatype,
	   N'<auto-generated>' AS default_value,
	   1 AS is_local,
	   0 AS is_for_output,
	   1 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@print_ind' AS runtime_parameter_name,
       N'This variable parameter determines how messages should be printed at runtime' AS runtime_parameter_desc,
	   N'char(1)' AS datatype,
	   N'<auto-generated>' AS default_value,
	   0 AS is_local,
	   0 AS is_for_output,
	   1 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@application_set_name' AS runtime_parameter_name,
       N'This variable automatically returns the name of the application set.' AS runtime_parameter_desc,
	   N'd_name' AS datatype,
	   N'<auto-generated>' AS default_value,
	   1 AS is_local,
	   0 AS is_for_output,
	   1 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@rowcount' AS runtime_parameter_name,
       N'Stores number of rows affected' AS runtime_parameter_desc,
	   N'int' AS datatype,
	   N'0' AS default_value,
	   1 AS is_local,
	   0 AS is_for_output,
	   1 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@elapsed_time' AS runtime_parameter_name,
       N'Elapsed time' AS runtime_parameter_desc,
	   N'datetime' AS datatype,
	   N'NULL' AS default_value,
	   0 AS is_local,
	   1 AS is_for_output,
	   1 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@procedure_name' AS runtime_parameter_name,
       N'This variable automatically returns the generated procedure''s name.' AS runtime_parameter_desc,
	   N'd_name' AS datatype,
	   N'OBJECT_NAME( @@PROCID )' AS default_value,
	   1 AS is_local,
	   0 AS is_for_output,
	   0 AS is_system_generated,
	   N'' AS domain_list,
	   0 AS limit_to_list_ind,
	   1 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind
UNION
SELECT N'@cob_date' AS runtime_parameter_name,
       N'Current Business date' AS runtime_parameter_desc,
	   N'datetime' AS datatype,
	   N'cob_date FROM t_entity WHERE entity = N''001''' AS default_value,
	   1 AS is_local,
	   0 AS is_for_output,
	   0 AS is_system_generated,
	   NULL AS domain_list,
	   0 AS limit_to_list_ind,
	   0 AS allow_blank_ind,
	   0 AS hide_ind,
	   0 AS locked_ind)

 MERGE INTO t_mex_runtime_parameter AS tgt
 USING CTE AS src
    ON tgt.runtime_parameter_name = src.runtime_parameter_name
   AND tgt.mex_id = @mex_id
  WHEN MATCHED THEN UPDATE
   SET tgt.runtime_parameter_desc = src.runtime_parameter_desc,
       tgt.datatype = src.datatype,
       tgt.default_value = src.default_value,
       tgt.is_local = src.is_local,
       tgt.is_for_output = src.is_for_output,
       tgt.is_system_generated = src.is_system_generated,
       tgt.domain_list = src.domain_list,
       tgt.limit_to_list_ind = src.limit_to_list_ind,
       tgt.allow_blank_ind = src.allow_blank_ind,
       tgt.hide_ind = src.hide_ind,
       tgt.locked_ind = src.locked_ind,
	   tgt.last_modified = CURRENT_TIMESTAMP,
	   tgt.modified_by = N'Automatic generation'
  WHEN NOT MATCHED THEN INSERT (
       mex_id,
       runtime_parameter_name,
       runtime_parameter_desc,
       datatype,
       default_value,
       is_local,
       is_for_output,
       is_system_generated,
       domain_list,
       limit_to_list_ind,
       allow_blank_ind,
       hide_ind,
       locked_ind,
       last_modified,
       modified_by)
VALUES (
       @mex_id,
       src.runtime_parameter_name,
       src.runtime_parameter_desc,
       src.datatype,
       src.default_value,
       src.is_local,
       src.is_for_output,
       src.is_system_generated,
       src.domain_list,
       src.limit_to_list_ind,
       src.allow_blank_ind,
       src.hide_ind,
       src.locked_ind,
	   CURRENT_TIMESTAMP,
	   N'Automatic generation');

-- Step 4: Generate the locations

-- Step 4.1: Generate the output location

SELECT @out_location_id = location_id
  FROM t_mex_location
 WHERE mex_id = @mex_id
   AND location_type = N'OUT'
   AND location_name = @etl_table;

IF @out_location_id IS NULL
BEGIN
  INSERT INTO t_mex_location (
         mex_id,
         location_name,
         location_desc,
         location_type,
         union_id,
         distinct_records,
         use_for_drillthrough,
         expose_as_odata_feed,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @etl_table AS location_name,
		 N'ETL Table' AS location_desc,
		 N'OUT' AS location_type,
		 NULL AS union_id,
		 0 AS distinct_records,
		 0 AS use_for_drillthrough,
		 0 AS expose_as_odata_feed,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;
  
  SELECT @out_location_id = location_id
    FROM t_mex_location
   WHERE mex_id = @mex_id
     AND location_type = N'OUT'
     AND location_name = @etl_table;

  INSERT INTO t_mex_location_table (
         mex_id,
         location_id,
         object_server,
         object_catalog,
         object_owner,
         object_name,
         object_desc,
         object_alias,
         object_type,
         join_type,
         join_hint,
         object_sequence,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @out_location_id AS location_id,
		 N'' AS object_server,
		 N'' AS object_catalog,
		 N'' AS object_owner,
		 @etl_table AS object_name,
		 N'ETL Table' AS object_desc,
		 N'[etl]' AS object_alias,
		 N'U' AS object_type,
		 1 AS join_type,
		 0 AS join_hint,
		 1 AS object_sequence,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;

  SELECT @out_object_id = [object_id]
    FROM t_mex_location_table
   WHERE mex_id = @mex_id
     AND location_id = @out_location_id;

  INSERT INTO t_mex_output_field (
         mex_id,
         output_location_id,
         object_id,
         field_sequence,
         output_field_name,
         output_friendly_name1,
         output_friendly_name2,
         output_friendly_name3,
         is_mapping_candidate,
         domain_list,
         limit_to_list_ind,
         allow_blank_ind,
         hide_ind,
         locked_ind,
         default_output_expression,
         use_for_drillback,
         last_modified,
         modified_by)
  SELECT DISTINCT @mex_id AS mex_id,
         @out_location_id AS output_location_id,
		 @out_object_id AS object_id,
		 column_id AS field_sequence,
		 name AS output_field_name,
		 dbo.fn_cvt_proper_ps (REPLACE(name,N'_',N' '),1) AS output_friendly_name1,
		 dbo.fn_cvt_proper_ps (REPLACE(name,N'_',N' '),1) AS output_friendly_name2,
		 dbo.fn_cvt_proper_ps (REPLACE(name,N'_',N' '),1) AS output_friendly_name3,
		 1 AS is_mapping_candidate,
		 NULL AS domain_list,
		 0 AS limit_to_list_ind,
		 0 AS allow_blank_ind,
		 0 AS hide_ind,
		 0 AS locked_ind,
		 ISNULL(defaults,N'') AS default_output_expression,
		 0 AS use_for_drillback,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM @syscolumns;
END; --IF @out_location_id IS NULL

-- Step 4.2.2: Generate the input location
SELECT @def_location_id = location_id
  FROM t_mex_location
 WHERE mex_id = @mex_id
   AND location_type = N'SRC'
   AND location_name = @fda_table;

IF @def_location_id IS NULL
BEGIN
  INSERT INTO t_mex_location (
         mex_id,
         location_name,
         location_desc,
         location_type,
         union_id,
         distinct_records,
         use_for_drillthrough,
         expose_as_odata_feed,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @fda_table AS location_name,
		 N'Staging table for ' + @fda_table AS location_desc,
		 N'SRC' AS location_type,
		 NULL AS union_id,
		 0 AS distinct_records,
		 0 AS use_for_drillthrough,
		 0 AS expose_as_odata_feed,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;
  
  SELECT @def_location_id = location_id
    FROM t_mex_location
   WHERE mex_id = @mex_id
     AND location_type = N'SRC'
     AND location_name = @fda_table;

  INSERT INTO t_mex_location_table (
         mex_id,
         location_id,
         object_server,
         object_catalog,
         object_owner,
         object_name,
         object_desc,
         object_alias,
         object_type,
         join_type,
         join_hint,
         object_sequence,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @def_location_id AS location_id,
		 N'' AS object_server,
		 N'staging' AS object_catalog,
		 N'' AS object_owner,
		 @fda_table AS object_name,
		 N'staging table for ' + @fda_table AS object_desc,
		 N'[stg]' AS object_alias,
		 N'V' AS object_type,
		 1 AS join_type,
		 0 AS join_hint,
		 1 AS object_sequence,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;

  SELECT @def_object_id = [object_id]
    FROM t_mex_location_table
   WHERE mex_id = @mex_id
     AND location_id = @def_location_id;

  INSERT INTO t_mex_source_field (
         mex_id,
         source_location_id,
         object_id,
         field_sequence,
         source_field_name,
         source_friendly_name1,
         source_friendly_name2,
         source_friendly_name3,
         is_mapping_candidate,
         domain_list,
         include_in_drillback,
         replace_null_by,
         writeback_ind,
         writeback_default,
         last_modified,
         modified_by)
  SELECT DISTINCT @mex_id AS mex_id,
         @def_location_id AS source_location_id,
		 @def_object_id AS object_id,
		 ROW_NUMBER() OVER (ORDER BY column_id) AS field_sequence,
		 name AS source_field_name,
		 dbo.fn_cvt_proper_ps (REPLACE(name,'_',N' '),1) AS source_friendly_name1,
		 dbo.fn_cvt_proper_ps (REPLACE(name,'_',N' '),1) AS source_friendly_name2,
		 dbo.fn_cvt_proper_ps (REPLACE(name,'_',N' '),1) AS source_friendly_name3,
		 1 AS is_mapping_candidate,
		 NULL AS domain_list,
		 0 AS include_in_drillback,
		 NULL AS replace_null_by,
		 0 AS writeback_ind,
		 NULL AS writeback_default,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM @syscolumns
   WHERE name NOT IN (N'flag','adjustment_id','line_nr','line_status','retry_count','line_date');

END; -- IF @def_location_id IS NULL

-- Step 5: Generate the job

SELECT @job_id = job_id
  FROM t_mex_job
 WHERE mex_id = @mex_id
   AND job_name = N'ETL Transformation';

IF @job_id IS NULL
BEGIN
  INSERT INTO t_mex_job (
         mex_id,
         nr_of_runs_to_retain,
         run_retention_days,
         last_run_nr,
         enabled,
         job_name,
         job_desc,
         start_step_id,
         application_type,
         run_timeout,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         0 AS nr_of_runs_to_retain,
		 0 AS run_retention_days,
		 0 AS last_run_nr,
		 1 AS [enabled],
		 N'ETL Transformation' AS job_name,
		 N'ETL Transformation job between staging and ETL tables' AS job_desc,
		 0 AS start_step_id,
		 3 AS application_type,
		 NULL AS run_timeout,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by;

  SELECT @job_id = job_id
    FROM t_mex_job
   WHERE mex_id = @mex_id
     AND job_name = N'ETL Transformation';         
END;

-- Step 5.1 Generate the default step
SELECT @step_id = step_id
  FROM t_mex_step
 WHERE mex_id = @mex_id
   AND job_id = @job_id
   AND step_name = N'Staging to ETL';

IF @step_id IS NULL
BEGIN
  INSERT INTO t_mex_step (
         mex_id,
         job_id,
         step_name,
         step_desc,
         step_sequence,
         enabled,
         warning_ind,
         top_count,
         is_for_insert,
         is_for_update,
         is_for_exec,
         on_success_action,
         on_success_step_id,
         on_fail_action,
         on_fail_step_id,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
         N'Staging to ETL' AS step_name,
         N'Staging to ETL data transformation' AS step_desc,
         ISNULL(MAX(step_sequence),0) + 1 AS step_sequence,
         1 AS [enabled],
         0 AS warning_ind,
         NULL AS top_count,
         1 AS is_for_insert,
         0 AS is_for_update,
         0 AS is_for_exec,
         1 AS on_success_action,
         -1 AS on_success_step_id,
         3 AS on_fail_action,
         -1 AS on_fail_step_id,
         CURRENT_TIMESTAMP AS last_modified,
         N'Automatic generation' AS modified_by
    FROM t_mex_step
   WHERE mex_id = @mex_id
     AND job_id = @job_id;
  
  SELECT @step_id = step_id
    FROM t_mex_step
   WHERE mex_id = @mex_id
     AND job_id = @job_id
     AND step_name = N'Staging to ETL';
END;

  DELETE FROM t_mex_field_mapping_placeholder
   WHERE mex_id = @mex_id
     AND job_id = @job_id
	 AND step_id = @step_id;

  DELETE FROM t_mex_field_mapping
   WHERE mex_id = @mex_id
     AND job_id = @job_id
	 AND step_id = @step_id;
 DELETE FROM t_mex_step_link_table
   WHERE mex_id = @mex_id
     AND job_id = @job_id
	 AND step_id = @step_id;

  DELETE FROM t_mex_step_link
   WHERE mex_id = @mex_id
     AND job_id = @job_id
	 AND step_id = @step_id;

  INSERT INTO t_mex_field_mapping (
         mex_id,
         job_id,
         step_id,
         output_field_id,
         priority,
         output_expression,
         original_expression,
         field_mapping_name,
         field_mapping_desc,
         is_conditional,
         is_identity,
         seed,
         increment,
         mapping_group_id,
         mapping_group_item_id,
         last_modified,
         modified_by)
  SELECT DISTINCT 
         @mex_id AS mex_id,
         @job_id AS job_id,
         @step_id AS step_id,
         output_field_id,
         1 AS priority,
         CASE WHEN output_field_name = 'line_nr' THEN N''''''
		      WHEN output_field_name = 'end_validity_date' THEN N'''31 Dec 9999'''
		      WHEN output_field_name = 'last_modified' THEN N'CURRENT_TIMESTAMP'
		      WHEN c.defaults LIKE '@%' THEN N'?1'
			  WHEN c.is_nullable = 0 
					THEN N'ISNULL(?1,' + CASE WHEN c.defaults IS NOT NULL THEN c.defaults
											  WHEN c.datatype LIKE '%char%' THEN N'''''' 
									    	  WHEN c.datatype LIKE '%date%' THEN N'''1 Jan 1900''' 
									    	  WHEN c.datatype LIKE '%int%' OR c.datatype LIKE '%numeric%' OR c.datatype LIKE '%bit%'  THEN N'0' 
									     END + N')'
		      WHEN c.defaults IS NOT NULL THEN c.defaults
			  ELSE N'?1'
			  END						AS output_expression,
         CASE WHEN output_field_name = 'line_nr' THEN N''''''
		      WHEN output_field_name = 'end_validity_date' THEN N'''31 Dec 9999'''
		      WHEN output_field_name = 'last_modified' THEN N'CURRENT_TIMESTAMP'
		      WHEN c.defaults LIKE '@%' THEN c.defaults
			  WHEN c.is_nullable = 0 
					THEN N'ISNULL([stg].' + mof.output_field_name + ',' + CASE WHEN c.defaults IS NOT NULL THEN c.defaults
											  WHEN c.datatype LIKE '%char%' THEN N'''''' 
									    	  WHEN c.datatype LIKE '%date%' THEN N'''1 Jan 1900''' 
									    	  WHEN c.datatype LIKE '%int%' OR c.datatype LIKE '%numeric%' OR c.datatype LIKE '%bit%'  THEN N'0' 
									     END + N')'
			  ELSE '[stg].' + output_field_name 
			  END 
			  AS original_expression,
         mof.output_field_name AS field_mapping_name,
         c.mae_description AS field_mapping_desc,
         0 AS is_conditional,
         CASE WHEN output_field_name = N'line_nr' THEN 1 ELSE 0 END AS is_identity,
         CASE WHEN output_field_name = N'line_nr' THEN 1 ELSE 0 END AS seed,
         CASE WHEN output_field_name = N'line_nr' THEN 1 ELSE 0 END AS increment,
         NULL AS mapping_group_id,
         NULL AS mapping_group_item_id,
         CURRENT_TIMESTAMP AS last_modified,
         N'Automatic generation' AS modified_by
    FROM t_mex_output_field mof
	JOIN @syscolumns c
	  ON mof.output_field_name = c.name
   WHERE 1=1
     AND mof.mex_id = @mex_id
	 AND mof.output_location_id = @out_location_id;

  INSERT INTO t_mex_step_link (
         mex_id,
         job_id,
         step_id,
         step_link_name,
         step_link_desc,
         step_link_sequence,
         link_to_id,
         link_type,
         link_subtype,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
         @step_id AS step_id,
         N'' AS step_link_name,
         N'' AS step_link_desc,
         1 AS step_link_sequence,
         location_id AS link_to_id,
         N'SRC' AS link_type,
         NULL AS link_subtype,
         CURRENT_TIMESTAMP AS last_modified,
         N'Automatic generation' AS modified_by
    FROM t_mex_location
   WHERE mex_id = @mex_id
     AND location_id = @def_location_id
  UNION
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
         @step_id AS step_id,
         N'' AS step_link_name,
         N'' AS step_link_desc,
         2 AS step_link_sequence,
         location_id AS link_to_id,
         N'OUT' AS link_type,
         NULL AS link_subtype,
         CURRENT_TIMESTAMP AS last_modified,
         N'Automatic generation' AS modified_by
    FROM t_mex_location
   WHERE mex_id = @mex_id
     AND location_id = @out_location_id;


  INSERT INTO t_mex_field_mapping_placeholder (
         mex_id,
         job_id,
         step_id,
         output_field_id,
         field_mapping_id,
         placeholder_nr,
         step_link_id,
         runtime_parameter_id,
         source_field_id,
         formula_id,
         aggregation_id,
         group_by_ind,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
         @step_id AS step_id,
         output_field_id,
         field_mapping_id,
         1 AS placeholder_nr,
         NULL AS step_link_id,
         mrp.runtime_parameter_id,
         NULL AS source_field_id,
         NULL AS formula_id,
         NULL AS aggregation_id,
         0 AS group_by_ind,
         CURRENT_TIMESTAMP AS last_modified,
         N'Automatic generation' AS modified_by
    FROM t_mex_field_mapping mfm
    JOIN t_mex_step_link msl
      ON mfm.mex_id = msl.mex_id
     AND mfm.job_id = msl.job_id
     AND mfm.step_id = msl.step_id
     AND msl.link_type = N'SRC'
    JOIN t_mex_runtime_parameter mrp
      ON mfm.mex_id = mrp.mex_id
     AND mfm.original_expression = mrp.runtime_parameter_name
    LEFT JOIN t_mex_source_field msf
      ON mfm.mex_id = msf.mex_id
     AND mfm.original_expression = N'[etl].' + msf.source_field_name
     AND msf.is_mapping_candidate = 1
   WHERE mfm.mex_id = @mex_id
     AND mfm.job_id = @job_id
     AND mfm.step_id = @step_id
     AND mfm.output_expression = N'?1';

 INSERT INTO t_mex_field_mapping_placeholder (
         mex_id,
         job_id,
         step_id,
         output_field_id,
         field_mapping_id,
         placeholder_nr,
         step_link_id,
         runtime_parameter_id,
         source_field_id,
         formula_id,
         aggregation_id,
         group_by_ind,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
         @step_id AS step_id,
         output_field_id,
         field_mapping_id,
         1 AS placeholder_nr,
         msl.step_link_id AS step_link_id,
         NULL AS runtime_parameter_id,
         msf.source_field_id AS source_field_id,
         NULL AS formula_id,
         NULL AS aggregation_id,
         0 AS group_by_ind,
         CURRENT_TIMESTAMP AS last_modified,
         N'Automatic generation' AS modified_by
    FROM t_mex_field_mapping mfm
    JOIN t_mex_step_link msl
      ON mfm.mex_id = msl.mex_id
     AND mfm.job_id = msl.job_id
     AND mfm.step_id = msl.step_id
     AND msl.link_type = N'SRC'
    JOIN t_mex_source_field msf
      ON mfm.mex_id = msf.mex_id
     AND mfm.field_mapping_name = msf.source_field_name
     AND msf.is_mapping_candidate = 1
   WHERE mfm.mex_id = @mex_id
     AND mfm.job_id = @job_id
     AND mfm.step_id = @step_id
     AND mfm.output_expression LIKE N'ISNULL%';

  INSERT INTO t_mex_field_mapping_placeholder (
         mex_id,
         job_id,
         step_id,
         output_field_id,
         field_mapping_id,
         placeholder_nr,
         step_link_id,
         runtime_parameter_id,
         source_field_id,
         formula_id,
         aggregation_id,
         group_by_ind,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
         @step_id AS step_id,
         output_field_id,
         field_mapping_id,
         1 AS placeholder_nr,
         msl.step_link_id AS step_link_id,
         NULL AS runtime_parameter_id,
         msf.source_field_id AS source_field_id,
         NULL AS formula_id,
         NULL AS aggregation_id,
         0 AS group_by_ind,
         CURRENT_TIMESTAMP AS last_modified,
         N'Automatic generation' AS modified_by
    FROM t_mex_field_mapping mfm
    JOIN t_mex_step_link msl
      ON mfm.mex_id = msl.mex_id
     AND mfm.job_id = msl.job_id
     AND mfm.step_id = msl.step_id
     AND msl.link_type = N'SRC'
    JOIN t_mex_source_field msf
      ON mfm.mex_id = msf.mex_id
     AND mfm.field_mapping_name = msf.source_field_name
     AND msf.is_mapping_candidate = 1
	LEFT JOIN t_mex_runtime_parameter mrp
      ON mfm.mex_id = mrp.mex_id
     AND mfm.original_expression = mrp.runtime_parameter_name
   WHERE mfm.mex_id = @mex_id
     AND mfm.job_id = @job_id
     AND mfm.step_id = @step_id
     AND mfm.output_expression = N'?1'
	 AND mrp.runtime_parameter_id IS NULL;


  INSERT INTO t_mex_step_link_table (
         mex_id,
         job_id,
         step_id,
         step_link_id,
         location_id,
         object_id,
         object_alias,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @job_id AS job_id,
         @step_id AS step_id,
         msl.step_link_id,
         ml.location_id,
         mlt.object_id,
         mlt.object_alias,
         CURRENT_TIMESTAMP AS last_modified,
         N'Automatic generation' AS modified_by
    FROM t_mex_step_link msl
    JOIN t_mex_location ml
      ON msl.mex_id = ml.mex_id
     AND msl.link_to_id = ml.location_id
    JOIN t_mex_location_table mlt
      ON msl.mex_id = mlt.mex_id
     AND ml.location_id = mlt.location_id
   WHERE msl.mex_id = @mex_id
     AND msl.job_id = @job_id
     AND msl.step_id = @step_id
     AND msl.link_type IN (N'SRC',N'REF');

--END;

-- Step 6: Link the job to the cluster
IF NOT EXISTS (SELECT 1
                 FROM t_mex_cluster_job
                WHERE mex_id = @mex_id
                  AND cluster_id = @cluster_id
                  AND job_id = @job_id)
BEGIN
  INSERT INTO t_mex_cluster_job (
         mex_id,
         cluster_id,
         job_sequence,
         job_id,
         enabled,
         start_step_id,
         last_modified,
         modified_by)
  SELECT @mex_id AS mex_id,
         @cluster_id AS cluster_id,
		 ISNULL(MAX(job_sequence) + 1,1) AS job_sequence,
		 @job_id AS job_id,
		 1 AS enabled,
		 0 AS start_step_id,
		 CURRENT_TIMESTAMP AS last_modified,
		 N'Automatic generation' AS modified_by
    FROM t_mex_cluster_job
   WHERE mex_id = @mex_id
     AND cluster_id = @cluster_id;
END;

RETURN(0); 

GO


