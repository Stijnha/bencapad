USE BENONESUMX;
GO

IF OBJECT_ID (N'dbo.p_roll_cob_ben',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_roll_cob_ben...';
  DROP PROCEDURE dbo.p_roll_cob_ben;
END
GO

PRINT N'Creating procedure p_roll_cob_ben...';
GO
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
/*
Roll cob date until a specific date
*/
-- * Notes:                                                                     *
-- ******************************************************************************

CREATE PROCEDURE dbo.p_roll_cob_ben
(
		@cob_date				DATETIME,
		@debug                  BIT = 0,
		@execute_immediately    BIT = 0
)
AS

SET NOCOUNT ON;

DECLARE @code_block            NVARCHAR(4000) = N'',
		@ErrorSeverity         INT,
		@ErrorState            INT,
		@Msg                   NVARCHAR(4000),
		@ProcName              SYSNAME = OBJECT_NAME(@@PROCID),
		@Raiserror             INT,
		@Rowcount              INT,
		@object_id			   INT,
        @SQL				   NVARCHAR(MAX) = '',
		@today				   DATE = CONVERT(DATE,CURRENT_TIMESTAMP);


BEGIN TRY
	
	IF EXISTS (
			SELECT 1
			  FROM t_entity
			 WHERE cob_date < @cob_date)
	BEGIN
		
		UPDATE t_entity
		   SET prev_cob_date = DATEADD(day,-1,@cob_date),
		       cob_date = @cob_date,
			   next_cob_date = DATEADD(day,1,@cob_date),
			   current_year = YEAR(@cob_date),
			   current_period = MONTH(@cob_date),
			   reporting_year = YEAR(@cob_date),
			   reporting_period = MONTH(@cob_date)
		 WHERE cob_date < @cob_date
	END;

END TRY

BEGIN CATCH
	SELECT	@Raiserror = 300000 + ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@Msg = ERROR_MESSAGE();

	RAISERROR (@Msg, @ErrorSeverity, @ErrorState);

	RETURN @Raiserror;
END CATCH
  
RETURN(0); 
GO

IF OBJECT_ID (N'p_roll_cob_ben',N'P') IS NOT NULL
	PRINT N'Procedure p_roll_cob_ben has been created';
ELSE
	PRINT N'Procedure p_roll_cob_ben has not been created due to errors';

GO
