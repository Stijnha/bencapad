USE BENONESUMX;
GO

IF OBJECT_ID (N'p_ale_config_ps',N'P') IS NOT NULL
BEGIN
  PRINT N'Dropping procedure p_ale_config_ps...';
  DROP PROCEDURE p_ale_config_ps;
END
GO

PRINT N'Creating procedure p_ale_config_ps...';
GO
-- ******************************************************************************
-- * Purpose:      test sourcetree                                                             *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************

CREATE PROCEDURE dbo.p_ale_config_ps
(
        @fda_table              SYSNAME,
		@debug                  BIT = 0,
		@execute_immediately    BIT = 1
)
AS

SET NOCOUNT ON;

DECLARE @table_key_columns d_ale_key_columns = '',
        @table_key_name d_ale_key_name;

DECLARE @tab TABLE (table_name SYSNAME,
                    column_name SYSNAME,
					key_ordinal SMALLINT,
					column_id SMALLINT,
					pk_name NVARCHAR(500));

INSERT INTO @tab                    
       (
	   table_name,
	   column_name,
	   key_ordinal,
	   column_id,
	   pk_name
	   )
SELECT @fda_table AS table_name,
       c.name AS column_name,
	   ic.key_ordinal AS key_ordinal,
	   c.column_id,
	   kc.name AS pk_name
  FROM sys.columns c
  JOIN sys.key_constraints kc
    ON c.object_id = kc.parent_object_id
   AND kc.type = 'PK'
  JOIN sys.indexes i
    ON c.object_id = i.object_id
   AND kc.name = i.name
  LEFT JOIN sys.index_columns ic
    ON i.index_id = ic.index_id
   AND c.object_id = ic.object_id
   AND c.column_id = ic.column_id
 WHERE c.object_id = OBJECT_ID(@fda_table)
   AND c.name NOT IN (N'last_modified',N'modified_by')
 ORDER BY c.column_id;

SELECT @table_key_columns = @table_key_columns + column_name +  N',' 
  FROM @tab
 WHERE key_ordinal IS NOT NULL
 ORDER BY key_ordinal;

 SELECT @table_key_columns = LEFT(@table_key_columns,LEN(@table_key_columns)-1);
 
SELECT @table_key_name = pk_name
  FROM @tab;

IF EXISTS (SELECT 1
             FROM t_ale_config_table
            WHERE table_name = @fda_table)
BEGIN
  UPDATE t_ale_config_table
     SET [ale_enabled_ind] = 1,
         [ale_marked_for_removal_ind] = 0,
         [ale_history_table_name] = N't_ale_history_'+RIGHT(@fda_table,LEN(@fda_table)-2),
         [ale_reporting_function_name] = N'fn_ale_rpt_'+RIGHT(@fda_table,LEN(@fda_table)-2),
         [ale_reporting_procedure_name] = N'p_ale_rpt_'+RIGHT(@fda_table,LEN(@fda_table)-2),
         [ale_rpt_changes_procedure_name] = N'p_ale_chg_'+RIGHT(@fda_table,LEN(@fda_table)-2),
         [table_key_name] = @table_key_name,
         [table_key_type] = N'ALL',
         [table_key_columns] = @table_key_columns,
         [ale_ins_trigger_name] = N'tr_ale_ins_' + RIGHT(@fda_table,LEN(@fda_table)-2),
         [ale_upd_trigger_name] = N'tr_ale_upd_' + RIGHT(@fda_table,LEN(@fda_table)-2),
         [ale_del_trigger_name] = N'tr_ale_del_' + RIGHT(@fda_table,LEN(@fda_table)-2),
         [use_default_archive_settings_ind] = 1,
         [archive_ind] = 1,
         [keep_history_rows] = 0,
         [keep_history_days] = 0,
         [keep_history_date] = N'1900-01-01T00:00:00',
         [use_default_audit_settings_ind] = 1,
         [insert_audit_event_id] = 5001,
         [update_audit_event_id] = 5002,
         [delete_audit_event_id] = 5003,
         [keychange_audit_event_id] = 5004,
         [create_insert_ale_history_ind] = 0,
         [create_update_ale_history_ind] = 0,
         [create_delete_ale_history_ind] = 0,
         [create_keychange_ale_history_ind] = 0,
         [store_only_changed_history_data_ind] = 0,
         [use_default_data_criterion_ind] = 1,
         [data_criterion] = N'',
         [description] = N''
   WHERE table_name = @fda_table;
END            
ELSE
BEGIN
  INSERT INTO [t_ale_config_table] (
         [table_name],
         [ale_enabled_ind],
         [ale_marked_for_removal_ind],
         [ale_history_table_name],
         [ale_reporting_function_name],
         [ale_reporting_procedure_name],
         [ale_rpt_changes_procedure_name],
         [table_key_name],
         [table_key_type],
         [table_key_columns],
         [ale_ins_trigger_name],
         [ale_upd_trigger_name],
         [ale_del_trigger_name],
         [use_default_archive_settings_ind],
         [archive_ind],
         [keep_history_rows],
         [keep_history_days],
         [keep_history_date],
         [use_default_audit_settings_ind],
         [insert_audit_event_id],
         [update_audit_event_id],
         [delete_audit_event_id],
         [keychange_audit_event_id],
         [create_insert_ale_history_ind],
         [create_update_ale_history_ind],
         [create_delete_ale_history_ind],
         [create_keychange_ale_history_ind],
         [store_only_changed_history_data_ind],
         [use_default_data_criterion_ind],
         [data_criterion],
         [description])       
  SELECT @fda_table,
         1,
         0,
         N't_ale_history_' + RIGHT(@fda_table,LEN(@fda_table)-2),
         N'fn_ale_rpt_' + RIGHT(@fda_table,LEN(@fda_table)-2),
         N'p_ale_rpt_' + RIGHT(@fda_table,LEN(@fda_table)-2),
         N'p_ale_chg_' + RIGHT(@fda_table,LEN(@fda_table)-2),
         @table_key_name,
         N'ALL',
         @table_key_columns,
         N'tr_ale_ins_' + RIGHT(@fda_table,LEN(@fda_table)-2),
         N'tr_ale_upd_' + RIGHT(@fda_table,LEN(@fda_table)-2),
         N'tr_ale_del_' + RIGHT(@fda_table,LEN(@fda_table)-2),
         1,
         1,
         0,
         0,
         N'1900-01-01T00:00:00',
         1,
         5001,
         5002,
         5003,
         5004,
         0,
         0,
         0,
         0,
         0,
         1,
         N'',
         N'';
END;

WITH CTE AS (
SELECT c.name AS column_name      
  FROM sys.columns c
 WHERE c.object_id = OBJECT_ID(@fda_table)
   AND c.name NOT IN (N'last_modified',N'modified_by'))

MERGE INTO t_ale_config_column AS tgt
USING CTE AS src
   ON tgt.column_name = src.column_name
  AND tgt.table_name = @fda_table
 WHEN MATCHED
  AND tgt.ale_enabled_ind = 0
      THEN UPDATE SET
	    tgt.ale_enabled_ind = 1,
		tgt.last_modified = CURRENT_TIMESTAMP,
		tgt.modified_by = dbo.fn_user()
 WHEN NOT MATCHED
      THEN INSERT
	    (
		table_name,
		column_name,
		ale_enabled_ind,
		last_modified,
		modified_by
		)
		VALUES
		(
		@fda_table,
		src.column_name,
		1,
		CURRENT_TIMESTAMP,
		dbo.fn_user()
		)
 WHEN NOT MATCHED BY SOURCE
  AND tgt.table_name = @fda_table
      THEN DELETE; 


EXEC p_ale_apply_config @table_name = @fda_table;

RETURN(0); 
GO



IF OBJECT_ID (N'p_ale_config_ps',N'P') IS NOT NULL
  PRINT N'Procedure p_ale_config_ps has been created';
ELSE
  PRINT N'Procedure p_ale_config_ps has not been created due to errors';

GO
