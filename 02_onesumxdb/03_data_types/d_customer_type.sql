DECLARE @dbname nvarchar(30)
DECLARE @sql nvarchar(max)

SET @dbname = (select platform_property_value from t_sys_platform_property
			   where platform_property_name='instance_name')

SET @sql = 'ALTER DATABASE ' + @dbname +' set single_user WITH ROLLBACK IMMEDIATE'
EXEC (@sql)


EXEC p_change_datatype @datatype       = N'd_customer_type',
                       @phystype       = N'nvarchar',
                       @prec           = N'30',
                       @scale          = NULL,
                       @preview_mode   = 0


SET @sql = 'ALTER DATABASE ' + @dbname +'  set multi_user'
EXEC (@sql)