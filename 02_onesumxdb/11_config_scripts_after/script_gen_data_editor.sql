
	EXEC p_adj_create_configuration_ps
			@fda_table = 't_mpg_coa_ben',
			@data_fields = NULL,
			@type_name = 't_mpg_coa_ben',
			@debug = 0,
			@execute_immediately = 1,
			@database_name = NULL,
			@recreate_validation = 0;

	EXEC p_ale_config_ps 
			@fda_table = 't_mpg_coa_ben',
			@debug = 0,
			@execute_immediately = 1;

	EXEC p_adj_create_configuration_ps
			@fda_table = 't_mpg_gl_accounts_ben',
			@data_fields = NULL,
			@type_name = 't_mpg_gl_accounts_ben',
			@debug = 0,
			@execute_immediately = 1,
			@database_name = NULL,
			@recreate_validation = 0;

	EXEC p_ale_config_ps 
			@fda_table = 't_mpg_gl_accounts_ben',
			@debug = 0,
			@execute_immediately = 1;

	EXEC p_adj_create_configuration_ps
			@fda_table = 't_mpg_intra_group_ben',
			@data_fields = NULL,
			@type_name = 't_mpg_intra_group_ben',
			@debug = 0,
			@execute_immediately = 1,
			@database_name = NULL,
			@recreate_validation = 0;

	EXEC p_ale_config_ps 
			@fda_table = 't_mpg_intra_group_ben',
			@debug = 0,
			@execute_immediately = 1;

	EXEC p_adj_create_configuration_ps
			@fda_table = 't_mpg_purpose_ben',
			@data_fields = NULL,
			@type_name = 't_mpg_purpose_ben',
			@debug = 0,
			@execute_immediately = 1,
			@database_name = NULL,
			@recreate_validation = 0;

	EXEC p_ale_config_ps 
			@fda_table = 't_mpg_purpose_ben',
			@debug = 0,
			@execute_immediately = 1;

	EXEC p_adj_create_configuration_ps
			@fda_table = 't_mpg_securitisation_ben',
			@data_fields = NULL,
			@type_name = 't_mpg_securitisation_ben',
			@debug = 0,
			@execute_immediately = 1,
			@database_name = NULL,
			@recreate_validation = 0;

	EXEC p_ale_config_ps 
			@fda_table = 't_mpg_securitisation_ben',
			@debug = 0,
			@execute_immediately = 1;

	EXEC p_adj_create_configuration_ps
			@fda_table = 't_mpg_syndicated_ben',
			@data_fields = NULL,
			@type_name = 't_mpg_syndicated_ben',
			@debug = 0,
			@execute_immediately = 1,
			@database_name = NULL,
			@recreate_validation = 0;

	EXEC p_ale_config_ps 
			@fda_table = 't_mpg_syndicated_ben',
			@debug = 0,
			@execute_immediately = 1;

	EXEC p_adj_create_configuration_ps
			@fda_table = 't_deal_type',
			@data_fields = NULL,
			@type_name = 't_deal_type',
			@debug = 0,
			@execute_immediately = 1,
			@database_name = NULL,
			@recreate_validation = 0;

	EXEC p_ale_config_ps 
			@fda_table = 't_deal_type',
			@debug = 0,
			@execute_immediately = 1;

	EXEC p_adj_create_configuration_ps
			@fda_table = 't_valuation_type',
			@data_fields = NULL,
			@type_name = 't_valuation_type',
			@debug = 0,
			@execute_immediately = 1,
			@database_name = NULL,
			@recreate_validation = 0;

	EXEC p_ale_config_ps 
			@fda_table = 't_valuation_type',
			@debug = 0,
			@execute_immediately = 1;

	EXEC p_adj_create_configuration_ps
			@fda_table = 't_provision_type',
			@data_fields = NULL,
			@type_name = 't_provision_type',
			@debug = 0,
			@execute_immediately = 1,
			@database_name = NULL,
			@recreate_validation = 0;

	EXEC p_ale_config_ps 
			@fda_table = 't_provision_type',
			@debug = 0,
			@execute_immediately = 1;

	EXEC p_adj_create_configuration_ps
			@fda_table = 't_deal_subtype',
			@data_fields = NULL,
			@type_name = 't_provision_type',
			@debug = 0,
			@execute_immediately = 1,
			@database_name = NULL,
			@recreate_validation = 0;

	EXEC p_ale_config_ps 
			@fda_table = 't_deal_subtype',
			@debug = 0,
			@execute_immediately = 1;
