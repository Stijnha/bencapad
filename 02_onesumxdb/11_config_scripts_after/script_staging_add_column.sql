
USE [staging]
GO

DROP TABLE dbo.t_back_office_balance_ben;

/****** Object:  Table [dbo].[t_back_office_balance_ben]    Script Date: 12/07/2018 2:15:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_back_office_balance_ben](
	[id] [int] CONSTRAINT id_gl PRIMARY KEY,
	[balance_date] [datetime] NULL,
	[entity] [nvarchar](12) NULL,
	[amount_class] [nvarchar](10) NULL,
	[customer_nr] [nvarchar](30) NULL,
	[account_code] [nvarchar](20) NULL,
	[deal_id] [nvarchar](40) NULL,
	[profit_centre] [nvarchar](12) NULL,
	[measurement_category] [nvarchar](6) NULL,
	[book_code] [nvarchar](20) NULL,
	[source_system] [nvarchar](40) NULL,
	[deal_date] [datetime] NULL,
	[value_date] [datetime] NULL,
	[maturity_date] [datetime] NULL,
	[int_rate_type] [nvarchar](40) NULL,
	[interest_rate] [numeric](15, 8) NULL,
	[purpose] [nvarchar](20) NULL,
	[is_secured] [tinyint] NULL,
	[is_securitised] [tinyint] NULL,
	[is_subordinated] [tinyint] NULL,
	[syndication_role] [nvarchar](20) NULL,
	[impaired] [nvarchar](40) NULL,
	[offset_type] [nvarchar](40) NULL,
	[revolving_fac] [nvarchar](40) NULL,
	[secured_res] [nvarchar](40) NULL,
	[secured_by_res_prop] [nvarchar](40) NULL,
	[state_proploc_uf] [nvarchar](40) NULL,
	[sett_credit] [nvarchar](40) NULL,
	[non_current_asset] [numeric](25, 8) NULL,
	[allocated] [numeric](25, 8) NULL,
	[on_off_balancesheet] [numeric](25, 8) NULL,
	[listed] [nvarchar](80) NULL,
	[portfolio] [nvarchar](80) NULL,
	[element1] [nvarchar](30) NULL,
	[element2] [nvarchar](30) NULL,
	[element3] [nvarchar](30) NULL,
	[element4] [nvarchar](20) NULL,
	[element5] [nvarchar](20) NULL,
	[element6] [nvarchar](20) NULL,
	[element7] [nvarchar](20) NULL,
	[element8] [nvarchar](20) NULL,
	[element9] [nvarchar](20) NULL,
	[domain_id] [int] NULL,
	[currency] [nvarchar](3) NULL,
	[ledger_balance] [numeric](25, 8) NULL,
	[cleared_balance] [numeric](25, 8) NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO


USE [staging]
GO

DROP TABLE dbo.t_customer;

/****** Object:  Table [dbo].[t_customer]    Script Date: 12/07/2018 2:15:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_customer](
	[id] [int] CONSTRAINT id_tcus PRIMARY KEY,
	[customer_nr] [nvarchar](30) NULL,
	[start_validity_date] [datetime] NULL,
	[end_validity_date] [datetime] NULL,
	[customer_legal_name] [nvarchar](128) NULL,
	[customer_shortname] [nvarchar](20) NULL,
	[first_name] [nvarchar](128) NULL,
	[middle_name] [nvarchar](128) NULL,
	[last_name] [nvarchar](128) NULL,
	[suffix] [nvarchar](20) NULL,
	[customer_description] [nvarchar](1024) NULL,
	[customer_street_name] [nvarchar](128) NULL,
	[customer_street_number] [nvarchar](10) NULL,
	[customer_post_code] [nvarchar](300) NULL,
	[customer_city] [nvarchar](30) NULL,
	[cust_name_address_1] [nvarchar](300) NULL,
	[cust_name_address_2] [nvarchar](300) NULL,
	[cust_name_address_3] [nvarchar](300) NULL,
	[cust_name_address_4] [nvarchar](300) NULL,
	[is_active] [tinyint] NULL,
	[is_parent_ind] [tinyint] NULL,
	[account_officer] [nvarchar](20) NULL,
	[nationality] [nvarchar](3) NULL,
	[domicile] [nvarchar](3) NULL,
	[intercompany] [nvarchar](2) NULL,
	[credit_worthiness] [nvarchar](1) NULL,
	[industry_code] [nvarchar](12) NULL,
	[customer_attribute1] [nvarchar](30) NULL,
	[customer_attribute2] [nvarchar](30) NULL,
	[customer_attribute3] [nvarchar](30) NULL,
	[customer_attribute4] [nvarchar](30) NULL,
	[customer_attribute5] [nvarchar](30) NULL,
	[customer_telephone_nr] [nvarchar](30) NULL,
	[customer_email] [nvarchar](320) NULL,
	[customer_type] [nvarchar](30) NULL,
	[economic_sector] [nvarchar](10) NULL,
	[global_customer] [nvarchar](30) NULL,
	[nace_code] [nvarchar](9) NULL,
	[lei_code] [nvarchar](20) NULL,
	[mis_master] [nvarchar](30) NULL,
	[is_repo_core_market_participant] [tinyint] NULL,
	[is_sme] [tinyint] NULL,
	[is_spe] [tinyint] NULL,
	[is_student] [tinyint] NULL,
	[is_rated] [tinyint] NULL,
	[is_defaulted] [tinyint] NULL,
	[is_internal_employee] [tinyint] NULL,
	[third_country_req_type_id] [int] NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_facility;

/****** Object:  Table [dbo].[t_facility]    Script Date: 12/07/2018 2:15:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_facility](
	[id] [int] CONSTRAINT id_tf PRIMARY KEY,
	[entity] [nvarchar](12) NULL,
	[facility_nr] [nvarchar](40) NULL,
	[start_validity_date] [datetime] NULL,
	[end_validity_date] [datetime] NULL,
	[customer_nr] [nvarchar](30) NULL,
	[record_id] [nvarchar](1) NULL,
	[parent_facility_nr] [nvarchar](40) NULL,
	[currency] [nvarchar](3) NULL,
	[is_secured_facility] [tinyint] NULL,
	[ccy_amount] [numeric](25, 8) NULL,
	[deal_date] [datetime] NULL,
	[start_date] [datetime] NULL,
	[maturity_date] [datetime] NULL,
	[is_committed] [tinyint] NULL,
	[is_cancellable] [tinyint] NULL,
	[is_unconditionally_cancellable] [tinyint] NULL,
	[cancellation_notice_period_days] [int] NULL,
	[secured_percentage] [numeric](13, 7) NULL,
	[deal_type] [nvarchar](30) NULL,
	[deal_subtype] [nvarchar](30) NULL,
	[facility_term_out_date] [datetime] NULL,
	[book_code] [nvarchar](20) NULL,
	[ifrs_class] [nvarchar](6) NULL,
	[election_type] [nvarchar](4) NULL,
	[is_held_for_sale] [tinyint] NULL,
	[is_revolving] [tinyint] NULL,
	[profit_centre] [nvarchar](12) NULL,
	[source_system] [nvarchar](40) NULL,
	[purpose] [nvarchar](20) NULL,
	[servicing_bank] [nvarchar](30) NULL,
	[business_unit] [nvarchar](5) NULL,
	[calendar] [nvarchar](10) NULL,
	[is_frozen] [tinyint] NULL,
	[syndication_role] [nvarchar](20) NULL,
	[pd_curve_code] [nvarchar](20) NULL,
	[lgd_curve_code] [nvarchar](20) NULL,
	[syndication_id] [nvarchar](40) NULL,
	[is_subordinated] [tinyint] NULL,
	[restructure_type] [nvarchar](5) NULL,
	[restructure_date] [datetime] NULL,
	[is_in_probation] [tinyint] NULL,
	[char_cust_element1] [nvarchar](40) NULL,
	[char_cust_element2] [nvarchar](40) NULL,
	[char_cust_element3] [nvarchar](40) NULL,
	[char_cust_element4] [nvarchar](40) NULL,
	[char_cust_element5] [nvarchar](40) NULL,
	[num_cust_element1] [numeric](25, 8) NULL,
	[num_cust_element2] [numeric](25, 8) NULL,
	[num_cust_element3] [numeric](25, 8) NULL,
	[num_cust_element4] [numeric](25, 8) NULL,
	[num_cust_element5] [numeric](25, 8) NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_facility_valuation;

/****** Object:  Table [dbo].[t_facility_valuation]    Script Date: 12/07/2018 2:16:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_facility_valuation](
	[id] [int] CONSTRAINT id_tfva PRIMARY KEY,
	[entity] [nvarchar](12) NULL,
	[facility_nr] [nvarchar](40) NULL,
	[valuation_date] [datetime] NULL,
	[valuation_type] [nvarchar](40) NULL,
	[amount] [numeric](25, 8) NULL,
	[currency] [nvarchar](3) NULL,
	[source_ref] [int] NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_provision;

/****** Object:  Table [dbo].[t_provision]    Script Date: 12/07/2018 2:16:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_provision](
	[id] [int] CONSTRAINT id_tpr PRIMARY KEY,
	[entity] [nvarchar](12) NULL,
	[provision_id] [nvarchar](40) NULL,
	[start_validity_date] [datetime] NULL,
	[end_validity_date] [datetime] NULL,
	[description] [nvarchar](1024) NULL,
	[provision_type] [int] NULL,
	[object_origin] [nvarchar](20) NULL,
	[object_key_type] [nvarchar](1) NULL,
	[object_key_value] [nvarchar](500) NULL,
	[provision_amount] [numeric](25, 8) NULL,
	[provision_currency] [nvarchar](3) NULL,
	[effective_date] [datetime] NULL,
	[last_review_date] [datetime] NULL,
	[reversal_date] [datetime] NULL,
	[credit_officer] [nvarchar](20) NULL,
	[char_cust_element1] [nvarchar](40) NULL,
	[char_cust_element2] [nvarchar](40) NULL,
	[char_cust_element3] [nvarchar](40) NULL,
	[char_cust_element4] [nvarchar](40) NULL,
	[char_cust_element5] [nvarchar](40) NULL,
	[num_cust_element1] [numeric](25, 8) NULL,
	[num_cust_element2] [numeric](25, 8) NULL,
	[num_cust_element3] [numeric](25, 8) NULL,
	[num_cust_element4] [numeric](25, 8) NULL,
	[num_cust_element5] [numeric](25, 8) NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_tfi_bond;

/****** Object:  Table [dbo].[t_tfi_bond]    Script Date: 12/07/2018 2:16:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_tfi_bond](
	[id] [int] CONSTRAINT id_ttb PRIMARY KEY,
	[tfi_id] [nvarchar](255) NULL,
	[start_validity_date] [datetime] NULL,
	[end_validity_date] [datetime] NULL,
	[is_settlement_in_cash] [tinyint] NULL,
	[unit_of_trading] [numeric](20, 3) NULL,
	[maturity_date] [datetime] NULL,
	[interest_basis] [int] NULL,
	[rate_variability] [nvarchar](10) NULL,
	[interest_rate] [numeric](15, 8) NULL,
	[guarantor_customer_nr] [nvarchar](30) NULL,
	[tranche_code] [nvarchar](20) NULL,
	[object_origin] [nvarchar](20) NULL,
	[object_key_type] [nvarchar](1) NULL,
	[object_key_value] [nvarchar](500) NULL,
	[rest_period] [nvarchar](20) NULL,
	[next_rest_date] [datetime] NULL,
	[next_repricing_date] [datetime] NULL,
	[quotation_method_code] [nvarchar](20) NULL,
	[is_interest_settled_on_last_capital_payment] [tinyint] NULL,
	[is_principal_payment_at_cob] [tinyint] NULL,
	[discount_curve] [nvarchar](20) NULL,
	[discount_constant_spread] [numeric](15, 8) NULL,
	[discount_spread_curve] [nvarchar](20) NULL,
	[average_balance] [numeric](25, 8) NULL,
	[exercise_style] [nvarchar](20) NULL,
	[cashflow_pattern_code] [nvarchar](20) NULL,
	[is_subordinated] [tinyint] NULL,
	[restructure_type] [nvarchar](5) NULL,
	[restructure_date] [datetime] NULL,
	[is_in_probation] [tinyint] NULL,
	[strike_price] [numeric](15, 8) NULL,
	[option_fixing_days] [int] NULL,
	[volatility_surface_code] [nvarchar](20) NULL,
	[calendar] [nvarchar](10) NULL,
	[pd_curve_code] [nvarchar](20) NULL,
	[lgd_curve_code] [nvarchar](20) NULL,
	[fair_value_hierarchy] [int] NULL,
	[char_cust_element1] [nvarchar](30) NULL,
	[char_cust_element2] [nvarchar](30) NULL,
	[char_cust_element3] [nvarchar](30) NULL,
	[char_cust_element4] [nvarchar](30) NULL,
	[char_cust_element5] [nvarchar](30) NULL,
	[num_cust_element1] [numeric](9, 4) NULL,
	[num_cust_element2] [numeric](9, 4) NULL,
	[num_cust_element3] [numeric](9, 4) NULL,
	[num_cust_element4] [numeric](9, 4) NULL,
	[num_cust_element5] [numeric](9, 4) NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_tfi_market;

/****** Object:  Table [dbo].[t_tfi_market]    Script Date: 12/07/2018 2:16:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_tfi_market](
	[id] [int] CONSTRAINT id_ttma PRIMARY KEY,
	[market] [nvarchar](255) NULL,
	[market_name] [nvarchar](128) NULL,
	[market_desc] [nvarchar](1024) NULL,
	[market_type] [nvarchar](128) NULL,
	[is_liquid_market] [tinyint] NULL,
	[char_cust_element1] [nvarchar](30) NULL,
	[char_cust_element2] [nvarchar](30) NULL,
	[char_cust_element3] [nvarchar](30) NULL,
	[char_cust_element4] [nvarchar](30) NULL,
	[char_cust_element5] [nvarchar](30) NULL,
	[num_cust_element1] [numeric](9, 4) NULL,
	[num_cust_element2] [numeric](9, 4) NULL,
	[num_cust_element3] [numeric](9, 4) NULL,
	[num_cust_element4] [numeric](9, 4) NULL,
	[num_cust_element5] [numeric](9, 4) NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_tfi_master;

/****** Object:  Table [dbo].[t_tfi_master]    Script Date: 12/07/2018 2:16:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_tfi_master](
	[id] [int] CONSTRAINT id_tfma PRIMARY KEY,
	[tfi_id] [nvarchar](255) NULL,
	[issue_date] [datetime] NULL,
	[tfi_name] [nvarchar](128) NULL,
	[tfi_desc] [nvarchar](1024) NULL,
	[home_market] [nvarchar](255) NULL,
	[issuer] [nvarchar](30) NULL,
	[tfi_cfi_code] [nvarchar](6) NULL,
	[tfi_type] [nvarchar](30) NULL,
	[tfi_subtype] [nvarchar](30) NULL,
	[issue_currency] [nvarchar](3) NULL,
	[eligible_central_bank_set_id] [int] NULL,
	[char_cust_element1] [nvarchar](30) NULL,
	[char_cust_element2] [nvarchar](30) NULL,
	[char_cust_element3] [nvarchar](30) NULL,
	[char_cust_element4] [nvarchar](30) NULL,
	[char_cust_element5] [nvarchar](30) NULL,
	[num_cust_element1] [numeric](9, 4) NULL,
	[num_cust_element2] [numeric](9, 4) NULL,
	[num_cust_element3] [numeric](9, 4) NULL,
	[num_cust_element4] [numeric](9, 4) NULL,
	[num_cust_element5] [numeric](9, 4) NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_tfi_trn_bond;

/****** Object:  Table [dbo].[t_tfi_trn_bond]    Script Date: 12/07/2018 2:16:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_tfi_trn_bond](
	[id] [int] CONSTRAINT id_ttbo PRIMARY KEY,
	[entity] [nvarchar](12) NULL,
	[deal_id] [nvarchar](40) NULL,
	[start_validity_date] [datetime] NULL,
	[end_validity_date] [datetime] NULL,
	[record_id] [nvarchar](1) NULL,
	[customer_nr] [nvarchar](30) NULL,
	[profit_centre] [nvarchar](12) NULL,
	[book_code] [nvarchar](20) NULL,
	[deal_type] [nvarchar](30) NULL,
	[deal_subtype] [nvarchar](30) NULL,
	[measurement_category] [nvarchar](6) NULL,
	[ifrs_class] [nvarchar](6) NULL,
	[election_type] [nvarchar](4) NULL,
	[is_held_for_sale] [tinyint] NULL,
	[source_system] [nvarchar](40) NULL,
	[broker_id] [nvarchar](8) NULL,
	[buy_sell_ind] [nvarchar](1) NULL,
	[tfi_id] [nvarchar](255) NULL,
	[trade_date] [datetime] NULL,
	[original_entry_date] [datetime] NULL,
	[value_date] [datetime] NULL,
	[reversal_date] [datetime] NULL,
	[safekeeping_account] [nvarchar](20) NULL,
	[maintenance_margin] [numeric](15, 8) NULL,
	[initial_margin] [numeric](15, 8) NULL,
	[underlying_object_origin] [nvarchar](20) NULL,
	[underlying_object_key_type] [nvarchar](1) NULL,
	[underlying_object_key_value] [nvarchar](500) NULL,
	[quantity] [numeric](20, 3) NULL,
	[trade_price] [numeric](25, 8) NULL,
	[currency] [nvarchar](3) NULL,
	[underlying_notional_amount] [numeric](25, 8) NULL,
	[trade_amount] [numeric](25, 8) NULL,
	[residual_amount] [numeric](25, 8) NULL,
	[related_deal_id] [nvarchar](40) NULL,
	[trading_market] [nvarchar](255) NULL,
	[back_to_back_deal_id] [nvarchar](40) NULL,
	[funding_centre] [nvarchar](12) NULL,
	[cost_centre] [nvarchar](12) NULL,
	[trader_id] [nvarchar](10) NULL,
	[cap_rate] [numeric](15, 8) NULL,
	[floor_rate] [numeric](15, 8) NULL,
	[next_claimable_coupon_date] [datetime] NULL,
	[char_cust_element1] [nvarchar](30) NULL,
	[char_cust_element2] [nvarchar](30) NULL,
	[char_cust_element3] [nvarchar](30) NULL,
	[char_cust_element4] [nvarchar](30) NULL,
	[char_cust_element5] [nvarchar](30) NULL,
	[num_cust_element1] [numeric](9, 4) NULL,
	[num_cust_element2] [numeric](9, 4) NULL,
	[num_cust_element3] [numeric](9, 4) NULL,
	[num_cust_element4] [numeric](9, 4) NULL,
	[num_cust_element5] [numeric](9, 4) NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_tfi_trn_bond_valuation;

/****** Object:  Table [dbo].[t_tfi_trn_bond_valuation]    Script Date: 12/07/2018 2:16:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_tfi_trn_bond_valuation](
	[id] [int] CONSTRAINT id_ttbv PRIMARY KEY,
	[entity] [nvarchar](12) NULL,
	[deal_id] [nvarchar](40) NULL,
	[valuation_type] [nvarchar](40) NULL,
	[valuation_date] [datetime] NULL,
	[currency] [nvarchar](3) NULL,
	[amount] [numeric](25, 8) NULL,
	[source_ref] [int] NULL,
	[process_flag1] [int] NULL,
	[process_flag2] [int] NULL,
	[process_flag3] [int] NULL,
	[process_flag4] [int] NULL,
	[process_flag5] [int] NULL,
	[drillback_id] [int] NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_trn_forex;

/****** Object:  Table [dbo].[t_trn_forex]    Script Date: 12/07/2018 2:17:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_trn_forex](
	[id] [int] CONSTRAINT id_ttf PRIMARY KEY,
	[deal_id] [nvarchar](40) NULL,
	[entity] [nvarchar](12) NULL,
	[start_validity_date] [datetime] NULL,
	[end_validity_date] [datetime] NULL,
	[customer_nr] [nvarchar](30) NULL,
	[profit_centre] [nvarchar](12) NULL,
	[book_code] [nvarchar](20) NULL,
	[record_id] [nvarchar](1) NULL,
	[deal_type] [nvarchar](30) NULL,
	[source_system] [nvarchar](40) NULL,
	[deal_subtype] [nvarchar](30) NULL,
	[deal_date] [datetime] NULL,
	[value_date] [datetime] NULL,
	[option_to_date] [datetime] NULL,
	[maturity_date] [datetime] NULL,
	[reversal_date] [datetime] NULL,
	[purchase_currency] [nvarchar](3) NULL,
	[purchase_amount] [numeric](25, 8) NULL,
	[exchange_rate] [numeric](15, 8) NULL,
	[sell_currency] [nvarchar](3) NULL,
	[sell_amount] [numeric](25, 8) NULL,
	[event_base_ccy_equiv] [numeric](25, 8) NULL,
	[orig_base_ccy_equiv] [numeric](25, 8) NULL,
	[orig_entry_date] [datetime] NULL,
	[funding_rate] [numeric](15, 8) NULL,
	[accr_funding_amount] [numeric](25, 8) NULL,
	[funding_rate2] [numeric](15, 8) NULL,
	[accr_funding_amount2] [numeric](25, 8) NULL,
	[funding_centre] [nvarchar](12) NULL,
	[related_deal_id] [nvarchar](40) NULL,
	[revaluation_result] [numeric](25, 8) NULL,
	[receipt_settl_ccy] [nvarchar](3) NULL,
	[payment_settl_ccy] [nvarchar](3) NULL,
	[in_currency] [nvarchar](3) NULL,
	[borrow_term_rate] [numeric](15, 8) NULL,
	[lending_term_rate] [numeric](15, 8) NULL,
	[swap_option_deal_id] [nvarchar](40) NULL,
	[linked_deal_id] [nvarchar](40) NULL,
	[sell_amount_npv] [numeric](25, 8) NULL,
	[buy_amount_npv] [numeric](25, 8) NULL,
	[trader_id] [nvarchar](10) NULL,
	[business_day_convention] [nvarchar](20) NULL,
	[discount_curve] [nvarchar](20) NULL,
	[discount_constant_spread] [numeric](15, 8) NULL,
	[discount_spread_curve] [nvarchar](20) NULL,
	[purchase_discount_curve] [nvarchar](20) NULL,
	[purchase_discount_constant_spread] [numeric](15, 8) NULL,
	[purchase_discount_spread_curve] [nvarchar](20) NULL,
	[sell_discount_curve] [nvarchar](20) NULL,
	[sell_discount_constant_spread] [numeric](15, 8) NULL,
	[sell_discount_spread_curve] [nvarchar](20) NULL,
	[measurement_category] [nvarchar](6) NULL,
	[ifrs_class] [nvarchar](6) NULL,
	[is_held_for_sale] [tinyint] NULL,
	[fair_value_hierarchy] [int] NULL,
	[fixing_offset_nr_of_time_units] [int] NULL,
	[fixing_offset_time_unit] [nchar](2) NULL,
	[calendar] [nvarchar](10) NULL,
	[char_cust_element1] [nvarchar](40) NULL,
	[char_cust_element2] [nvarchar](40) NULL,
	[char_cust_element3] [nvarchar](40) NULL,
	[char_cust_element4] [nvarchar](40) NULL,
	[char_cust_element5] [nvarchar](40) NULL,
	[num_cust_element1] [numeric](25, 8) NULL,
	[num_cust_element2] [numeric](25, 8) NULL,
	[num_cust_element3] [numeric](25, 8) NULL,
	[num_cust_element4] [numeric](25, 8) NULL,
	[num_cust_element5] [numeric](25, 8) NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_trn_forex_valuation;

/****** Object:  Table [dbo].[t_trn_forex_valuation]    Script Date: 12/07/2018 2:17:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_trn_forex_valuation](
	[id] [int] CONSTRAINT id_ttfv PRIMARY KEY,
	[deal_id] [nvarchar](40) NULL,
	[entity] [nvarchar](12) NULL,
	[valuation_type] [nvarchar](40) NULL,
	[valuation_date] [datetime] NULL,
	[currency] [nvarchar](3) NULL,
	[amount] [numeric](25, 8) NULL,
	[source_ref] [int] NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_trn_interest_bearing_accounts;

/****** Object:  Table [dbo].[t_trn_interest_bearing_accounts]    Script Date: 12/07/2018 2:17:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_trn_interest_bearing_accounts](
	[id] [int] CONSTRAINT id_iba PRIMARY KEY,
	[entity] [nvarchar](12) NULL,
	[deal_id] [nvarchar](40) NULL,
	[start_validity_date] [datetime] NULL,
	[end_validity_date] [datetime] NULL,
	[customer_nr] [nvarchar](30) NULL,
	[profit_centre] [nvarchar](12) NULL,
	[book_code] [nvarchar](20) NULL,
	[measurement_category] [nvarchar](6) NULL,
	[ifrs_class] [nvarchar](6) NULL,
	[election_type] [nvarchar](4) NULL,
	[is_held_for_sale] [tinyint] NULL,
	[record_id] [nvarchar](1) NULL,
	[source_system] [nvarchar](40) NULL,
	[deal_type] [nvarchar](30) NULL,
	[deal_subtype] [nvarchar](30) NULL,
	[deal_date] [datetime] NULL,
	[value_date] [datetime] NULL,
	[maturity_date] [datetime] NULL,
	[reversal_date] [datetime] NULL,
	[calendar] [nvarchar](10) NULL,
	[notice_date] [datetime] NULL,
	[nr_of_days_notice] [int] NULL,
	[currency] [nvarchar](3) NULL,
	[db_interest_rate] [numeric](15, 8) NULL,
	[cr_interest_rate] [numeric](15, 8) NULL,
	[interest_basis] [int] NULL,
	[db_total_interest] [numeric](25, 8) NULL,
	[cr_total_interest] [numeric](25, 8) NULL,
	[db_funding_rate] [numeric](15, 8) NULL,
	[cr_funding_rate] [numeric](15, 8) NULL,
	[funding_centre] [nvarchar](12) NULL,
	[related_deal_id] [nvarchar](40) NULL,
	[original_entry_date] [datetime] NULL,
	[rest_period] [nvarchar](20) NULL,
	[next_rest_date] [datetime] NULL,
	[is_interest_settled_on_last_capital_payment] [tinyint] NULL,
	[is_principal_payment_at_cob] [tinyint] NULL,
	[deposit_guarantee_scheme_code] [nvarchar](20) NULL,
	[average_balance] [numeric](25, 8) NULL,
	[pd_curve_code] [nvarchar](20) NULL,
	[lgd_curve_code] [nvarchar](20) NULL,
	[fair_value_hierarchy] [int] NULL,
	[servicing_bank] [nvarchar](30) NULL,
	[restructure_type] [nvarchar](5) NULL,
	[restructure_date] [datetime] NULL,
	[is_in_probation] [tinyint] NULL,
	[is_subordinated] [tinyint] NULL,
	[char_cust_element1] [nvarchar](40) NULL,
	[char_cust_element2] [nvarchar](40) NULL,
	[char_cust_element3] [nvarchar](40) NULL,
	[char_cust_element4] [nvarchar](40) NULL,
	[char_cust_element5] [nvarchar](40) NULL,
	[num_cust_element1] [numeric](25, 8) NULL,
	[num_cust_element2] [numeric](25, 8) NULL,
	[num_cust_element3] [numeric](25, 8) NULL,
	[num_cust_element4] [numeric](25, 8) NULL,
	[num_cust_element5] [numeric](25, 8) NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_trn_interest_bearing_accounts_valuation;

/****** Object:  Table [dbo].[t_trn_interest_bearing_accounts_valuation]    Script Date: 12/07/2018 2:17:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_trn_interest_bearing_accounts_valuation](
	[id] [int] CONSTRAINT id_ibav PRIMARY KEY,
	[deal_id] [nvarchar](40) NULL,
	[entity] [nvarchar](12) NULL,
	[valuation_type] [nvarchar](40) NULL,
	[valuation_date] [datetime] NULL,
	[currency] [nvarchar](3) NULL,
	[amount] [numeric](25, 8) NULL,
	[source_ref] [int] NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_trn_loan;

/****** Object:  Table [dbo].[t_trn_loan]    Script Date: 12/07/2018 2:17:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_trn_loan](
	[id] [int] CONSTRAINT id_ttl PRIMARY KEY,
	[entity] [nvarchar](12) NULL,
	[deal_id] [nvarchar](40) NULL,
	[start_validity_date] [datetime] NULL,
	[end_validity_date] [datetime] NULL,
	[customer_nr] [nvarchar](30) NULL,
	[profit_centre] [nvarchar](12) NULL,
	[book_code] [nvarchar](20) NULL,
	[measurement_category] [nvarchar](6) NULL,
	[ifrs_class] [nvarchar](6) NULL,
	[election_type] [nvarchar](4) NULL,
	[is_held_for_sale] [tinyint] NULL,
	[record_id] [nvarchar](1) NULL,
	[source_system] [nvarchar](40) NULL,
	[deal_type] [nvarchar](30) NULL,
	[deal_subtype] [nvarchar](30) NULL,
	[deal_date] [datetime] NULL,
	[value_date] [datetime] NULL,
	[maturity_date] [datetime] NULL,
	[reversal_date] [datetime] NULL,
	[currency] [nvarchar](3) NULL,
	[principal_amount] [numeric](25, 8) NULL,
	[residual_amount] [numeric](25, 8) NULL,
	[rate_variability] [nvarchar](10) NULL,
	[interest_rate] [numeric](15, 8) NULL,
	[base_rate] [numeric](15, 8) NULL,
	[spread] [numeric](15, 8) NULL,
	[reference_interest_code] [nvarchar](12) NULL,
	[reference_interest_type] [nvarchar](10) NULL,
	[margin] [numeric](25, 8) NULL,
	[next_repayment_date] [datetime] NULL,
	[repayment_freq] [nvarchar](1) NULL,
	[repayment_day_nr] [int] NULL,
	[local_industry_code] [nvarchar](12) NULL,
	[country_of_risk] [nvarchar](3) NULL,
	[account_officer] [nvarchar](20) NULL,
	[rollover_date] [datetime] NULL,
	[next_rollover_date] [datetime] NULL,
	[rollover_freq] [nvarchar](1) NULL,
	[rollover_day_nr] [int] NULL,
	[interest_basis] [int] NULL,
	[interest_payment_freq] [nvarchar](1) NULL,
	[total_interest] [numeric](25, 8) NULL,
	[next_interest_pay_date] [datetime] NULL,
	[interest_base_date] [datetime] NULL,
	[funding_rate] [numeric](15, 8) NULL,
	[matched_opportunity_rate] [numeric](15, 8) NULL,
	[accr_funding_amount] [numeric](25, 8) NULL,
	[funding_centre] [nvarchar](12) NULL,
	[related_deal_id] [nvarchar](40) NULL,
	[original_entry_date] [datetime] NULL,
	[settlement_currency] [nvarchar](3) NULL,
	[in_currency] [nvarchar](3) NULL,
	[is_subordinated] [tinyint] NULL,
	[rest_period] [nvarchar](20) NULL,
	[next_rest_date] [datetime] NULL,
	[is_interest_settled_on_last_capital_payment] [tinyint] NULL,
	[is_principal_payment_at_cob] [tinyint] NULL,
	[discount_curve] [nvarchar](20) NULL,
	[discount_constant_spread] [numeric](25, 8) NULL,
	[discount_spread_curve] [nvarchar](20) NULL,
	[average_balance] [numeric](25, 8) NULL,
	[cashflow_pattern_code] [nvarchar](20) NULL,
	[purpose] [nvarchar](20) NULL,
	[servicing_bank] [nvarchar](30) NULL,
	[is_serviced_only] [tinyint] NULL,
	[is_secured] [tinyint] NULL,
	[is_securitised] [tinyint] NULL,
	[business_unit] [nvarchar](5) NULL,
	[syndication_role] [nvarchar](20) NULL,
	[is_government_guaranteed] [tinyint] NULL,
	[eligible_central_bank_set_id] [int] NULL,
	[restructure_type] [nvarchar](5) NULL,
	[restructure_date] [datetime] NULL,
	[is_in_probation] [tinyint] NULL,
	[deposit_guarantee_scheme_code] [nvarchar](20) NULL,
	[calendar] [nvarchar](10) NULL,
	[strike_price] [numeric](15, 8) NULL,
	[cap_rate] [numeric](15, 8) NULL,
	[floor_rate] [numeric](15, 8) NULL,
	[pd_curve_code] [nvarchar](20) NULL,
	[lgd_curve_code] [nvarchar](20) NULL,
	[volatility_surface_code] [nvarchar](20) NULL,
	[syndication_id] [nvarchar](40) NULL,
	[fair_value_hierarchy] [int] NULL,
	[nr_of_days_notice] [int] NULL,
	[notice_date] [datetime] NULL,
	[next_repricing_date] [datetime] NULL,
	[char_cust_element1] [nvarchar](40) NULL,
	[char_cust_element2] [nvarchar](40) NULL,
	[char_cust_element3] [nvarchar](40) NULL,
	[char_cust_element4] [nvarchar](40) NULL,
	[char_cust_element5] [nvarchar](40) NULL,
	[num_cust_element1] [numeric](25, 8) NULL,
	[num_cust_element2] [numeric](25, 8) NULL,
	[num_cust_element3] [numeric](25, 8) NULL,
	[num_cust_element4] [numeric](25, 8) NULL,
	[num_cust_element5] [numeric](25, 8) NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_trn_loan_valuation;

/****** Object:  Table [dbo].[t_trn_loan_valuation]    Script Date: 12/07/2018 2:17:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_trn_loan_valuation](
	[id] [int] CONSTRAINT id_ttlv PRIMARY KEY,
	[deal_id] [nvarchar](40) NULL,
	[entity] [nvarchar](12) NULL,
	[valuation_type] [nvarchar](40) NULL,
	[valuation_date] [datetime] NULL,
	[currency] [nvarchar](3) NULL,
	[amount] [numeric](25, 8) NULL,
	[source_ref] [int] NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_trn_swap_ir;

/****** Object:  Table [dbo].[t_trn_swap_ir]    Script Date: 12/07/2018 2:17:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_trn_swap_ir](
	[id] [int] CONSTRAINT id_ttsw PRIMARY KEY,
	[entity] [nvarchar](12) NULL,
	[deal_id] [nvarchar](40) NULL,
	[start_validity_date] [datetime] NULL,
	[end_validity_date] [datetime] NULL,
	[customer_nr] [nvarchar](30) NULL,
	[profit_centre] [nvarchar](12) NULL,
	[book_code] [nvarchar](20) NULL,
	[leg1_deal_id] [nvarchar](40) NULL,
	[leg2_deal_id] [nvarchar](40) NULL,
	[related_deal_id] [nvarchar](40) NULL,
	[record_id] [nvarchar](1) NULL,
	[deal_type] [nvarchar](30) NULL,
	[quotation_method_code] [nvarchar](20) NULL,
	[deal_subtype] [nvarchar](30) NULL,
	[source_system] [nvarchar](40) NULL,
	[deal_description] [nvarchar](1024) NULL,
	[back_to_back_deal_id] [nvarchar](40) NULL,
	[orig_entry_date] [datetime] NULL,
	[deal_date] [datetime] NULL,
	[value_date] [datetime] NULL,
	[maturity_date] [datetime] NULL,
	[reversal_date] [datetime] NULL,
	[funding_centre] [nvarchar](12) NULL,
	[cost_centre] [nvarchar](12) NULL,
	[trader_id] [nvarchar](10) NULL,
	[trading_market] [nvarchar](255) NULL,
	[broker_id] [nvarchar](30) NULL,
	[notional_exchange_type] [nvarchar](20) NULL,
	[discount_curve] [nvarchar](20) NULL,
	[discount_constant_spread] [numeric](15, 8) NULL,
	[discount_spread_curve] [nvarchar](20) NULL,
	[measurement_category] [nvarchar](6) NULL,
	[ifrs_class] [nvarchar](6) NULL,
	[is_held_for_sale] [tinyint] NULL,
	[principal_adj_freq_nr_of_time_units] [int] NULL,
	[principal_adj_freq_time_unit] [nchar](2) NULL,
	[calendar] [nvarchar](10) NULL,
	[simulation_type] [nvarchar](50) NULL,
	[original_deal_id] [nvarchar](40) NULL,
	[fair_value_hierarchy] [int] NULL,
	[char_cust_element1] [nvarchar](40) NULL,
	[char_cust_element2] [nvarchar](40) NULL,
	[char_cust_element3] [nvarchar](40) NULL,
	[char_cust_element4] [nvarchar](40) NULL,
	[char_cust_element5] [nvarchar](40) NULL,
	[num_cust_element1] [numeric](25, 8) NULL,
	[num_cust_element2] [numeric](25, 8) NULL,
	[num_cust_element3] [numeric](25, 8) NULL,
	[num_cust_element4] [numeric](25, 8) NULL,
	[num_cust_element5] [numeric](25, 8) NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

USE [staging]
GO

DROP TABLE dbo.t_trn_swap_ir_valuation;

/****** Object:  Table [dbo].[t_trn_swap_ir_valuation]    Script Date: 12/07/2018 2:18:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_trn_swap_ir_valuation](
	[id] [int] CONSTRAINT id_ttsv PRIMARY KEY,
	[entity] [nvarchar](12) NULL,
	[deal_id] [nvarchar](40) NULL,
	[valuation_type] [nvarchar](40) NULL,
	[valuation_date] [datetime] NULL,
	[currency] [nvarchar](3) NULL,
	[amount] [numeric](25, 8) NULL,
	[source_ref] [int] NULL,
	[last_modified] [datetime] NULL,
	[modified_by] [nvarchar](255) NULL
);

GO

