

----------------------------------------------------------------------------------------------------
-- Populating table : t_sec_active_directory_group_role_mapping (Inserts And Updates)
--                    Checking if [role_id], [ads_group_id] is found
--                    Inserting new role_id
-- Ignoring fields  : last_modified,modified_by
-- Used Filter      : role_id in (select role_id from t_sec_roles where role_name LIKE N'PS%')
-- Lookups          : role_id is looked up in t_sec_roles on role_name
-- Data def scripted: No
-- Generated on     : DLVSQL0Q, BENONESUMXD (SQL Server 13)
-- Generated by     : BBLDTL\onesumxsetup
-- Generated at     : 2018-09-11 14:45:52.153
-- Popscript Version: trunk/System/database/procedures/p_createpopulationscript.sql - Revision: 2348 
----------------------------------------------------------------------------------------------------

/* Population script generation command: 
   EXEC p_createpopulationscript @object_name = N't_sec_active_directory_group_role_mapping'
                                ,@add_existence_check = 1
                                ,@add_update = 1
                                ,@data_filter = N'role_id in (select role_id from t_sec_roles where role_name LIKE N''PS%'')'
                                ,@field_search = N'role_id'
                                ,@table_search = N't_sec_roles'
                                ,@field_replace = N'role_name'
                                ,@field_id = N'role_id'
*/
IF EXISTS (SELECT * FROM sysobjects WHERE  name = N'fn_pop_get_new_role_id' AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
    DROP FUNCTION dbo.fn_pop_get_new_role_id
GO
IF EXISTS (SELECT * FROM sysobjects WHERE  name = N'fn_pop_get_role_id' AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
    DROP FUNCTION dbo.fn_pop_get_role_id
GO
CREATE FUNCTION dbo.fn_pop_get_new_role_id () 
RETURNS d_id
AS
BEGIN
  RETURN (SELECT COALESCE(MAX([role_id]),0) + 1
            FROM [t_sec_active_directory_group_role_mapping])
END
GO
CREATE FUNCTION dbo.fn_pop_get_role_id (@role_name d_user)
RETURNS d_id
AS
BEGIN
  RETURN (CASE WHEN (SELECT TOP 1 [role_id]
                        FROM [t_sec_roles]
                       WHERE (([role_name] = @role_name) OR (@role_name IS NULL AND [role_name] IS NULL))) IS NULL 
           THEN dbo.fn_pop_get_new_role_id () 
           ELSE
             (SELECT TOP 1 [role_id]
                FROM [t_sec_roles]
               WHERE (([role_name] = @role_name) OR (@role_name IS NULL AND [role_name] IS NULL)))
           END)
END
GO
DECLARE @updates INT
DECLARE @inserts INT
DECLARE @err INT
DECLARE @rowc INT
SET NOCOUNT ON
BEGIN TRAN
IF (SELECT OBJECT_ID('tempdb..#population_tracker')) IS NULL
BEGIN
    SELECT NEWID() as unique_id,
           CONVERT(sysname,'') as object_name,
           0 as inserts,
           0 as updates,
           0 as errors
      INTO #population_tracker
     WHERE 1 = 2
END
COMMIT TRAN
SET @inserts = 0
SET @updates = 0
IF NOT EXISTS(SELECT * FROM [t_sec_active_directory_group_role_mapping] WHERE ( ((dbo.fn_pop_get_role_id(N'PS Administrator') IS NULL AND role_id IS NULL) OR dbo.fn_pop_get_role_id(N'PS Administrator') = [role_id])  AND [ads_group_id] = 5))
BEGIN
    INSERT INTO [t_sec_active_directory_group_role_mapping] ([role_id],[ads_group_id],[enabled])
    SELECT dbo.fn_pop_get_role_id(N'PS Administrator'),5,1
    SELECT @err = @@ERROR, @rowc = @@ROWCOUNT
    SET @inserts = @inserts + @rowc
END
ELSE
BEGIN
    UPDATE [t_sec_active_directory_group_role_mapping]
    SET     [enabled] = 1
    WHERE   ((dbo.fn_pop_get_role_id(N'PS Administrator') IS NULL AND role_id IS NULL) OR dbo.fn_pop_get_role_id(N'PS Administrator') = [role_id])  AND [ads_group_id] = 5
    SELECT @err = @@ERROR, @rowc = @@ROWCOUNT
    SET @updates = @updates + @rowc
END
IF NOT EXISTS(SELECT * FROM [t_sec_active_directory_group_role_mapping] WHERE ( ((dbo.fn_pop_get_role_id(N'PS End User') IS NULL AND role_id IS NULL) OR dbo.fn_pop_get_role_id(N'PS End User') = [role_id])  AND [ads_group_id] = 2))
BEGIN
    INSERT INTO [t_sec_active_directory_group_role_mapping] ([role_id],[ads_group_id],[enabled])
    SELECT dbo.fn_pop_get_role_id(N'PS End User'),2,1
    SELECT @err = @@ERROR, @rowc = @@ROWCOUNT
    SET @inserts = @inserts + @rowc
END
ELSE
BEGIN
    UPDATE [t_sec_active_directory_group_role_mapping]
    SET     [enabled] = 1
    WHERE   ((dbo.fn_pop_get_role_id(N'PS End User') IS NULL AND role_id IS NULL) OR dbo.fn_pop_get_role_id(N'PS End User') = [role_id])  AND [ads_group_id] = 2
    SELECT @err = @@ERROR, @rowc = @@ROWCOUNT
    SET @updates = @updates + @rowc
END
IF NOT EXISTS(SELECT * FROM [t_sec_active_directory_group_role_mapping] WHERE ( ((dbo.fn_pop_get_role_id(N'PS Key User') IS NULL AND role_id IS NULL) OR dbo.fn_pop_get_role_id(N'PS Key User') = [role_id])  AND [ads_group_id] = 3))
BEGIN
    INSERT INTO [t_sec_active_directory_group_role_mapping] ([role_id],[ads_group_id],[enabled])
    SELECT dbo.fn_pop_get_role_id(N'PS Key User'),3,1
    SELECT @err = @@ERROR, @rowc = @@ROWCOUNT
    SET @inserts = @inserts + @rowc
END
ELSE
BEGIN
    UPDATE [t_sec_active_directory_group_role_mapping]
    SET     [enabled] = 1
    WHERE   ((dbo.fn_pop_get_role_id(N'PS Key User') IS NULL AND role_id IS NULL) OR dbo.fn_pop_get_role_id(N'PS Key User') = [role_id])  AND [ads_group_id] = 3
    SELECT @err = @@ERROR, @rowc = @@ROWCOUNT
    SET @updates = @updates + @rowc
END
IF NOT EXISTS(SELECT * FROM [t_sec_active_directory_group_role_mapping] WHERE ( ((dbo.fn_pop_get_role_id(N'PS Power User') IS NULL AND role_id IS NULL) OR dbo.fn_pop_get_role_id(N'PS Power User') = [role_id])  AND [ads_group_id] = 4))
BEGIN
    INSERT INTO [t_sec_active_directory_group_role_mapping] ([role_id],[ads_group_id],[enabled])
    SELECT dbo.fn_pop_get_role_id(N'PS Power User'),4,1
    SELECT @err = @@ERROR, @rowc = @@ROWCOUNT
    SET @inserts = @inserts + @rowc
END
ELSE
BEGIN
    UPDATE [t_sec_active_directory_group_role_mapping]
    SET     [enabled] = 1
    WHERE   ((dbo.fn_pop_get_role_id(N'PS Power User') IS NULL AND role_id IS NULL) OR dbo.fn_pop_get_role_id(N'PS Power User') = [role_id])  AND [ads_group_id] = 4
    SELECT @err = @@ERROR, @rowc = @@ROWCOUNT
    SET @updates = @updates + @rowc
END
IF (SELECT OBJECT_ID('tempdb..#population_tracker')) IS NOT NULL
BEGIN
    IF NOT EXISTS(SELECT * FROM #population_tracker WHERE unique_id='A5C747B6-1B64-490A-A9E4-1819ACA3A149')
        INSERT INTO #population_tracker (unique_id, object_name, inserts, updates, errors) VALUES ('A5C747B6-1B64-490A-A9E4-1819ACA3A149','t_sec_active_directory_group_role_mapping',@inserts,@updates,CASE WHEN ISNULL(@err,0) = 0 THEN 0 ELSE 1 END)
    ELSE
        UPDATE #population_tracker SET inserts=@inserts, updates=@updates,errors=ISNULL(errors,0) + CASE WHEN ISNULL(@err,0) = 0 THEN 0 ELSE 1 END WHERE unique_id='A5C747B6-1B64-490A-A9E4-1819ACA3A149'
END
PRINT 'Finished populating t_sec_active_directory_group_role_mapping: ' + CONVERT(NVARCHAR,@updates + @inserts) + N' records affected (' + CONVERT(NVARCHAR,@inserts) + N' inserts/' + CONVERT(NVARCHAR,@updates) + N' updates)'
GO
IF EXISTS (SELECT * FROM sysobjects WHERE  name = N'fn_pop_get_new_role_id' AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
    DROP FUNCTION dbo.fn_pop_get_new_role_id
GO
IF EXISTS (SELECT * FROM sysobjects WHERE  name = N'fn_pop_get_role_id' AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
    DROP FUNCTION dbo.fn_pop_get_role_id
GO
