USE BENONESUMX;

-- Checking if there's a rule on the table t_mex_runtime_parameter
-- Rules shouldn't be used any more since SQL 2008...

SET NOCOUNT ON;

DECLARE @datatype SYSNAME = N'd_bit',
        @fda_table SYSNAME,
		@column_name SYSNAME,
		@SQL NVARCHAR(MAX) = N'',
		@debug BIT = 1;

IF EXISTS (SELECT *
             FROM sys.types
            WHERE name = @datatype
              AND rule_object_id <> 0)
BEGIN       
  EXEC dbo.sp_unbindrule @objname=N'[dbo].[d_bit]';
END;

DECLARE cur CURSOR FOR
SELECT OBJECT_NAME(c.object_id) AS fda_table,
       c.name AS column_name
  FROM sys.columns c
  JOIN sys.tables ta
    ON c.object_id = ta.object_id
  JOIN sys.types t
    ON c.user_type_id = t.user_type_id
  LEFT JOIN sys.check_constraints cc
    ON c.object_id = cc.parent_object_id
   AND c.column_id = cc.parent_column_id
 WHERE t.name = @datatype
 AND cc.object_id IS NULL;

OPEN cur;

FETCH NEXT FROM cur INTO @fda_table, @column_name;

WHILE @@FETCH_STATUS = 0
BEGIN
  IF @debug = 1
    PRINT N'@fda_table: ' + @fda_table + '; @column_name: ' + @column_name;

  SELECT @SQL = N'ALTER TABLE ' + @fda_table + N' WITH CHECK
  ADD CONSTRAINT ck_' + RIGHT(@fda_table,LEN(@fda_table)-2) + N'_' + @column_name + N'_ps
  CHECK (' + @column_name + N' IN (0,1));';
  
  IF @debug = 1
    PRINT @SQL;

  EXEC sp_executesql @statement = @SQL;
  FETCH NEXT FROM cur INTO @fda_table, @column_name;
END;

CLOSE cur;
DEALLOCATE cur;

GO

