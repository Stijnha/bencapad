 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_mpg_coa_ben')
  BEGIN
    PRINT N'Dropping table [dbo].t_mpg_coa_ben ...'
    DROP TABLE [dbo].t_mpg_coa_ben
  END
GO
 
PRINT N'Creating table [dbo].t_mpg_coa_ben...'
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_mpg_coa_ben (
                             source_system         d_source_system           NOT NULL,
                             entity                d_entity                  NOT NULL,
                             deal_type             d_deal_type               NOT NULL
                                                   CONSTRAINT df1_mpg_coa_ben
                                                   DEFAULT (''),
                             account_code          d_account_code            NOT NULL
                                                   CONSTRAINT df2_mpg_coa_ben
                                                   DEFAULT (''),
                             revolving_facility    d_name                    NOT NULL
                                                   CONSTRAINT pk_mpg_coa_ben
                                                   PRIMARY KEY CLUSTERED (source_system,
                                                                          entity,
                                                                          deal_type,
                                                                          account_code,
                                                                          revolving_facility)
                                                   CONSTRAINT df3_mpg_coa_ben
                                                   DEFAULT (''),
                             deal_subtype          d_deal_subtype            NOT NULL
                                                   CONSTRAINT df4_mpg_coa_ben
                                                   DEFAULT (''),
                             rv_coa                d_value                   NOT NULL,
                             last_modified         DATETIME                  NOT NULL
                                                   CONSTRAINT df5_mpg_coa_ben
                                                   DEFAULT (getdate()),
                             modified_by           NVARCHAR(200)             NOT NULL
                                                   CONSTRAINT df6_mpg_coa_ben
                                                   DEFAULT (dbo.fn_user_default())
                           )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_mpg_coa_ben')
  BEGIN
    PRINT N'Table [dbo].t_mpg_coa_ben has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_mpg_coa_ben has NOT been created due to errors...'
  END
GO
