 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_mpg_intra_group_ben')
  BEGIN
    PRINT N'Dropping table [dbo].t_mpg_intra_group_ben ...'
    DROP TABLE [dbo].t_mpg_intra_group_ben
  END
GO
 
PRINT N'Creating table [dbo].t_mpg_intra_group_ben...'
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_mpg_intra_group_ben (
                                     entity                d_entity                NOT NULL,
                                     customer_nr           d_customer_nr           NOT NULL
                                                           CONSTRAINT pk_mpg_intra_group_ben
                                                           PRIMARY KEY CLUSTERED (entity,
                                                                                  customer_nr),
                                     related_party_type    d_name                  NOT NULL,
                                     last_modified         DATETIME                NOT NULL
                                                           CONSTRAINT df1_mpg_intra_group_ben
                                                           DEFAULT (getdate()),
                                     modified_by           NVARCHAR(200)           NOT NULL
                                                           CONSTRAINT df2_mpg_intra_group_ben
                                                           DEFAULT (dbo.fn_user_default())
                                   )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_mpg_intra_group_ben')
  BEGIN
    PRINT N'Table [dbo].t_mpg_intra_group_ben has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_mpg_intra_group_ben has NOT been created due to errors...'
  END
GO
