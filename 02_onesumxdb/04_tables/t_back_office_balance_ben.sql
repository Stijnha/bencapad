
--something

USE BENONESUMX;
GO

SET NOCOUNT ON;


IF OBJECT_ID('t_back_office_balance_ben_bak_migration','U') IS NOT NULL
	DROP TABLE t_back_office_balance_ben_bak_migration;

SELECT *
  INTO t_back_office_balance_ben_bak_migration
  FROM t_back_office_balance_ben;
GO

 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_back_office_balance_ben')
  BEGIN
    PRINT N'Dropping table [dbo].t_back_office_balance_ben ...'
    DROP TABLE [dbo].t_back_office_balance_ben
  END
GO
 
PRINT N'Creating table [dbo].t_back_office_balance_ben...'
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_back_office_balance_ben (
                                         balance_date            DATETIME                         NOT NULL,
                                         entity                  d_entity                         NOT NULL
                                                                 CONSTRAINT pk_back_office_balance_ben
                                                                 PRIMARY KEY CLUSTERED (balance_date,
                                                                                        amount_class,
                                                                                        customer_nr,
                                                                                        account_code,
                                                                                        deal_id,
                                                                                        profit_centre,
                                                                                        element1,
                                                                                        element2,
                                                                                        element3,
                                                                                        element4,
                                                                                        element5,
                                                                                        element6,
                                                                                        element7,
                                                                                        domain_id,
                                                                                        currency,
                                                                                        entity),
                                         amount_class            d_amount_class                   NOT NULL,
                                         customer_nr             d_customer_nr                    NOT NULL,
                                         account_code            d_account_code                   NOT NULL,
                                         deal_id                 d_deal_id                        NOT NULL,
                                         profit_centre           d_profit_centre                  NOT NULL,
                                         measurement_category    d_measurement_category           NULL,
                                         book_code               d_book_code                      NULL,
                                         source_system           d_source_system                  NULL,
                                         deal_date               DATETIME                         NULL,
                                         value_date              DATETIME                         NULL,
                                         maturity_date           DATETIME                         NULL,
                                         int_rate_type           d_c_gen_purp                     NULL,
                                         interest_rate           d_rate                           NULL,
                                         purpose                 d_credit_purpose                 NULL,
                                         is_secured              d_bit                            NULL
 
                                                                 CONSTRAINT ck_back_office_balance_ben_is_secured_ps
                                                                 CHECK ([is_secured]=(1) OR [is_secured]=(0)),
                                         is_securitised          d_bit                            NULL
 
                                                                 CONSTRAINT ck_back_office_balance_ben_is_securitised_ps
                                                                 CHECK ([is_securitised]=(1) OR [is_securitised]=(0)),
                                         is_subordinated         TINYINT                          NULL,
                                         syndication_role        d_syndication_role               NULL,
                                         impaired                d_c_gen_purp                     NULL,
                                         offset_type             d_c_gen_purp                     NULL,
                                         revolving_fac           d_c_gen_purp                     NULL,
                                         secured_res             d_c_gen_purp                     NULL,
                                         secured_by_res_prop     d_c_gen_purp                     NULL,
                                         state_proploc_uf        d_c_gen_purp                     NULL,
                                         sett_credit             d_c_gen_purp                     NULL,
                                         non_current_asset       d_amount                         NULL,
                                         allocated               d_amount                         NULL,
                                         on_off_balancesheet     d_amount                         NULL,
                                         listed                  d_c_gen_purp                     NULL,
                                         portfolio               d_c_gen_purp                     NULL,
                                         element1                d_element1                       NOT NULL,
                                         element2                d_element2                       NOT NULL,
                                         element3                d_element3                       NOT NULL,
                                         element4                d_element4                       NOT NULL,
                                         element5                d_element5                       NOT NULL,
                                         element6                d_element6                       NOT NULL,
                                         element7                d_element7                       NOT NULL,
                                         element8                d_element8                       NOT NULL,
                                         element9                d_element9                       NOT NULL,
                                         domain_id               d_id                             NOT NULL,
                                         currency                d_currency                       NOT NULL,
                                         ledger_balance          d_amount                         NOT NULL,
                                         cleared_balance         d_amount                         NOT NULL,
                                         last_modified           d_last_modified                  NOT NULL
                                                                 CONSTRAINT df1_back_office_balance_ben
                                                                 DEFAULT (getdate()),
                                         modified_by             d_user                           NOT NULL
                                                                 CONSTRAINT df2_back_office_balance_ben
                                                                 DEFAULT ([dbo].[fn_user_default]())
                                       )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_back_office_balance_ben')
  BEGIN
    PRINT N'Table [dbo].t_back_office_balance_ben has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_back_office_balance_ben has NOT been created due to errors...'
  END
GO

INSERT INTO t_back_office_balance_ben
SELECT *
  FROM t_back_office_balance_ben_bak_migration

IF OBJECT_ID('t_back_office_balance_ben_bak_migration','U') IS NOT NULL
	DROP TABLE t_back_office_balance_ben_bak_migration;
