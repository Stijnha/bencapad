USE BENONESUMX;
GO

IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_deployment_ps')
  BEGIN
    PRINT N'Dropping table [dbo].t_deployment_ps ...'
    DROP TABLE [dbo].t_deployment_ps
  END
GO
 
PRINT N'Creating table [dbo].t_deployment_ps...'
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_deployment_ps (
                               object            NVARCHAR(MAX)      NOT NULL,
                               type              NVARCHAR(50)       NOT NULL,
                               status            NVARCHAR(20)       NOT NULL,
                               execution_date    DATETIME           NULL,
                               modified_by       NVARCHAR(20)       NULL
                             )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_deployment_ps')
  BEGIN
    PRINT N'Table [dbo].t_deployment_ps has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_deployment_ps has NOT been created due to errors...'
  END
GO
