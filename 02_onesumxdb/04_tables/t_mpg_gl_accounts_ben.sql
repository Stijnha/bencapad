 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_mpg_gl_accounts_ben')
  BEGIN
    PRINT N'Dropping table [dbo].t_mpg_gl_accounts_ben ...'
    DROP TABLE [dbo].t_mpg_gl_accounts_ben
  END
GO
 
PRINT N'Creating table [dbo].t_mpg_gl_accounts_ben...'
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_mpg_gl_accounts_ben (
                                     account_code                d_account_code             NOT NULL,
                                     parent_entity_code          d_name                     NOT NULL
                                                                 CONSTRAINT pk_mpg_gl_accounts_ben
                                                                 PRIMARY KEY CLUSTERED (account_code,
                                                                                        parent_entity_code),
                                     account_code_description    d_source_name              NOT NULL,
                                     unearned_gl                 d_name                     NOT NULL,
                                     fees_gl                     d_name                     NOT NULL,
                                     impaired_gl                 d_name                     NOT NULL,
                                     portfolio                   d_c_gen_purp               NOT NULL,
                                     gl_type                     d_name                     NOT NULL,
                                     product                     d_name                     NOT NULL,
                                     asset_liability             d_name                     NOT NULL,
                                     on_off_balance_sheet        d_name                     NOT NULL,
                                     customer_nr                 d_customer_nr              NOT NULL,
                                     maturity_date               DATETIME                   NULL,
                                     maturity_band_original      TINYINT                    NULL,
                                     maturity_band_remaining     TINYINT                    NULL,
                                     purpose                     d_credit_purpose           NOT NULL,
                                     is_secured                  d_bit                      NOT NULL
 
                                                                 CONSTRAINT ck1_mpg_gl_accounts_ben
                                                                 CHECK ([is_secured]=(1) OR [is_secured]=(0)),
                                     secured_by_res_prop         d_c_gen_purp               NOT NULL,
                                     collateralization           d_c_gen_purp               NOT NULL,
                                     property_location           d_c_gen_purp               NOT NULL,
                                     listed_on_asx               d_c_gen_purp               NOT NULL,
                                     currency                    d_name                     NOT NULL,
                                     last_modified               DATETIME                   NOT NULL
                                                                 CONSTRAINT df1_mpg_gl_accounts_ben
                                                                 DEFAULT (getdate()),
                                     modified_by                 NVARCHAR(200)              NOT NULL
                                                                 CONSTRAINT df2_mpg_gl_accounts_ben
                                                                 DEFAULT (dbo.fn_user_default())
                                   )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_mpg_gl_accounts_ben')
  BEGIN
    PRINT N'Table [dbo].t_mpg_gl_accounts_ben has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_mpg_gl_accounts_ben has NOT been created due to errors...'
  END
GO
