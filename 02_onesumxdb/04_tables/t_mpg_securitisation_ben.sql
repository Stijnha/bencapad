 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_mpg_securitisation_ben')
  BEGIN
    PRINT N'Dropping table [dbo].t_mpg_securitisation_ben ...'
    DROP TABLE [dbo].t_mpg_securitisation_ben
  END
GO
 
PRINT N'Creating table [dbo].t_mpg_securitisation_ben...'
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_mpg_securitisation_ben (
                                        source_system           d_source_system           NOT NULL,
                                        funding_centre          d_profit_centre           NOT NULL
                                                                CONSTRAINT pk_mpg_securitisation_ben
                                                                PRIMARY KEY CLUSTERED (source_system,
                                                                                       funding_centre),
                                        securitisation_value    d_name                    NOT NULL
                                                                CONSTRAINT df1_mpg_securitisation_ben
                                                                DEFAULT (''),
                                        last_modified           DATETIME                  NOT NULL
                                                                CONSTRAINT df2_mpg_securitisation_ben
                                                                DEFAULT (getdate()),
                                        modified_by             NVARCHAR(200)             NOT NULL
                                                                CONSTRAINT df3_mpg_securitisation_ben
                                                                DEFAULT (dbo.fn_user_default())
                                      )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_mpg_securitisation_ben')
  BEGIN
    PRINT N'Table [dbo].t_mpg_securitisation_ben has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_mpg_securitisation_ben has NOT been created due to errors...'
  END
GO
