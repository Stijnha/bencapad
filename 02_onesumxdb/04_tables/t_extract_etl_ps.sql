 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_extract_etl_ps')
  BEGIN
    PRINT N'Dropping table [dbo].t_extract_etl_ps ...'
    DROP TABLE [dbo].t_extract_etl_ps
  END
GO
 
PRINT N'Creating table [dbo].t_extract_etl_ps...'
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_extract_etl_ps (
                                extract_id         d_id               NOT NULL,
                                type_id            d_id               NOT NULL
                                                   CONSTRAINT pk_extract_etl_ps
                                                   PRIMARY KEY CLUSTERED (extract_id,
                                                                          type_id),
                                type_name          d_name             NOT NULL,
                                table_name         d_name             NOT NULL,
                                is_enabled         d_bit              NOT NULL
 
                                                   CONSTRAINT ck1_extract_etl_ps
                                                   CHECK ([is_enabled]=(1) OR [is_enabled]=(0)),
                                execution_order    d_id               NOT NULL,
                                last_modified      DATETIME           NOT NULL
                                                   CONSTRAINT df1_extract_etl_ps
                                                   DEFAULT (getdate()),
                                modified_by        d_user             NOT NULL
                                                   CONSTRAINT df2_extract_etl_ps
                                                   DEFAULT (dbo.fn_user_default())
                              )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_extract_etl_ps')
  BEGIN
    PRINT N'Table [dbo].t_extract_etl_ps has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_extract_etl_ps has NOT been created due to errors...'
  END
GO
