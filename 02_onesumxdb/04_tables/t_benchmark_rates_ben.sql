USE BENONESUMX;
GO
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_benchmark_rates_ben')
  BEGIN
    PRINT N'Dropping table [dbo].t_benchmark_rates_ben ...'
    DROP TABLE [dbo].t_benchmark_rates_ben
  END
GO
 
PRINT N'Creating table [dbo].t_benchmark_rates_ben...'
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_benchmark_rates_ben (
                                     entity           d_entity                  NOT NULL,
                                     rate_date        DATETIME                  NOT NULL,
                                     currency         d_currency                NOT NULL,
                                     tenor            INT                       NOT NULL
                                                      CONSTRAINT pk_benchmark_rates_ben
                                                      PRIMARY KEY CLUSTERED (entity,
                                                                             rate_date,
                                                                             currency,
                                                                             tenor),
                                     rate             d_rate                    NOT NULL,
                                     last_modified    d_last_modified           NOT NULL
														CONSTRAINT df1_benchmark_rates_ben
                                                        DEFAULT (getdate()),
                                     modified_by      d_user                    NOT NULL
														CONSTRAINT df2_benchmark_rates_ben
                                                        DEFAULT (dbo.fn_user_default())
                                   )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_benchmark_rates_ben')
  BEGIN
    PRINT N'Table [dbo].t_benchmark_rates_ben has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_benchmark_rates_ben has NOT been created due to errors...'
  END
GO
