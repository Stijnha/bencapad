 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_mpg_purpose_ben')
  BEGIN
    PRINT N'Dropping table [dbo].t_mpg_purpose_ben ...'
    DROP TABLE [dbo].t_mpg_purpose_ben
  END
GO
 
PRINT N'Creating table [dbo].t_mpg_purpose_ben...'
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_mpg_purpose_ben (
                                 source_system    d_source_system           NOT NULL,
                                 from_purpose     d_name                    NOT NULL
                                                  CONSTRAINT pk_mpg_purpose_ben
                                                  PRIMARY KEY CLUSTERED (source_system,
                                                                         from_purpose),
                                 to_purpose       d_name                    NOT NULL,
                                 last_modified    DATETIME                  NOT NULL
                                                  CONSTRAINT df1_mpg_purpose_ben
                                                  DEFAULT (getdate()),
                                 modified_by      NVARCHAR(200)             NOT NULL
                                                  CONSTRAINT df2_mpg_purpose_ben
                                                  DEFAULT (dbo.fn_user_default())
                               )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_mpg_purpose_ben')
  BEGIN
    PRINT N'Table [dbo].t_mpg_purpose_ben has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_mpg_purpose_ben has NOT been created due to errors...'
  END
GO
