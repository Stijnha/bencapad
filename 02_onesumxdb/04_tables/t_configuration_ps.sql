USE BENONESUMX;
GO


IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_configuration_ps')
  BEGIN
    PRINT N'Dropping table [dbo].t_configuration_ps ...'
    DROP TABLE [dbo].t_configuration_ps
  END
GO
 
PRINT N'Creating table [dbo].t_configuration_ps...'
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_configuration_ps (
                                  configuration_id    d_id                         NOT NULL
                                                      CONSTRAINT pk_configuration_ps
                                                      PRIMARY KEY CLUSTERED (configuration_id),
                                  config_name         d_object_key_value           NOT NULL,
                                  config_value        d_object_key_value           NOT NULL,
                                  last_modified       DATETIME                     NOT NULL
                                                      CONSTRAINT df1_configuration_ps
                                                      DEFAULT (getdate()),
                                  modified_by         d_user                       NOT NULL
                                                      CONSTRAINT df2_configuration_ps
                                                      DEFAULT ([dbo].[fn_user_default]())
                                )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_configuration_ps')
  BEGIN
    PRINT N'Table [dbo].t_configuration_ps has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_configuration_ps has NOT been created due to errors...'
  END
GO
