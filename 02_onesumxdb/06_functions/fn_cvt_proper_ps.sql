
IF NOT EXISTS (SELECT * FROM sys.objects WHERE name = N'fn_cvt_proper_ps' AND type = N'FN' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    IF EXISTS (SELECT * FROM sys.objects WHERE name = N'fn_cvt_proper_ps' AND schema_id = SCHEMA_ID('dbo'))
      BEGIN
        PRINT N'Dropping function [dbo].[fn_cvt_proper_ps] (because the previous version has an incompatible type)...'
        EXEC (N'DROP FUNCTION [dbo].[fn_cvt_proper_ps]')
      END
    PRINT N'Creating function [dbo].[fn_cvt_proper_ps] ...'
    EXEC (N'CREATE FUNCTION [dbo].[fn_cvt_proper_ps] (@input nvarchar ,@convert_all bit ) RETURNS int AS BEGIN RETURN 0 END')  
    WAITFOR DELAY N'00:00:00.003'
  END
    
PRINT N'Altering function [dbo].[fn_cvt_proper_ps] ...'
GO
ALTER FUNCTION dbo.fn_cvt_proper_ps
(
        @input                  NVARCHAR(MAX),
		@convert_all            BIT
)
RETURNS NVARCHAR(MAX)
AS

BEGIN
  DECLARE @converted NVARCHAR(MAX);
  IF @convert_all = 0
  BEGIN
    SELECT @converted = UPPER(LEFT(@input,1)) + LOWER (RIGHT(@input,LEN(@input)-1));
  END
  ELSE -- IF @convert_all = 0
  BEGIN
    SELECT @converted = N'';
    SELECT @converted = @converted + UPPER(LEFT(value,1)) + LOWER (RIGHT(value,LEN(value)-1)) + N' '
      FROM dbo.fn_parse_list (@input, N'',N' ');    
  END
  RETURN @converted;
END

GO
IF EXISTS (SELECT * FROM sys.objects WHERE name = N'fn_cvt_proper_ps' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Function [dbo].[fn_cvt_proper_ps] has been altered...'
END ELSE BEGIN
    PRINT N'Function [dbo].[fn_cvt_proper_ps] has NOT been altered due to errors!'
END
GO
