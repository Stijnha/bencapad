USE BENONESUMX;
GO
/*
SELECT 'EXEC p_generate_etl_ps @fda_table = ''' + table_name + ''', @debug = 0;'
  FROM t_extract_etl_ps
*/
EXEC p_generate_etl_ps @fda_table = 't_account_code', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_customer', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_trn_loan', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_trn_loan_valuation', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_trn_interest_bearing_accounts', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_trn_interest_bearing_accounts_valuation', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_back_office_balance_ben', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_facility', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_facility_valuation', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_provision', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_tfi_bond', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_tfi_market', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_tfi_master', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_tfi_trn_bond', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_tfi_trn_bond_valuation', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_trn_forex', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_trn_forex_valuation', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_trn_swap_ir', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_trn_swap_ir_valuation', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_mpg_gl_accounts_ben', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_trn_interest_bearing_accounts_property', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_trn_loan_property', @debug = 0;
EXEC p_generate_etl_ps @fda_table = 't_facility_property', @debug = 0;
