DELETE from financial_inst

INSERT INTO financial_inst(fi_objid,fi_id,fi_internalid,fi_fullname,fi_address,fi_postcode,fi_city,fi_state,fi_biccode,fi_isocode,fi_currency,fi_personincharge,fi_telephone,fi_telext,fi_email,fi_closingdate,fi_identifier_id)
VALUES ('001','001','001','Bendigo and Adelaide Bank Limited','The Bendigo Centre, Bath Lane','3550','Bendigo','VIC',0,0,'AUD','Andrea Moore','03 54856664','AU','andrea.moore@bendigoadelaide.com.au','20190630 00:00:00 AM','11068049178')

INSERT INTO financial_inst(fi_objid,fi_id,fi_internalid,fi_fullname,fi_address,fi_postcode,fi_city,fi_state,fi_biccode,fi_isocode,fi_currency,fi_personincharge,fi_telephone,fi_telext,fi_email,fi_closingdate,fi_identifier_id)
VALUES ('301','301','301','Rural Bank Limited','Level 6, 80 Grenfell Street','5000','Adelaide','SA',0,0,'AUD','Mark Reynolds','08 71099313','AU','mark.reynolds@ruralbank.com.au','20190630 00:00:00 AM','74083938416')

INSERT INTO financial_inst(fi_objid,fi_id,fi_internalid,fi_fullname,fi_address,fi_postcode,fi_city,fi_state,fi_biccode,fi_isocode,fi_currency,fi_personincharge,fi_telephone,fi_telext,fi_email,fi_closingdate,fi_identifier_id)
VALUES ('023','023','023','Leveraged Equities Limited','Level 8, 80 Grenfell Street','5000','Adelaide','SA',0,0,'AUD','Andrea Moore','03 54856664','AU','andrea.moore@bendigoadelaide.com.au','20190630 00:00:00 AM','26051629282')